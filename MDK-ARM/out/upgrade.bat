set path=%~f0
set prjName=%path:~-34,20%

set out_name
Setlocal ENABLEDELAYEDEXPANSION
::启用命令扩展，参加setlocal /?命令
set str1=%prjName%
set ch1=\
::注意，这里是区分大小写的！
set str=%str1%
::复制字符串，用来截短，而不影响源字符串
:next
if not "%str%"=="" (
set /a num+=1
set out_name=%out_name%!str:~0,1!
if "!str:~0,1!"=="%ch1%" goto last
::比较首字符是否为要求的字符，如果是则跳出循环
set "str=%str:~1%"
goto next
)
set /a num=0
::没有找到字符时，将num置零
:last
echo 字符'%ch1%'在字符串"%str1%"中的首次出现位置为%num%
set name=%out_name:~0,-1%
for /f "delims=" %%i in ('dir /b bin\*%name%*.bin') do set andBinName=%%i
echo %andBinName%
for /f "delims=" %%x in (JflashPath.txt) do set jflash=%%x
echo %jflash%
"%jflash%" -openprj"download.jflash" -open"bin\%andBinName%",0x8040000 -auto