/**
  ******************************************************************************
  * This file is part of the TouchGFX 4.10.0 distribution.
  *
  * <h2><center>&copy; Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

#include <touchgfx/hal/Types.hpp>
#include <touchgfx/widgets/EnKeyboard.hpp>
#include "debug.h"

using namespace touchgfx;
namespace EnKeyBoard
{
EnKeyboard::EnKeyboard()
    : Container(), keyListener(0), bufferSize(0), bufferPosition(0), highlightImage(), cancelIsEmitted(false),firstDraw(true)
{
    setTouchable(true);
		
    keyMappingList = static_cast<KeyMappingList*>(0);
    buffer = static_cast<Unicode::UnicodeChar*>(0);
    layout = static_cast<Layout*>(0);
	
		boxWithBorderBtn.setColor(touchgfx::Color::getColorFrom24BitRGB(235, 236, 239));
		boxWithBorderBtn.setBorderColor(touchgfx::Color::getColorFrom24BitRGB(85, 163, 235));
		boxWithBorderBtn.setBorderSize(2);
		boxWithBorderBtn.setVisible(false);
		Container::add(boxWithBorderBtn);	

}


void EnKeyboard::setBuffer(Unicode::UnicodeChar* newBuffer, uint16_t newBufferSize)
{
    buffer = newBuffer;
    bufferSize = newBufferSize;

    enteredText.setWildcard(buffer);

    // Place cursor at end of string if we already have something
    // in the edit buffer.
    bufferPosition = Unicode::strlen(buffer);
}



void EnKeyboard::setLayout(const Layout* newLayout)
{
	Log("setLayout");
    layout = newLayout;
    if (newLayout != 0)
    {
//        image.setBitmap(Bitmap(newLayout->bitmap));
        enteredText.setTypedText(newLayout->textAreaFont);
        enteredText.setColor(newLayout->textAreaFontColor);
        enteredText.setPosition(newLayout->textAreaPosition.x, newLayout->textAreaPosition.y,
                                newLayout->textAreaPosition.width, newLayout->textAreaPosition.height);
    }
				
//    highlightImage.setVisible(false);
//    Container::add(highlightImage);
    enteredText.setColor(Color::getColorFrom24BitRGB(0, 0, 0));
    Container::add(enteredText);
 	  invalidate();
}

void EnKeyboard::setTextIndentation()
{
    if (layout != 0)
    {
			
				  const Font* font = layout->textAreaFont.getFont();
					uint16_t fontHeight = font->getMinimumTextHeight();
					
					uint16_t offset = (layout->textAreaPosition.height - fontHeight) / 2;
			
        uint8_t indentation = layout->textAreaFont.getFont()->getMaxPixelsLeft();
        enteredText.setPosition(layout->textAreaPosition.x - indentation, layout->textAreaPosition.y+offset,
                                layout->textAreaPosition.width + indentation * 2, layout->textAreaPosition.height-offset);
        enteredText.setIndentation(indentation);
    }
}

EnKeyboard::Key EnKeyboard::getKeyForCoordinates(int16_t x, int16_t y) const
{
    Key key;
    key.keyId = 0; // No key
    if (layout != 0)
    {
        for (uint8_t i = 0; i < layout->numberOfKeys; i++)
        {
            if (layout->keyArray[i].keyArea.intersect(x, y))
            {
                key = layout->keyArray[i];
                break;
            }
        }
    }
    return key;
}

EnKeyboard::CallbackArea EnKeyboard::getCallbackAreaForCoordinates(int16_t x, int16_t y) const
{
    CallbackArea area;
    area.callback = reinterpret_cast<GenericCallback<>*>(0);
    if (layout != 0)
    {
        for (uint8_t i = 0; i < layout->numberOfCallbackAreas; i++)
        {
            if (layout->callbackAreaArray[i].keyArea.intersect(x, y))
            {
                area = layout->callbackAreaArray[i];
                break;
            }
        }
    }
    return area;
}

void EnKeyboard::DrawBack() const
{
	Log("DrawBack");
	touchgfx::Rect area = getRect();
	uint8_t borderSize= 2;
	uint8_t alpha = 0xff;
	colortype color = touchgfx::Color::getColorFrom24BitRGB(235, 236, 239);
	colortype borderColor = touchgfx::Color::getColorFrom24BitRGB(85, 163, 235);
	
	
	touchgfx::Rect dirty = area & Rect(borderSize, borderSize, getWidth() - (2 * borderSize), getHeight() - (2 * borderSize));
	translateRectToAbsolute(dirty);
	HAL::lcd().fillRect(dirty, color, alpha);

	if (borderSize > 0)
	{
			Rect borders[4] =
			{
					Rect(0, 0, getWidth(), borderSize),                                                     // Upper
					Rect(0, getHeight() - borderSize, getWidth(), borderSize),                              // lower
					Rect(0, borderSize, borderSize, getHeight() - (2 * borderSize)),                        // left
					Rect(getWidth() - borderSize, borderSize, borderSize, getHeight() - (2 * borderSize))   // right
			};

			for (int i = 0; i < 4; i++)
			{
					Rect borderDirty = borders[i] & area;
					if (!borderDirty.isEmpty())
					{
							translateRectToAbsolute(borderDirty);
							HAL::lcd().fillRect(borderDirty, borderColor, alpha);
					}
			}
	}
	
		Font* font = FontManager::getFont(layout->keyFont);
		uint8_t fontHeight = font->getMinimumTextHeight();
		for (uint8_t i = 0; i < layout->numberOfKeys; i++)
		{
				const EnKeyboard::Key& key = layout->keyArray[i];
				Rect keyArea=key.keyArea;
				uint16_t offset = (keyArea.height - fontHeight) / 2;
				keyArea.y += offset;
				keyArea.height -= offset;
				translateRectToAbsolute(keyArea);
				HAL::lcd().fillRect(keyArea,touchgfx::Color::getColorFrom24BitRGB(158,158,158),alpha);
			//Log("numberOfKeys i:%d",i);
		}
	
		
		
		for (uint8_t i = 0; i < layout->numberOfCallbackAreas; i++)
		{
			EnKeyboard::CallbackArea & callarea = layout->callbackAreaArray[i];
			 Rect keyArea=callarea.keyArea;
			uint16_t offset = (keyArea.height - fontHeight) / 2;
			keyArea.y += offset;
			keyArea.height -= offset;
			translateRectToAbsolute(keyArea);
			HAL::lcd().fillRect(keyArea,touchgfx::Color::getColorFrom24BitRGB(158,158,158),alpha);	
		}

		Rect txtRect = layout->textAreaPosition;
		translateRectToAbsolute(txtRect);
		HAL::lcd().fillRect(txtRect,touchgfx::Color::getColorFrom24BitRGB(0xff,0xff,0xff),alpha);	
}


void EnKeyboard::draw(const Rect& invalidatedArea) const
{
    assert(layout && "No layout configured for Keyboard");
		Log("firstDraw:%d",firstDraw);
		if(firstDraw)
		{
			DrawBack();
		}	
    if (layout != 0)
    {
        Font* font = FontManager::getFont(layout->keyFont);
        assert(font && "Keyboard::draw: Unable to find font, is font db initialized?");
        if (font != 0)
        {
            // Setup visuals for h-center of "string"
            LCD::StringVisuals visuals;
            visuals.font = font;
            visuals.alignment = CENTER;
            visuals.color = layout->keyFontColor;
            // String with room for a single character
            Unicode::UnicodeChar character[2] = { 0, 0 }; // The last is important as string terminator.

            uint16_t fontHeight = font->getMinimumTextHeight();
            for (uint8_t i = 0; i < layout->numberOfKeys; i++)
            {
                const Key& key = layout->keyArray[i];
                if (key.keyArea.intersect(invalidatedArea))
                {
                    uint8_t keyId = key.keyId;
                    Unicode::UnicodeChar c = getCharForKey(keyId);
                    if (c != 0)
                    {
                        // Get a copy of the keyArea and v-center the area for the character
                        Rect keyArea = key.keyArea;
                        uint16_t offset = (keyArea.height - fontHeight) / 2;
                        keyArea.y += offset;
                        keyArea.height -= offset;
                        // Calculate the invalidated area relative to the key
                        Rect invalidatedAreaRelative = key.keyArea & invalidatedArea;
                        invalidatedAreaRelative.x -= keyArea.x;
                        invalidatedAreaRelative.y -= keyArea.y;
                        // Set up string with one character
                        character[0] = c;
                        translateRectToAbsolute(keyArea);	
                        HAL::lcd().drawString(keyArea, invalidatedAreaRelative, visuals, character);
												
                    }
                }
            }
						
        }
				
    }
}


void EnKeyboard::handleClickEvent(const ClickEvent& evt)
{
		
    ClickEvent::ClickEventType type = evt.getType();
    int16_t x = evt.getX();
    int16_t y = evt.getY();
    EnKeyboard::CallbackArea callbackArea = getCallbackAreaForCoordinates(x, y);
    if (callbackArea.callback != 0)
    {
        if (type == ClickEvent::PRESSED)
        {	
					firstDraw = true;
					invalidate();
					//DrawBack();
					firstDraw = false;
					//
					Font* font = FontManager::getFont(layout->keyFont);
					uint16_t fontHeight = font->getMinimumTextHeight();
					Rect keyArea = callbackArea.keyArea;
					uint16_t offset = (keyArea.height - fontHeight) / 2;
					keyArea.y += offset;
					keyArea.height -= offset;
					boxWithBorderBtn.setPosition(keyArea.x, keyArea.y,keyArea.width,keyArea.height);
					boxWithBorderBtn.setVisible(true);
					boxWithBorderBtn.invalidate();
					
        }

        if ((type == ClickEvent::RELEASED) && callbackArea.callback->isValid())
        {
            if (cancelIsEmitted)
            {
                cancelIsEmitted = false;
            }
            else
            {
                callbackArea.callback->execute();
                if (keyListener)
                {
                    keyListener->execute(0);
                }
            }
        }
    }
    else
    {
        EnKeyboard::Key key = getKeyForCoordinates(x, y);

        if (key.keyId != 0)
        {
            if (type == ClickEvent::PRESSED)
            {
//							DrawBack();
							firstDraw = true;
					invalidate();
					//DrawBack();
//					firstDraw = false;
//							Font* font = FontManager::getFont(layout->keyFont);
//							uint16_t fontHeight = font->getMinimumTextHeight();
//							Rect keyArea = key.keyArea;
//							uint16_t offset = (keyArea.height - fontHeight) / 2;
//							keyArea.y += offset;
//							keyArea.height -= offset;
//							boxWithBorderBtn.setPosition(keyArea.x, keyArea.y,keyArea.width,keyArea.height);
//							boxWithBorderBtn.setVisible(true);
							//boxWithBorderBtn.invalidate();
							//invalidate();
//							touchgfx::Rect area = getAbsoluteRect();
//							invalidateRect(area);
//								Font* font = FontManager::getFont(layout->keyFont);
//								uint16_t fontHeight = font->getMinimumTextHeight();							
//								Rect keyArea=key.keyArea;
//								uint16_t offset = (keyArea.height - fontHeight) / 2;
//								keyArea.y += offset;
//								keyArea.height -= offset;
//								translateRectToAbsolute(keyArea);
//								HAL::lcd().fillRect(keyArea,touchgfx::Color::getColorFrom24BitRGB(100,100,100));	
//								invalidateRect(keyArea);
//								invalidate();

            }
						
            //if (type == ClickEvent::RELEASED)
            {
                if (cancelIsEmitted)
                {
                    cancelIsEmitted = false;
                }
                else
                {
                    if (buffer)
                    {
                        Unicode::UnicodeChar c = getCharForKey(key.keyId);
                        if (c != 0)
                        {
                            uint16_t prevBufferPosition = bufferPosition;
                            if (bufferPosition < (bufferSize - 1))
                            {
                                buffer[bufferPosition++] = c;
                                buffer[bufferPosition] = 0;
                            }
                            if (prevBufferPosition != bufferPosition)
                            {
																Log("updata text");
                                enteredText.invalidate();
                                if (keyListener)
                                {
                                    keyListener->execute(c);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if (type == ClickEvent::RELEASED || type == ClickEvent::CANCEL)
    {
				DrawBack();
				boxWithBorderBtn.setVisible(false);
				//boxWithBorderBtn.invalidate();
				invalidate();
				Log("Release");
//				Font* font = FontManager::getFont(layout->keyFont);
//				uint16_t fontHeight = font->getMinimumTextHeight();							
//				Rect keyArea=callbackArea.keyArea;
//				uint16_t offset = (keyArea.height - fontHeight) / 2;
//				keyArea.y += offset;
//				keyArea.height -= offset;
//				translateRectToAbsolute(keyArea);
//				//HAL::lcd().fillRect(keyArea,touchgfx::Color::getColorFrom24BitRGB(160,160,160));
//				invalidate();
//				//invalidateRect(keyArea);
        if (type == ClickEvent::CANCEL)
        {
            cancelIsEmitted = true;
        }
    }
}

void EnKeyboard::handleDragEvent(const DragEvent& evt)
{
    if (highlightImage.isVisible() && (!highlightImage.getRect().intersect(static_cast<int16_t>(evt.getNewX()), static_cast<int16_t>(evt.getNewY()))) && (cancelIsEmitted == false))
    {
        // Send a CANCEL click event, if user has dragged out of currently pressed/highlighted key.
        touchgfx::ClickEvent cancelEvent(touchgfx::ClickEvent::CANCEL, static_cast<int16_t>(evt.getOldX()), static_cast<int16_t>(evt.getOldY()));
        handleClickEvent(cancelEvent);
    }
}

Unicode::UnicodeChar EnKeyboard::getCharForKey(uint8_t keyId) const
{
    Unicode::UnicodeChar ch = 0;
    if (keyMappingList != 0)
    {
        for (uint8_t i = 0; i < keyMappingList->numberOfKeys; i++)
        {
            if (keyMappingList->keyMappingArray[i].keyId == keyId)
            {
                ch = keyMappingList->keyMappingArray[i].keyValue;
                break;
            }
        }
    }
    return ch;
}

void EnKeyboard::setupDrawChain(const Rect& invalidatedArea, Drawable** nextPreviousElement)
{
    // Keyboard is a Container, and they do not normally appear in the draw chain (they just draw children).
    // But this particular container actually has a draw() function implementation, so we must change default
    // behavior.
    // First, add children
    Container::setupDrawChain(invalidatedArea, nextPreviousElement);
    // Then add yourself
    Drawable::setupDrawChain(invalidatedArea, nextPreviousElement);
}

void EnKeyboard::setBufferPosition(uint16_t newPos)
{
    bufferPosition = newPos;
    enteredText.invalidate();
}

void EnKeyboard::setKeymappingList(const KeyMappingList* newKeyMappingList)
{
    keyMappingList = newKeyMappingList;
    invalidate();
}
} // namespace touchgfx
