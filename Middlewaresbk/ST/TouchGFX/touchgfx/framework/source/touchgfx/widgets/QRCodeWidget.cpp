#include <touchgfx/hal/Types.hpp>
#include <touchgfx/widgets/QRCodeWidget.hpp>
#include "stdio.h"
#include <touchgfx/Color.hpp>
#include "QR_Encode.h"
#include "malloc.h"
#include "Debug.h"
#include "SysConfig.h"
#include "IntTo64.h"
#include "GuiInclude.h"

QRCodeWidget::QRCodeWidget() : 
    code(0),
    scale(1)
{
}

void QRCodeWidget::setQRCode(QRCode *qrCode)
{
    code = qrCode;
    updateSize();
}

void QRCodeWidget::SetIccid(char *data)
{
	strncpy(iccid,data,ICCID_LEN);
}

void QRCodeWidget::draw(const touchgfx::Rect& invalidatedArea) const
{
    if(!code)
    {
        return;
    }
    touchgfx::Rect absolute = getAbsoluteRect();
		Log("absolute x:%d,y:%d,w:%d,h:%d",absolute.x,absolute.y,absolute.width,absolute.height);
	#define QR_BLOCK_SIZE 4	
	u8 *cotest=(u8 *)mymalloc(SRAMIN,120);
	char ip[41];
	if(!GetIp(ip,41))
	{
		myfree(SRAMIN,cotest);
		return ;
	}
	
	char *chip = (char *)mymalloc(SRAMIN,40);
	if(!chip)
	{
		myfree(SRAMIN,cotest);
		return ;
	}
	GetChipId(chip,40);
	
	char  userCode[40]={0};
	u8 res = GetUserCode(userCode,40);
	
	//sprintf((char *)cotest,"http://%s/downLoad_apk?EqpId=%s&chip=%s",ip,EqpIdStr,chip);
	//sprintf((char *)cotest,"prdc=%s&EqpId=%s&chip=%s",userCode,EqpIdStr,chip);
	sprintf((char *)cotest,"prdc=%s&device=%s&soft=%s&userCode=%s&iccid=%s",PRODUC_CODE,chip,SOFT_CODE,userCode,iccid);
	myfree(SRAMIN,chip);
	EncodeData((char *)cotest);
	if(m_nSymbleSize*2>240)	
	{
		myfree(SRAMIN,cotest);
		Log("m_nSymbleSize*2>240");
		return;
	}
	Log("m_nSymbleSize:%d n* QR_BLOCK_SIZE:%d",m_nSymbleSize,QR_BLOCK_SIZE);
	touchgfx::Rect rect;
	rect.x = absolute.x;
	rect.y = absolute.y;
	rect.width = absolute.width;
	rect.height = absolute.height;
	
	for(u16 i=0;i<m_nSymbleSize;i++)
	{
		for(u16 j=0;j<m_nSymbleSize;j++)
		{
			rect.x = absolute.x+i*QR_BLOCK_SIZE;
			rect.y = absolute.y+j*QR_BLOCK_SIZE;
			rect.width = QR_BLOCK_SIZE;
			rect.height = QR_BLOCK_SIZE;
			if(m_byModuleData[i][j]==1)
			{
				touchgfx::HAL::lcd().fillRect(rect,touchgfx::Color::getColorFrom24BitRGB(0,0,0));
			}
			else
			{
				touchgfx::HAL::lcd().fillRect(rect,touchgfx::Color::getColorFrom24BitRGB(0xff,0xff,0xff));
			}
		}	
	}
	myfree(SRAMIN,cotest);
}




touchgfx::Rect QRCodeWidget::getSolidRect() const
{
    return touchgfx::Rect(0,0,getWidth(), getHeight());
}

void QRCodeWidget::setScale(uint8_t s)
{
    scale = s;
    updateSize();
}

void QRCodeWidget::updateSize()
{
    if(code)
    {
        setWidth(code->getWidth() * scale);
        setHeight(code->getHeight() * scale);
    }    
}
