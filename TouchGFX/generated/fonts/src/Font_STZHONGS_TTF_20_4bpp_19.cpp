#include <touchgfx/hal/Types.hpp>

FONT_GLYPH_LOCATION_FLASH_PRAGMA
KEEP extern const uint8_t unicodes_STZHONGS_TTF_20_4bpp_19[] FONT_GLYPH_LOCATION_FLASH_ATTRIBUTE =
{
    // Unicode: [0x9875, ]
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xB0, 0x09, 0x99, 0x99, 0x99, 0xA9, 0x9A, 0x99, 0x99,
    0xEB, 0x9E, 0x00, 0x00, 0x00, 0xE0, 0x2D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x01, 0xF1, 0x05,
    0x00, 0x71, 0x01, 0x00, 0x00, 0xC0, 0xAE, 0xFA, 0x99, 0x99, 0xFB, 0x2D, 0x00, 0x00, 0xC0, 0x0F,
    0x00, 0x00, 0x00, 0xF2, 0x0C, 0x00, 0x00, 0xC0, 0x0F, 0x10, 0x14, 0x00, 0xF2, 0x0A, 0x00, 0x00,
    0xC0, 0x0F, 0x30, 0xEF, 0x01, 0xF2, 0x0A, 0x00, 0x00, 0xC0, 0x0F, 0x30, 0x9F, 0x00, 0xF2, 0x0A,
    0x00, 0x00, 0xC0, 0x0F, 0x30, 0x8F, 0x00, 0xF2, 0x0A, 0x00, 0x00, 0xC0, 0x0F, 0x40, 0x6F, 0x00,
    0xF2, 0x0A, 0x00, 0x00, 0xC0, 0x0F, 0x70, 0x5F, 0x00, 0xF2, 0x0A, 0x00, 0x00, 0xC0, 0x0F, 0xB0,
    0x2F, 0x00, 0xF2, 0x0B, 0x00, 0x00, 0xC0, 0x09, 0xF4, 0x5B, 0x36, 0x81, 0x02, 0x00, 0x00, 0x00,
    0x30, 0xCE, 0x01, 0xC3, 0x9E, 0x03, 0x00, 0x00, 0x00, 0xF7, 0x19, 0x00, 0x00, 0xF8, 0x8F, 0x00,
    0x00, 0xC4, 0x3A, 0x00, 0x00, 0x00, 0x60, 0xFF, 0x02, 0x85, 0x26, 0x00, 0x00, 0x00, 0x00, 0x00,
    0xE6, 0x01,
    // Unicode: [0x9AD8, ]
    0x00, 0x00, 0x00, 0x60, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 0xEF, 0x04, 0x00,
    0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFC, 0x05, 0x00, 0xE2, 0x1C, 0x9A, 0x99, 0x99, 0x99, 0xDB,
    0x99, 0x99, 0xDB, 0xAD, 0x00, 0x00, 0x47, 0x00, 0x00, 0x00, 0x5A, 0x00, 0x00, 0x00, 0x00, 0xFB,
    0x99, 0x99, 0x99, 0xEF, 0x02, 0x00, 0x00, 0x00, 0xEB, 0x00, 0x00, 0x00, 0xAE, 0x00, 0x00, 0x00,
    0x00, 0xEB, 0x00, 0x00, 0x00, 0xAE, 0x00, 0x00, 0x00, 0x00, 0xEB, 0x99, 0x99, 0x99, 0x8F, 0x00,
    0x00, 0x10, 0x38, 0x24, 0x00, 0x00, 0x00, 0x01, 0x65, 0x00, 0x00, 0xDF, 0x89, 0x88, 0x88, 0x88,
    0x88, 0xFD, 0x09, 0x00, 0xAF, 0x20, 0x03, 0x00, 0x70, 0x01, 0xFA, 0x01, 0x00, 0xAF, 0x40, 0xBF,
    0x99, 0xFA, 0x1D, 0xFA, 0x01, 0x00, 0xAF, 0x30, 0x4F, 0x00, 0xF1, 0x08, 0xFA, 0x01, 0x00, 0xAF,
    0x30, 0x4F, 0x00, 0xF1, 0x08, 0xFA, 0x01, 0x00, 0xAF, 0x40, 0xBF, 0x99, 0xF9, 0x08, 0xFA, 0x01,
    0x00, 0xAF, 0x30, 0x18, 0x00, 0x70, 0x34, 0xFC, 0x01, 0x10, 0xAF, 0x00, 0x00, 0x00, 0x00, 0xD4,
    0xFF, 0x00, 0x10, 0x5C, 0x00, 0x00, 0x00, 0x00, 0x40, 0x5D, 0x00
};
