#include <touchgfx/hal/Types.hpp>

FONT_GLYPH_LOCATION_FLASH_PRAGMA
KEEP extern const uint8_t unicodes_STZHONGS_TTF_20_4bpp_0[] FONT_GLYPH_LOCATION_FLASH_ATTRIBUTE =
{
    // Unicode: [0x003F, ]
    0x40, 0xEB, 0xDF, 0x39, 0x00, 0xF6, 0x4E, 0x21, 0xFA, 0x05, 0xD7, 0x03, 0x00, 0xB0, 0x2F, 0x00,
    0x00, 0x00, 0x50, 0x7F, 0x00, 0x00, 0x00, 0x40, 0x8F, 0x00, 0x00, 0x00, 0xB0, 0x5F, 0x00, 0x10,
    0x62, 0xFC, 0x0B, 0x00, 0xF1, 0xFF, 0x8E, 0x00, 0x00, 0xE1, 0x23, 0x00, 0x00, 0x00, 0xE1, 0x00,
    0x00, 0x00, 0x00, 0x61, 0x00, 0x00, 0x00, 0x00, 0x72, 0x01, 0x00, 0x00, 0x00, 0xFB, 0x09, 0x00,
    0x00, 0x00, 0xE7, 0x05, 0x00, 0x00,
    // Unicode: [0x0046, ]
    0xFC, 0xFF, 0xFF, 0xFF, 0xFF, 0x0A, 0x60, 0x9F, 0x22, 0x32, 0xD6, 0x0E, 0x40, 0x8F, 0x00, 0x00,
    0x10, 0x5D, 0x40, 0x8F, 0x00, 0x00, 0x00, 0x85, 0x40, 0x8F, 0x00, 0xA0, 0x04, 0x00, 0x40, 0x8F,
    0x10, 0xE4, 0x04, 0x00, 0x40, 0xFF, 0xFF, 0xFF, 0x04, 0x00, 0x40, 0x8F, 0x10, 0xF5, 0x04, 0x00,
    0x40, 0x8F, 0x00, 0xC0, 0x04, 0x00, 0x40, 0x8F, 0x00, 0xA0, 0x04, 0x00, 0x40, 0x8F, 0x00, 0x20,
    0x01, 0x00, 0x40, 0x8F, 0x00, 0x00, 0x00, 0x00, 0x60, 0xAF, 0x00, 0x00, 0x00, 0x00, 0xFC, 0xFF,
    0x3D, 0x00, 0x00, 0x00,
    // Unicode: [0x0049, ]
    0xD3, 0xFF, 0xDF, 0x00, 0xFA, 0x07, 0x00, 0xF8, 0x05, 0x00, 0xF8, 0x04, 0x00, 0xF8, 0x04, 0x00,
    0xF8, 0x04, 0x00, 0xF8, 0x04, 0x00, 0xF8, 0x04, 0x00, 0xF8, 0x04, 0x00, 0xF8, 0x04, 0x00, 0xF8,
    0x04, 0x00, 0xF8, 0x05, 0x00, 0xFA, 0x07, 0xD3, 0xFF, 0xDF,
    // Unicode: [0x0057, ]
    0xFA, 0xFF, 0x2D, 0xE1, 0xFF, 0xAF, 0x00, 0xD3, 0xFF, 0x0D, 0x70, 0xBF, 0x00, 0x00, 0xFC, 0x06,
    0x00, 0x00, 0xAD, 0x00, 0x10, 0xEF, 0x00, 0x00, 0xF6, 0x08, 0x00, 0x10, 0x4F, 0x00, 0x00, 0xFB,
    0x03, 0x00, 0xF1, 0x0D, 0x00, 0x50, 0x0E, 0x00, 0x00, 0xF6, 0x07, 0x00, 0xF3, 0x2F, 0x00, 0x90,
    0x09, 0x00, 0x00, 0xF2, 0x0C, 0x00, 0xE8, 0x7F, 0x00, 0xE0, 0x05, 0x00, 0x00, 0xC0, 0x1F, 0x00,
    0x6D, 0xBF, 0x00, 0xE3, 0x01, 0x00, 0x00, 0x70, 0x6F, 0x20, 0x0E, 0xFC, 0x01, 0xA8, 0x00, 0x00,
    0x00, 0x30, 0xAF, 0x70, 0x0A, 0xF8, 0x05, 0x5C, 0x00, 0x00, 0x00, 0x00, 0xED, 0xC0, 0x05, 0xF3,
    0x1A, 0x1F, 0x00, 0x00, 0x00, 0x00, 0xF9, 0xF6, 0x01, 0xD0, 0x6E, 0x0B, 0x00, 0x00, 0x00, 0x00,
    0xF4, 0xBE, 0x00, 0x90, 0xDF, 0x06, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x6F, 0x00, 0x40, 0xFF, 0x02,
    0x00, 0x00, 0x00, 0x00, 0xA0, 0x1F, 0x00, 0x00, 0xCE, 0x00, 0x00, 0x00
};
