#include <touchgfx/hal/Types.hpp>

FONT_GLYPH_LOCATION_FLASH_PRAGMA
KEEP extern const uint8_t unicodes_STZHONGS_TTF_16_4bpp_10[] FONT_GLYPH_LOCATION_FLASH_ATTRIBUTE =
{
    // Unicode: [0x505C, ]
    0x00, 0x10, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0xD0, 0x2B, 0x00, 0xBC, 0x02, 0x00, 0x00,
    0x00, 0xF2, 0x08, 0x00, 0xF4, 0x04, 0xD1, 0x04, 0x00, 0xE7, 0x85, 0x88, 0x88, 0x88, 0x88, 0x07,
    0x00, 0x7C, 0x00, 0x9D, 0x77, 0xC7, 0x08, 0x00, 0x40, 0x7F, 0x00, 0x5D, 0x00, 0xC0, 0x08, 0x00,
    0xB0, 0x6F, 0x00, 0xAD, 0x77, 0xD7, 0x07, 0x00, 0x65, 0x6D, 0x40, 0x28, 0x00, 0x80, 0x44, 0x01,
    0x06, 0x6D, 0xB4, 0x77, 0x77, 0x77, 0xE7, 0x2D, 0x00, 0x7D, 0x8D, 0x00, 0x00, 0x00, 0x89, 0x02,
    0x00, 0x7D, 0x47, 0x88, 0xE8, 0x8A, 0x89, 0x00, 0x00, 0x6D, 0x00, 0x00, 0xF1, 0x05, 0x00, 0x00,
    0x00, 0x6D, 0x00, 0x00, 0xF1, 0x05, 0x00, 0x00, 0x00, 0x6E, 0x00, 0x00, 0xF1, 0x05, 0x00, 0x00,
    0x00, 0x6E, 0x00, 0xA5, 0xFC, 0x05, 0x00, 0x00, 0x00, 0x4D, 0x00, 0x00, 0x8C, 0x00, 0x00, 0x00,
    // Unicode: [0x5173, ]
    0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x73, 0x00, 0x00, 0xE4, 0x06, 0x00, 0x00,
    0x00, 0x80, 0x2D, 0x00, 0xDA, 0x01, 0x00, 0x00, 0x00, 0x10, 0x8E, 0x20, 0x3E, 0x10, 0x00, 0x00,
    0x00, 0x00, 0x47, 0x90, 0x04, 0xE8, 0x04, 0x00, 0x81, 0x88, 0x98, 0xAF, 0x88, 0x88, 0x07, 0x00,
    0x00, 0x00, 0x30, 0x5F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x4F, 0x00, 0x20, 0x2B, 0x00,
    0x76, 0x77, 0xA7, 0xAF, 0x77, 0xA7, 0x9C, 0x00, 0x00, 0x00, 0x80, 0x7E, 0x02, 0x00, 0x00, 0x00,
    0x00, 0x00, 0xD0, 0x2A, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF6, 0x03, 0x7A, 0x00, 0x00, 0x00,
    0x00, 0x30, 0x7E, 0x00, 0xE2, 0x19, 0x00, 0x00, 0x00, 0xD6, 0x04, 0x00, 0x40, 0xFE, 0x6A, 0x01,
    0x94, 0x06, 0x00, 0x00, 0x00, 0xC3, 0x28, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    // Unicode: [0x529F, ]
    0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x70, 0x5E, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x49, 0x70, 0x1F, 0x00, 0x00, 0x81, 0x87, 0x9C, 0xAB, 0x72, 0x0F, 0x20, 0x00,
    0x00, 0x30, 0x3F, 0x73, 0xB7, 0x7E, 0xF8, 0x1C, 0x00, 0x30, 0x3F, 0x00, 0x80, 0x0C, 0xE0, 0x08,
    0x00, 0x30, 0x3F, 0x00, 0x90, 0x0B, 0xF0, 0x07, 0x00, 0x30, 0x3F, 0x00, 0xB0, 0x0A, 0xF0, 0x07,
    0x00, 0x30, 0x3F, 0x00, 0xD0, 0x08, 0xF0, 0x06, 0x00, 0x30, 0x3F, 0x63, 0xF1, 0x05, 0xF1, 0x05,
    0x00, 0x61, 0xEF, 0x18, 0xF5, 0x01, 0xF2, 0x05, 0xB1, 0xFF, 0x2B, 0x00, 0x9C, 0x00, 0xF3, 0x04,
    0x90, 0x5E, 0x00, 0x60, 0x4D, 0x11, 0xF8, 0x02, 0x10, 0x02, 0x00, 0xC3, 0x42, 0xFB, 0xCF, 0x00,
    0x00, 0x00, 0x30, 0x19, 0x00, 0xB0, 0x1A, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00,
    // Unicode: [0x5728, ]
    0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF3, 0x09, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0xF8, 0x03, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0xAD, 0x00, 0x00, 0xF4, 0x07,
    0x70, 0x88, 0xA8, 0x8F, 0x88, 0x88, 0x88, 0x17, 0x00, 0x00, 0xB0, 0x0A, 0xA0, 0x06, 0x00, 0x00,
    0x00, 0x30, 0xE9, 0x02, 0xE0, 0x07, 0x00, 0x00, 0x00, 0x30, 0x6F, 0x00, 0xE0, 0x07, 0x00, 0x00,
    0x00, 0xA0, 0x2F, 0x00, 0xE0, 0x07, 0xBA, 0x01, 0x00, 0xD7, 0x3F, 0x88, 0xE8, 0x8B, 0x88, 0x04,
    0x50, 0x39, 0x2F, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x54, 0x30, 0x2F, 0x00, 0xE0, 0x07, 0x00, 0x00,
    0x00, 0x30, 0x2F, 0x00, 0xE0, 0x07, 0x10, 0x00, 0x00, 0x30, 0x3F, 0x00, 0xE0, 0x07, 0xD1, 0x09,
    0x00, 0x40, 0x7F, 0x88, 0x88, 0x88, 0x88, 0x38, 0x00, 0x40, 0x1B, 0x00, 0x00, 0x00, 0x00, 0x00
};
