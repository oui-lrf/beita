#include <touchgfx/hal/Types.hpp>

FONT_GLYPH_LOCATION_FLASH_PRAGMA
KEEP extern const uint8_t unicodes_STZHONGS_TTF_24_4bpp_15[] FONT_GLYPH_LOCATION_FLASH_ATTRIBUTE =
{
    // Unicode: [0x7801, ]
    0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0xA7, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00,
    0xF7, 0x35, 0x66, 0x66, 0x66, 0xFE, 0x1D, 0x00, 0x80, 0xCC, 0xCC, 0xCC, 0xFF, 0x6F, 0x67, 0x66,
    0x66, 0xFF, 0x07, 0x00, 0x10, 0x00, 0xD0, 0x7F, 0x00, 0x00, 0x64, 0x01, 0x10, 0xFF, 0x00, 0x00,
    0x00, 0x00, 0xF1, 0x1F, 0x00, 0x00, 0xF7, 0x0B, 0x30, 0xDF, 0x00, 0x00, 0x00, 0x00, 0xF4, 0x0B,
    0x00, 0x00, 0xF8, 0x05, 0x50, 0xBF, 0x00, 0x00, 0x00, 0x00, 0xF9, 0x05, 0x00, 0x00, 0xFA, 0x04,
    0x60, 0x9F, 0x00, 0x00, 0x00, 0x00, 0xDD, 0x00, 0x55, 0x00, 0xFB, 0x02, 0x80, 0x7F, 0x00, 0x00,
    0x00, 0x30, 0xEF, 0xAA, 0xFE, 0x07, 0xFD, 0x00, 0xA0, 0x6F, 0x00, 0x00, 0x00, 0x90, 0xAF, 0x00,
    0xFE, 0x03, 0xEE, 0x00, 0xB0, 0x4F, 0x65, 0x00, 0x00, 0xF2, 0xAF, 0x00, 0xFE, 0x41, 0xEF, 0x99,
    0xE9, 0xAF, 0xFE, 0x1B, 0x00, 0xC8, 0xAF, 0x00, 0xFE, 0x51, 0xAE, 0x22, 0x22, 0x32, 0xFF, 0x08,
    0x20, 0x4D, 0xAF, 0x00, 0xFE, 0x01, 0x01, 0x00, 0x00, 0x20, 0xFF, 0x01, 0xA0, 0x43, 0xAF, 0x00,
    0xFE, 0x01, 0x00, 0x00, 0x21, 0x40, 0xFF, 0x00, 0x51, 0x40, 0xAF, 0x00, 0xFE, 0x01, 0x00, 0x00,
    0xEC, 0x54, 0xDF, 0x00, 0x00, 0x40, 0xAF, 0x00, 0xFE, 0xB7, 0xAA, 0xAA, 0xBB, 0x7A, 0xCF, 0x00,
    0x00, 0x40, 0xEF, 0xBB, 0xFF, 0x01, 0x00, 0x00, 0x00, 0x90, 0xAF, 0x00, 0x00, 0x40, 0xAF, 0x00,
    0xFE, 0x01, 0x00, 0x00, 0x00, 0xB0, 0x8F, 0x00, 0x00, 0x40, 0xAF, 0x00, 0xAE, 0x01, 0x00, 0x02,
    0x00, 0xF1, 0x5F, 0x00, 0x00, 0x40, 0xAF, 0x00, 0x00, 0x00, 0x10, 0xC9, 0x9B, 0xFD, 0x1E, 0x00,
    0x00, 0x20, 0x15, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF8, 0xFF, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0xD0, 0x3A, 0x00, 0x00,
    // Unicode: [0x786E, ]
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x20, 0xEF, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x00, 0x80, 0xCF,
    0x02, 0x12, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE2, 0x1B, 0xE1, 0x7F, 0x44, 0xED, 0x05, 0x00,
    0x30, 0xAB, 0xCA, 0xBC, 0xDB, 0x8D, 0xF8, 0x89, 0x98, 0xDF, 0x07, 0x00, 0x00, 0x00, 0xF1, 0x4F,
    0x00, 0x30, 0x6F, 0x00, 0xB0, 0x1B, 0x00, 0x00, 0x00, 0x00, 0xF4, 0x0E, 0x00, 0xC0, 0x07, 0x00,
    0xC5, 0x01, 0x37, 0x00, 0x00, 0x00, 0xF7, 0x08, 0x00, 0xE7, 0x9D, 0x99, 0xAE, 0xA9, 0xFF, 0x07,
    0x00, 0x00, 0xFB, 0x02, 0x42, 0xA6, 0x5F, 0x52, 0xCF, 0x22, 0xEF, 0x02, 0x00, 0x10, 0xDF, 0x87,
    0xAF, 0xA0, 0x4F, 0x40, 0xCF, 0x00, 0xEF, 0x00, 0x00, 0x70, 0xBF, 0x65, 0xEF, 0xA2, 0x3F, 0x40,
    0xCF, 0x00, 0xEF, 0x00, 0x00, 0xD0, 0x9F, 0x20, 0xAF, 0xA0, 0xBF, 0xBA, 0xEF, 0xAA, 0xEF, 0x00,
    0x00, 0xD6, 0x9F, 0x20, 0xAF, 0xA0, 0x3F, 0x40, 0xCF, 0x10, 0xEF, 0x00, 0x10, 0x3D, 0x9F, 0x20,
    0xAF, 0xA0, 0x2F, 0x40, 0xCF, 0x00, 0xEF, 0x00, 0xA0, 0x04, 0x9F, 0x20, 0xAF, 0xB0, 0x1F, 0x40,
    0xCF, 0x00, 0xEF, 0x00, 0x51, 0x00, 0x9F, 0x20, 0xAF, 0xC0, 0xAF, 0xBA, 0xEF, 0xAA, 0xEF, 0x00,
    0x00, 0x00, 0x9F, 0x20, 0xAF, 0xE0, 0x0D, 0x40, 0xCF, 0x00, 0xEF, 0x00, 0x00, 0x00, 0xDF, 0xBA,
    0xAF, 0xF1, 0x0A, 0x40, 0xCF, 0x00, 0xEF, 0x00, 0x00, 0x00, 0x9F, 0x20, 0xAF, 0xF5, 0x06, 0x40,
    0xCF, 0x00, 0xEF, 0x00, 0x00, 0x00, 0x9F, 0x10, 0x18, 0xDA, 0x00, 0x40, 0xCF, 0x00, 0xEF, 0x00,
    0x00, 0x10, 0x9F, 0x00, 0x20, 0x6F, 0x00, 0x40, 0xEF, 0x7A, 0xEF, 0x00, 0x00, 0x10, 0x19, 0x00,
    0xA0, 0x09, 0x00, 0x40, 0x6C, 0xE2, 0xAF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x87, 0x00, 0x00, 0x00,
    0x00, 0x90, 0x1A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    // Unicode: [0x79F0, ]
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x66, 0x00, 0xF0, 0x8E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0xD7, 0xFF, 0x03, 0xF3, 0x7F,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x50, 0xFA, 0xFF, 0xAC, 0x08, 0xF6, 0x0D, 0x00, 0x00, 0x01, 0x00,
    0x70, 0xBC, 0xE9, 0x2F, 0x00, 0x00, 0xFA, 0x06, 0x00, 0x40, 0x7F, 0x00, 0x00, 0x00, 0xD0, 0x1F,
    0x00, 0x00, 0xFE, 0xBC, 0xBB, 0xDB, 0xFF, 0x07, 0x00, 0x00, 0xD0, 0x1F, 0x41, 0x40, 0x7F, 0x00,
    0x00, 0x90, 0x9F, 0x03, 0x21, 0x22, 0xD2, 0x3F, 0xEB, 0xA3, 0x1D, 0xE0, 0x4B, 0xD0, 0x08, 0x00,
    0xA2, 0x99, 0xFB, 0xAF, 0x99, 0xF7, 0x06, 0xE0, 0x2F, 0x94, 0x00, 0x00, 0x00, 0x00, 0xF8, 0x1F,
    0x00, 0xB8, 0x00, 0xE0, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFD, 0x5F, 0x10, 0x1C, 0x02, 0xE0,
    0x1F, 0x00, 0x00, 0x00, 0x00, 0x30, 0xFF, 0xDF, 0x6C, 0x34, 0x9F, 0xE1, 0x3F, 0x04, 0x00, 0x00,
    0x00, 0x80, 0xFF, 0x3F, 0xFE, 0x73, 0xCF, 0xE2, 0x2F, 0x4D, 0x00, 0x00, 0x00, 0xE0, 0xDC, 0x1F,
    0xF8, 0xC6, 0x3F, 0xE0, 0x1F, 0xF7, 0x04, 0x00, 0x00, 0xF5, 0xD4, 0x1F, 0xC1, 0xF2, 0x0B, 0xE0,
    0x1F, 0xE1, 0x2E, 0x00, 0x00, 0xAC, 0xD0, 0x1F, 0x00, 0xF6, 0x04, 0xE0, 0x1F, 0xA0, 0xCF, 0x00,
    0x40, 0x1D, 0xD0, 0x1F, 0x00, 0xBC, 0x00, 0xE0, 0x1F, 0x50, 0xFF, 0x03, 0xC0, 0x03, 0xD0, 0x1F,
    0x30, 0x2F, 0x00, 0xE0, 0x1F, 0x20, 0xFF, 0x05, 0x56, 0x00, 0xD0, 0x1F, 0x90, 0x07, 0x00, 0xE0,
    0x1F, 0x00, 0xEE, 0x01, 0x00, 0x00, 0xD0, 0x1F, 0xB2, 0x20, 0x02, 0xE0, 0x1F, 0x00, 0x46, 0x00,
    0x00, 0x00, 0xE0, 0x1F, 0x25, 0x30, 0xEB, 0xFD, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x1F,
    0x00, 0x00, 0x50, 0xFF, 0x0D, 0x00, 0x00, 0x00, 0x00, 0x00, 0xD0, 0x07, 0x00, 0x00, 0x00, 0xA9,
    0x02, 0x00, 0x00, 0x00,
    // Unicode: [0x7BA1, ]
    0x00, 0x00, 0x70, 0x5A, 0x00, 0x00, 0x00, 0x79, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0xBF,
    0x02, 0x02, 0x30, 0xDF, 0x05, 0x20, 0x03, 0x00, 0x00, 0x00, 0xF6, 0x0C, 0x30, 0xAF, 0x90, 0x3E,
    0x00, 0xD1, 0x5F, 0x00, 0x00, 0x10, 0xEE, 0xCC, 0xAA, 0xAA, 0xF6, 0xED, 0xCC, 0xCC, 0xBC, 0x02,
    0x00, 0x90, 0x4F, 0xE2, 0x25, 0x01, 0x5A, 0xC0, 0x2B, 0x00, 0x00, 0x00, 0x00, 0xF5, 0x05, 0xC0,
    0x3F, 0xBE, 0x04, 0x40, 0xCF, 0x00, 0x00, 0x00, 0x20, 0x5D, 0x02, 0x90, 0x1E, 0xF8, 0x08, 0x00,
    0xAD, 0x10, 0x00, 0x00, 0xA1, 0x42, 0x08, 0x30, 0x03, 0xD1, 0x04, 0x00, 0x13, 0xC0, 0x3C, 0x00,
    0x00, 0xA0, 0xBD, 0xBB, 0xBB, 0xBB, 0xBB, 0xBB, 0xBB, 0xFB, 0xEF, 0x04, 0x00, 0xF5, 0x88, 0x05,
    0x00, 0x00, 0x00, 0x70, 0x04, 0xF4, 0x19, 0x00, 0x60, 0xFF, 0xD5, 0xDF, 0xCC, 0xCC, 0xCC, 0xFC,
    0x8F, 0x5A, 0x00, 0x00, 0x80, 0x7D, 0xC0, 0x3F, 0x00, 0x00, 0x00, 0xE0, 0x3F, 0x01, 0x00, 0x00,
    0x00, 0x00, 0xC0, 0x3F, 0x00, 0x00, 0x00, 0xE0, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xCF,
    0xBB, 0xBB, 0xBB, 0xFB, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x3F, 0x00, 0x00, 0x00, 0xE0,
    0x1D, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x3F, 0x00, 0x00, 0x00, 0x30, 0x47, 0x00, 0x00, 0x00,
    0x00, 0x00, 0xC0, 0xCF, 0xCC, 0xCC, 0xCC, 0xCC, 0xFF, 0x0B, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x3F,
    0x00, 0x00, 0x00, 0x20, 0xFF, 0x03, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x3F, 0x00, 0x00, 0x00, 0x20,
    0xFF, 0x01, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xBF, 0xAA, 0xAA, 0xAA, 0xAA, 0xFF, 0x01, 0x00, 0x00,
    0x00, 0x00, 0xD0, 0x5F, 0x33, 0x33, 0x33, 0x53, 0xFF, 0x01, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x1A,
    0x00, 0x00, 0x00, 0x20, 0x9E, 0x00, 0x00, 0x00,
    // Unicode: [0x7C92, ]
    0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x90, 0x7D,
    0x00, 0x00, 0x00, 0x99, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x90, 0x4F, 0x00, 0x00, 0x00, 0xF2,
    0x0B, 0x00, 0x00, 0x00, 0x30, 0x07, 0x80, 0x2F, 0xB0, 0x08, 0x00, 0xB0, 0x6F, 0x00, 0x00, 0x00,
    0x00, 0x5B, 0x80, 0x2F, 0xF1, 0x5E, 0x00, 0x80, 0x5F, 0x20, 0x19, 0x00, 0x00, 0xE6, 0x81, 0x2F,
    0xE7, 0x03, 0x00, 0x40, 0x09, 0xC0, 0xCF, 0x01, 0x00, 0xF2, 0x88, 0x2F, 0x5D, 0xB3, 0xAA, 0xAA,
    0xAA, 0xCA, 0xCC, 0x07, 0x00, 0xF0, 0x89, 0x8F, 0x07, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00,
    0x00, 0x80, 0x82, 0x6F, 0xD3, 0x04, 0x04, 0x00, 0x00, 0xDE, 0x06, 0x00, 0xA0, 0xBB, 0xDB, 0xBF,
    0xFE, 0x4F, 0x28, 0x00, 0x10, 0xFF, 0x07, 0x00, 0x10, 0x00, 0xF2, 0x2F, 0x00, 0x00, 0x85, 0x00,
    0x30, 0xCF, 0x00, 0x00, 0x00, 0x00, 0xF7, 0x3F, 0x00, 0x00, 0xD2, 0x00, 0x50, 0x9F, 0x00, 0x00,
    0x00, 0x00, 0xFC, 0xAF, 0x4B, 0x00, 0xF0, 0x04, 0x70, 0x5F, 0x00, 0x00, 0x00, 0x30, 0xFF, 0x2F,
    0xFA, 0x0A, 0xD0, 0x09, 0x90, 0x1F, 0x00, 0x00, 0x00, 0x90, 0xAE, 0x2F, 0xE1, 0x3F, 0xC0, 0x0D,
    0xB0, 0x0C, 0x00, 0x00, 0x00, 0xE2, 0x87, 0x2F, 0x90, 0x2F, 0xC0, 0x1F, 0xE0, 0x08, 0x00, 0x00,
    0x00, 0xC9, 0x80, 0x2F, 0x10, 0x05, 0xB0, 0x3F, 0xF2, 0x04, 0x00, 0x00, 0x30, 0x2E, 0x80, 0x2F,
    0x00, 0x00, 0xB0, 0x1F, 0xE4, 0x00, 0x00, 0x00, 0xC0, 0x03, 0x80, 0x2F, 0x00, 0x00, 0xA0, 0x08,
    0xA7, 0x00, 0x23, 0x00, 0x46, 0x00, 0x80, 0x2F, 0x00, 0x00, 0x00, 0x00, 0x5A, 0x20, 0xEE, 0x03,
    0x00, 0x00, 0x90, 0x2F, 0xB1, 0xBB, 0xBB, 0xBB, 0xBE, 0xDB, 0xFF, 0x1E, 0x00, 0x00, 0x90, 0x2F,
    0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x90, 0x0A, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    // Unicode: [0x7D2F, ]
    0x00, 0x00, 0x68, 0x01, 0x00, 0x00, 0x00, 0x00, 0x20, 0x6D, 0x00, 0x00, 0x00, 0xFB, 0xAB, 0xAA,
    0xFB, 0xAE, 0xAA, 0xCA, 0xFF, 0x07, 0x00, 0x00, 0xFA, 0x03, 0x00, 0xF1, 0x0C, 0x00, 0x40, 0xBF,
    0x00, 0x00, 0x00, 0xFA, 0x9A, 0x99, 0xF9, 0x9E, 0x99, 0xA9, 0xBF, 0x00, 0x00, 0x00, 0xFA, 0x25,
    0x22, 0xF3, 0x2C, 0x22, 0x62, 0xBF, 0x00, 0x00, 0x00, 0xFA, 0x03, 0x00, 0xF1, 0x0C, 0x00, 0x40,
    0xBF, 0x00, 0x00, 0x00, 0xFB, 0xAB, 0xAA, 0xFB, 0xAE, 0xAA, 0xCA, 0xCF, 0x00, 0x00, 0x00, 0xEB,
    0x02, 0x10, 0xFB, 0x1A, 0x00, 0x40, 0x7D, 0x00, 0x00, 0x00, 0x12, 0x00, 0xD3, 0x8F, 0x13, 0x00,
    0x57, 0x00, 0x00, 0x00, 0x00, 0x00, 0x82, 0xAF, 0x02, 0x00, 0xC2, 0xFF, 0x07, 0x00, 0x00, 0x00,
    0xC8, 0xFF, 0x8C, 0x88, 0x98, 0xFF, 0x8D, 0x06, 0x00, 0x00, 0x00, 0xB8, 0x89, 0x56, 0xC7, 0xDF,
    0x58, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0xC7, 0x8C, 0x03, 0x30, 0xCA, 0x28, 0x00, 0x00,
    0x60, 0xBA, 0xFE, 0xCF, 0x9A, 0x99, 0x99, 0xC9, 0xFF, 0x08, 0x00, 0x70, 0xDF, 0xAC, 0x89, 0xE7,
    0x6F, 0x23, 0x01, 0xF3, 0x2F, 0x00, 0x10, 0x01, 0xB7, 0x03, 0xD0, 0x3F, 0x11, 0x00, 0x60, 0x0E,
    0x00, 0x00, 0x50, 0xFF, 0x1A, 0xD0, 0x3F, 0xB2, 0x8C, 0x03, 0x01, 0x00, 0x00, 0xF5, 0x5E, 0x00,
    0xD0, 0x3F, 0x00, 0xF7, 0xCF, 0x04, 0x00, 0x60, 0xAF, 0x01, 0x00, 0xE0, 0x3F, 0x00, 0x40, 0xFE,
    0x2F, 0x10, 0xC9, 0x04, 0x30, 0xA9, 0xFB, 0x2F, 0x00, 0x00, 0xE2, 0x4F, 0xA2, 0x05, 0x00, 0x00,
    0x50, 0xFF, 0x0D, 0x00, 0x00, 0x20, 0x1C, 0x01, 0x00, 0x00, 0x00, 0x00, 0xA9, 0x02, 0x00, 0x00,
    0x00, 0x00,
    // Unicode: [0x7EB8, ]
    0x00, 0x00, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x57, 0x00, 0x00, 0x00, 0x00, 0xF8, 0x2B,
    0x00, 0x01, 0x00, 0x20, 0xE8, 0xFF, 0x05, 0x00, 0x00, 0x00, 0xFD, 0x2A, 0x10, 0x8E, 0x73, 0xFB,
    0xFF, 0xDF, 0x1C, 0x00, 0x00, 0x50, 0xCF, 0x00, 0x10, 0xFF, 0x9A, 0xF9, 0x2E, 0x00, 0x00, 0x00,
    0x00, 0xB0, 0x2E, 0x20, 0x11, 0xEF, 0x00, 0xF2, 0x0D, 0x00, 0x00, 0x00, 0x00, 0xF3, 0x06, 0xB0,
    0x5D, 0xEF, 0x00, 0xF1, 0x0D, 0x00, 0x00, 0x00, 0x00, 0xAA, 0x00, 0xF3, 0x6E, 0xEF, 0x00, 0xF1,
    0x0D, 0x00, 0x00, 0x00, 0x60, 0x1E, 0x00, 0xFB, 0x16, 0xEF, 0x00, 0xF1, 0x0D, 0xC1, 0x03, 0x00,
    0xF9, 0xDE, 0xEE, 0xAF, 0x10, 0xEF, 0x22, 0xF2, 0x2E, 0xFB, 0x4E, 0x00, 0xF5, 0x5A, 0xC1, 0x1D,
    0x10, 0xFF, 0x99, 0xF9, 0x9F, 0x99, 0x89, 0x00, 0x20, 0x00, 0xF4, 0x03, 0x10, 0xEF, 0x00, 0xE0,
    0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7C, 0x00, 0x10, 0xEF, 0x00, 0xC0, 0x1F, 0x00, 0x00, 0x00,
    0x00, 0x90, 0x0B, 0x00, 0x21, 0xEF, 0x00, 0xB0, 0x3F, 0x00, 0x00, 0x00, 0x30, 0xFC, 0xDD, 0xEE,
    0x5B, 0xEF, 0x00, 0x90, 0x5F, 0x00, 0x00, 0x00, 0x20, 0xFF, 0x9E, 0x04, 0x10, 0xEF, 0x00, 0x60,
    0x8F, 0x00, 0x00, 0x00, 0x00, 0x46, 0x00, 0x00, 0x10, 0xEF, 0x00, 0x30, 0xCF, 0x00, 0x80, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x53, 0xEF, 0x00, 0x00, 0xFD, 0x02, 0x82, 0x00, 0x00, 0x00, 0x73, 0xEB,
    0x29, 0xEF, 0x10, 0x97, 0xF7, 0x0A, 0x75, 0x00, 0x82, 0xEB, 0xEF, 0x18, 0x30, 0xEF, 0xE8, 0x08,
    0xE1, 0x7F, 0x6A, 0x00, 0xD0, 0xEF, 0x17, 0x00, 0x90, 0xFF, 0x4E, 0x00, 0x50, 0xFF, 0x8F, 0x00,
    0x30, 0x19, 0x00, 0x00, 0x90, 0xDF, 0x02, 0x00, 0x00, 0xF6, 0xDF, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x10, 0x1B, 0x00, 0x00, 0x00, 0x30, 0xEA, 0x02,
    // Unicode: [0x7EBF, ]
    0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xB5, 0x03,
    0x00, 0x40, 0xCF, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFA, 0x4F, 0x00, 0x20, 0xFF, 0x82,
    0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFE, 0x05, 0x00, 0x10, 0xFF, 0x20, 0xDF, 0x01, 0x00, 0x00,
    0x00, 0x50, 0x9F, 0x00, 0x00, 0x00, 0xFF, 0x00, 0xFA, 0x04, 0x00, 0x00, 0x00, 0xC0, 0x1D, 0x30,
    0x4D, 0x00, 0xFF, 0x00, 0x84, 0x49, 0x00, 0x00, 0x00, 0xF5, 0x03, 0xA0, 0xEF, 0x02, 0xFE, 0x00,
    0x30, 0xFF, 0x09, 0x00, 0x10, 0x6D, 0x00, 0xF3, 0x2E, 0x10, 0xFE, 0xA8, 0xAB, 0x68, 0x03, 0x00,
    0xB1, 0xAF, 0xBB, 0xFD, 0x75, 0xAC, 0xFE, 0x14, 0x00, 0x00, 0x00, 0x00, 0xD0, 0xBF, 0x86, 0x9F,
    0x00, 0x00, 0xFB, 0x03, 0x00, 0xB2, 0x02, 0x00, 0x30, 0x03, 0xD1, 0x0C, 0x00, 0x00, 0xFA, 0x04,
    0x00, 0xF9, 0x4E, 0x00, 0x00, 0x00, 0xE8, 0x02, 0x00, 0x00, 0xF9, 0x99, 0xAA, 0x58, 0x02, 0x00,
    0x00, 0x30, 0x4E, 0x00, 0x80, 0xBA, 0xFC, 0x19, 0x00, 0x7B, 0x00, 0x00, 0x00, 0xD4, 0x5A, 0x87,
    0x99, 0x01, 0xF4, 0x0A, 0x60, 0xFF, 0x08, 0x00, 0x40, 0xFF, 0xFF, 0x7C, 0x02, 0x00, 0xF1, 0x0E,
    0xE3, 0x7F, 0x00, 0x00, 0x00, 0xDA, 0x27, 0x00, 0x00, 0x00, 0xC0, 0x6F, 0xFD, 0x08, 0x70, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x84, 0x02, 0x70, 0xFF, 0x8F, 0x00, 0xB0, 0x00, 0x00, 0x00, 0x30, 0xD8,
    0x2A, 0x00, 0x50, 0xFF, 0x09, 0x00, 0xB2, 0x00, 0x00, 0x94, 0xFD, 0x4C, 0x00, 0x00, 0xF8, 0xFE,
    0x3E, 0x00, 0xA5, 0x00, 0x40, 0xFF, 0x6E, 0x00, 0x00, 0xC3, 0x8E, 0x91, 0xEF, 0x05, 0xB9, 0x00,
    0x00, 0xA8, 0x01, 0x00, 0xA2, 0x8E, 0x01, 0x00, 0xFA, 0xCF, 0xDF, 0x00, 0x00, 0x00, 0x00, 0x20,
    0x5A, 0x00, 0x00, 0x00, 0x70, 0xFF, 0xFF, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x82, 0xFD, 0x07,
    // Unicode: [0x7F6E, ]
    0x00, 0x72, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x2B, 0x00, 0x00, 0x00, 0xF2, 0xCF, 0xCC,
    0xEF, 0xCC, 0xFC, 0xCE, 0xEC, 0xEF, 0x03, 0x00, 0x00, 0xF2, 0x0D, 0x10, 0xCF, 0x00, 0xF3, 0x0B,
    0xC0, 0x7F, 0x00, 0x00, 0x00, 0xF2, 0x0D, 0x10, 0xCF, 0x00, 0xF3, 0x0B, 0xC0, 0x4F, 0x00, 0x00,
    0x00, 0xF2, 0x0D, 0x10, 0xCF, 0x00, 0xF3, 0x0B, 0xC0, 0x4F, 0x00, 0x00, 0x00, 0xF2, 0xCF, 0xCC,
    0xCC, 0xCE, 0xCC, 0xCC, 0xEC, 0x5F, 0x00, 0x00, 0x00, 0xF3, 0x09, 0x00, 0x00, 0xDE, 0x06, 0x00,
    0x90, 0x59, 0x00, 0x00, 0x10, 0x11, 0x00, 0x00, 0x10, 0xFF, 0x05, 0x00, 0x10, 0xFB, 0x0A, 0x00,
    0xB0, 0xBB, 0xBB, 0xBB, 0xBB, 0xFF, 0xBB, 0xBB, 0xBB, 0xBB, 0x5B, 0x00, 0x00, 0x00, 0x42, 0x00,
    0x30, 0xEF, 0x00, 0x30, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF4, 0xCF, 0xDC, 0xEF, 0xCC, 0xEC,
    0xAF, 0x01, 0x00, 0x00, 0x00, 0x00, 0xF3, 0x0F, 0x00, 0x00, 0x00, 0xB0, 0x9F, 0x00, 0x00, 0x00,
    0x00, 0x00, 0xF3, 0x1F, 0x11, 0x11, 0x11, 0xB1, 0x5F, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF3, 0xBF,
    0xBB, 0xBB, 0xBB, 0xEB, 0x5F, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF3, 0x0F, 0x00, 0x00, 0x00, 0xB0,
    0x5F, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF3, 0xDF, 0xCC, 0xCC, 0xCC, 0xEC, 0x5F, 0x00, 0x00, 0x00,
    0x00, 0x00, 0xF3, 0x0F, 0x00, 0x00, 0x00, 0xB0, 0x5F, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF3, 0xCF,
    0xCC, 0xCC, 0xCC, 0xEC, 0x5F, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF3, 0x1F, 0x11, 0x11, 0x11, 0xB1,
    0x5F, 0x90, 0x05, 0x00, 0x01, 0x00, 0xF3, 0x0F, 0x00, 0x00, 0x00, 0xB0, 0x5F, 0xF6, 0x7F, 0x00,
    0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0x04
};
