// Autogenerated, do not edit

#include <fonts/GeneratedFont.hpp>

FONT_TABLE_LOCATION_FLASH_PRAGMA
KEEP extern const touchgfx::GlyphNode glyphs_STZHONGS_TTF_24_4bpp[] FONT_TABLE_LOCATION_FLASH_ATTRIBUTE =
{
    {     0, 0x0020,   0,   0,   0,   0,   8,   0,   0, 0x00 },
    {     0, 0x002D,   8,   2,   7,   1,  10,   0,   0, 0x00 },
    {     8, 0x0030,  13,  17,  17,   1,  15,   0,   0, 0x00 },
    {   127, 0x0031,   8,  17,  17,   4,  15,   0,   0, 0x00 },
    {   195, 0x0032,  12,  18,  17,   1,  15,   0,   0, 0x00 },
    {   303, 0x0033,  13,  17,  17,   1,  15,   0,   0, 0x00 },
    {   422, 0x0034,  12,  17,  17,   2,  15,   0,   0, 0x00 },
    {   524, 0x0035,  12,  17,  17,   2,  15,   0,   0, 0x00 },
    {   626, 0x0036,  13,  17,  17,   1,  15,   0,   0, 0x00 },
    {   745, 0x0037,  13,  17,  17,   1,  15,   0,   0, 0x00 },
    {   864, 0x0038,  13,  17,  17,   1,  15,   0,   0, 0x00 },
    {   983, 0x0039,  13,  17,  17,   1,  15,   0,   0, 0x00 },
    {  1102, 0x003F,  12,  17,  17,   1,  13,   0,   0, 0x00 },
    {  1204, 0x0043,  16,  17,  17,   1,  18,   0,   0, 0x00 },
    {  1340, 0x0046,  15,  17,  17,   1,  15,   0,   0, 0x00 },
    {  1476, 0x0049,   8,  17,  17,   0,   8,   0,   0, 0x00 },
    {  1544, 0x004B,  17,  17,  17,   1,  17,   0,   0, 0x00 },
    {  1697, 0x004D,  20,  17,  17,   1,  22,   0,   0, 0x00 },
    {  1867, 0x004F,  17,  17,  17,   1,  19,   0,   0, 0x00 },
    {  2020, 0x0050,  14,  17,  17,   1,  15,   0,   0, 0x00 },
    {  2139, 0x0056,  17,  17,  17,   0,  17,   0,   0, 0x00 },
    {  2292, 0x0057,  23,  17,  17,   0,  23,   0,   0, 0x00 },
    {  2496, 0x0073,  11,  12,  12,   1,  12,   0,   0, 0x00 },
    {     0, 0x2103,  20,  18,  17,   2,  24,   0,   0, 0x00 },
    {     0, 0x4E0A,  23,  21,  18,   1,  24,   0,   0, 0x00 },
    {   252, 0x4E0B,  22,  22,  18,   1,  24,   0,   0, 0x00 },
    {   494, 0x4E2D,  20,  23,  19,   2,  24,   0,   0, 0x00 },
    {   724, 0x4E3B,  24,  21,  19,   0,  24,   0,   0, 0x00 },
    {   976, 0x4F20,  23,  22,  18,   1,  24,   0,   0, 0x00 },
    {  1240, 0x4FDD,  24,  23,  19,   0,  24,   0,   0, 0x00 },
    {     0, 0x503C,  24,  23,  19,   0,  24,   0,   0, 0x00 },
    {   276, 0x505C,  23,  23,  19,   0,  24,   0,   0, 0x00 },
    {   552, 0x5165,  22,  21,  18,   1,  24,   0,   0, 0x00 },
    {   783, 0x5173,  22,  24,  19,   1,  24,   0,   0, 0x00 },
    {  1047, 0x5176,  24,  24,  19,   0,  24,   0,   0, 0x00 },
    {  1335, 0x51C0,  23,  23,  19,   1,  24,   0,   0, 0x00 },
    {  1611, 0x51C6,  22,  23,  19,   1,  24,   0,   0, 0x00 },
    {  1864, 0x524D,  22,  23,  19,   1,  24,   0,   0, 0x00 },
    {  2117, 0x529F,  22,  24,  19,   1,  24,   0,   0, 0x00 },
    {  2381, 0x52A1,  23,  23,  19,   1,  24,   0,   0, 0x00 },
    {  2657, 0x52A8,  22,  23,  18,   1,  24,   0,   0, 0x00 },
    {  2910, 0x5316,  23,  23,  19,   0,  24,   0,   0, 0x00 },
    {  3186, 0x5347,  24,  23,  19,   0,  24,   0,   0, 0x00 },
    {  3462, 0x5361,  23,  23,  19,   0,  24,   0,   0, 0x00 },
    {  3738, 0x538B,  23,  22,  18,   1,  24,   0,   0, 0x00 },
    {  4002, 0x53C2,  24,  24,  19,   0,  24,   0,   0, 0x00 },
    {  4290, 0x53D6,  24,  22,  18,   0,  24,   0,   0, 0x00 },
    {  4554, 0x53F7,  23,  22,  18,   1,  24,   0,   0, 0x00 },
    {  4818, 0x540D,  21,  23,  19,   0,  24,   0,   0, 0x00 },
    {  5071, 0x540E,  23,  23,  18,   1,  24,   0,   0, 0x00 },
    {  5347, 0x542F,  21,  23,  19,   0,  24,   0,   0, 0x00 },
    {  5600, 0x552E,  23,  23,  19,   0,  24,   0,   0, 0x00 },
    {  5876, 0x5668,  24,  23,  18,   0,  24,   0,   0, 0x00 },
    {  6152, 0x56DE,  19,  21,  17,   3,  24,   0,   0, 0x00 },
    {  6362, 0x5728,  24,  23,  19,   0,  24,   0,   0, 0x00 },
    {  6638, 0x5730,  23,  21,  17,   0,  24,   0,   0, 0x00 },
    {     0, 0x5907,  23,  22,  18,   0,  24,   0,   0, 0x00 },
    {   264, 0x5916,  23,  23,  19,   0,  24,   0,   0, 0x00 },
    {   540, 0x59CB,  22,  24,  19,   1,  24,   0,   0, 0x00 },
    {   804, 0x5B83,  22,  23,  19,   1,  24,   0,   0, 0x00 },
    {  1057, 0x5B9A,  23,  24,  19,   1,  24,   0,   0, 0x00 },
    {  1345, 0x5BA4,  23,  22,  19,   1,  24,   0,   0, 0x00 },
    {  1609, 0x5BC6,  24,  23,  19,   0,  24,   0,   0, 0x00 },
    {  1885, 0x5C4F,  23,  22,  18,   0,  24,   0,   0, 0x00 },
    {  2149, 0x5E26,  23,  23,  19,   1,  24,   0,   0, 0x00 },
    {  2425, 0x5E38,  24,  23,  19,   0,  24,   0,   0, 0x00 },
    {  2701, 0x5E74,  23,  23,  19,   1,  24,   0,   0, 0x00 },
    {  2977, 0x5E8F,  23,  24,  19,   0,  24,   0,   0, 0x00 },
    {  3265, 0x5EA6,  23,  24,  19,   1,  24,   0,   0, 0x00 },
    {  3553, 0x5EF6,  23,  23,  18,   1,  24,   0,   0, 0x00 },
    {  3829, 0x5F00,  23,  23,  18,   1,  24,   0,   0, 0x00 },
    {  4105, 0x5F02,  23,  22,  18,   1,  24,   0,   0, 0x00 },
    {  4369, 0x5F53,  20,  23,  19,   2,  24,   0,   0, 0x00 },
    {  4599, 0x5F55,  23,  22,  18,   1,  24,   0,   0, 0x00 },
    {  4863, 0x5F85,  23,  23,  19,   0,  24,   0,   0, 0x00 },
    {     0, 0x6001,  24,  23,  19,   0,  24,   0,   0, 0x00 },
    {   276, 0x603B,  22,  23,  19,   1,  24,   0,   0, 0x00 },
    {   529, 0x611F,  22,  23,  19,   1,  24,   0,   0, 0x00 },
    {   782, 0x6237,  21,  23,  19,   1,  24,   0,   0, 0x00 },
    {  1035, 0x62A5,  23,  23,  19,   1,  24,   0,   0, 0x00 },
    {  1311, 0x62AC,  22,  23,  19,   1,  24,   0,   0, 0x00 },
    {  1564, 0x62BD,  22,  23,  19,   1,  24,   0,   0, 0x00 },
    {  1817, 0x636E,  23,  23,  19,   1,  24,   0,   0, 0x00 },
    {  2093, 0x63A2,  24,  23,  19,   0,  24,   0,   0, 0x00 },
    {  2369, 0x6545,  23,  24,  19,   0,  24,   0,   0, 0x00 },
    {  2657, 0x6570,  23,  24,  19,   1,  24,   0,   0, 0x00 },
    {  2945, 0x65E0,  22,  23,  18,   1,  24,   0,   0, 0x00 },
    {  3198, 0x65E5,  15,  21,  17,   5,  24,   0,   0, 0x00 },
    {  3366, 0x65F6,  21,  23,  19,   2,  24,   0,   0, 0x00 },
    {  3619, 0x6682,  22,  23,  19,   1,  24,   0,   0, 0x00 },
    {  3872, 0x6708,  19,  22,  18,   0,  24,   0,   0, 0x00 },
    {  4092, 0x6709,  23,  22,  18,   0,  24,   0,   0, 0x00 },
    {  4356, 0x670D,  24,  22,  18,   0,  24,   0,   0, 0x00 },
    {  4620, 0x672A,  24,  23,  19,   0,  24,   0,   0, 0x00 },
    {  4896, 0x672C,  23,  22,  18,   1,  24,   0,   0, 0x00 },
    {  5160, 0x673A,  24,  22,  18,   0,  24,   0,   0, 0x00 },
    {  5424, 0x67E5,  24,  22,  19,   0,  24,   0,   0, 0x00 },
    {     0, 0x6807,  24,  22,  18,   0,  24,   0,   0, 0x00 },
    {   264, 0x6821,  24,  24,  19,   0,  24,   0,   0, 0x00 },
    {   552, 0x6837,  24,  23,  19,   0,  24,   0,   0, 0x00 },
    {   828, 0x683C,  23,  24,  19,   0,  24,   0,   0, 0x00 },
    {  1116, 0x68C0,  24,  23,  19,   0,  24,   0,   0, 0x00 },
    {  1392, 0x6B62,  23,  20,  18,   1,  24,   0,   0, 0x00 },
    {  1632, 0x6B63,  22,  21,  18,   1,  24,   0,   0, 0x00 },
    {  1863, 0x6C14,  24,  23,  19,   0,  24,   0,   0, 0x00 },
    {  2139, 0x6CB9,  22,  22,  18,   1,  24,   0,   0, 0x00 },
    {  2381, 0x6CF5,  22,  22,  18,   1,  24,   0,   0, 0x00 },
    {  2623, 0x6D41,  23,  24,  19,   1,  24,   0,   0, 0x00 },
    {  2911, 0x6D4B,  22,  24,  19,   0,  24,   0,   0, 0x00 },
    {  3175, 0x6D53,  23,  23,  19,   1,  24,   0,   0, 0x00 },
    {  3451, 0x6E05,  22,  23,  19,   1,  24,   0,   0, 0x00 },
    {  3704, 0x6E29,  24,  22,  18,   0,  24,   0,   0, 0x00 },
    {  3968, 0x6E7F,  24,  22,  18,   0,  24,   0,   0, 0x00 },
    {     0, 0x70C3,  23,  23,  19,   1,  24,   0,   0, 0x00 },
    {   276, 0x70DF,  22,  24,  19,   1,  24,   0,   0, 0x00 },
    {   540, 0x70ED,  24,  22,  18,   0,  24,   0,   0, 0x00 },
    {   804, 0x70F7,  23,  23,  18,   1,  24,   0,   0, 0x00 },
    {  1080, 0x7248,  23,  23,  19,   1,  24,   0,   0, 0x00 },
    {  1356, 0x7269,  23,  23,  19,   0,  24,   0,   0, 0x00 },
    {  1632, 0x72B6,  23,  24,  19,   1,  24,   0,   0, 0x00 },
    {  1920, 0x7387,  24,  23,  19,   0,  24,   0,   0, 0x00 },
    {  2196, 0x7528,  21,  22,  18,   2,  24,   0,   0, 0x00 },
    {  2438, 0x7532,  18,  22,  18,   3,  24,   0,   0, 0x00 },
    {  2636, 0x7535,  21,  23,  18,   3,  24,   0,   0, 0x00 },
    {  2889, 0x77E5,  22,  22,  18,   1,  24,   0,   0, 0x00 },
    {     0, 0x7801,  24,  22,  18,   0,  24,   0,   0, 0x00 },
    {   264, 0x786E,  23,  24,  19,   0,  24,   0,   0, 0x00 },
    {   552, 0x79F0,  23,  23,  19,   0,  24,   0,   0, 0x00 },
    {   828, 0x7BA1,  23,  22,  18,   0,  24,   0,   0, 0x00 },
    {  1092, 0x7C92,  24,  23,  19,   0,  24,   0,   0, 0x00 },
    {  1368, 0x7D2F,  22,  22,  18,   0,  24,   0,   0, 0x00 },
    {  1610, 0x7EB8,  23,  22,  18,   1,  24,   0,   0, 0x00 },
    {  1874, 0x7EBF,  23,  23,  19,   0,  24,   0,   0, 0x00 },
    {  2150, 0x7F6E,  23,  21,  18,   1,  24,   0,   0, 0x00 },
    {     0, 0x819C,  24,  24,  19,   0,  24,   0,   0, 0x00 },
    {   288, 0x81EA,  15,  23,  19,   5,  24,   0,   0, 0x00 },
    {   472, 0x83B7,  24,  24,  19,   0,  24,   0,   0, 0x00 },
    {     0, 0x884C,  23,  23,  19,   0,  24,   0,   0, 0x00 },
    {   276, 0x8B66,  24,  23,  19,   0,  24,   0,   0, 0x00 },
    {   552, 0x8BA1,  23,  23,  19,   0,  24,   0,   0, 0x00 },
    {   828, 0x8BA4,  24,  23,  19,   0,  24,   0,   0, 0x00 },
    {  1104, 0x8BB0,  24,  23,  19,   0,  24,   0,   0, 0x00 },
    {  1380, 0x8BBE,  24,  23,  18,   0,  24,   0,   0, 0x00 },
    {  1656, 0x8BD5,  23,  23,  19,   0,  24,   0,   0, 0x00 },
    {  1932, 0x8BE2,  23,  23,  19,   0,  24,   0,   0, 0x00 },
    {  2208, 0x8C61,  23,  23,  19,   1,  24,   0,   0, 0x00 },
    {  2484, 0x8D85,  24,  22,  18,   0,  24,   0,   0, 0x00 },
    {  2748, 0x8FD0,  24,  23,  19,   0,  24,   0,   0, 0x00 },
    {  3024, 0x8FD4,  24,  22,  17,   0,  24,   0,   0, 0x00 },
    {  3288, 0x8FDB,  24,  24,  19,   0,  24,   0,   0, 0x00 },
    {     0, 0x9000,  23,  23,  18,   0,  24,   0,   0, 0x00 },
    {   276, 0x91C7,  24,  22,  18,   0,  24,   0,   0, 0x00 },
    {   540, 0x91CF,  23,  21,  18,   0,  24,   0,   0, 0x00 },
    {   792, 0x9501,  23,  24,  19,   0,  24,   0,   0, 0x00 },
    {  1080, 0x95ED,  19,  23,  19,   3,  24,   0,   0, 0x00 },
    {  1310, 0x95F4,  21,  23,  19,   2,  24,   0,   0, 0x00 },
    {  1563, 0x964D,  23,  23,  19,   1,  24,   0,   0, 0x00 },
    {  1839, 0x9664,  23,  24,  20,   1,  24,   0,   0, 0x00 },
    {  2127, 0x9694,  21,  23,  18,   2,  24,   0,   0, 0x00 },
    {  2380, 0x969C,  22,  24,  20,   2,  24,   0,   0, 0x00 },
    {  2644, 0x96C6,  24,  24,  19,   0,  24,   0,   0, 0x00 },
    {  2932, 0x975E,  22,  22,  18,   2,  24,   0,   0, 0x00 },
    {     0, 0x9875,  22,  22,  18,   1,  24,   0,   0, 0x00 },
    {   242, 0x9897,  24,  23,  18,   0,  24,   0,   0, 0x00 },
    {   518, 0x98CE,  24,  23,  18,   0,  24,   0,   0, 0x00 }
};

// STZHONGS_TTF_24_4bpp
extern const touchgfx::GlyphNode glyphs_STZHONGS_TTF_24_4bpp[];
extern const uint8_t unicodes_STZHONGS_TTF_24_4bpp_0[];
extern const uint8_t unicodes_STZHONGS_TTF_24_4bpp_4[];
extern const uint8_t unicodes_STZHONGS_TTF_24_4bpp_9[];
extern const uint8_t unicodes_STZHONGS_TTF_24_4bpp_10[];
extern const uint8_t unicodes_STZHONGS_TTF_24_4bpp_11[];
extern const uint8_t unicodes_STZHONGS_TTF_24_4bpp_12[];
extern const uint8_t unicodes_STZHONGS_TTF_24_4bpp_13[];
extern const uint8_t unicodes_STZHONGS_TTF_24_4bpp_14[];
extern const uint8_t unicodes_STZHONGS_TTF_24_4bpp_15[];
extern const uint8_t unicodes_STZHONGS_TTF_24_4bpp_16[];
extern const uint8_t unicodes_STZHONGS_TTF_24_4bpp_17[];
extern const uint8_t unicodes_STZHONGS_TTF_24_4bpp_18[];
extern const uint8_t unicodes_STZHONGS_TTF_24_4bpp_19[];
extern const uint8_t* const unicodes_STZHONGS_TTF_24_4bpp[] =
{
    unicodes_STZHONGS_TTF_24_4bpp_0,
    0,
    0,
    0,
    unicodes_STZHONGS_TTF_24_4bpp_4,
    0,
    0,
    0,
    0,
    unicodes_STZHONGS_TTF_24_4bpp_9,
    unicodes_STZHONGS_TTF_24_4bpp_10,
    unicodes_STZHONGS_TTF_24_4bpp_11,
    unicodes_STZHONGS_TTF_24_4bpp_12,
    unicodes_STZHONGS_TTF_24_4bpp_13,
    unicodes_STZHONGS_TTF_24_4bpp_14,
    unicodes_STZHONGS_TTF_24_4bpp_15,
    unicodes_STZHONGS_TTF_24_4bpp_16,
    unicodes_STZHONGS_TTF_24_4bpp_17,
    unicodes_STZHONGS_TTF_24_4bpp_18,
    unicodes_STZHONGS_TTF_24_4bpp_19
};
extern const touchgfx::KerningNode kerning_STZHONGS_TTF_24_4bpp[];
touchgfx::GeneratedFont& getFont_STZHONGS_TTF_24_4bpp();

touchgfx::GeneratedFont& getFont_STZHONGS_TTF_24_4bpp()
{
    static touchgfx::GeneratedFont STZHONGS_TTF_24_4bpp(glyphs_STZHONGS_TTF_24_4bpp, 165, 24, 5, 4, 1, 0, 1, unicodes_STZHONGS_TTF_24_4bpp, kerning_STZHONGS_TTF_24_4bpp, 63, 0, 0);
    return STZHONGS_TTF_24_4bpp;
}
