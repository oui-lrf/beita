#include <touchgfx/hal/Types.hpp>

FONT_GLYPH_LOCATION_FLASH_PRAGMA
KEEP extern const uint8_t unicodes_RobotoCondensed_Regular_24_4bpp_4[] FONT_GLYPH_LOCATION_FLASH_ATTRIBUTE =
{
    // Unicode: [0x201D, quotedblright]
    0xA0, 0x8F, 0xF6, 0x0C, 0xA0, 0x8F, 0xF6, 0x0C, 0xA0, 0x6F, 0xF6, 0x0A, 0xC0, 0x1E, 0xF8, 0x04,
    0xF0, 0x09, 0xDB, 0x00, 0xF2, 0x03, 0x7D, 0x00, 0x20, 0x00, 0x02, 0x00,
    // Unicode: [0x2022, bullet]
    0x20, 0x67, 0x00, 0xE2, 0xFF, 0x0A, 0xF6, 0xFF, 0x0F, 0xF7, 0xFF, 0x1F, 0xF5, 0xFF, 0x0D, 0x90,
    0xDE, 0x04,
    // Unicode: [0x20AC, Euro]
    0x00, 0x00, 0x92, 0xFD, 0xAE, 0x00, 0x30, 0xFE, 0xDF, 0xCE, 0x00, 0xD0, 0xCF, 0x01, 0x10, 0x00,
    0xF5, 0x3F, 0x00, 0x00, 0x00, 0xF9, 0x0D, 0x00, 0x00, 0x00, 0xFA, 0x0B, 0x00, 0x00, 0xF3, 0xFF,
    0xFF, 0xFF, 0x4F, 0xC2, 0xFE, 0xCE, 0xCC, 0x3C, 0x00, 0xFA, 0x0A, 0x00, 0x00, 0xC2, 0xFE, 0xCE,
    0xCC, 0x3C, 0xF3, 0xFF, 0xFF, 0xFF, 0x4F, 0x00, 0xFA, 0x0B, 0x00, 0x00, 0x00, 0xF8, 0x0D, 0x00,
    0x00, 0x00, 0xF4, 0x3F, 0x00, 0x00, 0x00, 0xC0, 0xCF, 0x02, 0x10, 0x00, 0x20, 0xFE, 0xDF, 0xCE,
    0x00, 0x00, 0x91, 0xFD, 0xAE
};
