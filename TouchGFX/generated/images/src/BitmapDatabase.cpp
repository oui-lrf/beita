// 4.12.2 dither_algorithm=2 alpha_dither=yes layout_rotation=0 opaque_image_format=RGB888 non_opaque_image_format=ARGB8888 section=ExtFlashSection extra_section=ExtFlashSection generate_png=no 0x753fd475
// Generated by imageconverter. Please, do not edit!

#include <touchgfx/Bitmap.hpp>
#include <BitmapDatabase.hpp>

extern const unsigned char _bluebtn[]; // BITMAP_BLUEBTN_ID = 0, Size: 110x40 pixels
extern const unsigned char _bluepress[]; // BITMAP_BLUEPRESS_ID = 1, Size: 160x60 pixels
extern const unsigned char _bluerelease[]; // BITMAP_BLUERELEASE_ID = 2, Size: 160x60 pixels
extern const unsigned char _greenpress[]; // BITMAP_GREENPRESS_ID = 3, Size: 160x60 pixels
extern const unsigned char _greenrelease[]; // BITMAP_GREENRELEASE_ID = 4, Size: 160x60 pixels
extern const unsigned char _icon[]; // BITMAP_ICON_ID = 5, Size: 32x32 pixels
extern const unsigned char _keyboard_background[]; // BITMAP_KEYBOARD_BACKGROUND_ID = 6, Size: 310x318 pixels
extern const unsigned char _keyboard_key_highlighted[]; // BITMAP_KEYBOARD_KEY_HIGHLIGHTED_ID = 7, Size: 60x60 pixels
extern const unsigned char _pidbtn[]; // BITMAP_PIDBTN_ID = 8, Size: 40x32 pixels
extern const unsigned char _radio_button_selected[]; // BITMAP_RADIO_BUTTON_SELECTED_ID = 9, Size: 42x42 pixels
extern const unsigned char _radio_button_selected_pressed[]; // BITMAP_RADIO_BUTTON_SELECTED_PRESSED_ID = 10, Size: 42x42 pixels
extern const unsigned char _radio_button_unselected[]; // BITMAP_RADIO_BUTTON_UNSELECTED_ID = 11, Size: 42x42 pixels
extern const unsigned char _radio_button_unselected_pressed[]; // BITMAP_RADIO_BUTTON_UNSELECTED_PRESSED_ID = 12, Size: 42x42 pixels
extern const unsigned char _redpress[]; // BITMAP_REDPRESS_ID = 13, Size: 160x60 pixels
extern const unsigned char _redrelease[]; // BITMAP_REDRELEASE_ID = 14, Size: 160x60 pixels
extern const unsigned char _small_blubtn[]; // BITMAP_SMALL_BLUBTN_ID = 15, Size: 70x40 pixels
extern const unsigned char _small_blubtn_press[]; // BITMAP_SMALL_BLUBTN_PRESS_ID = 16, Size: 70x40 pixels

const touchgfx::Bitmap::BitmapData bitmap_database[] =
{
    { _bluebtn, 0, 110, 40, 5, 0, 100, (uint8_t)(touchgfx::Bitmap::ARGB8888) >> 3, 40, (uint8_t)(touchgfx::Bitmap::ARGB8888) & 0x7 },
    { _bluepress, 0, 160, 60, 19, 8, 122, (uint8_t)(touchgfx::Bitmap::ARGB8888) >> 3, 43, (uint8_t)(touchgfx::Bitmap::ARGB8888) & 0x7 },
    { _bluerelease, 0, 160, 60, 19, 8, 122, (uint8_t)(touchgfx::Bitmap::ARGB8888) >> 3, 43, (uint8_t)(touchgfx::Bitmap::ARGB8888) & 0x7 },
    { _greenpress, 0, 160, 60, 19, 8, 122, (uint8_t)(touchgfx::Bitmap::ARGB8888) >> 3, 43, (uint8_t)(touchgfx::Bitmap::ARGB8888) & 0x7 },
    { _greenrelease, 0, 160, 60, 19, 8, 122, (uint8_t)(touchgfx::Bitmap::ARGB8888) >> 3, 43, (uint8_t)(touchgfx::Bitmap::ARGB8888) & 0x7 },
    { _icon, 0, 32, 32, 29, 18, 2, (uint8_t)(touchgfx::Bitmap::ARGB8888) >> 3, 5, (uint8_t)(touchgfx::Bitmap::ARGB8888) & 0x7 },
    { _keyboard_background, 0, 310, 318, 0, 0, 310, (uint8_t)(touchgfx::Bitmap::RGB888) >> 3, 318, (uint8_t)(touchgfx::Bitmap::RGB888) & 0x7 },
    { _keyboard_key_highlighted, 0, 60, 60, 0, 0, 60, (uint8_t)(touchgfx::Bitmap::RGB888) >> 3, 60, (uint8_t)(touchgfx::Bitmap::RGB888) & 0x7 },
    { _pidbtn, 0, 40, 32, 2, 1, 36, (uint8_t)(touchgfx::Bitmap::ARGB8888) >> 3, 30, (uint8_t)(touchgfx::Bitmap::ARGB8888) & 0x7 },
    { _radio_button_selected, 0, 42, 42, 10, 10, 22, (uint8_t)(touchgfx::Bitmap::ARGB8888) >> 3, 21, (uint8_t)(touchgfx::Bitmap::ARGB8888) & 0x7 },
    { _radio_button_selected_pressed, 0, 42, 42, 10, 10, 22, (uint8_t)(touchgfx::Bitmap::ARGB8888) >> 3, 21, (uint8_t)(touchgfx::Bitmap::ARGB8888) & 0x7 },
    { _radio_button_unselected, 0, 42, 42, 10, 10, 22, (uint8_t)(touchgfx::Bitmap::ARGB8888) >> 3, 21, (uint8_t)(touchgfx::Bitmap::ARGB8888) & 0x7 },
    { _radio_button_unselected_pressed, 0, 42, 42, 10, 10, 21, (uint8_t)(touchgfx::Bitmap::ARGB8888) >> 3, 22, (uint8_t)(touchgfx::Bitmap::ARGB8888) & 0x7 },
    { _redpress, 0, 160, 60, 19, 8, 122, (uint8_t)(touchgfx::Bitmap::ARGB8888) >> 3, 43, (uint8_t)(touchgfx::Bitmap::ARGB8888) & 0x7 },
    { _redrelease, 0, 160, 60, 19, 8, 122, (uint8_t)(touchgfx::Bitmap::ARGB8888) >> 3, 43, (uint8_t)(touchgfx::Bitmap::ARGB8888) & 0x7 },
    { _small_blubtn, 0, 70, 40, 2, 1, 66, (uint8_t)(touchgfx::Bitmap::ARGB8888) >> 3, 38, (uint8_t)(touchgfx::Bitmap::ARGB8888) & 0x7 },
    { _small_blubtn_press, 0, 70, 40, 2, 1, 66, (uint8_t)(touchgfx::Bitmap::ARGB8888) >> 3, 38, (uint8_t)(touchgfx::Bitmap::ARGB8888) & 0x7 }
};

namespace BitmapDatabase
{
const touchgfx::Bitmap::BitmapData* getInstance()
{
    return bitmap_database;
}
uint16_t getInstanceSize()
{
    return (uint16_t)(sizeof(bitmap_database) / sizeof(touchgfx::Bitmap::BitmapData));
}
}
