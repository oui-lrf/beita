/* DO NOT EDIT THIS FILE */
/* This file is autogenerated by the text-database code generator */

#include <stdint.h>
#include <touchgfx/Unicode.hpp>

extern const uint32_t indicesGb[];

TEXT_LOCATION_FLASH_PRAGMA
KEEP extern const uint32_t indicesGb[] TEXT_LOCATION_FLASH_ATTRIBUTE =
{
    35, // T_Year
    35, // T_VD16
    35, // T_VD48
    35, // T_HZ24
    35, // T_Rob24
    35, // T_EnteredText
    254, // T_NumMode
    282, // T_AlphaMode
    35, // T_Rob32
    35, // T_HZ20
    139, // T_SingleUseId130
    149, // T_SingleUseId181
    154, // T_SingleUseId182
    343, // T_SingleUseId183
    307, // T_SingleUseId184
    349, // T_SingleUseId186
    349, // T_SingleUseId194
    349, // T_SingleUseId195
    349, // T_SingleUseId196
    349, // T_SingleUseId197
    349, // T_SingleUseId198
    349, // T_SingleUseId199
    349, // T_SingleUseId200
    353, // T_SingleUseId201
    353, // T_SingleUseId203
    353, // T_SingleUseId204
    353, // T_SingleUseId205
    355, // T_SingleUseId208
    357, // T_SingleUseId209
    359, // T_SingleUseId210
    349, // T_SingleUseId211
    349, // T_SingleUseId212
    270, // T_SingleUseId214
    316, // T_SingleUseId215
    249, // T_SingleUseId216
    128, // T_SingleUseId218
    266, // T_SingleUseId219
    45, // T_SingleUseId225
    159, // T_SingleUseId226
    35, // T_HZ16
    134, // T_SingleUseId305
    286, // T_SingleUseId306
    144, // T_SingleUseId307
    292, // T_SingleUseId313
    365, // T_SingleUseId332
    201, // T_SingleUseId345
    52, // T_SingleUseId362
    52, // T_SingleUseId365
    37, // T_SingleUseId366
    322, // T_SingleUseId368
    262, // T_SingleUseId369
    328, // T_EnterKey
    331, // T_SingleUseId377
    186, // T_SingleUseId378
    304, // T_SingleUseId379
    340, // T_SingleUseId380
    313, // T_SingleUseId382
    310, // T_SingleUseId383
    151, // T_SingleUseId384
    346, // T_SingleUseId385
    301, // T_SingleUseId386
    298, // T_SingleUseId381
    274, // T_SingleUseId389
    278, // T_SingleUseId390
    286, // T_Cancel
    244, // T_CapsLock
    295, // T_BackSpace
    194, // T_SingleUseId392
    239, // T_SingleUseId393
    361, // T_SingleUseId399
    289, // T_SingleUseId401
    292, // T_SingleUseId423
    365, // T_SingleUseId426
    292, // T_SingleUseId428
    365, // T_SingleUseId430
    289, // T_SingleUseId432
    361, // T_SingleUseId434
    289, // T_SingleUseId436
    361, // T_SingleUseId438
    234, // T_SingleUseId447
    229, // T_SingleUseId448
    224, // T_SingleUseId449
    219, // T_SingleUseId450
    304, // T_SingleUseId451
    20, // T_SingleUseId452
    214, // T_SingleUseId454
    209, // T_SingleUseId455
    0, // T_SingleUseId456
    204, // T_SingleUseId457
    199, // T_SingleUseId458
    361, // T_SingleUseId460
    361, // T_SingleUseId461
    116, // T_SingleUseId475
    59, // T_SingleUseId478
    86, // T_SingleUseId479
    86, // T_SingleUseId480
    104, // T_SingleUseId481
    66, // T_SingleUseId482
    86, // T_SingleUseId483
    86, // T_SingleUseId484
    98, // T_SingleUseId485
    73, // T_SingleUseId486
    86, // T_SingleUseId487
    86, // T_SingleUseId488
    80, // T_SingleUseId489
    86, // T_SingleUseId490
    92, // T_SingleUseId491
    86, // T_SingleUseId492
    110, // T_SingleUseId493
    86, // T_SingleUseId494
    258, // T_SingleUseId495
    334, // T_SingleUseId496
    334, // T_SingleUseId497
    325, // T_SingleUseId498
    351, // T_SingleUseId499
    337, // T_SingleUseId500
    334, // T_SingleUseId501
    189, // T_SingleUseId394
    184, // T_SingleUseId391
    179, // T_SingleUseId502
    174, // T_SingleUseId503
    169, // T_SingleUseId504
    164, // T_SingleUseId505
    344, // T_SingleUseId506
    344, // T_SingleUseId507
    319, // T_SingleUseId508
    54, // T_SingleUseId509
    54, // T_SingleUseId510
    54, // T_SingleUseId511
    256, // T_SingleUseId513
    256, // T_SingleUseId514
    363, // T_SingleUseId515
    363, // T_SingleUseId516
    122, // T_SingleUseId517
    122, // T_SingleUseId518
    122, // T_SingleUseId519
    122, // T_SingleUseId520
    122, // T_SingleUseId521
    122  // T_SingleUseId522
};
