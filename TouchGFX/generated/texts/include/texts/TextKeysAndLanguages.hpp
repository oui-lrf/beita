/* DO NOT EDIT THIS FILE */
/* This file is autogenerated by the text-database code generator */

#ifndef TEXTKEYSANDLANGUAGES_HPP
#define TEXTKEYSANDLANGUAGES_HPP


typedef enum
{
    GB,
    NUMBER_OF_LANGUAGES
} LANGUAGES;


typedef enum
{
    T_YEAR,
    T_VD16,
    T_VD48,
    T_HZ24,
    T_ROB24,
    T_ENTEREDTEXT,
    T_NUMMODE,
    T_ALPHAMODE,
    T_ROB32,
    T_HZ20,
    T_SINGLEUSEID130,
    T_SINGLEUSEID181,
    T_SINGLEUSEID182,
    T_SINGLEUSEID183,
    T_SINGLEUSEID184,
    T_SINGLEUSEID186,
    T_SINGLEUSEID194,
    T_SINGLEUSEID195,
    T_SINGLEUSEID196,
    T_SINGLEUSEID197,
    T_SINGLEUSEID198,
    T_SINGLEUSEID199,
    T_SINGLEUSEID200,
    T_SINGLEUSEID201,
    T_SINGLEUSEID203,
    T_SINGLEUSEID204,
    T_SINGLEUSEID205,
    T_SINGLEUSEID208,
    T_SINGLEUSEID209,
    T_SINGLEUSEID210,
    T_SINGLEUSEID211,
    T_SINGLEUSEID212,
    T_SINGLEUSEID214,
    T_SINGLEUSEID215,
    T_SINGLEUSEID216,
    T_SINGLEUSEID218,
    T_SINGLEUSEID219,
    T_SINGLEUSEID225,
    T_SINGLEUSEID226,
    T_HZ16,
    T_SINGLEUSEID305,
    T_SINGLEUSEID306,
    T_SINGLEUSEID307,
    T_SINGLEUSEID313,
    T_SINGLEUSEID332,
    T_SINGLEUSEID345,
    T_SINGLEUSEID362,
    T_SINGLEUSEID365,
    T_SINGLEUSEID366,
    T_SINGLEUSEID368,
    T_SINGLEUSEID369,
    T_ENTERKEY,
    T_SINGLEUSEID377,
    T_SINGLEUSEID378,
    T_SINGLEUSEID379,
    T_SINGLEUSEID380,
    T_SINGLEUSEID382,
    T_SINGLEUSEID383,
    T_SINGLEUSEID384,
    T_SINGLEUSEID385,
    T_SINGLEUSEID386,
    T_SINGLEUSEID381,
    T_SINGLEUSEID389,
    T_SINGLEUSEID390,
    T_CANCEL,
    T_CAPSLOCK,
    T_BACKSPACE,
    T_SINGLEUSEID392,
    T_SINGLEUSEID393,
    T_SINGLEUSEID399,
    T_SINGLEUSEID401,
    T_SINGLEUSEID423,
    T_SINGLEUSEID426,
    T_SINGLEUSEID428,
    T_SINGLEUSEID430,
    T_SINGLEUSEID432,
    T_SINGLEUSEID434,
    T_SINGLEUSEID436,
    T_SINGLEUSEID438,
    T_SINGLEUSEID447,
    T_SINGLEUSEID448,
    T_SINGLEUSEID449,
    T_SINGLEUSEID450,
    T_SINGLEUSEID451,
    T_SINGLEUSEID452,
    T_SINGLEUSEID454,
    T_SINGLEUSEID455,
    T_SINGLEUSEID456,
    T_SINGLEUSEID457,
    T_SINGLEUSEID458,
    T_SINGLEUSEID460,
    T_SINGLEUSEID461,
    T_SINGLEUSEID475,
    T_SINGLEUSEID478,
    T_SINGLEUSEID479,
    T_SINGLEUSEID480,
    T_SINGLEUSEID481,
    T_SINGLEUSEID482,
    T_SINGLEUSEID483,
    T_SINGLEUSEID484,
    T_SINGLEUSEID485,
    T_SINGLEUSEID486,
    T_SINGLEUSEID487,
    T_SINGLEUSEID488,
    T_SINGLEUSEID489,
    T_SINGLEUSEID490,
    T_SINGLEUSEID491,
    T_SINGLEUSEID492,
    T_SINGLEUSEID493,
    T_SINGLEUSEID494,
    T_SINGLEUSEID495,
    T_SINGLEUSEID496,
    T_SINGLEUSEID497,
    T_SINGLEUSEID498,
    T_SINGLEUSEID499,
    T_SINGLEUSEID500,
    T_SINGLEUSEID501,
    T_SINGLEUSEID394,
    T_SINGLEUSEID391,
    T_SINGLEUSEID502,
    T_SINGLEUSEID503,
    T_SINGLEUSEID504,
    T_SINGLEUSEID505,
    T_SINGLEUSEID506,
    T_SINGLEUSEID507,
    T_SINGLEUSEID508,
    T_SINGLEUSEID509,
    T_SINGLEUSEID510,
    T_SINGLEUSEID511,
    T_SINGLEUSEID513,
    T_SINGLEUSEID514,
    T_SINGLEUSEID515,
    T_SINGLEUSEID516,
    T_SINGLEUSEID517,
    T_SINGLEUSEID518,
    T_SINGLEUSEID519,
    T_SINGLEUSEID520,
    T_SINGLEUSEID521,
    T_SINGLEUSEID522,
    NUMBER_OF_TEXT_KEYS
} TEXTS;

#endif // TEXTKEYSANDLANGUAGES_HPP
