/*********************************************************************************/
/********** THIS FILE IS GENERATED BY TOUCHGFX DESIGNER, DO NOT MODIFY ***********/
/*********************************************************************************/
#include <gui_generated/containers/SetBackFrameContainBase.hpp>
#include <touchgfx/Color.hpp>
#include <texts/TextKeysAndLanguages.hpp>

SetBackFrameContainBase::SetBackFrameContainBase() :
    flexButtonCallback(this, &SetBackFrameContainBase::flexButtonCallbackHandler)
{
    setWidth(800);
    setHeight(450);

    box1.setPosition(0, 0, 800, 450);
    box1.setColor(touchgfx::Color::getColorFrom24BitRGB(65, 146, 226));

    boxWithBorder3.setPosition(10, 10, 780, 50);
    boxWithBorder3.setColor(touchgfx::Color::getColorFrom24BitRGB(244, 244, 244));
    boxWithBorder3.setBorderColor(touchgfx::Color::getColorFrom24BitRGB(197, 201, 204));
    boxWithBorder3.setBorderSize(1);

    boxWithBorder3_1.setPosition(10, 60, 780, 380);
    boxWithBorder3_1.setColor(touchgfx::Color::getColorFrom24BitRGB(248, 248, 248));
    boxWithBorder3_1.setBorderColor(touchgfx::Color::getColorFrom24BitRGB(197, 201, 204));
    boxWithBorder3_1.setBorderSize(1);

    boxWithBorder1.setPosition(24, 70, 125, 358);
    boxWithBorder1.setColor(touchgfx::Color::getColorFrom24BitRGB(233, 235, 240));
    boxWithBorder1.setBorderColor(touchgfx::Color::getColorFrom24BitRGB(197, 201, 204));
    boxWithBorder1.setBorderSize(2);

    BackBtn.setBoxWithBorderPosition(0, 0, 107, 62);
    BackBtn.setBorderSize(2);
    BackBtn.setBoxWithBorderColors(touchgfx::Color::getColorFrom24BitRGB(240, 119, 54), touchgfx::Color::getColorFrom24BitRGB(218, 107, 46), touchgfx::Color::getColorFrom24BitRGB(133, 47, 30), touchgfx::Color::getColorFrom24BitRGB(133, 47, 30));
    BackBtn.setText(TypedText(T_SINGLEUSEID451));
    BackBtn.setTextPosition(0, 19, 107, 62);
    BackBtn.setTextColors(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255), touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
    BackBtn.setPosition(33, 356, 107, 62);
    BackBtn.setAction(flexButtonCallback);

    TempParamSetBtn.setBoxWithBorderPosition(0, 0, 107, 62);
    TempParamSetBtn.setBorderSize(2);
    TempParamSetBtn.setBoxWithBorderColors(touchgfx::Color::getColorFrom24BitRGB(85, 163, 235), touchgfx::Color::getColorFrom24BitRGB(108, 197, 39), touchgfx::Color::getColorFrom24BitRGB(50, 92, 20), touchgfx::Color::getColorFrom24BitRGB(50, 92, 20));
    TempParamSetBtn.setText(TypedText(T_SINGLEUSEID447));
    TempParamSetBtn.setTextPosition(0, 19, 107, 62);
    TempParamSetBtn.setTextColors(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255), touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
    TempParamSetBtn.setPosition(33, 264, 107, 62);
    TempParamSetBtn.setAction(flexButtonCallback);

    TimerSetBtn.setBoxWithBorderPosition(0, 0, 107, 62);
    TimerSetBtn.setBorderSize(2);
    TimerSetBtn.setBoxWithBorderColors(touchgfx::Color::getColorFrom24BitRGB(85, 163, 235), touchgfx::Color::getColorFrom24BitRGB(108, 197, 39), touchgfx::Color::getColorFrom24BitRGB(34, 86, 136), touchgfx::Color::getColorFrom24BitRGB(50, 92, 20));
    TimerSetBtn.setText(TypedText(T_SINGLEUSEID449));
    TimerSetBtn.setTextPosition(0, 19, 107, 62);
    TimerSetBtn.setTextColors(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255), touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
    TimerSetBtn.setPosition(33, 203, 107, 62);

    SysSetBtn.setBoxWithBorderPosition(0, 0, 107, 62);
    SysSetBtn.setBorderSize(2);
    SysSetBtn.setBoxWithBorderColors(touchgfx::Color::getColorFrom24BitRGB(85, 163, 235), touchgfx::Color::getColorFrom24BitRGB(108, 197, 39), touchgfx::Color::getColorFrom24BitRGB(34, 86, 136), touchgfx::Color::getColorFrom24BitRGB(50, 92, 20));
    SysSetBtn.setText(TypedText(T_SINGLEUSEID450));
    SysSetBtn.setTextPosition(0, 19, 107, 62);
    SysSetBtn.setTextColors(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255), touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
    SysSetBtn.setPosition(33, 81, 107, 62);
    SysSetBtn.setAction(flexButtonCallback);

    OverParamBtn.setBoxWithBorderPosition(0, 0, 107, 62);
    OverParamBtn.setBorderSize(2);
    OverParamBtn.setBoxWithBorderColors(touchgfx::Color::getColorFrom24BitRGB(85, 163, 235), touchgfx::Color::getColorFrom24BitRGB(108, 197, 39), touchgfx::Color::getColorFrom24BitRGB(34, 86, 136), touchgfx::Color::getColorFrom24BitRGB(50, 92, 20));
    OverParamBtn.setText(TypedText(T_SINGLEUSEID448));
    OverParamBtn.setTextPosition(0, 19, 107, 62);
    OverParamBtn.setTextColors(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255), touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
    OverParamBtn.setPosition(33, 142, 107, 62);
    OverParamBtn.setAction(flexButtonCallback);

    textArea1_5.setXY(355, 18);
    textArea1_5.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
    textArea1_5.setLinespacing(0);
    textArea1_5.setTypedText(touchgfx::TypedText(T_SINGLEUSEID130));

    add(box1);
    add(boxWithBorder3);
    add(boxWithBorder3_1);
    add(boxWithBorder1);
    add(BackBtn);
    add(TempParamSetBtn);
    add(TimerSetBtn);
    add(SysSetBtn);
    add(OverParamBtn);
    add(textArea1_5);
}

void SetBackFrameContainBase::initialize()
{
	
}

void SetBackFrameContainBase::flexButtonCallbackHandler(const touchgfx::AbstractButtonContainer& src)
{
    if (&src == &BackBtn)
    {
        //BackBtnClick
        //When BackBtn clicked execute C++ code
        //Execute C++ code
        application().gotoMainScreenNoTransition();
    }
    else if (&src == &TempParamSetBtn)
    {
        //ParamBtnClick
        //When TempParamSetBtn clicked call virtual function
        //Call ParamBtnClick
        ParamBtnClick();
    }
    else if (&src == &SysSetBtn)
    {
        //SysBtnClick
        //When SysSetBtn clicked call virtual function
        //Call SysBtnClick
        SysBtnClick();
    }
    else if (&src == &OverParamBtn)
    {
        //OverParamBtnClick
        //When OverParamBtn clicked call virtual function
        //Call OverParamBtnClick
        OverParamBtnClick();
    }
}
