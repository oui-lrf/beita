#ifndef TIMESETCONTAIN_HPP
#define TIMESETCONTAIN_HPP

#include <gui_generated/containers/OverSetContain.hpp>
#include <gui/common/CustomKeyboard.hpp>

class OverSetContain : public OverSetContain
{
public:
    OverSetContain();
    virtual ~OverSetContain() {}
			
		virtual void initialize();
		/*
     * Custom Action Handlers
     */

		CustomKeyboard *keyboard;
		GenericCallback<bool>* useSMOCDrawingCallback; 
private:
	/**
     * Callback for when keys are pressed on the keyboard.
     */
    Callback<OverSetContain,uint32_t,Unicode::UnicodeChar *> okPressed;
	/**
     * Callback for the keyboard mode button.
     */
    Callback<OverSetContain> cancelPressed;
	/**
     * Callback handler for key presses.
     * @param keyChar The UnicodeChar for the key that was pressed.
     */
    void okPressedHandler(uint32_t keyId,Unicode::UnicodeChar * string); 
	/**
     * Callback handler for the mode button.
     */
    void cancelPressedHandler();
void DisplayParam(void);			
		
protected:
		void OpenKeyboard(uint32_t keyId);
};

#endif // TIMESETCONTAIN_HPP
