#ifndef PARAMSETCONTAIN_HPP
#define PARAMSETCONTAIN_HPP

#include <gui_generated/containers/ParamSetContainBase.hpp>
#include <gui/common/CustomKeyboard.hpp>
#include <gui/common/EnCustomKeyboard.hpp>

class ParamSetContain : public ParamSetContainBase
{
public:
    ParamSetContain();
    virtual ~ParamSetContain() {}

    virtual void initialize();
			
		/*
     * Custom Action Handlers
     */
		
    virtual void RemoteIpBtnClick();
    virtual void RemotePortBtnClick();
    virtual void LocalIpBtnClick();
    virtual void LocalPortBtnClick();
    virtual void LocalOKBtnClick();
    virtual void RemoteOKBtnClick();
		virtual void TestBtnClick();
		virtual void NameBtnClick();
    virtual void PassBtnClick();
    virtual void UserOkBtnClick();
			
void UpdataName(char *str);
void UpdataPass(char *str);
void UpdataRemoteIp(char *str);
void UpdataRemoteIpSta(bool sta);
void UpdataRemotePort(u16 value);
void UpdataLocalIp(char *str);
void UpdataLocalPort(u16 value);
	CustomKeyboard *keyboard;
	EnCustomKeyboard *enKeyboard;
	GenericCallback<bool>* useSMOCDrawingCallback; 
private:
	/**
     * Callback for when keys are pressed on the keyboard.
     */
    Callback<ParamSetContain,uint32_t,Unicode::UnicodeChar *> okPressed;
	/**
     * Callback for the keyboard mode button.
     */
    Callback<ParamSetContain> cancelPressed;

	  Callback<ParamSetContain,uint32_t,Unicode::UnicodeChar *> enOkPressed;
	/**
     * Callback for the keyboard mode button.
     */
    Callback<ParamSetContain> enCancelPressed;
	/**
     * Callback handler for key presses.
     * @param keyChar The UnicodeChar for the key that was pressed.
     */
    void okPressedHandler(uint32_t keyId,Unicode::UnicodeChar * string); 
	/**
     * Callback handler for the mode button.
     */
    void cancelPressedHandler();
    void enOkPressedHandler(uint32_t keyId,Unicode::UnicodeChar * string); 
	/**
     * Callback handler for the mode button.
     */
    void enCancelPressedHandler();
	 bool test;

	void DisplayParam(void);
protected:
	void OpenEnKeyboard(uint32_t keyId);
	void OpenKeyboard(uint32_t keyId);
};

#endif // PARAMSETCONTAIN_HPP
