#ifndef HEAD_HPP
#define HEAD_HPP

#include <gui_generated/containers/HeadBase.hpp>
#include "GuiInclude.h"

class Head : public HeadBase
{
public:
    Head();
    virtual ~Head() {}
	
    virtual void initialize();
	void UpdataTime();
	void UpdataNetSta(NET_STA sta);
	void UpdataNetSignal(u16 signal);
	void UpdataNetType(NET_TYPE type);
protected:
};

#endif // HEAD_HPP
