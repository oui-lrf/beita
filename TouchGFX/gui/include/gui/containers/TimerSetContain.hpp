#ifndef TIMERSETCONTAIN_HPP
#define TIMERSETCONTAIN_HPP

#include <gui_generated/containers/TimerSetContainBase.hpp>
#include <gui/common/CustomKeyboard.hpp>
class TimerSetContain : public TimerSetContainBase
{
public:
    TimerSetContain();
    virtual ~TimerSetContain() {}
	
    virtual void initialize();
	 /*
     * Custom Action Handlers
     */
    virtual void StartTimeHourBtnClick1();

    virtual void StartTimeHourBtnClick2();

    virtual void StartTimeHourBtnClick3();

    virtual void StartTimeHourBtnClick4();

    virtual void StartTimeMinBtnClick1();

    virtual void StartTimeMinBtnClick2();

    virtual void StartTimeMinBtnClick3();

    virtual void StartTimeMinBtnClick4();

    virtual void StopTimeHourBtnClick1();

    virtual void StopTimeHourBtnClick2();

    virtual void StopTimeHourBtnClick3();

    virtual void StopTimeHourBtnClick4();

    virtual void StopTimeMinBtnClick1();

    virtual void StopTimeMinBtnClick2();

    virtual void StopTimeMinBtnClick3();

    virtual void StopTimeMinBtnClick4();

    virtual void TimeTempBtnClick1();

    virtual void TimeTempBtnClick2();

    virtual void TimeTempBtnClick3();

    virtual void TimeTempBtnClick4();
	
    virtual void TimeSelectBtnClick1();
    virtual void TimeSelectBtnClick2();
    virtual void TimeSelectBtnClick3();
    virtual void TimeSelectBtnClick4();
	GenericCallback<bool>* useSMOCDrawingCallback; 
	CustomKeyboard *keyboard;
private:
	/**
     * Callback for when keys are pressed on the keyboard.
     */
    Callback<TimerSetContain,uint32_t,Unicode::UnicodeChar *> okPressed;
	/**
     * Callback for the keyboard mode button.
     */
    Callback<TimerSetContain> cancelPressed;
	/**
     * Callback handler for key presses.
     * @param keyChar The UnicodeChar for the key that was pressed.
     */
    void okPressedHandler(uint32_t keyId,Unicode::UnicodeChar * string); 
	/**
     * Callback handler for the mode button.
     */
    void cancelPressedHandler();
	void DisplayParam(void);
protected:
	void OpenKeyboard(uint32_t keyId);
	
	
};

#endif // TIMERSETCONTAIN_HPP
