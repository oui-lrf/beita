#ifndef DATARECORD_HPP
#define DATARECORD_HPP

#include <gui_generated/containers/DataRecordBase.hpp>
#include "FileRecord.h"
#include "TimePort.h"

class DataRecord : public DataRecordBase
{
public:
    DataRecord();
    virtual ~DataRecord() {}
			
		void AddRecord(FileRecord *record)
		{
			Unicode::snprintf(IdTextAreaBuffer, IDTEXTAREA_SIZE, "%d",record->id);
			//IdTextArea.invalidate();
			
			char value[VALUETEXTAREA_SIZE]={0};

			switch(record->valueType)
			{
				case VALUE_U32:
					snprintf(value,VALUETEXTAREA_SIZE,"%d%s",record->value,record->unit);//
				break;
				case VALUE_FLOAT:
					snprintf(value,VALUETEXTAREA_SIZE,"%.2f%s",*(float *)&(record->value),record->unit);//
				break;
			}
			GbkToUnicode((unsigned char *)value,valueTextAreaBuffer,VALUETEXTAREA_SIZE,1);
			//valueTextArea.invalidate();
			
			//memset(NameTextAreaBuffer,0,NAMETEXTAREA_SIZE*2);
			GbkToUnicode((unsigned char *)(record->name),NameTextAreaBuffer,NAMETEXTAREA_SIZE,1);
			//NameTextArea.invalidate();
			
			char time[TIMEAREA_SIZE]={0};
			IntToTimeStr(time,TIMEAREA_SIZE,record->time);
			Unicode::fromUTF8((unsigned char *)time,TimeAreaBuffer,TIMEAREA_SIZE);
			//TimeArea.invalidate();
		}
		
		void DeleteRecord(void)
		{
			
			IdTextAreaBuffer[0]=0;
			//IdTextArea.invalidate();
			
			valueTextAreaBuffer[0]=0;
			//valueTextArea.invalidate();
			
			NameTextAreaBuffer[0]=0;
			//NameTextArea.invalidate();
			
			TimeAreaBuffer[0]=0;
			//TimeArea.invalidate();
		}

    virtual void initialize();
protected:
};

#endif // DATARECORD_HPP
