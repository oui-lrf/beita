#ifndef SETBACKFRAMECONTAIN_HPP
#define SETBACKFRAMECONTAIN_HPP

#include <gui_generated/containers/SetBackFrameContainBase.hpp>

typedef enum{
	PARAM_BTN,
	TIME_PARAM_BTN,
	TIMER_BTN,
	SYS_BTN,
}TOP_BTN;

class SetBackFrameContain : public SetBackFrameContainBase
{
public:
    SetBackFrameContain();
    virtual ~SetBackFrameContain() {}

    virtual void initialize();
	/*
     * Custom Action Handlers
     */
    virtual void TimerBtnClick();

    virtual void ParamBtnClick();

    virtual void SysBtnClick();

    virtual void OverParamBtnClick();
			
			

	GenericCallback<uint8_t>* btnClickCallback; 
	uint8_t topBtn;
protected:
	
	void OpenTopBtn(uint8_t topBtn);
};

#endif // SETBACKFRAMECONTAIN_HPP
