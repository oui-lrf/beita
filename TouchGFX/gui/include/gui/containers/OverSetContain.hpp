#ifndef OVERSETCONTAIN_HPP
#define OVERSETCONTAIN_HPP

#include <gui_generated/containers/OverSetContainBase.hpp>
#include <gui/common/CustomKeyboard.hpp>

class OverSetContain : public OverSetContainBase
{
public:
    OverSetContain();
    virtual ~OverSetContain() {}

    virtual void initialize();
		CustomKeyboard *keyboard;
		GenericCallback<bool>* useSMOCDrawingCallback; 
			
		virtual void NmhcBtnClick1()
    {
        // Override and implement this function in OverSetContain
			OpenKeyboard((uint32_t )&NmhcBtn1);
    }

    virtual void NmhcBtnClick2()
    {
        // Override and implement this function in OverSetContain
			OpenKeyboard((uint32_t )&NmhcBtn2);
    }

    virtual void NmhcBtnClick3()
    {
        // Override and implement this function in OverSetContain
			OpenKeyboard((uint32_t )&NmhcBtn3);
    }

    virtual void PmBtnClick1()
    {
        // Override and implement this function in OverSetContain
			OpenKeyboard((uint32_t )&PmBtn1);
    }

    virtual void PmBtnClick2()
    {
        // Override and implement this function in OverSetContain
			OpenKeyboard((uint32_t )&PmBtn2);
    }

    virtual void PmBtnClick3()
    {
        // Override and implement this function in OverSetContain
			OpenKeyboard((uint32_t )&PmBtn3);
    }

    virtual void SmokeBtnClick1()
    {
        // Override and implement this function in OverSetContain
			OpenKeyboard((uint32_t )&SmokeBtn1);
    }

    virtual void SmokeBtnClick2()
    {
        // Override and implement this function in OverSetContain
			OpenKeyboard((uint32_t )&SmokeBtn2);
    }

    virtual void SmokeBtnClick3()
    {
        // Override and implement this function in OverSetContain
			OpenKeyboard((uint32_t )&SmokeBtn3);
    }
private:
	/**
     * Callback for when keys are pressed on the keyboard.
     */
    Callback<OverSetContain,uint32_t,Unicode::UnicodeChar *> okPressed;
	/**
     * Callback for the keyboard mode button.
     */
    Callback<OverSetContain> cancelPressed;
	/**
     * Callback handler for key presses.
     * @param keyChar The UnicodeChar for the key that was pressed.
     */
    void okPressedHandler(uint32_t keyId,Unicode::UnicodeChar * string); 
	/**
     * Callback handler for the mode button.
     */
    void cancelPressedHandler();
void DisplayParam(void);			
		
protected:
		void OpenKeyboard(uint32_t keyId);
};

#endif // OVERPARAMSETCONTAIN_HPP
