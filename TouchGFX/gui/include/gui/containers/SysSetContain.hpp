#ifndef SYSSETCONTAIN_HPP
#define SYSSETCONTAIN_HPP

#include <gui_generated/containers/SysSetContainBase.hpp>
#include <gui/common/CustomKeyboard.hpp>
#include <gui/common/EnCustomKeyboard.hpp>
#include <touchgfx/widgets/QRCode.hpp>
#include <touchgfx/widgets/QRCodeWidget.hpp>
class SysSetContain : public SysSetContainBase
{
public:
    SysSetContain();
    virtual ~SysSetContain();

    virtual void initialize();
	/*
     * Custom Action Handlers
     */
    virtual void YearBtnClick();
    virtual void MonthBtnClick();
    virtual void DayBtnClick();
    virtual void HourBtnClick();
    virtual void MinBtnClick();
    virtual void SecBtnClick();
    virtual void RecoveryBtnClick();
		virtual void RadioButtonWifiSelect();
		virtual void IpBtnClick();
    virtual void PortBtnClick();
		virtual void UserCodeBtnClick();
    virtual void RadioButtonGprsSelect();

    virtual void RadioButtonCloseSelect();

    virtual void RadioButtonWifiDeSelect();

    virtual void RadioButtonGprsDeSelect();

    virtual void RadioButtonCloseDeSelect();
		
		
		virtual void WifiConfigBtnClick();
    virtual void WifiNameBtnClick();
    virtual void WifiPassBtnClick();
    virtual void ScreenBtnClick();
    virtual void ScreenSwBtnClick();
		
		
			
	GenericCallback<bool>* useSMOCDrawingCallback; 
	GenericCallback<uint8_t>* netTypeChangeCallback;
	CustomKeyboard *keyboard;
	EnCustomKeyboard *enKeyboard;
	QRCodeWidget qrCode;
	void DisplayNetType(u8 type);
	void UpdataUserCode(char *code);
	void UpdataIccid(char *data);
	void UpdataUserIp(char *data);
	void UpdataUserPort(u16 data);
private:
	/**
     * Callback for when keys are pressed on the keyboard.
     */
    Callback<SysSetContain,uint32_t,Unicode::UnicodeChar *> okPressed;
	/**
     * Callback for the keyboard mode button.
     */
    Callback<SysSetContain> cancelPressed;
	  Callback<SysSetContain,uint32_t,Unicode::UnicodeChar *> enOkPressed;
	/**
     * Callback for the keyboard mode button.
     */
    Callback<SysSetContain> enCancelPressed;
	/**
     * Callback handler for key presses.
     * @param keyChar The UnicodeChar for the key that was pressed.
     */
    void okPressedHandler(uint32_t keyId,Unicode::UnicodeChar * string); 
	/**
     * Callback handler for the mode button.
     */
    void cancelPressedHandler();

    void enOkPressedHandler(uint32_t keyId,Unicode::UnicodeChar * string); 
	/**
     * Callback handler for the mode button.
     */
    void enCancelPressedHandler();
		
	void DisplayParam(void);
	
protected:
	void OpenKeyboard(uint32_t keyId);
	void OpenEnKeyboard(uint32_t keyId);
	u8 selectActive;
};

#endif // SYSSETCONTAIN_HPP
