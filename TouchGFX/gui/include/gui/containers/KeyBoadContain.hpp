#ifndef KEYBOADCONTAIN_HPP
#define KEYBOADCONTAIN_HPP

#include <gui_generated/containers/KeyBoadContainBase.hpp>

class KeyBoadContain : public KeyBoadContainBase
{
public:
    KeyBoadContain();
    virtual ~KeyBoadContain() {}
    virtual void initialize();
	virtual void KeyClose(void);
	void SetCloseCall(void);
protected:
};

#endif // KEYBOADCONTAIN_HPP
