#ifndef DATA_PRESENTER_HPP
#define DATA_PRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>

using namespace touchgfx;

class DataView;

class DataPresenter : public Presenter, public ModelListener
{
public:
    DataPresenter(DataView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();

    virtual ~DataPresenter() {};
		virtual void WidgetStateChanged(uint16_t widgetId);
private:
    DataPresenter();

    DataView& view;
};


#endif // DATA_PRESENTER_HPP
