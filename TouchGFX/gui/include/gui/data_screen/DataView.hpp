#ifndef DATA_VIEW_HPP
#define DATA_VIEW_HPP

#include <gui_generated/data_screen/DataViewBase.hpp>
#include <gui/data_screen/DataPresenter.hpp>
#include "FileRecord.h"
#include <gui/common/CustomKeyboard.hpp>
#include <gui/containers/DataRecord.hpp>

typedef enum{
	DATA_HIS,
	DATA_WARING,
}DATA_TYPE; 

class DataView : public DataViewBase
{
public:
    DataView();
    virtual ~DataView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
		virtual void RecordScrollList1UpdateItem(DataRecord& item, int16_t itemIndex);
			
    virtual void DataRecordBtnSelect();
    virtual void WaringRecordBtnSelect();
    virtual void YearBtnClick();
    virtual void MonthBtnClick();
    virtual void DayBtnClick();
    virtual void FindBtnClick();
    virtual void ClearBtnClick();
		virtual void NextBtnClick();
    virtual void LastBtnClick();
		virtual void PageBtnClick();
		virtual void OutBtnClick();
		
		void UpdataNetSta(NET_STA sta);
		void UpdataNetSignal(u16 signal);
		void UpdataNetType(NET_TYPE type);
		void UpdataTime()
		{
			 head.UpdataTime();
		}
protected:
	CustomKeyboard keyboard;
	DATA_TYPE showDataType;
	void okPressedHandler(uint32_t keyId,Unicode::UnicodeChar * string); 
  void cancelPressedHandler();
	void OpenKeyboard(uint32_t keyId);
	Callback<DataView,uint32_t,Unicode::UnicodeChar *> okPressed;
	Callback<DataView> cancelPressed;

	char folder[30];
	char fileName[30];
	//void UpdataRecordList(void);
	void UpdataRecordListItem(void);
  static const int numberOfPageItem = 7;
	u16  pageIndex;
	
};

#endif // DATA_VIEW_HPP
