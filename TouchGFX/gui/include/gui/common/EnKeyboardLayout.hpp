#ifndef ENKEYBOARD_LAYOUT
#define ENKEYBOARD_LAYOUT

#include <touchgfx/widgets/EnKeyboard.hpp>
#include <touchgfx/hal/Types.hpp>
#include <fonts/ApplicationFontProvider.hpp>
#include "BitmapDatabase.hpp"

using namespace touchgfx;
 namespace EnKeyBoard{
 
 
/**
 * Array specifying the keys used in the CustomKeyboard.
 */
static const EnKeyboard::Key keyArray[30] =
{
	
             { 1, Rect(10, 91, 39, 60), 0},
        { 2, Rect(10 + 42, 91, 39, 60), 0},
    { 3, Rect(10 + 42 * 2, 91, 39, 60), 0},
    { 4, Rect(10 + 42 * 3, 91, 39, 60), 0},
    { 5, Rect(10 + 42 * 4, 91, 39, 60), 0},
    { 6, Rect(10 + 42 * 5, 91, 39, 60), 0},
    { 7, Rect(10 + 42 * 6, 91, 39, 60), 0},
    { 8, Rect(10 + 42 * 7, 91, 39, 60), 0},
    { 9, Rect(10 + 42 * 8, 91, 39, 60), 0},
    {10, Rect(10 + 42 * 9, 91, 39, 60), 0},
   {11, Rect(10 + 42 * 10, 91, 39, 60), 0},

             {12, Rect(10, 157, 39, 60), 0},
        {13, Rect(10 + 42, 157, 39, 60), 0},
    {14, Rect(10 + 42 * 2, 157, 39, 60), 0},
    {15, Rect(10 + 42 * 3, 157, 39, 60), 0},
    {16, Rect(10 + 42 * 4, 157, 39, 60), 0},
    {17, Rect(10 + 42 * 5, 157, 39, 60), 0},
    {18, Rect(10 + 42 * 6, 157, 39, 60), 0},
    {19, Rect(10 + 42 * 7, 157, 39, 60), 0},
    {20, Rect(10 + 42 * 8, 157, 39, 60), 0},
    {21, Rect(10 + 42 * 9, 157, 39, 60), 0},
   {22, Rect(10 + 42 * 10, 157, 39, 60), 0},

             {23, Rect(94, 223, 39, 60), 0},
        {24, Rect(94 + 42, 223, 39, 60), 0},
    {25, Rect(94 + 42 * 2, 223, 39, 60), 0},
    {26, Rect(94 + 42 * 3, 223, 39, 60), 0},
    {27, Rect(94 + 42 * 4, 223, 39, 60), 0},
    {28, Rect(94 + 42 * 5, 223, 39, 60), 0},
    {29, Rect(94 + 42 * 6, 223, 39, 60), 0},
            {30, Rect(93, 290, 295, 60), 0}
};

/**{Rect(258, 149, 56, 40), 0, BITMAP_KEYBOARD_KEY_DELETE_HIGHLIGHTED_ID},  // backspace
 * Callback areas for the special buttons on the CustomKeyboard.
 */
static EnKeyboard::CallbackArea callbackAreas[5] =
{
    {Rect(7, 223, 84, 60), 0, 0},     // caps-lock
    {Rect(391, 224, 80, 60), 0, 0},  // backspace
    {Rect(7, 289, 84, 60), 0, 0},       // mode
		{Rect(391, 290, 80, 60), 0, 0}, 		//�س�
		{Rect(420, 16, 50, 60), 0, 0}, 							//cancel
		    
};

/**
 * The layout for the CustomKeyboard.
 */
static const EnKeyboard::Layout layout =
{
    0,
    keyArray,
    30,
    callbackAreas,
    5,
		Rect(10, 20, 400, 60),
    TypedText(T_ENTEREDTEXT),
#if !defined(USE_BPP) || USE_BPP==16
    0xFFFF,
#elif USE_BPP==24
    0xFFFFFF,
#elif USE_BPP==4
    0xF,
#elif USE_BPP==2
    0x3,
#else
#error Unknown USE_BPP
#endif
	Typography::ROB32,
    0
};
}
#endif
