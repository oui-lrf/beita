#ifndef ENKEYBOARD_WIDGET_HPP
#define ENKEYBOARD_WIDGET_HPP

#include <touchgfx/widgets/Widget.hpp>
#include <touchgfx/hal/Types.hpp>
#include <touchgfx/widgets/BoxWithBorder.hpp>
#include <touchgfx/widgets/Widget.hpp>
#include <touchgfx/Bitmap.hpp>
#include <touchgfx/Color.hpp>

using namespace touchgfx;
/**
* @class QRCodeWidget
* 
* @brief A widget for displaying a QRCode
* 
*        The widget will be drawn in black and white.
*        The values of the supplied QRCode will determine the appearance
*        The widget can be scaled in order to display the qr code in an appropriate size
*
*        @sa touchgfx::Widget
*/
class KeyBackWidget : public touchgfx::Widget
{
public:
    /**
     * @fn QRCodeWidget::QRCodeWidget();
     *
     * @brief Default constructor
     */
    KeyBackWidget();
    virtual void draw(const touchgfx::Rect& invalidatedArea) const;
    virtual Rect getSolidRect() const;
    void setColor(colortype color)
    {
        this->color = color;
    }
		

    /**
     * @fn inline colortype BoxWithBorder::getColor() const
     *
     * @brief Gets the color.
     *
     * @return The color.
     */
    inline colortype getColor() const
    {
        return color;
    }

    /**
     * @fn void BoxWithBorder::setBorderColor(colortype color)
     *
     * @brief Sets border color.
     *
     * @param color The color.
     */
    void setBorderColor(colortype color)
    {
        this->borderColor = color;
    }

    /**
     * @fn inline colortype BoxWithBorder::getBorderColor() const
     *
     * @brief Gets border color.
     *
     * @return The border color.
     */
    inline colortype getBorderColor() const
    {
        return borderColor;
    }

    /**
     * @fn void BoxWithBorder::setBorderSize(uint8_t size)
     *
     * @brief Sets border size.
     *
     * @param size The size.
     */
    void setBorderSize(uint8_t size)
    {
        borderSize = size;
    }

    /**
     * @fn inline uint8_t BoxWithBorder::getBorderSize() const
     *
     * @brief Gets border size.
     *
     * @return The border size.
     */
    inline uint8_t getBorderSize() const
    {
        return borderSize;
    }

    /**
     * @fn void BoxWithBorder::setAlpha(uint8_t alpha)
     *
     * @brief Sets an alpha.
     *
     * @param alpha The alpha.
     */
    void setAlpha(uint8_t alpha)
    {
        this->alpha = alpha;
    }

    /**
     * @fn inline uint8_t BoxWithBorder::getAlpha() const
     *
     * @brief Gets the alpha.
     *
     * @return The alpha.
     */
    inline uint8_t getAlpha() const
    {
        return alpha;
    }
		void *keyArray;
		uint8_t numberOfKeys;
		uint8_t fontHeight;
		uint8_t       numberOfCallbackAreas;
		 void* callbackAreaArray;
		void SetKeyArray(void *array,uint8_t num,uint8_t fontH)
		{
			keyArray = array;
			numberOfKeys = num;
			fontHeight = fontH;
		}
		void SetCallBackArray(void *array,uint8_t num)
		{
			callbackAreaArray = array;
			numberOfCallbackAreas = num;
		}
		Rect textRect;
		
		void SetTextAreaRect(Rect rect)
		{
			textRect = rect;
		}
		
private:
protected:
	uint8_t     alpha;         ///< The alpha
	colortype   color;         ///< The color
	colortype   borderColor;   ///< The border color
	uint8_t     borderSize;    ///< Size of the border

};
#endif
