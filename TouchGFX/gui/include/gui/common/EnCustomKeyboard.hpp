#ifndef ENTGFXKEYBOARD_HPP_
#define ENTGFXKEYBOARD_HPP_


#include <touchgfx/containers/Container.hpp>
#include <touchgfx/widgets/Box.hpp>
#include <texts/TextKeysAndLanguages.hpp>
#include <gui/common/EnKeyboardLayout.hpp>
#include <fonts/ApplicationFontProvider.hpp>
#include <gui/common/EnKeyboardKeyMapping.hpp>
#include <touchgfx/widgets/EnKeyboard.hpp>
using namespace touchgfx;
using namespace EnKeyBoard;
namespace EnKeyBoard{
/**
 * An alphanumeric keyboard with backspace, space-bar and delete functionality.
 * Keyboard mappings for the keyboard are defined in gui/include/gui/common/KeyboardKeyMapping.hpp.
 * Keyboard layout for the keyboard is defined in gui/include/gui/common/KeyboardLayout.hpp.
 * Please note that the ApplicationFontProvider is initialized and set with the FontManager in main.cpp
 * The fonts and the characters used by the keyboard have to be defined in the assets/texts/texts.xlsx file.
 */
class EnCustomKeyboard : public Container
{
public:
    EnCustomKeyboard();
    virtual ~EnCustomKeyboard() { }

    /*
     * Override setTouchable to also affect the keyboard object inside this
     * EnCustomKeyboard.
     */
    void setTouchable(bool touch);
		GenericCallback<uint32_t,Unicode::UnicodeChar *>* okCallback; 
		GenericCallback<>* cancelCallback; 
			uint32_t keyId;
	void ClearKeyBoard();
private:
    /*
     * The size of the buffer that is used by the keyboard.
     * The size determines how much text the keyboard can contain in its textfield.
     */
    static const uint8_t BUFFER_SIZE = 25;

    /**
     * The keyboard which this EnCustomKeyboard wraps.
     */
    EnKeyboard keyboard;

    /**
     * The buffer used by the keyboard for text input.
     */
    Unicode::UnicodeChar buffer[BUFFER_SIZE];

    /**
     * Used to display text on top of the button for changing keyboard mode.
     */
    TextArea modeBtnTextArea;
		TextArea enterBtnTextArea;
		TextArea closeTextArea;
		TextArea capsTextArea;
		TextArea backSpaceTextArea;

    /**
     * Callback for the capslock button.
     */
    Callback<EnCustomKeyboard> capslockPressed;

    /**
     * Callback for the backspace button.
     */
    Callback<EnCustomKeyboard> backspacePressed;

    /**
     * Callback for the keyboard mode button.
     */
    Callback<EnCustomKeyboard> modePressed;
		Callback<EnCustomKeyboard> cancelPressed;
		 Callback<EnCustomKeyboard> enterPressed;

    /**
     * Callback for when keys are pressed on the keyboard.
     */
    Callback<EnCustomKeyboard, Unicode::UnicodeChar> keyPressed;

    /**
     * True if the keyboard should show alpha keys, false for numeric.
     */
    bool alphaKeys;

    /**
     * True if the keyboard should show upper-case keys.
     */
    bool uppercaseKeys;

    /**
     * True if the input position in the text field, and hence the buffer, is at the beginning.
     */
    bool firstCharacterEntry;

    /*
     * Sets the correct key mappings of the keyboard according to alpha/numeric and upper-case/lower-case.
     */
    void setKeyMappingList();

    /**
     * Callback handler for the backspace button.
     */
    void backspacePressedHandler();

    /**
     * Callback handler for the caps-lock button.
     */
    void capslockPressedHandler();

    /**
     * Callback handler for the mode button.
     */
    void modePressedHandler();
		void cancelPressedHandler();
		void enterPressedHandler();

    /**
     * Callback handler for key presses.
     * @param keyChar The UnicodeChar for the key that was pressed.
     */
    void keyPressedhandler(Unicode::UnicodeChar keyChar);
};
}
#endif /* ENTGFXKEYBOARD_HPP_ */
