#ifndef MAIN_VIEW_HPP
#define MAIN_VIEW_HPP

#include <gui_generated/main_screen/MainViewBase.hpp>
#include <gui/main_screen/MainPresenter.hpp>
#include "GuiInclude.h"


class MainView : public MainViewBase
{
public:
    MainView();
	virtual ~MainView() {}
	virtual void setupScreen();
	virtual void tearDownScreen();
	
	void UpdataNetSta(NET_STA sta);
	void UpdataNetSignal(u16 signal);
	void UpdataNetType(NET_TYPE type);
		void UpdataTime()
		{
			 head.UpdataTime();
		}
	  void UpdataVolText1(u32 value)
		{
			Unicode::snprintfFloat(VolText1Buffer,VOLTEXT1_SIZE, "%.2f",(*(float *)&value)/10.0f);
			VolText1.invalidate();	
		}
    void UpdataVolText2(u32 value)
		{
			Unicode::snprintfFloat(VolText2Buffer, VOLTEXT2_SIZE, "%.2f",(*(float *)&value)/10.0f);
			VolText2.invalidate();
		}
    void UpdataVolText3(u32 value)
		{
			Unicode::snprintfFloat(VolText3Buffer, VOLTEXT3_SIZE, "%.2f",(*(float *)&value)/10.0f);
			VolText3.invalidate();		
		}
   
		void UpdataCurText1(u32 value)
		{
			Unicode::snprintfFloat(CurText1Buffer,CURTEXT1_SIZE, "%.2f",(*(float *)&value)/1000.0f*120.0f);
			CurText1.invalidate();	
		}
    void UpdataCurText2(u32 value)
		{
			Unicode::snprintfFloat(CurText2Buffer, CURTEXT2_SIZE, "%.2f",(*(float *)&value)/1000.0f*120.0f);
			CurText2.invalidate();
		}
    void UpdataCurText3(u32 value)
		{
			Unicode::snprintfFloat(CurText3Buffer, CURTEXT3_SIZE, "%.2f",(*(float *)&value)/1000.0f*120.0f);
			CurText3.invalidate();		
		}
		
    void UpdataAllConsumeText(u32 value)
		{
			Unicode::snprintfFloat(AllConsumeTextBuffer, ALLCONSUMETEXT_SIZE, "%.2f",*(float *)&value*120.0f);
			AllConsumeText.invalidate();			
			
		}
    void UpdataMonthConsumeText(u32 value)
		{
			Unicode::snprintfFloat(MonthConsumeTextBuffer, MONTHCONSUMETEXT_SIZE, "%.2f",*(float *)&value);
			MonthConsumeText.invalidate();			
		}
    void UpdataNowPowText(u32 value)
		{
			Unicode::snprintfFloat(NowPowTextBuffer, NOWPOWTEXT_SIZE, "%.2f",(*(float *)&value)/10000.0f*120.0f);
			NowPowText.invalidate();				
		}

protected:
    uint16_t tickCounter;

	virtual void ClearWaringBtnClick();
	virtual void RecoarBtnClick();
	virtual void RunBtnClick();
	virtual void TimerBtnClick();
};

#endif // MAIN_VIEW_HPP
