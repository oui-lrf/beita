#ifndef MAIN_PRESENTER_HPP
#define MAIN_PRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>


using namespace touchgfx;

class MainView;

class MainPresenter : public Presenter, public ModelListener
{
public:
    MainPresenter(MainView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();

    virtual ~MainPresenter() {};
		virtual void WidgetStateChanged(uint16_t widgetId);
	//from view 
    void SetSw(uint8_t bit,bool new_state);
		void DeviceRunControl(u8 value)
		{
			model->RunStaControl(value);
		}
		void DeviceRunModeControl(u8 value)
		{
			model->RunModeControl(value);
		}
private:
    MainPresenter();

    MainView& view;
};


#endif // MAIN_PRESENTER_HPP
