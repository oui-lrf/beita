#ifndef SERVER_VIEW_HPP
#define SERVER_VIEW_HPP

#include <gui_generated/server_screen/ServerViewBase.hpp>
#include <gui/server_screen/ServerPresenter.hpp>

class ServerView : public ServerViewBase
{
public:
    ServerView();
    virtual ~ServerView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // SERVER_VIEW_HPP
