#ifndef SERVER_PRESENTER_HPP
#define SERVER_PRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>

using namespace touchgfx;

class ServerView;

class ServerPresenter : public Presenter, public ModelListener
{
public:
    ServerPresenter(ServerView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();

    virtual ~ServerPresenter() {};

private:
    ServerPresenter();

    ServerView& view;
};


#endif // SERVER_PRESENTER_HPP
