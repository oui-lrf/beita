#ifndef MODEL_HPP
#define MODEL_HPP

#include <touchgfx/Utils.hpp>
#include "Water.h"
#include "DeviceRunPort.h"
#include "GuiPort.h"
#include "GuiInclude.h"

class ModelListener;

/**
 * The Model class defines the data model in the model-view-presenter paradigm.
 * The Model is a singular object used across all presenters. The currently active
 * presenter will have a pointer to the Model through deriving from ModelListener.
 *
 * The Model will typically contain UI state information that must be kept alive
 * through screen transitions. It also usually provides the interface to the rest
 * of the system (the backend). As such, the Model can receive events and data from
 * the backend and inform the current presenter of such events through the modelListener
 * pointer, which is automatically configured to point to the current presenter.
 * Conversely, the current presenter can trigger events in the backend through the Model.
 */
class Model
{
public:
    Model();

    /**
     * Sets the modelListener to point to the currently active presenter. Called automatically
     * when switching screen.
     */
    void bind(ModelListener* listener)
    {
        modelListener = listener;
    }

    /**
     * This function will be called automatically every frame. Can be used to e.g. sample hardware
     * peripherals or read events from the surrounding system and inject events to the GUI through
     * the ModelListener interface.
     */
    void tick();
	 
		
	u32 GetNowPow(){return nowPow;}
	u32 GetMonthConsume(){return monthConsume;}
	u32 GetAllConsume(){return allConsume;}

	u32 getVolValue1(){return volValue1;}
	u32 getVolValue2(){return volValue2;}
	u32 getVolValue3(){return volValue3;}
	
	u32 getCurValue1(){return curValue1;}
	u32 getCurValue2(){return curValue2;}
	u32 getCurValue3(){return curValue3;}
	
	bool GetSw1(){return sw1;}
	bool GetSw2(){return sw2;}
	bool GetSw3(){return sw3;}
	
	bool GetAlign1(){return align1;}
	bool GetAlign2(){return align2;}
	bool GetAlign3(){return align3;}
	
	DEVICE_WARING_STA getWaringSta(){return waringSta;};
	DEVICE_RUN_STA  getSysRunSta(){return sysRunSta;};

	NET_STA getNetSta(){return netSta;};
	NET_TYPE getNetType(){return netType;};
	void UiSetNetType(u8 type);
	u16 getNetSignal(){return netSignal;};

	void SendOutSwMsg(uint8_t bit,bool state);
	void RunStaControl(u8 value);
	void RunModeControl(u8 value);
u16 GetLocalPort(){return localPort;};
u16 GetRemotePort(){return remotePort;};

char * GetLocalIp(){return localIp;};
char * GetRemoteIp(){return remoteIp;};
bool GetRemoteIpSta(){return remoteIpSta;};
char * GetName(){return name;};
char * GetPass(){return pass;};

	char* GetUserCode(){return userCode;}
	char* GetUserIp(){return userIp;}
	u16 GetUserPort(){return userPort;}
	char *GetIccid(){return iccid;};	
protected:
    /**
     * Pointer to the currently active presenter.
     */
  ModelListener* modelListener;
	uint16_t tk;
	u16 localPort;
	u16 remotePort;
	char localIp[20];
	char remoteIp[20];
	char name[20];
	char pass[20];

	bool remoteIpSta;
	bool sw1;
	bool sw2;
	bool sw3;

	bool align1;
	bool align2;
	bool align3;

	u32 nowPow;
	u32 monthConsume;
	u32 allConsume;

	
	u32 volValue1;
	u32 volValue2;
	u32 volValue3;
	
	u32 curValue1;
	u32 curValue2;
	u32 curValue3;
	
	
	DEVICE_RUN_STA  sysRunSta;
	NET_STA netSta;
	NET_TYPE netType;
	u16 netSignal;
	DEVICE_WARING_STA waringSta;
	u8 SetValueById(uint16_t widgetId,u32 data);
	
	char userCode[USER_CODE_LEN+1];
	char userIp[MAX_IP_LEN+1];
	u16 userPort;
	char iccid[ICCID_LEN+1];
//	float pumpOpenRate;	
};

#endif /* MODEL_HPP */
