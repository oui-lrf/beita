#ifndef SET_VIEW_HPP
#define SET_VIEW_HPP

#include <gui_generated/set_screen/SetViewBase.hpp>
#include <gui/set_screen/SetPresenter.hpp>
#include <gui/containers/SetBackFrameContain.hpp>
#include <gui/containers/TimerSetContain.hpp>
#include <gui/containers/ParamSetContain.hpp>
#include <gui/containers/SysSetContain.hpp>
#include <gui/containers/OverSetContain.hpp>

#include "GuiPort.h"
class SetView : public SetViewBase
{
public:
    SetView();
    virtual ~SetView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
	 /*
     * Custom Action Handlers
     */
	void UpdataNetSta(NET_STA sta);
	void UpdataNetSignal(u16 signal);
	void UpdataNetType(NET_TYPE type);
	void UpdataValueById(u16 id,int value);
			
void UpdataRemoteIp(char *str){paramSetContain.UpdataRemoteIp(str);}
void UpdataRemotePort(u16 value){paramSetContain.UpdataRemotePort(value);}
void UpdataLocalIp(char *str){paramSetContain.UpdataLocalIp(str);}
void UpdataLocalPort(u16 value){paramSetContain.UpdataLocalPort(value);}

void UpdataName(char *str){paramSetContain.UpdataName(str);}
void UpdataPass(char *str){paramSetContain.UpdataPass(str);}
void UpdataRemoteIpSta(bool sta){paramSetContain.UpdataRemoteIpSta(sta);};

void UpdataUserCode(char *code){sysSetContain.UpdataUserCode(code);};
void UpdataIccid(char *data){sysSetContain.UpdataIccid(data);};
void UpdataUserIp(char *data){sysSetContain.UpdataUserIp(data);};
void UpdataUserPort(u16 data){sysSetContain.UpdataUserPort(data);};
	
	CustomKeyboard keyboard;
	EnCustomKeyboard enKeyboard;
	void UpdataTime()
	{
		 head.UpdataTime();
	}
private:
	 uint16_t tickCounter;
	 Callback<SetView, bool> childContainUseSMOCDrawingCall;
	 Callback<SetView, uint8_t>framBtnClick;
	 Callback<SetView, uint8_t>netTypeChangeCall;
		Callback<SetView,uint32_t,Unicode::UnicodeChar *> okPressed;
		Callback<SetView> cancelPressed;
	  void okPressedHandler(uint32_t keyId,Unicode::UnicodeChar * string); 
    void cancelPressedHandler();
protected:
	void childContainUseSMOCDrawinghandler(bool sta);
	void framBtnClickHandler(uint8_t topBtn);
	void NetTypeChangeCallHandler(u8 type);
	//virtual void handleClickEvent(const ClickEvent& event);
	void OpenTopPage(uint8_t pageId);
	TimerSetContain timerSetContain;
	ParamSetContain paramSetContain;
	SysSetContain sysSetContain;
	OverSetContain timeParamSetContain;
	void OpenKeyboard(uint32_t keyId);
	bool useSMOC;
};

#endif // SET_VIEW_HPP
