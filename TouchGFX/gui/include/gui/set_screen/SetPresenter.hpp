#ifndef SET_PRESENTER_HPP
#define SET_PRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>

using namespace touchgfx;

class SetView;

class SetPresenter : public Presenter, public ModelListener
{
public:
    SetPresenter(SetView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();
		void UiSetNetType(u8 type);

virtual ~SetPresenter() {};
virtual void WidgetStateChanged(uint16_t widgetId);
	



private:
    SetPresenter();

    SetView& view;
};


#endif // SET_PRESENTER_HPP
