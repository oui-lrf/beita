#include <gui/data_screen/DataView.hpp>
#include <gui/data_screen/DataPresenter.hpp>

DataPresenter::DataPresenter(DataView& v)
    : view(v)
{
}

void DataPresenter::activate()
{
	view.UpdataNetSta(model->getNetSta());
	view.UpdataNetType(model->getNetType());
	view.UpdataNetSignal(model->getNetSignal());
}

void DataPresenter::deactivate()
{

}


void DataPresenter::WidgetStateChanged(uint16_t widgetId)
{
	//Log("WidgetStateChanged widgetId:%d",widgetId);
	switch(widgetId)
	{
		case HEAD_NET_STA:
			view.UpdataNetSta(model->getNetSta());
		break;
		case HEAD_NET_TYPE:
			view.UpdataNetType(model->getNetType());
		break;
		case HEAD_NET_SIGNAL:
			view.UpdataNetSignal(model->getNetSignal());
		break;
		case HEAD_TIME:
			view.UpdataTime();
		break;
		
	}
}

