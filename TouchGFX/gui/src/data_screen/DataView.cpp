#include <gui/data_screen/DataView.hpp>
#include "usart.h"

DataView::DataView():showDataType(DATA_HIS),okPressed(this, &DataView::okPressedHandler),cancelPressed(this, &DataView::cancelPressedHandler),
pageIndex(0)
{
	strncpy(folder,"0:/data",20);
	char utfstr[10];
	RTC_DateTypeDef RTC_DateStruct;
  GetDateStruct(&RTC_DateStruct);
	sprintf(fileName,"%04d%02d%02d",2000+RTC_DateStruct.Year,RTC_DateStruct.Month,RTC_DateStruct.Date);
}

void DataView::setupScreen()
{
	Screen::useSMOCDrawing(false);	
	RTC_DateTypeDef RTC_DateStruct;
  GetDateStruct(&RTC_DateStruct);
	Unicode::snprintf(YearBtnBuffer,YEARBTN_SIZE, "%04d",2000+RTC_DateStruct.Year);
	Unicode::snprintf(MonthBtnBuffer,MONTHBTN_SIZE, "%02d",RTC_DateStruct.Month);
	Unicode::snprintf(DayBtnBuffer,DAYBTN_SIZE, "%02d",RTC_DateStruct.Date);
	pageIndex = 0;
	//////////////////////////
		u32 size=0;
	char path[40] ={0};
	sprintf(path,"%s/%s",folder,fileName);
	FIL file; 
	u8 res=f_open(&file,(const TCHAR*)path,FA_READ);//模式0,或者尝试打开失败,则创建新文件	 
	if(res==0)
	{
	  size = f_size(&file);
		f_close(&file);			
		u16 allCount = size/sizeof(FileRecord);
		int allPage = allCount/numberOfPageItem + ((allCount%numberOfPageItem)?1:0);
		pageIndex = allPage-1;
	}

	////////////////////////
	Unicode::snprintf(PageBtnBuffer,PAGEBTN_SIZE, "%d",pageIndex+1);
  DataViewBase::setupScreen();
	RecordScrollList1.setNumberOfItems(numberOfPageItem);
	RecordScrollList1.invalidate();
}

void DataView::tearDownScreen()
{
	Screen::useSMOCDrawing(true);	
  DataViewBase::tearDownScreen();
}


void DataView::okPressedHandler(uint32_t keyId,Unicode::UnicodeChar * str)
{
	#define TEXT_BUTTON touchgfx::WildcardTextButtonStyle< touchgfx::BoxWithBorderButtonStyle< touchgfx::ClickButtonTrigger > > 
	TEXT_BUTTON* btn;//
	btn = (TEXT_BUTTON *)keyId;
	uint8_t sv =0;
	char utfstr[40];
	Unicode::toUTF8(str,(uint8_t *)utfstr,40);
	
	
		if (btn == &YearBtn)
		{		
			uint16_t  value  = 0;
			int num = Unicode::atoi(str);
			value =num-48;
			if(num >0 && num <3000)
			{
				Unicode::snprintf(YearBtnBuffer,YEARBTN_SIZE, "%04d",num);
				sv =1;
			}
		}
		else if (btn == &MonthBtn)
		{
			int num = Unicode::atoi(str);
			if(num >0 && num <=12)
			{
				Unicode::snprintf(MonthBtnBuffer,MONTHBTN_SIZE, "%02d",num);
				sv =1;
			}
		}
		else if (btn == &DayBtn)
		{
			int num = Unicode::atoi(str);
			if(num >0 && num <=31)
			{
				Unicode::snprintf(DayBtnBuffer,DAYBTN_SIZE, "%02d",num);
				sv =1;
			}
		}
		else if (btn == &PageBtn)
		{
			int num = Unicode::atoi(str);
			if(num >=0 && num <10000)
			{
				u32 size=0;
				char path[40] ={0};
				sprintf(path,"%s/%s",folder,fileName);
				FIL file; 
				u8 res=f_open(&file,(const TCHAR*)path,FA_READ);//模式0,或者尝试打开失败,则创建新文件	 
				if(res==0)
				{
					size = f_size(&file);
					f_close(&file);		
				}
				u16 allCount = size/sizeof(FileRecord);
				int allPage = allCount/numberOfPageItem + ((allCount%numberOfPageItem)?1:0);
				if(num > allPage)
				{
					pageIndex = allPage -1;
				}
				else
				{
					pageIndex = num-1;
				}
				Unicode::snprintf(PageBtnBuffer,PAGEBTN_SIZE, "%d",pageIndex+1);
				UpdataRecordListItem();
				sv =1;
			}
		}		
		
		
		
	if(sv ==1)
	{
		btn->invalidate();
		
	}
	remove(keyboard);
	keyboard.invalidate();
	keyboard.ClearKeyBoard();
	Screen::useSMOCDrawing(false);	
}


void DataView::cancelPressedHandler()
{
	Screen::useSMOCDrawing(false);	
	keyboard.ClearKeyBoard();
	remove(keyboard);
	keyboard.invalidate();
}

//按键应该放到框架界面
void DataView::OpenKeyboard(uint32_t keyId)
{
	keyboard.setPosition(200, 100, 310, 318);
	keyboard.okCallback = &okPressed;
	keyboard.cancelCallback=&cancelPressed;
	if(keyboard.keyId == 0)
	{
		Screen::useSMOCDrawing(true);	
		keyboard.keyId=keyId;
		add(keyboard);
		keyboard.invalidate();
	}
}
 

void DataView::RecordScrollList1UpdateItem(DataRecord& item, int16_t itemIndex)
{
	char path[40] ={0};
	sprintf(path,"%s/%s",folder,fileName);
	u16 pageItemIndex = pageIndex*numberOfPageItem + itemIndex;
	Log("RecordScrollList1UpdateItem path:%s,pageItemIndex:%d",path,pageItemIndex);
	FIL file; 
	u8 res=f_open(&file,(const TCHAR*)path,FA_READ);//模式0,或者尝试打开失败,则创建新文件	 
	if(res==0)
	{
		u32 readed;
		FileRecord rcd={0};
		res = f_lseek(&file,pageItemIndex*sizeof(FileRecord)); 
		if(res == 0)
		{
			res = f_read(&file,&rcd,sizeof(FileRecord),&readed);
			if(res ==0 && readed == sizeof(FileRecord))
			{		
				rcd.id=pageItemIndex+1;	
				item.AddRecord(&rcd);
			}
			else
			{
				item.DeleteRecord();
			}
		}
		else
		{
			item.DeleteRecord();
		}

		f_close(&file);
	}
	else
	{
		item.DeleteRecord();
	}
	item.invalidate();
//		u32 readed;
//		FileRecord rcd={0};
//		item.AddRecord(&rcd);
}


void DataView::DataRecordBtnSelect()
{
	strncpy(folder,"0:/data",20);
	showDataType = DATA_HIS;
	pageIndex = 0;
	FindBtnClick();

}

void DataView::WaringRecordBtnSelect()
{
	
	strncpy(folder,"0:/waring",20);
	showDataType = DATA_WARING;
	pageIndex = 0;
	FindBtnClick();

}

void DataView::YearBtnClick()
{
	Log("YearBtnClick");
	OpenKeyboard((uint32_t)&YearBtn);
}
void DataView::MonthBtnClick()
{
	Log("MonthBtnClick");
	OpenKeyboard((uint32_t)&MonthBtn);
}
void DataView::DayBtnClick()
{
	Log("DayBtnClick");
	OpenKeyboard((uint32_t)&DayBtn);
}
void DataView::FindBtnClick()
{
	Log("FindBtnClick");
	char utfstr[10];
	fileName[0]=0;
	Unicode::toUTF8(YearBtnBuffer,(uint8_t *)utfstr,10);
	strcat(fileName,utfstr);
	Unicode::toUTF8(MonthBtnBuffer,(uint8_t *)utfstr,10);
	strcat(fileName,utfstr);
	if(showDataType == DATA_HIS)
	{
		Unicode::toUTF8(DayBtnBuffer,(uint8_t *)utfstr,10);
		strcat(fileName,utfstr);	
	}
	Log("fine fileName:%s",fileName);
	pageIndex = 0;
	//////////////////////////
		u32 size=0;
	char path[40] ={0};
	sprintf(path,"%s/%s",folder,fileName);
	FIL file; 
	u8 res=f_open(&file,(const TCHAR*)path,FA_READ);//模式0,或者尝试打开失败,则创建新文件	 
	if(res==0)
	{
	  size = f_size(&file);
		f_close(&file);			
		u16 allCount = size/sizeof(FileRecord);
		int allPage = allCount/numberOfPageItem + ((allCount%numberOfPageItem)?1:0);
		pageIndex = allPage-1;
	}

	////////////////////////
	
	//pageIndex = 0;
	//UpdataRecordList();
	Unicode::snprintf(PageBtnBuffer,PAGEBTN_SIZE, "%d",pageIndex+1);
	PageBtn.invalidate();
	UpdataRecordListItem();
}
void DataView::ClearBtnClick()
{
	Log("ClearBtnClick folder:%s",folder);
	pageIndex = 0;
	//mf_scan_files((u8 *)folder);
	if(f_deldir(folder) !=0 )
	{
		u8 res=f_mkfs("0:",0,4096);
		if(res == 0)
		{
			Log("mkfs ok");
		}
		else
		{
			Log("mkfs failed");
		}
	}
	mf_mkdir((u8 *)folder);
	UpdataRecordListItem();
}


//void DataView::UpdataRecordList()
//{	
////	u32 size=0;
////	char path[40] ={0};
////	sprintf(path,"%s/%s",folder,fileName);
////	FIL file; 
////	u8 res=f_open(&file,(const TCHAR*)path,FA_READ);//模式0,或者尝试打开失败,则创建新文件	 
////	if(res==0)
////	{
////	  size = f_size(&file);
////		f_close(&file);		
////	}

//	//Log("path:%s UpdataRecordList:%d",path,size);
//	//RecordScrollList1.setNumberOfItems(size/sizeof(FileRecord));
//	
////	RecordScrollList1.setNumberOfItems(numberOfPageItem);
////	RecordScrollList1.invalidate();
//}

void DataView::UpdataRecordListItem()
{
	for (int i = 0; i < numberOfPageItem; i++)//
	{
			//RecordScrollList1ListItems[i].initialize();
		RecordScrollList1.itemChanged(i);
	}
}
	
void DataView::UpdataNetSta(NET_STA sta)
{
	head.UpdataNetSta(sta);
}

void DataView::UpdataNetSignal(u16 signal)
{
	head.UpdataNetSignal(signal);
}

void DataView::UpdataNetType(NET_TYPE type)
{
	head.UpdataNetType(type);
}

//在查找按钮的基础上加入导出
void DataView::OutBtnClick()
{
//	Log("OutBtnClick");
//	//读取所有文件，把每一个文件串口输出
//		Log("FindBtnClick");
//	char utfstr[10];
//	fileName[0]=0;
//	Unicode::toUTF8(YearBtnBuffer,(uint8_t *)utfstr,10);
//	strcat(fileName,utfstr);
//	Unicode::toUTF8(MonthBtnBuffer,(uint8_t *)utfstr,10);
//	strcat(fileName,utfstr);
//	if(showDataType == DATA_HIS)
//	{
//		Unicode::toUTF8(DayBtnBuffer,(uint8_t *)utfstr,10);
//		strcat(fileName,utfstr);	
//	}
//	Log("fine fileName:%s",fileName);
//	pageIndex = 0;
//	//////////////////////////
//		u32 size=0;
//	char path[40] ={0};
//	sprintf(path,"%s/%s",folder,fileName);
//	FIL file; 
//	u8 res=f_open(&file,(const TCHAR*)path,FA_READ);//模式0,或者尝试打开失败,则创建新文件	 
//	if(res==0)
//	{
//	  size = f_size(&file);
//		u16 allCount = size/sizeof(FileRecord);
//		
//		u32 readed;
//		FileRecord rcd={0};
//		#define TIM_STR_LEN 24
//		#define VALU_STR_LEN 13
//		char time[TIM_STR_LEN]={0};
//		char data[100]={0};
//		for(u16 i=0;i<allCount;i++)
//		{		
//			res = f_read(&file,&rcd,sizeof(FileRecord),&readed);
//			if(res ==0 && readed == sizeof(FileRecord))
//			{		
//				
//				char value[VALU_STR_LEN]={0};
//				if(rcd.valueType == VALUE_FLOAT)
//				{
//						snprintf(value,VALU_STR_LEN,"%.2f%s",*(float *)&(rcd.value),rcd.unit);//
//				}
//				else
//				{
//						snprintf(value,VALU_STR_LEN,"%d%s",rcd.value,rcd.unit);//
//				}				
//				IntToTimeStr(time,TIM_STR_LEN,rcd.time);
//				snprintf(data,100,"序号:%04d 名称:%-12s 数值:%-13s 时间:%s\r\n",i+1,rcd.name,value,time);
//				UsartSendBuf(2,(u8 *)data,strlen(data));
//			}
//		}
//		f_close(&file);			
//		int allPage = allCount/numberOfPageItem + ((allCount%numberOfPageItem)?1:0);
//		pageIndex = allPage-1;
//	}

//	////////////////////////
//	
//	//pageIndex = 0;
//	//UpdataRecordList();
//	Unicode::snprintf(PageBtnBuffer,PAGEBTN_SIZE, "%d",pageIndex+1);
//	PageBtn.invalidate();
//	UpdataRecordListItem();
}

void DataView::NextBtnClick()
{
	//小于总页数
	
	u32 size=0;
	char path[40] ={0};
	sprintf(path,"%s/%s",folder,fileName);
	FIL file; 
	u8 res=f_open(&file,(const TCHAR*)path,FA_READ);//模式0,或者尝试打开失败,则创建新文件	 
	if(res==0)
	{
	  size = f_size(&file);
		f_close(&file);		
	}
	u16 allCount = size/sizeof(FileRecord);
	int allPage = allCount/numberOfPageItem + ((allCount%numberOfPageItem)?1:0);
	if(pageIndex +1<allPage)
	{
		pageIndex ++;	
		Unicode::snprintf(PageBtnBuffer,PAGEBTN_SIZE, "%d",pageIndex+1);
		PageBtn.invalidate();
		//Log("NextBtnClick allCount:%d allPage:%d numberOfPageItem:%d pageIndex:%d",allCount,allPage,numberOfPageItem,pageIndex);
		//UpdataRecordList();
		UpdataRecordListItem();
	}
}
void DataView::LastBtnClick()
{
	if(pageIndex >0)
	{
		pageIndex --;
		Unicode::snprintf(PageBtnBuffer,PAGEBTN_SIZE, "%d",pageIndex+1);
		PageBtn.invalidate();
		Log("LastBtnClick pageIndex:%d",pageIndex);
		//UpdataRecordList();
		UpdataRecordListItem();
	}
}

void DataView::PageBtnClick()
{
	OpenKeyboard((uint32_t)&PageBtn);
}
	
