#include <gui/common/EnCustomKeyboard.hpp>
#include "stdio.h"

using namespace EnKeyBoard;
EnCustomKeyboard::EnCustomKeyboard() : keyboard(),
    modeBtnTextArea(),enterBtnTextArea(),
    capslockPressed(this, &EnCustomKeyboard::capslockPressedHandler),
    backspacePressed(this, &EnCustomKeyboard::backspacePressedHandler),
    modePressed(this, &EnCustomKeyboard::modePressedHandler),
    keyPressed(this, &EnCustomKeyboard::keyPressedhandler),
		enterPressed(this, &EnCustomKeyboard::enterPressedHandler),																			 
    cancelPressed(this, &EnCustomKeyboard::cancelPressedHandler),
    alphaKeys(true),
    uppercaseKeys(false),
    firstCharacterEntry(false),
		keyId(0)
{
    //Set the callbacks for the callback areas of the keyboard and set its layout.
    layout.callbackAreaArray[0].callback = &capslockPressed;
    layout.callbackAreaArray[1].callback = &backspacePressed;
    layout.callbackAreaArray[2].callback = &modePressed;
		layout.callbackAreaArray[3].callback = &enterPressed;
    layout.callbackAreaArray[4].callback = &cancelPressed;
		keyboard.setPosition(0, 0, 480, 360);
    keyboard.setLayout(&layout);
    keyboard.setKeyListener(keyPressed);
		
    keyboard.setTextIndentation();
    //Allocate the buffer associated with keyboard.
    memset(buffer, 0, sizeof(buffer));
    keyboard.setBuffer(buffer, BUFFER_SIZE);
		
	
		
    uppercaseKeys = true;
    firstCharacterEntry = true;
		Font* font = FontManager::getFont(layout.keyFont);
		uint8_t fontHeight = font->getMinimumTextHeight();
		for(uint8_t i=0;i<5;i++)
		{
			touchgfx::Rect keyArea = layout.callbackAreaArray[i].keyArea;
			uint16_t offset = (keyArea.height - fontHeight) / 2;
			keyArea.y += offset;
			keyArea.height -= offset;
			TextArea *text;
			switch(i)
			{
				case 0:
					text = &capsTextArea;
					text->setTypedText(TypedText(T_CAPSLOCK));				  
				break;
			  case 1:
					text = &backSpaceTextArea;
					text->setTypedText(TypedText(T_BACKSPACE));
				break;
				case 2:
					text = &modeBtnTextArea;
				break;
				case 3:		
					text = &enterBtnTextArea;
					text->setTypedText(TypedText(T_ENTERKEY));
				break;
				case 4:

					text = &closeTextArea;
					text->setTypedText(TypedText(T_CANCEL));
				break;				
			
			}
			text->setPosition(keyArea.x,keyArea.y,keyArea.width,keyArea.height);
			text->setColor(Color::getColorFrom24BitRGB(0xFF, 0xFF, 0xFF));
		}
	
    setKeyMappingList();
    add(keyboard);
    add(modeBtnTextArea);
		add(enterBtnTextArea);
		add(backSpaceTextArea);
		add(closeTextArea);
		add(capsTextArea);
}

void EnCustomKeyboard::setKeyMappingList()
{
    if (alphaKeys)
    {
        modeBtnTextArea.setTypedText(TypedText(T_ALPHAMODE));
        if (uppercaseKeys)
        {
            keyboard.setKeymappingList(&keyMappingListAlphaUpper);
        }
        else
        {
            keyboard.setKeymappingList(&keyMappingListAlphaLower);
        }
    }
    else
    {
        modeBtnTextArea.setTypedText(TypedText(T_NUMMODE));
        if (uppercaseKeys)
        {
            keyboard.setKeymappingList(&keyMappingListNumUpper);
        }
        else
        {
            keyboard.setKeymappingList(&keyMappingListNumLower);
        }
    }
}

void EnCustomKeyboard::backspacePressedHandler()
{
    uint16_t pos = keyboard.getBufferPosition();
    if (pos > 0)
    {
        //Delete the previous entry in the buffer and decrement the position.
        buffer[pos - 1] = 0;
        keyboard.setBufferPosition(pos - 1);

        //Change keymappings if we have reached the first position.
        if (1 == pos)
        {
            firstCharacterEntry = true;
            uppercaseKeys = true;
            setKeyMappingList();
        }
    }
}

void EnCustomKeyboard::capslockPressedHandler()
{
    uppercaseKeys = !uppercaseKeys;
    setKeyMappingList();
}

void EnCustomKeyboard::cancelPressedHandler()
{
	printf("cancelPressedHandler\r\n");
	if(cancelCallback)
	{
		cancelCallback->execute();
	}
}

void EnCustomKeyboard::ClearKeyBoard()
{
	keyId =0;
	buffer[0]=0;
	keyboard.setBufferPosition(0);
}

void EnCustomKeyboard::enterPressedHandler()
{
	printf("enterPressedHandler\r\n");
	if(okCallback !=0)
	{
		okCallback->execute(keyId,keyboard.getBuffer());
	}
}

void EnCustomKeyboard::modePressedHandler()
{
    alphaKeys = !alphaKeys;

    // if we have changed back to alpha and still has no chars in the buffer,
    // we show upper case letters.
    if (firstCharacterEntry && alphaKeys)
    {
        uppercaseKeys = true;
    }
    else
    {
        uppercaseKeys = false;
    }
    setKeyMappingList();
}

void EnCustomKeyboard::keyPressedhandler(Unicode::UnicodeChar keyChar)
{
    // After the first keypress, the keyboard will shift to lowercase.
	
	uint8_t keyBuf[BUFFER_SIZE];
	Unicode::toUTF8(keyboard.getBuffer(),keyBuf,BUFFER_SIZE);
	printf("-->keyPressedhandler:%c,buf:%s,len:%d\r\n",keyChar,keyBuf,keyboard.getBufferPosition());
	
	
	
//    if (firstCharacterEntry && keyChar != 0)
//    {
//        firstCharacterEntry = false;
//        uppercaseKeys = false;
//        setKeyMappingList();
//    }
}

void EnCustomKeyboard::setTouchable(bool touch)
{
    Container::setTouchable(touch);
    keyboard.setTouchable(touch);
}
