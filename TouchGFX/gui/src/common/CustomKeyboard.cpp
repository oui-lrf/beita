#include <gui/common/CustomKeyboard.hpp>
#include <gui/main_screen/MainView.hpp>

CustomKeyboard::CustomKeyboard() : keyboard(),
    modeBtnTextArea(),
    cancelPressed(this, &CustomKeyboard::cancelPressedHandler),
    backspacePressed(this, &CustomKeyboard::backspacePressedHandler),
    okPressed(this, &CustomKeyboard::okPressedHandler),
    keyPressed(this, &CustomKeyboard::keyPressedhandler),
    alphaKeys(true),
    uppercaseKeys(false),
    firstCharacterEntry(false),
	keyId(0)
{
    //Set the callbacks for the callback areas of the keyboard and set its layout.
    layout.callbackAreaArray[0].callback = &cancelPressed;
    layout.callbackAreaArray[1].callback = &backspacePressed;
    layout.callbackAreaArray[2].callback = &okPressed;
    keyboard.setLayout(&layout);
    keyboard.setKeyListener(keyPressed);
    keyboard.setPosition(0, 0, 310, 318);
    keyboard.setTextIndentation();
    memset(buffer, 0, sizeof(buffer));
    keyboard.setBuffer(buffer, BUFFER_SIZE);
    uppercaseKeys = true;
    firstCharacterEntry = true;
    modeBtnTextArea.setPosition(0, 0, 43, 43);
    modeBtnTextArea.setColor(Color::getColorFrom24BitRGB(0xFF, 0xFF, 0xFF));
    setKeyMappingList();
    add(keyboard);
    add(modeBtnTextArea);
}

void CustomKeyboard::setKeyMappingList()
{
	keyboard.setKeymappingList(&keyMappingListNumLower);
}

void CustomKeyboard::backspacePressedHandler()
{
	printf("-->backspacePressedHandler\r\n");
    uint16_t pos = keyboard.getBufferPosition();
    if (pos > 0)
    {
        //Delete the previous entry in the buffer and decrement the position.
        buffer[pos - 1] = 0;
        keyboard.setBufferPosition(pos - 1);

        //Change keymappings if we have reached the first position.
        if (1 == pos)
        {
            firstCharacterEntry = true;
            uppercaseKeys = true;
            setKeyMappingList();
        }
    }
}

void CustomKeyboard::cancelPressedHandler()
{
	printf("-->cancelPressedHandler cancelCallback:%d\r\n",cancelCallback);
	
	if(cancelCallback)
	{
		cancelCallback->execute();
	}
}


void CustomKeyboard::ClearKeyBoard()
{
	keyId =0;
	buffer[0]=0;
	keyboard.setBufferPosition(0);
}

void CustomKeyboard::okPressedHandler()
{	
	if(okCallback !=0)
	{
		okCallback->execute(keyId,keyboard.getBuffer());
	}
}

void CustomKeyboard::keyPressedhandler(Unicode::UnicodeChar keyChar)
{
 
}

void CustomKeyboard::setTouchable(bool touch)
{
    Container::setTouchable(touch);
    keyboard.setTouchable(touch);
}
