#include <gui/model/Model.hpp>
#include <gui/model/ModelListener.hpp>
#include <gui/main_screen/MainPresenter.hpp>
#include "GuiInclude.h"

extern osMessageQId msgQueueUIHandle;


Model::Model() : modelListener(0),tk(0),sysRunSta(DEVICE_RUN_NULL),waringSta(DEVICE_WARING_NULL)
{
	strcpy(localIp,"0.0.0.0");
	strcpy(remoteIp,"0.0.0.0");
}


	u32 volValue1;
	u32 volValue2;
	u32 volValue3;
	
	u32 curValue1;
	u32 curValue2;
	u32 curValue3;

u8 Model::SetValueById(uint16_t widgetId,u32 data)
{
	u8 rt=0;
	switch(widgetId)
	{
	 case NOW_POW: 
			nowPow =data;
			rt =1;		
	 break;
	 case MONTH_CONSUME: 
			monthConsume =data;
			rt =1;	
	 break;
	 case ALL_CONSUME: 
			allConsume =data;
			rt =1;	 
	 break;
	
	 case CUR1: 
				curValue1 = data;
				rt =1;						 
	 break;
	 case CUR2: 
				curValue2 = data;
				rt =1;				 
	 break;
	 case CUR3: 
				curValue3 = data;
				rt =1;					 
	 break;
	
	 case VOL1: 
				volValue1 = data;
				rt =1;					 
	 break;
	 case VOL2: 
				volValue2 = data;
				rt =1;					 
	 break;
	 case VOL3: 
				volValue3 = data;
				rt =1;				 
	 break;
		case HEAD_NET_STA:
			if(netSta !=  (NET_STA)data)
			{
				netSta =  (NET_STA)data;
				rt =1;			
			}
		break;
		case HEAD_NET_TYPE:
			if(netType != (NET_TYPE)data)
			{
				netType =  (NET_TYPE)data;
				rt =1;			
			}
		break;
		case HEAD_NET_SIGNAL:
			if(netSignal != (u16)data)
			{
				netSignal =  (u16)data;
				rt =1;			
			}
		break;
		case SET_REMOTE_IP_STA:
			if(remoteIpSta != (bool)data)
			{
				remoteIpSta =  (bool)data;
				rt =1;			
			}
		break;
		case SET_LOCAL_IP:{
			char *str = (char *)data;
			if(str)
			{
				int len = strlen(str);
				if(len > 0 && len <20)
				{
					strncpy(localIp,str,20);
					rt =1;
				}	
				GuiFree(str);
			}
		break;
		}
		case SET_REMOTE_IP:{
			char *str = (char *)data;
			if(str)
			{
				int len = strlen(str);
				if(len > 0 && len <20)
				{
					strncpy(remoteIp,str,20);
					rt =1;
				}	
				GuiFree(str);
			}
		break;
		}
		case SET_USER_CODE:{
			char *str = (char *)data;
			if(str)
			{
				int len = strlen(str);
				if(len > 0 && len <USER_CODE_LEN)
				{
					strncpy(userCode,str,USER_CODE_LEN);
					rt =1;
				}	
				GuiFree(str);
			}
		break;
		}
		case SET_ICCID:{
			char *str = (char *)data;
			if(str)
			{
				int len = strlen(str);
				if(len > 0 && len <ICCID_LEN)
				{
					strncpy(iccid,str,ICCID_LEN);
					rt =1;
				}	
				GuiFree(str);
			}
		break;
		}
		case SET_USER_IP:{
			char *str = (char *)data;
			if(str)
			{
				int len = strlen(str);
				if(len > 0 && len <MAX_IP_LEN)
				{
					strncpy(userIp,str,MAX_IP_LEN);
					rt =1;
				}	
				GuiFree(str);
			}
		break;
		} 
		case SET_USER_PORT: {
			if(userPort != (u16)data)
			{
				userPort = (u16)data;
				rt =1;
			}			 
	 break;
		}
		
		case SET_PASS:{
			char *str = (char *)data;
			if(str)
			{
				int len = strlen(str);
				if(len > 0 && len <20)
				{
					strncpy(pass,str,20);
					rt =1;
				}	
				GuiFree(str);
			}			
		break;
		}
		case SET_NAME:{
			char *str = (char *)data;
			if(str)
			{
				int len = strlen(str);
				if(len > 0 && len <20)
				{
					strncpy(name,str,20);
					rt =1;
				}	
				GuiFree(str);
			}
		break;
		}
		case SET_LOCAL_PORT:
			if(localPort != (u16)data)
			{
				localPort =  (u16)data;
				rt =1;			
			}
		break;
		case SET_REMOTE_PORT:
			if(remotePort != (u16)data)
			{
				remotePort =  (u16)data;
				rt =1;			
			}
		break;

		default :
			
		break;
	}
	return rt;
}

void Model::tick()
{
	//检测频繁更新的数据并显示，如温度、模拟量输入等
	if(tk++ >= 0xffff)tk=0;
	osEvent event;
	/* wait for a message from the queue or a timeout */
    event = osMessageGet(msgQueueUIHandle,0);
	if (event.status == osEventMessage)
  {
    UiMsg *msg =(UiMsg *)event.value.p;
		u8 res = SetValueById(msg->widGetId,msg->data);
		if(res)
		{
			modelListener->WidgetStateChanged(msg->widGetId);		
		}
		vPortFree(msg);
	}
	if (tk % 30 == 0)
	{
		modelListener->WidgetStateChanged(HEAD_TIME);
	}
	
	
	if(tk % 300 == 0)
	{
//		printf("xPortGetFreeHeapSize:%d",xPortGetFreeHeapSize());
	}
	
}

//设置开关量输出
void Model::SendOutSwMsg(uint8_t bit,bool state)
{
	//给控制线程发送消息
	//SendRunOutSwMsg(bit,state);
	HandControlSw(bit,state);
}
//界面操作更新网络类型
void Model::UiSetNetType(u8 type)
{
	//调用外界接口函数
	//ChangeNetType(type);
}

void Model::RunStaControl(u8 value)
{
	DeviceRunControl(value);
}

void Model::RunModeControl(u8 value)
{
	
}


