#include <gui/set_screen/SetView.hpp>
#include <gui/set_screen/SetPresenter.hpp>

SetPresenter::SetPresenter(SetView& v)
    : view(v)
{
}

void SetPresenter::activate()
{
	view.UpdataNetSta(model->getNetSta());
	view.UpdataNetType(model->getNetType());
	view.UpdataNetSignal(model->getNetSignal());
	view.UpdataTime();
		
	view.UpdataLocalIp(model->GetLocalIp());
	view.UpdataRemoteIp(model->GetRemoteIp());
	view.UpdataLocalPort(model->GetLocalPort());
	view.UpdataRemotePort(model->GetRemotePort());
		
	view.UpdataName(model->GetName());
	view.UpdataPass(model->GetPass());
	view.UpdataRemoteIpSta(model->GetRemoteIpSta());
		
	view.UpdataIccid(model->GetIccid());
}

void SetPresenter::deactivate()
{

}

void SetPresenter::UiSetNetType(u8 type)
{
	model->UiSetNetType(type);
}

void SetPresenter::WidgetStateChanged(uint16_t widgetId)
{
	switch(widgetId)
	{
		case HEAD_NET_STA:
			view.UpdataNetSta(model->getNetSta());
		break;
		case HEAD_NET_TYPE:
			view.UpdataNetType(model->getNetType());
		break;
		case HEAD_NET_SIGNAL:
			view.UpdataNetSignal(model->getNetSignal());
		break;
		case HEAD_TIME:
			view.UpdataTime();
		break;
		case SET_LOCAL_IP:
			view.UpdataLocalIp(model->GetLocalIp());
		break;
		case SET_REMOTE_IP:
			view.UpdataRemoteIp(model->GetRemoteIp());
		break;
		case SET_REMOTE_IP_STA:
			view.UpdataRemoteIpSta(model->GetRemoteIpSta());
		break;
		case SET_LOCAL_PORT:
			view.UpdataLocalPort(model->GetLocalPort());
		break;
		case SET_REMOTE_PORT:
			view.UpdataRemotePort(model->GetRemotePort());
		break;
		case SET_NAME:
			view.UpdataName(model->GetName());
		break;
		case SET_PASS:
			view.UpdataPass(model->GetPass());
		break;
		case SET_USER_CODE:
			view.UpdataUserCode(model->GetUserCode());
		break;
		case SET_ICCID:
			view.UpdataIccid(model->GetIccid());
		break;		
		case SET_USER_IP:
			view.UpdataUserIp(model->GetUserIp());
		break;
		case SET_USER_PORT:
			view.UpdataUserPort(model->GetUserPort());
		break;
		
	}
	
}


