#include <gui/set_screen/SetView.hpp>
#include "GuiInclude.h"

SetView::SetView():childContainUseSMOCDrawingCall(this, &SetView::childContainUseSMOCDrawinghandler),
	framBtnClick(this,&SetView::framBtnClickHandler),netTypeChangeCall(this,&SetView::NetTypeChangeCallHandler),useSMOC(true),
	okPressed(this, &SetView::okPressedHandler),cancelPressed(this, &SetView::cancelPressedHandler)
{
	
}

void SetView::childContainUseSMOCDrawinghandler(bool sta)
{
	Log("childContainUseSMOCDrawinghandler old :%d ,sta:%d",useSMOC,sta);
	if(useSMOC != sta)
	{
		useSMOC = sta;
		Screen::useSMOCDrawing(sta);	
	}

}

void SetView::setupScreen()
{
	paramSetContain.useSMOCDrawingCallback = &childContainUseSMOCDrawingCall;
	timeParamSetContain.useSMOCDrawingCallback = &childContainUseSMOCDrawingCall;
	timerSetContain.useSMOCDrawingCallback = &childContainUseSMOCDrawingCall;
	sysSetContain.useSMOCDrawingCallback = &childContainUseSMOCDrawingCall;
	sysSetContain.netTypeChangeCallback = &netTypeChangeCall;
	
	
	setBackFrameContain.btnClickCallback = &framBtnClick;
	Screen::useSMOCDrawing(false);	
	useSMOC = false;
	tickCounter=0;
	
	paramSetContain.setXY(160, 100);
	paramSetContain.keyboard = &keyboard;
	paramSetContain.enKeyboard = &enKeyboard;
	add(paramSetContain);	
	
//	sysSetContain.setXY(160, 100);
//	sysSetContain.keyboard = &keyboard;
//	sysSetContain.enKeyboard = &enKeyboard;
//	add(sysSetContain);		
	
	SetViewBase::setupScreen();
	//移除除首页外的3个界面
}

void SetView::tearDownScreen()
{
	Log("tearDownScreen");
  SetViewBase::tearDownScreen();
	useSMOC = true;
	Screen::useSMOCDrawing(true); 
}


void SetView::okPressedHandler(uint32_t keyId,Unicode::UnicodeChar * str)
{
	#define TEXT_BUTTON touchgfx::WildcardTextButtonStyle< touchgfx::BoxWithBorderButtonStyle< touchgfx::ClickButtonTrigger > > 
	TEXT_BUTTON* btn;//
	btn = (TEXT_BUTTON *)keyId;
	uint8_t sv =0;
	char utfstr[40];
	Unicode::toUTF8(str,(uint8_t *)utfstr,40);
	if(btn !=0)
	{
//		if(keyId == SYS_BTN)
//		{
//			char resstr[40];
//			if(GetScreenPass(resstr,40))
//			{
//				if(strcmp(utfstr,resstr) == 0)
//				{
//					sysSetContain.setXY(160, 100);
//					sysSetContain.keyboard = &keyboard;
//					sysSetContain.enKeyboard = &enKeyboard;
//					add(sysSetContain);
//					sysSetContain.invalidate();
//				}
//			}
//		}
		if(sv ==1)
		{
			btn->invalidate();
			
		}
		
	}
	remove(keyboard);
	keyboard.invalidate();
	keyboard.ClearKeyBoard();
	childContainUseSMOCDrawinghandler(false);
}
void SetView::cancelPressedHandler()
{
	childContainUseSMOCDrawinghandler(false);
	keyboard.ClearKeyBoard();
	remove(keyboard);
	keyboard.invalidate();
}

//按键应该放到框架界面
void SetView::OpenKeyboard(uint32_t keyId)
{
	keyboard.setPosition(140, 100, 310, 318);
	keyboard.okCallback = &okPressed;
	keyboard.cancelCallback=&cancelPressed;
	if(keyboard.keyId == 0)
	{
		childContainUseSMOCDrawinghandler(true);
		keyboard.keyId=keyId;
		add(keyboard);
		keyboard.invalidate();
	}
}


void SetView::NetTypeChangeCallHandler(uint8_t type)
{
	Log("NetTypeChangeCallHandler");
	presenter->UiSetNetType(type);
}

void SetView::framBtnClickHandler(uint8_t topBtn)
{
	Log("framBtnClickHandler oldBtn:%d,topBtn:%d",setBackFrameContain.topBtn,topBtn);	
	//结束对应的老界面
	switch(setBackFrameContain.topBtn)
	{
		case PARAM_BTN:
				remove(paramSetContain);
				paramSetContain.invalidate();
		break;
		case TIMER_BTN:
				remove(timerSetContain);
				timerSetContain.invalidate();
		break;
		case SYS_BTN:
				remove(sysSetContain);
				sysSetContain.invalidate();
		break;
		case TIME_PARAM_BTN:
				remove(timeParamSetContain);
				timeParamSetContain.invalidate();
		break;
	
	}
	//开启新界面
	switch(topBtn)
	{
		case PARAM_BTN:
			paramSetContain.setXY(160, 100);
			paramSetContain.keyboard = &keyboard;
			paramSetContain.enKeyboard = &enKeyboard;
			add(paramSetContain);
			paramSetContain.invalidate();
		break;
		case TIMER_BTN:
			timerSetContain.setXY(160, 100);
			timerSetContain.keyboard = &keyboard;
			add(timerSetContain);
			timerSetContain.invalidate();
		break;
		case SYS_BTN:			
//			OpenKeyboard(topBtn);
			sysSetContain.setXY(160, 100);
			sysSetContain.keyboard = &keyboard;
			sysSetContain.enKeyboard = &enKeyboard;
			add(sysSetContain);
			sysSetContain.invalidate();
		break;
		case TIME_PARAM_BTN:
			timeParamSetContain.setXY(160, 100);
			timeParamSetContain.keyboard = &keyboard;
			add(timeParamSetContain);
			timeParamSetContain.invalidate();
		break;
	}
}

void SetView::UpdataNetSta(NET_STA sta)
{
	head.UpdataNetSta(sta);
}

void SetView::UpdataNetSignal(u16 signal)
{
	head.UpdataNetSignal(signal);
}

void SetView::UpdataNetType(NET_TYPE type)
{
	head.UpdataNetType(type);
	sysSetContain.DisplayNetType(type);
}


