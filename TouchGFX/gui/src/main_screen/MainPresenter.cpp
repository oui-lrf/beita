#include <gui/main_screen/MainView.hpp>
#include <gui/main_screen/MainPresenter.hpp>
#include "GuiInclude.h"

MainPresenter::MainPresenter(MainView& v)
    : view(v)
{
}

void MainPresenter::activate()
{
	view.UpdataNetSta(model->getNetSta());
	view.UpdataNetType(model->getNetType());
	view.UpdataNetSignal(model->getNetSignal());
	for(uint16_t i=0;i<=VOL3;i++)
	{
		WidgetStateChanged(i);
	}
}

void MainPresenter::deactivate()
{
	
}

void MainPresenter::SetSw(uint8_t bit,bool new_state)
{
	model->SendOutSwMsg(bit,new_state);//�ı����  
}

void MainPresenter::WidgetStateChanged(uint16_t widgetId)
{
	//Log("WidgetStateChanged widgetId:%d",widgetId);
	switch(widgetId)
	{
		case NOW_POW: 
			view.UpdataNowPowText(model->GetNowPow());
	 break;
	 case MONTH_CONSUME: 
		 	view.UpdataMonthConsumeText(model->GetMonthConsume());
	 break;
	 case ALL_CONSUME: 
		 	view.UpdataAllConsumeText(model->GetAllConsume());
	 break;
	
	 case CUR1: 
		 	view.UpdataCurText1(model->getCurValue1());		 
	 break;
	 case CUR2: 
		 	view.UpdataCurText2(model->getCurValue2());				 
	 break;
	 case CUR3: 
		 	view.UpdataCurText3(model->getCurValue3());		 
	 break;
	
	 case VOL1: 
		 	view.UpdataVolText1(model->getVolValue1());			 
	 break;
	 case VOL2: 
		 	view.UpdataVolText2(model->getVolValue2());			 
	 break;
	 case VOL3: 
		 	view.UpdataVolText3(model->getVolValue3());				 
	 break;
		
		case HEAD_NET_STA:
			view.UpdataNetSta(model->getNetSta());
		break;
		case HEAD_NET_TYPE:
			view.UpdataNetType(model->getNetType());
		break;
		case HEAD_NET_SIGNAL:
			view.UpdataNetSignal(model->getNetSignal());
		break;
		case HEAD_TIME:
			view.UpdataTime();
		break;
		
	}
}

