#include <gui/main_screen/MainView.hpp>
#include "BitmapDatabase.hpp"
#include <touchgfx/Color.hpp>
#include <touchgfx/widgets/Image.hpp>
#include "GuiInclude.h"

MainView::MainView()
{
	
}

void MainView::setupScreen()
{
	Screen::useSMOCDrawing(false);	
  MainViewBase::setupScreen();
	tickCounter=0;
}

void MainView::tearDownScreen()
{
  MainViewBase::tearDownScreen();
	Screen::useSMOCDrawing(true);	
}


void MainView::UpdataNetSta(NET_STA sta)
{
	head.UpdataNetSta(sta);
}

void MainView::UpdataNetSignal(u16 signal)
{
	head.UpdataNetSignal(signal);
}

void MainView::UpdataNetType(NET_TYPE type)
{
	head.UpdataNetType(type);
}

void  MainView::ClearWaringBtnClick()
{
	// Override and implement this function in MainView
//	ClearWaringBtn.setLabelColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
//	ClearWaringBtn.invalidate();
//	ClearDeviceWaring();
	//presenter->SendControlMsg(DEVICE_RUN_CLEAR_WARING,0);
}

void  MainView::RecoarBtnClick()
{
	// Override and implement this function in MainView
}


void  MainView::TimerBtnClick()
{
	Log("TimerBtnClick");
}

void  MainView::RunBtnClick()
{

}
