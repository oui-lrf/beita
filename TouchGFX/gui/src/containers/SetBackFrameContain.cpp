#include <gui/containers/SetBackFrameContain.hpp>


SetBackFrameContain::SetBackFrameContain():topBtn(PARAM_BTN)
{

}
void SetBackFrameContain::initialize()
{
    SetBackFrameContainBase::initialize();
	//在那一页，本页按钮设置为按下，并禁止按下
	TempParamSetBtn.setPressed(true);
	TempParamSetBtn.setTouchable(false);
	
	SysSetBtn.setPressed(false);
	SysSetBtn.setTouchable(true);
	
	OverParamBtn.setPressed(false);
	OverParamBtn.setTouchable(false);
	
	TimerSetBtn.setTouchable(false);
	TimerSetBtn.setPressed(false);
	
}
void SetBackFrameContain::OpenTopBtn(uint8_t topBtn)
{
	switch(topBtn)
	{
		case PARAM_BTN:
			TempParamSetBtn.setPressed(false);
			TempParamSetBtn.setTouchable(true);
			TempParamSetBtn.invalidate();
		break;
		case TIMER_BTN:
			TimerSetBtn.setPressed(false);
			TimerSetBtn.setTouchable(true);
			TimerSetBtn.invalidate();
		break;
		case SYS_BTN:
			SysSetBtn.setPressed(false);
			SysSetBtn.setTouchable(true);
			SysSetBtn.invalidate();
		break;
		case TIME_PARAM_BTN:
			OverParamBtn.setPressed(false);
			OverParamBtn.setTouchable(true);
			OverParamBtn.invalidate();
		break;
	}
}

void SetBackFrameContain::ParamBtnClick()
{
	TempParamSetBtn.setPressed(true);
	TempParamSetBtn.setTouchable(false);
	OpenTopBtn(topBtn);
	if(btnClickCallback)
	{
		btnClickCallback->execute(PARAM_BTN);
	}
	topBtn = PARAM_BTN;
}

void SetBackFrameContain::TimerBtnClick()
{
	TimerSetBtn.setPressed(true);
	TimerSetBtn.setTouchable(false);
	OpenTopBtn(topBtn);
	if(btnClickCallback)
	{
		btnClickCallback->execute(TIMER_BTN);
	}
	topBtn = TIMER_BTN;
}

 void SetBackFrameContain::SysBtnClick()
{
	SysSetBtn.setPressed(true);
	SysSetBtn.setTouchable(false);
	OpenTopBtn(topBtn);
	if(btnClickCallback)
	{
		btnClickCallback->execute(SYS_BTN);
	}
	topBtn = SYS_BTN;
}

 void SetBackFrameContain::OverParamBtnClick()
{
	OverParamBtn.setPressed(true);
	OverParamBtn.setTouchable(false);
	OpenTopBtn(topBtn);
	if(btnClickCallback)
	{
		btnClickCallback->execute(TIME_PARAM_BTN);
	}
	topBtn = TIME_PARAM_BTN;
}


