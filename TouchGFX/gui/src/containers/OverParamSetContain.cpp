#include <gui/containers/OverParamSetContain.hpp>


OverParamSetContain::OverParamSetContain():okPressed(this, &OverParamSetContain::okPressedHandler),
	cancelPressed(this, &OverParamSetContain::cancelPressedHandler)
{
	DisplayParam();
}

void OverParamSetContain::initialize()
{
    OverSetContain::initialize();
}


void OverParamSetContain::okPressedHandler(uint32_t keyId,Unicode::UnicodeChar * str)
{
//	#define TEXT_BUTTON touchgfx::WildcardTextButtonStyle< touchgfx::BoxWithBorderButtonStyle< touchgfx::ClickButtonTrigger > > 
//	TEXT_BUTTON* btn;//
//	btn = (TEXT_BUTTON *)keyId;
//	uint8_t sv =0;
//	if(btn !=0)
//	{
//		uint16_t  value  = 0;
//		int num = Unicode::atoi(str);

//		if(sv ==1)
//		{
//			btn->invalidate();
//			PostFreeEvent(EVENT_SAVE_DEVICE_FLASH);
//		}
//	}
	
	remove(*keyboard);
	keyboard->invalidate();
	keyboard->ClearKeyBoard();
	if(useSMOCDrawingCallback !=0)
	{
		useSMOCDrawingCallback->execute(false);
	}
	this->invalidate();
}

void OverParamSetContain::cancelPressedHandler()
{
	printf("ParamSetContain-->cancelPressedHandler\r\n");	
	if(useSMOCDrawingCallback !=0)
	{
		useSMOCDrawingCallback->execute(false);
	}
	keyboard->ClearKeyBoard();
	remove(*keyboard);
	keyboard->invalidate();

	this->invalidate();
}

void OverParamSetContain::OpenKeyboard(uint32_t keyId)
{
	keyboard->setPosition(140, 0, 310, 318);
	keyboard->okCallback = &okPressed;
	keyboard->cancelCallback=&cancelPressed;
	if(keyboard->keyId == 0)
	{
		if(useSMOCDrawingCallback !=0)
		{
			useSMOCDrawingCallback->execute(true);
		}
		keyboard->keyId=keyId;
		add(*keyboard);
		keyboard->invalidate();
	}
}



void OverParamSetContain::DisplayParam()
{

}
