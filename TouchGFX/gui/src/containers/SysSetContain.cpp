#include <gui/containers/SysSetContain.hpp>
#include "GuiInclude.h"

SysSetContain::SysSetContain():okPressed(this, &SysSetContain::okPressedHandler),enOkPressed(this, &SysSetContain::enOkPressedHandler),
	selectActive(false),
	cancelPressed(this, &SysSetContain::cancelPressedHandler),enCancelPressed(this, &SysSetContain::enCancelPressedHandler)
{
	//Log("SysSetContain SysSetContain");
	QRCode code;
	//qrCode.setXY();
	qrCode.setPosition(456,90,132,132);
	qrCode.setQRCode(&code);
	qrCode.setScale(1);
	add(qrCode);
	
//	radioButtonWIFI.setSelected(true);
	DisplayParam();
}

 SysSetContain::~SysSetContain()
{
//	Log("SysSetContain ~SysSetContain");
//	radioButtonWIFI.setSelected(false);
}

void SysSetContain::initialize()
{
	//Log("SysSetContain initialize");
  SysSetContainBase::initialize();
		
		
}



void SysSetContain::DisplayParam()
{
	
	Log("DisplayParam");
	RTC_TimeTypeDef RTC_TimeStruct;
	RTC_DateTypeDef RTC_DateStruct;
	GetTimeStruct(&RTC_TimeStruct);
  GetDateStruct(&RTC_DateStruct);
	Unicode::snprintf(YearBtnBuffer,YEARBTN_SIZE, "%04d",2000+RTC_DateStruct.Year);
	Unicode::snprintf(MonthBtnBuffer,MONTHBTN_SIZE, "%02d",RTC_DateStruct.Month);
	Unicode::snprintf(DayBtnBuffer,DAYBTN_SIZE, "%02d",RTC_DateStruct.Date);
	Unicode::snprintf(HourBtnBuffer,HOURBTN_SIZE, "%02d",RTC_TimeStruct.Hours);
	Unicode::snprintf(MinBtnBuffer,MINBTN_SIZE, "%02d",RTC_TimeStruct.Minutes);
	Unicode::snprintf(SecBtnBuffer,SECBTN_SIZE, "%02d",RTC_TimeStruct.Seconds);
	
	
	char str[41];
	snprintf(str,41,"%s-%s",PRODUC_CODE,SOFT_CODE);
	GbkToUnicode((unsigned char *)str,VersionTextBuffer,VERSIONTEXT_SIZE,1);
	
	u8 res = GetIp(str,41);
	if(res)
	{
		GbkToUnicode((unsigned char *)str,IpBtnBuffer,IPBTN_SIZE,1);
	}
	Unicode::snprintf(PortBtnBuffer,PORTBTN_SIZE, "%d",GetPort());
	 res = GetUserCode(str,41);
	if(res)
	{
		GbkToUnicode((unsigned char *)str,UserCodeBtnBuffer,USERCODEBTN_SIZE,1);
	}
	res = GetWifiName(str,41);
	if(res)
	{
		GbkToUnicode((unsigned char *)str,WifiNameBtnBuffer,WIFINAMEBTN_SIZE,1);
	}
	res = GetWifiPass(str,41);
	if(res)
	{
		GbkToUnicode((unsigned char *)str,WifiPassBtnBuffer,WIFIPASSBTN_SIZE,1);
	}	
	res = GetScreenPass(str,41);
	if(res)
	{
		GbkToUnicode((unsigned char *)str,ScreenBtnBuffer,SCREENBTN_SIZE,1);
	}
	u8 old = GetScreenSw();
	ScreenSwBtn.forceState(old);
}

void SysSetContain::UpdataUserCode(char *code)
{
	if(code)
	{
		GbkToUnicode((unsigned char *)code,UserCodeBtnBuffer,USERCODEBTN_SIZE,1);
		UserCodeBtn.invalidate();
	}
}

void SysSetContain::UpdataIccid(char *data)
{
	if(data)
	{
		GbkToUnicode((unsigned char *)data,IccidTextBuffer,ICCIDTEXT_SIZE,1);
		IccidText.invalidate();
		qrCode.SetIccid(data);
		qrCode.invalidate();
	}
}

void SysSetContain::UpdataUserIp(char *data)
{
	if(data)
	{
		GbkToUnicode((unsigned char *)data,IpBtnBuffer,IPBTN_SIZE,1);
		IpBtn.invalidate();
	}
}

void SysSetContain::UpdataUserPort(u16 data)
{
	Unicode::snprintf(PortBtnBuffer,PORTBTN_SIZE, "%d",data);
	PortBtn.invalidate();
}


void SysSetContain::DisplayNetType(u8 type)
{
	switch(type)
	{
		case NET_TYPE_WIFI:
			radioButtonWIFI.setSelected(true);
		break;
		case NET_TYPE_GPRS:
			radioButtonGprs.setSelected(true);
		break;
		case NET_TYPE_CLOSE:
			radioButtonClose.setSelected(true);
		break;
	}
}


void SysSetContain::OpenKeyboard(uint32_t keyId)
{
	keyboard->setPosition(140, 0, 310, 318);
	keyboard->okCallback = &okPressed;
	keyboard->cancelCallback=&cancelPressed;
	if(keyboard->keyId == 0)
	{
		if(useSMOCDrawingCallback !=0)
		{
			useSMOCDrawingCallback->execute(true);
		}
		keyboard->keyId=keyId;
		add(*keyboard);
		keyboard->invalidate();
	}
}

void SysSetContain::OpenEnKeyboard(uint32_t keyId)
{
	enKeyboard->setPosition(0, 0, 480, 360);
	enKeyboard->okCallback = &enOkPressed;
	enKeyboard->cancelCallback=&enCancelPressed;
	Log("OpenEnKeyboard keyId:%d",keyId);
	if(enKeyboard->keyId == 0)
	{
		if(useSMOCDrawingCallback !=0)
		{
			useSMOCDrawingCallback->execute(true);
		}
		enKeyboard->keyId=keyId;
		add(*enKeyboard);
		enKeyboard->invalidate();
	}
}

void SysSetContain::okPressedHandler(uint32_t keyId,Unicode::UnicodeChar * str)
{
	#define TEXT_BUTTON touchgfx::WildcardTextButtonStyle< touchgfx::BoxWithBorderButtonStyle< touchgfx::ClickButtonTrigger > > 
	TEXT_BUTTON* btn;//
	btn = (TEXT_BUTTON *)keyId;
	uint8_t sv =0;
	char utfstr[40];
	Unicode::toUTF8(str,(uint8_t *)utfstr,40);
	if(btn !=0)
	{
		
		if(keyId == (uint32_t)&RecoveryBtn)
		{
			char resstr[20];
			if(GetScreenPass(resstr,20))
			{
				if(strcmp(utfstr,resstr) == 0)
				{
					PostFreeEvent(EVENT_OPEN_FACTORY_RESET);
				}
			}
			
			goto brk;
		}
		else if(keyId == (uint32_t)&WifiConfigBtn)
		{
			Unicode::toUTF8(str,(uint8_t *)utfstr,10);
			
			char resstr[40];
			if(GetScreenPass(resstr,40))
			{
				if(strcmp(utfstr,resstr) == 0)
				{
					//WifiStartConfig();
				}
			}
			goto brk;
		}
		

		RTC_TimeTypeDef RTC_TimeStruct;
		RTC_DateTypeDef RTC_DateStruct;
		GetTimeStruct(&RTC_TimeStruct);
    GetDateStruct(&RTC_DateStruct);
		
		if (btn == &YearBtn)
		{		
			uint16_t  value  = 0;
			int num = Unicode::atoi(str);
			value =num-48;
			if(num >0 && num <3000)
			{
				RTC_Set_Date(value,RTC_DateStruct.Month,RTC_DateStruct.Date,0);
				Unicode::snprintf(YearBtnBuffer,YEARBTN_SIZE, "%04d",num);
				sv =1;
			}
			
		}
		else if (btn == &MonthBtn)
		{
			int num = Unicode::atoi(str);
			if(num >0 && num <=12)
			{
				RTC_Set_Date(RTC_DateStruct.Year,num,RTC_DateStruct.Date,0);
				Unicode::snprintf(MonthBtnBuffer,MONTHBTN_SIZE, "%02d",num);
				sv =1;
			}
			
		}
		else if (btn == &DayBtn)
		{
			int num = Unicode::atoi(str);
			if(num >0 && num <=31)
			{
				RTC_Set_Date(RTC_DateStruct.Year,RTC_DateStruct.Month,num,0);
				Unicode::snprintf(DayBtnBuffer,DAYBTN_SIZE, "%02d",num);
				sv =1;
			}
			
		}
		else if (btn == &HourBtn)
		{
			int num = Unicode::atoi(str);
			if(num >=0 && num <24)
			{
				RTC_Set_Time(num,RTC_TimeStruct.Minutes,RTC_TimeStruct.Minutes,RTC_HOURFORMAT12_PM);
				Unicode::snprintf(HourBtnBuffer,HOURBTN_SIZE, "%02d",num);
				sv =1;
			}
			
		}
		else if (btn == &MinBtn)
		{
			int num = Unicode::atoi(str);
			if(num >=0 && num <60)
			{
				RTC_Set_Time(RTC_TimeStruct.Hours,num,RTC_TimeStruct.Minutes,RTC_HOURFORMAT12_PM);
				Unicode::snprintf(MinBtnBuffer,MINBTN_SIZE, "%02d",num);
				sv =1;
			}
			
		}
		else if (btn == &SecBtn)
		{
			int num = Unicode::atoi(str);
			if(num >=0 && num <60)
			{
				RTC_Set_Time(RTC_TimeStruct.Hours,RTC_TimeStruct.Minutes,num,RTC_HOURFORMAT12_PM);
				Unicode::snprintf(SecBtnBuffer,SECBTN_SIZE, "%02d",num);
				sv =1;
			}
			
		}
		else if (btn == &PortBtn)
		{
			int num = Unicode::atoi(str);
			if(SetPort(num))
			{
				Unicode::snprintf(PortBtnBuffer,PORTBTN_SIZE, "%d",num);
				sv =1;
				PostFreeEvent(EVENT_SAVE_NET_FLASH);
			}
		}
		else if(btn == &ScreenBtn)
		{
			if(SetScreenPass(utfstr))
			{
				Unicode::strncpy(ScreenBtnBuffer,str,SCREENBTN_SIZE);
				sv =1;
				PostFreeEvent(EVENT_SAVE_SYS_FLASH);
			}
		}
		
		if(sv ==1)
		{
			btn->invalidate();
			
		}
		
	}
	brk:
	remove(*keyboard);
	keyboard->invalidate();
	keyboard->ClearKeyBoard();
	if(useSMOCDrawingCallback !=0)
	{
		useSMOCDrawingCallback->execute(false);
	}
	this->invalidate();
}


void SysSetContain::enOkPressedHandler(uint32_t keyId,Unicode::UnicodeChar * str)
{
	#define TEXT_BUTTON touchgfx::WildcardTextButtonStyle< touchgfx::BoxWithBorderButtonStyle< touchgfx::ClickButtonTrigger > > 
	TEXT_BUTTON* btn;//
	btn = (TEXT_BUTTON *)keyId;
	uint8_t sv =0;
	char utfstr[40];
	Unicode::toUTF8(str,(uint8_t *)utfstr,40);
	if(btn !=0)
	{
		if (btn == &IpBtn)
		{
			if(SetIp(utfstr))
			{
				Unicode::strncpy(IpBtnBuffer,str,IPBTN_SIZE);
				sv =1;
				PostFreeEvent(EVENT_SAVE_NET_FLASH);
			}
			
		}
		else if (btn == &UserCodeBtn)
		{
			if(SetUserCode(utfstr))
			{
				Unicode::strncpy(UserCodeBtnBuffer,str,USERCODEBTN_SIZE);
				sv =1;
				PostFreeEvent(EVENT_SAVE_DEVICE_FLASH);
			}
			
		}
		else if(btn == &WifiNameBtn)
		{
			if(SetWifiName(utfstr))
			{
				Unicode::strncpy(WifiNameBtnBuffer,str,WIFINAMEBTN_SIZE);
				sv =1;
				PostFreeEvent(EVENT_SAVE_NET_FLASH);
			}
		}
	  else if(btn == &WifiPassBtn)
		{
			if(SetWifiPass(utfstr))
			{
				Unicode::strncpy(WifiPassBtnBuffer,str,WIFIPASSBTN_SIZE);
				sv =1;
				PostFreeEvent(EVENT_SAVE_NET_FLASH);
			}
		}
		
		if(sv ==1)
		{
			btn->invalidate();
			
		}
		
	}
	brk:
	remove(*enKeyboard);
	enKeyboard->invalidate();
	enKeyboard->ClearKeyBoard();
	if(useSMOCDrawingCallback !=0)
	{
		useSMOCDrawingCallback->execute(false);
	}
	this->invalidate();
}
void SysSetContain::cancelPressedHandler()
{
	if(useSMOCDrawingCallback !=0)
	{
		useSMOCDrawingCallback->execute(false);
	}
	keyboard->ClearKeyBoard();
	remove(*keyboard);
	keyboard->invalidate();

	this->invalidate();
}

void SysSetContain::enCancelPressedHandler()
{
	if(useSMOCDrawingCallback !=0)
	{
		useSMOCDrawingCallback->execute(false);
	}
	enKeyboard->ClearKeyBoard();
	remove(*enKeyboard);
	enKeyboard->invalidate();

	this->invalidate();
}
/*
 * Custom Action Handlers
 */
void SysSetContain::YearBtnClick()
{
	// Override and implement this function in SysSetContain
	OpenKeyboard((uint32_t )&YearBtn);
}

void SysSetContain::MonthBtnClick()
{
	// Override and implement this function in SysSetContain
	OpenKeyboard((uint32_t )&MonthBtn);
}

void SysSetContain::DayBtnClick()
{
	// Override and implement this function in SysSetContain
	OpenKeyboard((uint32_t )&DayBtn);
}

void SysSetContain:: HourBtnClick()
{
	// Override and implement this function in SysSetContain
	OpenKeyboard((uint32_t )&HourBtn);
}

void SysSetContain::MinBtnClick()
{
	// Override and implement this function in SysSetContain
	OpenKeyboard((uint32_t )&MinBtn);
}

void SysSetContain:: SecBtnClick()
{
	// Override and implement this function in SysSetContain
	OpenKeyboard((uint32_t )&SecBtn);
}

void SysSetContain::WifiConfigBtnClick()
{
	//printf("-->TempAlineBtnClick\r\n");
	//OpenKeyboard((uint32_t )&WifiConfigBtn);
//	WifiStartConfig();
}

void SysSetContain::IpBtnClick()
{
		// Override and implement this function in SysSetContain
			//OpenKeyboard((uint32_t )&IpBtn);
		Log("IpBtnClick");
		OpenEnKeyboard((uint32_t )&IpBtn);
}

 void SysSetContain::UserCodeBtnClick()
{
		// Override and implement this function in SysSetContain
		OpenEnKeyboard((uint32_t )&UserCodeBtn);
}

void SysSetContain::PortBtnClick()
{
		// Override and implement this function in SysSetContain
		OpenKeyboard((uint32_t )&PortBtn);
}

void SysSetContain::WifiNameBtnClick()
{
	OpenEnKeyboard((uint32_t )&WifiNameBtn);
}
void SysSetContain::WifiPassBtnClick()
{
	OpenEnKeyboard((uint32_t )&WifiPassBtn);
}
void SysSetContain::ScreenBtnClick()
{
	OpenKeyboard((uint32_t )&ScreenBtn);
}
void SysSetContain::ScreenSwBtnClick()
{
	u8 old = GetScreenSw();
	SetScreenSw(!old);
	ScreenSwBtn.forceState(!old);
	PostFreeEvent(EVENT_SAVE_SYS_FLASH);
}

void SysSetContain::RecoveryBtnClick()
{
	OpenKeyboard((uint32_t)(&RecoveryBtn));
}

void SysSetContain::RadioButtonWifiSelect()
{
	Log("RadioButtonWifiSelect");
	if(selectActive)
	{
		if(netTypeChangeCallback !=0)
		{
			netTypeChangeCallback->execute(NET_TYPE_WIFI);
		}
	}
	else
	{
		selectActive=true;
	}
}

void SysSetContain::RadioButtonGprsSelect(void)
{
	Log("RadioButtonGprsSelect:%d",selectActive);
	if(selectActive)
	{
		if(netTypeChangeCallback !=0)
		{
			netTypeChangeCallback->execute(NET_TYPE_GPRS);
		}
	}
	else
	{
		selectActive=true;
	}

}

void SysSetContain::RadioButtonCloseSelect()
{
	Log("RadioButtonCloseSelect");
	if(selectActive)
	{
		if(netTypeChangeCallback !=0 )
		{
			netTypeChangeCallback->execute(NET_TYPE_CLOSE);
		}	
	}
	else
	{
		selectActive=true;
	}
}

void SysSetContain::RadioButtonWifiDeSelect()
{
	//Log("RadioButtonWifiDeSelect");
}

void SysSetContain::RadioButtonGprsDeSelect()
{
	//Log("RadioButtonGprsDeSelect");
}

void SysSetContain::RadioButtonCloseDeSelect()
{
	//Log("RadioButtonCloseDeSelect");
}

