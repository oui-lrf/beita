#include <gui/containers/Head.hpp>
#include "GuiInclude.h"


Head::Head()
{

}

void Head::initialize()
{
		UpdataTime();
    HeadBase::initialize();
}

void Head::UpdataTime()
{
	 	RTC_TimeTypeDef RTC_TimeStruct;
  	RTC_DateTypeDef RTC_DateStruct;
		GetTimeStruct(&RTC_TimeStruct);
    GetDateStruct(&RTC_DateStruct);
    Unicode::snprintf(textYearBuffer, 12, "%02d-%02d-20%02d", RTC_DateStruct.Date,RTC_DateStruct.Month,RTC_DateStruct.Year);
		Unicode::snprintf(textTimeBuffer, 10, "%02d:%02d:%02d", RTC_TimeStruct.Hours,RTC_TimeStruct.Minutes,RTC_TimeStruct.Seconds);
	//	Log("UpdataTime");
		textYear.invalidate();
    textTime.invalidate();
}
void Head::UpdataNetSta(NET_STA sta)
{
	switch(sta)
	{
		case NET_STA_CLOSE:
			GbkToUnicode((unsigned char *)"关闭",textNetStaBuffer,TEXTNETSTA_SIZE,1);
		break;
	  case NET_STA_OPENING:
			GbkToUnicode((unsigned char *)"打开中",textNetStaBuffer,TEXTNETSTA_SIZE,1);
		break;
		case NET_STA_CONNECTEDING:
			GbkToUnicode((unsigned char *)"连接中",textNetStaBuffer,TEXTNETSTA_SIZE,1);
		break;
		case NET_STA_CONNECTED:
			GbkToUnicode((unsigned char *)"已连接",textNetStaBuffer,TEXTNETSTA_SIZE,1);
		break;
	}
  textNetSta.invalidate();
	
}

void Head::UpdataNetSignal(u16 signal)
{
	Unicode::snprintf(textNetSignalBuffer, TEXTNETSIGNAL_SIZE, "%d",signal);
  textNetSignal.invalidate();
}

void Head::UpdataNetType(NET_TYPE type)
{
	switch(type)
	{
		case NET_TYPE_WIFI:
			Unicode::snprintf(textNetTypeBuffer, TEXTNETTYPE_SIZE, "WIFI");
		break;
		case NET_TYPE_GPRS:
			Unicode::snprintf(textNetTypeBuffer, TEXTNETTYPE_SIZE, "GPRS");
		break;
		case NET_TYPE_CLOSE:
			Unicode::snprintf(textNetTypeBuffer, TEXTNETTYPE_SIZE, "");
			Unicode::snprintf(textNetSignalBuffer, TEXTNETSIGNAL_SIZE, "0");
			textNetSignal.invalidate();
		break;
	}
  textNetType.invalidate();
}
