#include <gui/containers/ParamSetContain.hpp>
#include "GuiInclude.h"


ParamSetContain::ParamSetContain():okPressed(this, &ParamSetContain::okPressedHandler),cancelPressed(this, &ParamSetContain::cancelPressedHandler),
	enOkPressed(this, &ParamSetContain::enOkPressedHandler),enCancelPressed(this, &ParamSetContain::enCancelPressedHandler)
{
	DisplayParam();	
}

void ParamSetContain::initialize()
{
  ParamSetContainBase::initialize();
}

void ParamSetContain::enOkPressedHandler(uint32_t keyId,Unicode::UnicodeChar * str)
{
	#define TEXT_BUTTON touchgfx::WildcardTextButtonStyle< touchgfx::BoxWithBorderButtonStyle< touchgfx::ClickButtonTrigger > > 
	TEXT_BUTTON* btn;//
	btn = (TEXT_BUTTON *)keyId;
	uint8_t sv =0;
	char utfstr[40];
	Unicode::toUTF8(str,(uint8_t *)utfstr,40);
	u16 len = Unicode::strlen(str);

		if(btn == &RemoteIpBtn && len >0 && len < 18)
		{
			Unicode::strncpy(RemoteIpBtnBuffer,str,REMOTEIPBTN_SIZE);
			sv =1;
		}
		else if(btn == &LocalIpBtn && len >0 && len < 18)
		{
			Unicode::strncpy(LocalIpBtnBuffer,str,LOCALIPBTN_SIZE);
			sv =1;
		}
		else if(btn == &NameBtn && len >0 && len < 12)
		{
			Unicode::strncpy(NameBtnBuffer,str,NAMEBTN_SIZE);
			sv =1;
		}
		else if(btn == &PassBtn && len >0 && len < 12)
		{
			Unicode::strncpy(PassBtnBuffer,str,PASSBTN_SIZE);
			sv =1;
		}		
		if(sv ==1)
		{
			btn->invalidate();
		}
	remove(*enKeyboard);
	enKeyboard->invalidate();
	enKeyboard->ClearKeyBoard();
	if(useSMOCDrawingCallback !=0)
	{
		useSMOCDrawingCallback->execute(false);
	}
	this->invalidate();
}

void ParamSetContain::okPressedHandler(uint32_t keyId,Unicode::UnicodeChar * str)
{
	#define TEXT_BUTTON touchgfx::WildcardTextButtonStyle< touchgfx::BoxWithBorderButtonStyle< touchgfx::ClickButtonTrigger > > 
	TEXT_BUTTON* btn;//
	btn = (TEXT_BUTTON *)keyId;
	uint8_t sv =0;
	if(btn !=0)
	{
		uint8_t ut8Buf[30]={0};
		Unicode::toUTF8(str,ut8Buf, 10);
		Log("ut8Buf:%s",ut8Buf);
		u16 len = Unicode::strlen(str);

		if(btn == &RemotePortBtn && len >0 && len < 6)
		{
			Unicode::strncpy(RemotePortBtnBuffer,str,REMOTEPORTBTN_SIZE);
			sv =1;
		}
		else if(btn == &LocalPortBtn && len >0 && len < 6)
		{
			Unicode::strncpy(LocalPortBtnBuffer,str,LOCALPORTBTN_SIZE);
			sv =1;
		}
		if(sv ==1)
		{
			btn->invalidate();
		}
	}
	
	remove(*keyboard);
	keyboard->invalidate();
	keyboard->ClearKeyBoard();
	if(useSMOCDrawingCallback !=0)
	{
		useSMOCDrawingCallback->execute(false);
	}
	this->invalidate();
}

void ParamSetContain::cancelPressedHandler()
{
	//printf("ParamSetContain-->cancelPressedHandler\r\n");	
	if(useSMOCDrawingCallback !=0)
	{
		useSMOCDrawingCallback->execute(false);
	}
	keyboard->ClearKeyBoard();
	remove(*keyboard);
	keyboard->invalidate();

	this->invalidate();
}

void ParamSetContain::enCancelPressedHandler()
{
	if(useSMOCDrawingCallback !=0)
	{
		useSMOCDrawingCallback->execute(false);
	}
	enKeyboard->ClearKeyBoard();
	remove(*enKeyboard);
	enKeyboard->invalidate();

	this->invalidate();
}

void ParamSetContain::OpenKeyboard(uint32_t keyId)
{
	keyboard->setPosition(140, 0, 310, 318);
	keyboard->okCallback = &okPressed;
	keyboard->cancelCallback=&cancelPressed;
//	printf("-->keyId:%d\r\n",keyboard->keyId);
	if(keyboard->keyId == 0)
	{
		if(useSMOCDrawingCallback !=0)
		{
			useSMOCDrawingCallback->execute(true);
		}
		keyboard->keyId=keyId;
		add(*keyboard);
		keyboard->invalidate();
	}
}
 
void ParamSetContain::OpenEnKeyboard(uint32_t keyId)
{
	enKeyboard->setPosition(0, 0, 480, 360);
	enKeyboard->okCallback = &enOkPressed;
	enKeyboard->cancelCallback=&enCancelPressed;
	Log("OpenEnKeyboard keyId:%d",keyId);
	if(enKeyboard->keyId == 0)
	{
		if(useSMOCDrawingCallback !=0)
		{
			useSMOCDrawingCallback->execute(true);
		}
		enKeyboard->keyId=keyId;
		add(*enKeyboard);
		enKeyboard->invalidate();
	}
}


void ParamSetContain::DisplayParam()
{
	
//	DeviceParam deviceParam;
//	GetDeviceParam(&deviceParam);
//	GbkToUnicode(deviceParam.sensorType?(unsigned char *)"有泵":(unsigned char *)"无泵",SensorTypeBtnBuffer,SENSORTYPEBTN_SIZE,1);
	if(test)
	{
		GbkToUnicode((unsigned char *)"开",TestBtnBuffer,TESTBTN_SIZE,1);
	}
	else
	{
		GbkToUnicode((unsigned char *)"关",TestBtnBuffer,TESTBTN_SIZE,1);
	}
}
void ParamSetContain::LocalIpBtnClick()
{
	OpenEnKeyboard((uint32_t)(&LocalIpBtn));
}
void ParamSetContain::RemoteIpBtnClick()
{
	OpenEnKeyboard((uint32_t)(&RemoteIpBtn));
}
void ParamSetContain::NameBtnClick()
{
	OpenEnKeyboard((uint32_t)(&NameBtn));
}
void ParamSetContain::PassBtnClick()
{
	OpenEnKeyboard((uint32_t)(&PassBtn));
}


void ParamSetContain::RemotePortBtnClick()
{
	OpenKeyboard((uint32_t)(&RemotePortBtn));
}

void ParamSetContain::LocalPortBtnClick()
{
	OpenKeyboard((uint32_t)(&LocalPortBtn));
}

#include "DeviceRunPort.h"

void ParamSetContain::RemoteOKBtnClick()
{
	char sendBuf[40]={0};
	u8 ip[20]={0};
	u8 port[8]={0};
	Unicode::toUTF8(RemoteIpBtnBuffer,ip,20);
	Unicode::toUTF8(RemotePortBtnBuffer,port,8);
	sprintf(sendBuf,"SetRemoteIp=\"%s\",%s\r\n",ip,port);
	Log(sendBuf);
	CamUsartSend((u8 *)sendBuf,strlen(sendBuf));

}


void ParamSetContain::UserOkBtnClick()
{
	char sendBuf[60]={0};
	u8 name[20]={0};
	u8 pass[20]={0};
	Unicode::toUTF8(NameBtnBuffer,name,20);
	Unicode::toUTF8(PassBtnBuffer,pass,20);
	sprintf(sendBuf,"SetUser=\"%s\",\"%s\"\r\n",name,pass);
	Log(sendBuf);
	CamUsartSend((u8 *)sendBuf,strlen(sendBuf));
}

void ParamSetContain::LocalOKBtnClick()
{
	char sendBuf[40]={0};
	u8 ip[20]={0};
	u8 port[8]={0};
	Unicode::toUTF8(LocalIpBtnBuffer,ip,20);
	Unicode::toUTF8(LocalPortBtnBuffer,port,8);
	sprintf(sendBuf,"SetLocalIp=\"%s\",%s\r\n",ip,port);
	Log(sendBuf);
	CamUsartSend((u8 *)sendBuf,strlen(sendBuf));
}

//void ParamSetContain::UpdataRemoteIp(char *str)
//{
//	GbkToUnicode((unsigned char *)str,RemoteIpBtnBuffer,REMOTEIPBTN_SIZE,1);
//	RemoteIpBtn.invalidate();
//}

void ParamSetContain::UpdataName(char *str)
{
	GbkToUnicode((unsigned char *)str,NameBtnBuffer,NAMEBTN_SIZE,1);
	NameBtn.invalidate();
}

void ParamSetContain::UpdataPass(char *str)
{
	GbkToUnicode((unsigned char *)str,PassBtnBuffer,PASSBTN_SIZE,1);
	PassBtn.invalidate();
}

void ParamSetContain::UpdataRemoteIpSta(bool sta)
{
	if(sta)
	{
		RemoteIpBtn.setWildcardTextColors(touchgfx::Color::getColorFrom24BitRGB(0, 255, 0), touchgfx::Color::getColorFrom24BitRGB(0, 255, 0));
		RemotePortBtn.setWildcardTextColors(touchgfx::Color::getColorFrom24BitRGB(0, 255, 0), touchgfx::Color::getColorFrom24BitRGB(0, 255, 0));
	}
	else
	{
		RemoteIpBtn.setWildcardTextColors(touchgfx::Color::getColorFrom24BitRGB(10, 10, 10), touchgfx::Color::getColorFrom24BitRGB(10, 10, 10));
		RemotePortBtn.setWildcardTextColors(touchgfx::Color::getColorFrom24BitRGB(10, 10, 10), touchgfx::Color::getColorFrom24BitRGB(10, 10, 10));
	}
	RemoteIpBtn.invalidate();
	RemotePortBtn.invalidate();
}

void ParamSetContain::UpdataRemoteIp(char *str)
{
	GbkToUnicode((unsigned char *)str,RemoteIpBtnBuffer,REMOTEIPBTN_SIZE,1);
	RemoteIpBtn.invalidate();
}
void ParamSetContain::UpdataLocalIp(char *str)
{
	GbkToUnicode((unsigned char *)str,LocalIpBtnBuffer,LOCALIPBTN_SIZE,1);
	LocalIpBtn.invalidate();
}
void ParamSetContain::UpdataRemotePort(u16 value)
{
	Unicode::snprintf(RemotePortBtnBuffer,REMOTEPORTBTN_SIZE, "%d",value);
	RemotePortBtn.invalidate();
}

void ParamSetContain::UpdataLocalPort(u16 value)
{
	Unicode::snprintf(LocalPortBtnBuffer,LOCALPORTBTN_SIZE, "%d",value);
	LocalPortBtn.invalidate();
}


void ParamSetContain::TestBtnClick()
{
	test = !test;
	char send[60];
	u16 unicodeBuf[60];
	if(test)
	{
		GbkToUnicode((unsigned char *)"开",TestBtnBuffer,TESTBTN_SIZE,1);
		strcpy(send,"SetName=0,\"车间1 风机:开,净化:开\"");
		Log("cam name:%s",send);
		
		GbkToUnicode((unsigned char *)send,unicodeBuf,60,1);
		Unicode::toUTF8(unicodeBuf,(unsigned char *)send,60);
		CamUsartSend((u8 *)send,strlen(send));
	}
	else
	{
		GbkToUnicode((unsigned char *)"关",TestBtnBuffer,TESTBTN_SIZE,1);
	strcpy(send,"SetName=0,\"车间1 风机:关,净化:关\"");
		Log("cam name:%s",send);
		GbkToUnicode((unsigned char *)send,unicodeBuf,60,1);
		Unicode::toUTF8(unicodeBuf,(unsigned char *)send,60);
		CamUsartSend((u8 *)send,strlen(send));
	} 
	TestBtn.invalidate();

}
