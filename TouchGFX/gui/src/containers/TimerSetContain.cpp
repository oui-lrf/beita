#include <gui/containers/TimerSetContain.hpp>
#include "GuiInclude.h"

TimerSetContain::TimerSetContain():okPressed(this, &TimerSetContain::okPressedHandler),
	cancelPressed(this, &TimerSetContain::cancelPressedHandler)
{
	DisplayParam();
}

void TimerSetContain::initialize()
{
   TimerSetContainBase::initialize();
}
void TimerSetContain::DisplayParam()
{
	DeviceParam deviceParam;
	GetDeviceParam(&deviceParam);
	
	Unicode::snprintf(StartTimeHourBtn1Buffer,STARTTIMEHOURBTN1_SIZE, "%d", deviceParam.openTime1/60);
	Unicode::snprintf(StartTimeHourBtn2Buffer,STARTTIMEHOURBTN2_SIZE, "%d", deviceParam.openTime2/60);
	Unicode::snprintf(StartTimeHourBtn3Buffer,STARTTIMEHOURBTN3_SIZE, "%d", deviceParam.openTime3/60);
	Unicode::snprintf(StartTimeHourBtn4Buffer,STARTTIMEHOURBTN4_SIZE, "%d", deviceParam.openTime4/60);
	Unicode::snprintf(StartTimeMinBtn1Buffer,STARTTIMEMINBTN1_SIZE, "%d", deviceParam.openTime1%60);
	Unicode::snprintf(StartTimeMinBtn2Buffer,STARTTIMEMINBTN2_SIZE, "%d", deviceParam.openTime2%60);
	Unicode::snprintf(StartTimeMinBtn3Buffer,STARTTIMEMINBTN3_SIZE, "%d", deviceParam.openTime3%60);
	Unicode::snprintf(StartTimeMinBtn4Buffer,STARTTIMEMINBTN4_SIZE, "%d", deviceParam.openTime4%60);
	Unicode::snprintf(StopTimeHourBtn1Buffer,STOPTIMEHOURBTN1_SIZE, "%d", deviceParam.closeTime1/60);
	Unicode::snprintf(StopTimeHourBtn2Buffer,STOPTIMEHOURBTN2_SIZE, "%d", deviceParam.closeTime2/60);
	Unicode::snprintf(StopTimeHourBtn3Buffer,STOPTIMEHOURBTN3_SIZE, "%d", deviceParam.closeTime3/60);
	Unicode::snprintf(StopTimeHourBtn4Buffer,STOPTIMEHOURBTN4_SIZE, "%d", deviceParam.closeTime4/60);
	
	Unicode::snprintf(StopTimeMinBtn1Buffer,STOPTIMEMINBTN1_SIZE, "%d", deviceParam.closeTime1%60);
	Unicode::snprintf(StopTimeMinBtn2Buffer,STOPTIMEMINBTN2_SIZE, "%d", deviceParam.closeTime2%60);
	Unicode::snprintf(StopTimeMinBtn3Buffer,STOPTIMEMINBTN3_SIZE, "%d", deviceParam.closeTime3%60);
	Unicode::snprintf(StopTimeMinBtn4Buffer,STOPTIMEMINBTN4_SIZE, "%d", deviceParam.closeTime4%60);
	
	Unicode::snprintf(TimeTempBtn1Buffer,TIMETEMPBTN1_SIZE, "%d", deviceParam.timerUserData1);
	Unicode::snprintf(TimeTempBtn2Buffer,TIMETEMPBTN2_SIZE, "%d", deviceParam.timerUserData2);
	Unicode::snprintf(TimeTempBtn3Buffer,TIMETEMPBTN3_SIZE, "%d", deviceParam.timerUserData3);
	Unicode::snprintf(TimeTempBtn4Buffer,TIMETEMPBTN4_SIZE, "%d", deviceParam.timerUserData4);
	
	TimeSelectBtn1.forceState(deviceParam.timerSta1);
	TimeSelectBtn2.forceState(deviceParam.timerSta2);
	TimeSelectBtn3.forceState(deviceParam.timerSta3);
	TimeSelectBtn4.forceState(deviceParam.timerSta4);
	
}

void TimerSetContain::OpenKeyboard(uint32_t keyId)
{
	keyboard->setPosition(140, 0, 310, 318);
	keyboard->okCallback = &okPressed;
	keyboard->cancelCallback=&cancelPressed;
	
	Log("OpenKeyboard:%d",keyboard->keyId);
	if(keyboard->keyId == 0)
	{
		if(useSMOCDrawingCallback !=0)
		{
			useSMOCDrawingCallback->execute(true);
		}
		keyboard->keyId=keyId;
		add(*keyboard);
		keyboard->invalidate();
	}
}

void TimerSetContain::okPressedHandler(uint32_t keyId,Unicode::UnicodeChar * str)
{
	Log("okPressedHandler keyId:%d,str:%s",keyId,str);
	#define TEXT_BUTTON touchgfx::WildcardTextButtonStyle< touchgfx::BoxWithBorderButtonStyle< touchgfx::ClickButtonTrigger > > 
	TEXT_BUTTON* btn;//
	btn = (TEXT_BUTTON *)keyId;
	uint8_t sv =0;
	if(btn !=0)
	{
		uint16_t  value  = 0;
		int num = Unicode::atoi(str);
		Log("StopTempBtn:%d",num);
		if (btn == &StartTimeHourBtn1)
		{
			value = num*60 + GetOpenTime1()%60;
			if(SetOpenTime1(value))
			{
				Unicode::snprintf(StartTimeHourBtn1Buffer, STARTTIMEHOURBTN1_SIZE, "%d", value/60);
				Unicode::snprintf(StartTimeMinBtn1Buffer, STARTTIMEMINBTN1_SIZE, "%d", value%60);
				StartTimeMinBtn1.invalidate();
				sv =1;
			}
		}
		else if(btn == &StartTimeHourBtn2)
		{
			value = num*60 + GetOpenTime2()%60;
			if(SetOpenTime2(value))
			{
				Unicode::snprintf(StartTimeHourBtn2Buffer, STARTTIMEHOURBTN2_SIZE, "%d", value/60);
				Unicode::snprintf(StartTimeMinBtn2Buffer, STARTTIMEMINBTN2_SIZE, "%d", value%60);
				StartTimeMinBtn2.invalidate();
				sv =1;
			}
		}
		else if (btn == &StartTimeHourBtn3)
		{
			value = num*60 + GetOpenTime3()%60;
			if(SetOpenTime3(value))
			{
				Unicode::snprintf(StartTimeHourBtn3Buffer, STARTTIMEHOURBTN3_SIZE, "%d", value/60);
				Unicode::snprintf(StartTimeMinBtn3Buffer, STARTTIMEMINBTN3_SIZE, "%d", value%60);
				StartTimeMinBtn3.invalidate();
				sv =1;
			}
		}
		else if (btn == &StartTimeHourBtn4)
		{
			value = num*60 + GetOpenTime4()%60;
			if(SetOpenTime4(value))
			{
				Unicode::snprintf(StartTimeHourBtn4Buffer, STARTTIMEHOURBTN4_SIZE, "%d", value/60);
				Unicode::snprintf(StartTimeMinBtn4Buffer, STARTTIMEMINBTN4_SIZE, "%d", value%60);
				StartTimeMinBtn4.invalidate();
				sv =1;
			}
		}
		else if (btn == &StartTimeMinBtn1)
		{
			value  = 60*(GetOpenTime1()/60)+num;
			if(SetOpenTime1(value))
			{
				Unicode::snprintf(StartTimeMinBtn1Buffer, STARTTIMEMINBTN1_SIZE, "%d", value%60);
				Unicode::snprintf(StartTimeHourBtn1Buffer, STARTTIMEHOURBTN1_SIZE, "%d",value/60);
				StartTimeHourBtn1.invalidate();
				sv =1;
			}
		}
		else if (btn == &StartTimeMinBtn2)
		{
			value  = 60*(GetOpenTime2()/60)+num;
			if(SetOpenTime2(value))
			{
				Unicode::snprintf(StartTimeMinBtn2Buffer, STARTTIMEMINBTN2_SIZE, "%d", value%60);
				Unicode::snprintf(StartTimeHourBtn2Buffer, STARTTIMEHOURBTN2_SIZE, "%d", value/60);
				StartTimeHourBtn2.invalidate();
				sv =1;
			}
		}
		else if (btn == &StartTimeMinBtn3)
		{
			value  = 60*(GetOpenTime3()/60)+num;
			if(SetOpenTime3(value))
			{
				Unicode::snprintf(StartTimeMinBtn3Buffer, STARTTIMEMINBTN3_SIZE, "%d", value%60);
				Unicode::snprintf(StartTimeHourBtn3Buffer, STARTTIMEHOURBTN3_SIZE, "%d", value/60);
				StartTimeHourBtn3.invalidate();
				sv =1;
			}
		}
		else if (btn == &StartTimeMinBtn4)
		{
			value  = 60*(GetOpenTime4()/60)+num;
			if(SetOpenTime4(value))
			{
				Unicode::snprintf(StartTimeMinBtn4Buffer, STARTTIMEMINBTN4_SIZE, "%d", value%60);
				Unicode::snprintf(StartTimeHourBtn4Buffer, STARTTIMEHOURBTN4_SIZE, "%d", value/60);
				StartTimeHourBtn4.invalidate();
				sv =1;
			}
		}
		else if (btn == &StopTimeHourBtn1)
		{
			value = num*60 + GetCloseTime1()%60;
			if(SetCloseTime1(value))
			{
				Unicode::snprintf(StopTimeHourBtn1Buffer, STOPTIMEHOURBTN1_SIZE, "%d", value/60);
				Unicode::snprintf(StopTimeMinBtn1Buffer, STOPTIMEMINBTN1_SIZE, "%d", value%60);
				StopTimeMinBtn1.invalidate();
				sv =1;
			}
		}
		else if (btn == &StopTimeHourBtn2)
		{
			value = num*60 + GetCloseTime2()%60;
			if(SetCloseTime2(value))
			{
				Unicode::snprintf(StopTimeHourBtn2Buffer, STOPTIMEHOURBTN2_SIZE, "%d", value/60);
				Unicode::snprintf(StopTimeMinBtn2Buffer, STOPTIMEMINBTN2_SIZE, "%d", value%60);
				StopTimeMinBtn2.invalidate();
				sv =1;
			}
		}
		else if (btn == &StopTimeHourBtn3)
		{
			value = num*60 + GetCloseTime3()%60;
			if(SetCloseTime3(value))
			{
				Unicode::snprintf(StopTimeHourBtn3Buffer, STOPTIMEHOURBTN3_SIZE, "%d", value/60);
				Unicode::snprintf(StopTimeMinBtn3Buffer, STOPTIMEMINBTN3_SIZE, "%d", value%60);
				StopTimeMinBtn3.invalidate();
				sv =1;
			}
		}
		else if (btn == &StopTimeHourBtn4)
		{
			value = num*60 + GetCloseTime4()%60;
			if(SetCloseTime4(value))
			{
				Unicode::snprintf(StopTimeHourBtn4Buffer, STOPTIMEHOURBTN4_SIZE, "%d", value/60);
				Unicode::snprintf(StopTimeMinBtn4Buffer, STOPTIMEMINBTN4_SIZE, "%d", value%60);
				StopTimeMinBtn4.invalidate();
				
				sv =1;
			}
		}
		else if (btn == &StopTimeMinBtn1)
		{
			value  = 60*(GetCloseTime1()/60)+num;
			if(SetCloseTime1(value))
			{
				Unicode::snprintf(StopTimeMinBtn1Buffer, STOPTIMEMINBTN1_SIZE, "%d", value%60);
				Unicode::snprintf(StopTimeHourBtn1Buffer, STOPTIMEHOURBTN1_SIZE, "%d", value/60);
				StopTimeHourBtn1.invalidate();
				sv =1;
			}
		}else if (btn == &StopTimeMinBtn2)
		{
			value  = 60*(GetCloseTime2()/60)+num;
			if(SetCloseTime2(value))
			{
				Unicode::snprintf(StopTimeMinBtn2Buffer, STOPTIMEMINBTN2_SIZE, "%d", value%60);
				Unicode::snprintf(StopTimeHourBtn2Buffer, STOPTIMEHOURBTN2_SIZE, "%d", value/60);
				StopTimeHourBtn2.invalidate();
				sv =1;
			}
		}
		else if (btn == &StopTimeMinBtn3)
		{
			value  = 60*(GetCloseTime3()/60)+num;
			if(SetCloseTime3(value))
			{
				Unicode::snprintf(StopTimeMinBtn3Buffer, STOPTIMEMINBTN3_SIZE, "%d", value%60);
				Unicode::snprintf(StopTimeHourBtn3Buffer, STOPTIMEHOURBTN3_SIZE, "%d", value/60);
				StopTimeHourBtn3.invalidate();
				sv =1;
			}
		}
		else if (btn == &StopTimeMinBtn4)
		{
			value  = 60*(GetCloseTime4()/60)+num;
			if(SetCloseTime4(value))
			{
				Unicode::snprintf(StopTimeMinBtn4Buffer, STOPTIMEMINBTN4_SIZE, "%d", value%60);
				Unicode::snprintf(StopTimeHourBtn4Buffer, STOPTIMEHOURBTN4_SIZE, "%d", value/60);
				StopTimeHourBtn4.invalidate();
				sv =1;
			}
		}
		else if (btn == &TimeTempBtn1)
		{
			if(SetTimeStopTemp1(num))
			{
				Unicode::snprintf(TimeTempBtn1Buffer, TIMETEMPBTN1_SIZE, "%d", num);
				sv =1;
			}
		}
		else if (btn == &TimeTempBtn2)
		{
			if(SetTimeStopTemp2(num))
			{
				Unicode::snprintf(TimeTempBtn2Buffer, TIMETEMPBTN2_SIZE, "%d", num);
				sv =1;
			}
		}
		else if (btn == &TimeTempBtn3)
		{
			if(SetTimeStopTemp3(num))
			{
				Unicode::snprintf(TimeTempBtn3Buffer, TIMETEMPBTN3_SIZE, "%d", num);
				sv =1;
			}
		}
		else if (btn == &TimeTempBtn4)
		{
			if(SetTimeStopTemp4(num))
			{
				Unicode::snprintf(TimeTempBtn4Buffer, TIMETEMPBTN4_SIZE, "%d", num);
				sv =1;
			}
		}
		if(sv ==1)
		{
			btn->invalidate();
			PostFreeEvent(EVENT_SAVE_DEVICE_FLASH);
		}
		
	}
	
	remove(*keyboard);
	keyboard->invalidate();
	keyboard->ClearKeyBoard();
	if(useSMOCDrawingCallback !=0)
	{
		useSMOCDrawingCallback->execute(false);
	}
	this->invalidate();
}

void TimerSetContain::cancelPressedHandler()
{
	Log("cancelPressedHandler");	
	if(useSMOCDrawingCallback !=0)
	{
		useSMOCDrawingCallback->execute(false);
	}
	keyboard->ClearKeyBoard();
	remove(*keyboard);
	keyboard->invalidate();

	this->invalidate();
}
/*
 * Custom Action Handlers
 */
void TimerSetContain::StartTimeHourBtnClick1()
{
	// Override and implement this function in TimerSetContain
	OpenKeyboard((uint32_t )&StartTimeHourBtn1);
}

void TimerSetContain::StartTimeHourBtnClick2()
{
	// Override and implement this function in TimerSetContain
	OpenKeyboard((uint32_t )&StartTimeHourBtn2);
}

void TimerSetContain::StartTimeHourBtnClick3()
{
	// Override and implement this function in TimerSetContain
	OpenKeyboard((uint32_t )&StartTimeHourBtn3);
}

void TimerSetContain::StartTimeHourBtnClick4()
{
	// Override and implement this function in TimerSetContain
	Log("StartTimeHourBtnClick4");
	OpenKeyboard((uint32_t )&StartTimeHourBtn4);
}

void TimerSetContain::StartTimeMinBtnClick1()
{
	// Override and implement this function in TimerSetContain
	Log("StartTimeMinBtnClick1");
	OpenKeyboard((uint32_t )&StartTimeMinBtn1);
}

void TimerSetContain::StartTimeMinBtnClick2()
{
	// Override and implement this function in TimerSetContain
	OpenKeyboard((uint32_t )&StartTimeMinBtn2);
}

void TimerSetContain::StartTimeMinBtnClick3()
{
	// Override and implement this function in TimerSetContain
	OpenKeyboard((uint32_t )&StartTimeMinBtn3);
}

void TimerSetContain::StartTimeMinBtnClick4()
{
	// Override and implement this function in TimerSetContain
	OpenKeyboard((uint32_t )&StartTimeMinBtn4);
}

void TimerSetContain::StopTimeHourBtnClick1()
{
	// Override and implement this function in TimerSetContain
	OpenKeyboard((uint32_t )&StopTimeHourBtn1);
}

void TimerSetContain::StopTimeHourBtnClick2()
{
	// Override and implement this function in TimerSetContain
	OpenKeyboard((uint32_t )&StopTimeHourBtn2);
}

void TimerSetContain::StopTimeHourBtnClick3()
{
	// Override and implement this function in TimerSetContain
	OpenKeyboard((uint32_t )&StopTimeHourBtn3);
}

void TimerSetContain::StopTimeHourBtnClick4()
{
	// Override and implement this function in TimerSetContain
	OpenKeyboard((uint32_t )&StopTimeHourBtn4);
}

void TimerSetContain::StopTimeMinBtnClick1()
{
	// Override and implement this function in TimerSetContain
	OpenKeyboard((uint32_t )&StopTimeMinBtn1);
}

void TimerSetContain::StopTimeMinBtnClick2()
{
	// Override and implement this function in TimerSetContain
	OpenKeyboard((uint32_t )&StopTimeMinBtn2);
}

void TimerSetContain::StopTimeMinBtnClick3()
{
	// Override and implement this function in TimerSetContain
	OpenKeyboard((uint32_t )&StopTimeMinBtn3);
}

void TimerSetContain::StopTimeMinBtnClick4()
{
	// Override and implement this function in TimerSetContain
	OpenKeyboard((uint32_t )&StopTimeMinBtn4);
}

void TimerSetContain::TimeTempBtnClick1()
{
	// Override and implement this function in TimerSetContain
	OpenKeyboard((uint32_t )&TimeTempBtn1);
}

void TimerSetContain::TimeTempBtnClick2()
{
	// Override and implement this function in TimerSetContain
	OpenKeyboard((uint32_t )&TimeTempBtn2);
}

void TimerSetContain::TimeTempBtnClick3()
{
	// Override and implement this function in TimerSetContain
	OpenKeyboard((uint32_t )&TimeTempBtn3);
}

void TimerSetContain::TimeTempBtnClick4()
{
	// Override and implement this function in TimerSetContain
	OpenKeyboard((uint32_t )&TimeTempBtn4);
}


void TimerSetContain::TimeSelectBtnClick1()
{
	// Override and implement this function in TimerSetContain
	SetTimerSta1(!GetTimerSta1());
	PostFreeEvent(EVENT_SAVE_DEVICE_FLASH);
	
}

void TimerSetContain::TimeSelectBtnClick2()
{
	// Override and implement this function in TimerSetContain
	SetTimerSta2(!GetTimerSta2());
	PostFreeEvent(EVENT_SAVE_DEVICE_FLASH);
}

void TimerSetContain::TimeSelectBtnClick3()
{
	// Override and implement this function in TimerSetContain
	SetTimerSta3(!GetTimerSta3());
	PostFreeEvent(EVENT_SAVE_DEVICE_FLASH);
}

void TimerSetContain::TimeSelectBtnClick4()
{
	// Override and implement this function in TimerSetContain
	SetTimerSta4(!GetTimerSta4());
	PostFreeEvent(EVENT_SAVE_DEVICE_FLASH);
}
