#include <gui/containers/OverSetContain.hpp>


OverSetContain::OverSetContain():okPressed(this, &OverSetContain::okPressedHandler),
	cancelPressed(this, &OverSetContain::cancelPressedHandler)
{
	DisplayParam();
}

void OverSetContain::initialize()
{
    OverSetContain::initialize();
}

void OverSetContain::okPressedHandler(uint32_t keyId,Unicode::UnicodeChar * str)
{
	#define TEXT_BUTTON touchgfx::WildcardTextButtonStyle< touchgfx::BoxWithBorderButtonStyle< touchgfx::ClickButtonTrigger > > 
	TEXT_BUTTON* btn;//
	btn = (TEXT_BUTTON *)keyId;
	uint8_t sv =0;
	if(btn !=0)
	{
		uint16_t  value  = 0;
		int num = Unicode::atoi(str);
		
		uint8_t ut8Buf[30]={0};
		Unicode::toUTF8(str,ut8Buf, 10);
		Log("ut8Buf:%s",ut8Buf);
		float fvalue=0.0f;
		//if(strstr((char *)ut8Buf,"."))
		{
			fvalue =std::atof((char *)ut8Buf);
		}
		
		if (btn == &SmokeBtn1)
		{
			if(SetSmoke1(fvalue))
			{
				Unicode::snprintfFloat(SmokeBtn1Buffer, SMOKEBTN1_SIZE, "%.1f", fvalue);
				sv =1;
			}
		}
		else if (btn == &SmokeBtn2)
		{
			if(SetSmoke2(fvalue))
			{
				Unicode::snprintfFloat(SmokeBtn2Buffer, SMOKEBTN2_SIZE, "%.1f", fvalue);
				sv =1;
			}
		}
		else if (btn == &SmokeBtn3)
		{
			if(SetSmoke3(fvalue))
			{
				Unicode::snprintfFloat(SmokeBtn3Buffer, SMOKEBTN3_SIZE, "%.1f", fvalue);
				sv =1;
			}
		}
		else if (btn == &PmBtn1)
		{
			if(SetPm1(fvalue))
			{
				Unicode::snprintfFloat(PmBtn1Buffer, PMBTN1_SIZE, "%.1f", fvalue);
				sv =1;
			}
		}
		else if (btn == &PmBtn2)
		{
			if(SetPm2(fvalue))
			{
				Unicode::snprintfFloat(PmBtn2Buffer, PMBTN2_SIZE, "%.1f", fvalue);
				sv =1;
			}
		}
		else if (btn == &PmBtn3)
		{
			if(SetPm3(fvalue))
			{
				Unicode::snprintfFloat(PmBtn3Buffer, PMBTN3_SIZE, "%.1f", fvalue);
				sv =1;
			}
		}
		else if (btn == &NmhcBtn1)
		{
			if(SetNmhc1(fvalue))
			{
				Unicode::snprintfFloat(NmhcBtn1Buffer, NMHCBTN1_SIZE, "%.1f", fvalue);
				sv =1;
			}
		}
		else if (btn == &NmhcBtn2)
		{
			if(SetNmhc2(fvalue))
			{
				Unicode::snprintfFloat(NmhcBtn2Buffer, NMHCBTN2_SIZE, "%.1f", fvalue);
				sv =1;
			}
		}		
		else if (btn == &NmhcBtn3)
		{
			if(SetNmhc3(fvalue))
			{
				Unicode::snprintfFloat(NmhcBtn3Buffer, NMHCBTN3_SIZE, "%.1f", fvalue);
				sv =1;
			}
		}
		
		if(sv ==1)
		{
			btn->invalidate();
			PostFreeEvent(EVENT_SAVE_DEVICE_FLASH);
		}
	}
	
	remove(*keyboard);
	keyboard->invalidate();
	keyboard->ClearKeyBoard();
	if(useSMOCDrawingCallback !=0)
	{
		useSMOCDrawingCallback->execute(false);
	}
	this->invalidate();
}

void OverSetContain::cancelPressedHandler()
{
	printf("ParamSetContain-->cancelPressedHandler\r\n");	
	if(useSMOCDrawingCallback !=0)
	{
		useSMOCDrawingCallback->execute(false);
	}
	keyboard->ClearKeyBoard();
	remove(*keyboard);
	keyboard->invalidate();

	this->invalidate();
}

void OverSetContain::OpenKeyboard(uint32_t keyId)
{
	keyboard->setPosition(140, 0, 310, 318);
	keyboard->okCallback = &okPressed;
	keyboard->cancelCallback=&cancelPressed;
	if(keyboard->keyId == 0)
	{
		if(useSMOCDrawingCallback !=0)
		{
			useSMOCDrawingCallback->execute(true);
		}
		keyboard->keyId=keyId;
		add(*keyboard);
		keyboard->invalidate();
	}
}

void OverSetContain::DisplayParam()
{
	DeviceParam deviceParam;
	GetDeviceParam(&deviceParam);
	
	Unicode::snprintfFloat(SmokeBtn1Buffer,SMOKEBTN1_SIZE, "%.1f", deviceParam.smoke1);
	Unicode::snprintfFloat(SmokeBtn2Buffer,SMOKEBTN2_SIZE, "%.1f", deviceParam.smoke2);
	Unicode::snprintfFloat(SmokeBtn3Buffer,SMOKEBTN3_SIZE, "%.1f", deviceParam.smoke3);
	
	Unicode::snprintfFloat(PmBtn1Buffer,PMBTN1_SIZE, "%.1f", deviceParam.pm1);	
	Unicode::snprintfFloat(PmBtn2Buffer,PMBTN2_SIZE, "%.1f", deviceParam.pm2);	
	Unicode::snprintfFloat(PmBtn3Buffer,PMBTN3_SIZE, "%.1f", deviceParam.pm3);
	
	Unicode::snprintfFloat(NmhcBtn1Buffer,NMHCBTN1_SIZE, "%.1f", deviceParam.nmhc1);
	Unicode::snprintfFloat(NmhcBtn2Buffer,NMHCBTN2_SIZE, "%.1f", deviceParam.nmhc2);
	Unicode::snprintfFloat(NmhcBtn3Buffer,NMHCBTN3_SIZE, "%.1f", deviceParam.nmhc3);
}





