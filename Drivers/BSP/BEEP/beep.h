#ifndef __BEEP_H__
#define __BEEP_H__
#include "sys.h"
typedef enum
{
	BEEP_OPEN=1,
	BEEP_CLOSE=0,
}BEEP_STA;

#define BEEP PCout(1)   

void beep_init(void);
#endif
