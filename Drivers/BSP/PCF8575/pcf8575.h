#ifndef __PCF8575_H
#define __PCF8575_H

#ifdef __cplusplus
 extern "C" {
#endif

#include "sys.h"
#include "stdbool.h"

#define IO_PCF8575_ADDR 0x40
#define INPUT_PCF8575_ADDR 0x42

uint8_t PCF8575_Check(u8 addr);
void SwitchContrl(u8 addr,uint8_t num, bool ONorOFF);
void Set16BitPin(u8 addr,uint16_t pin);
void CloseAll(u8 addr);
void OpenAll(u8 addr);
u16 PCF8575_ReadTwoByte(u8 addr);
	 
	 
	#define IO_SW1				0
	#define IO_SW2				1
	#define IO_SW3				2
	#define IO_SW4				3
	#define IO_SW5				4
	#define IO_SW6				5
	#define IO_SW7				6
	#define IO_SW8				7
	#define IO_SW9				8
	#define IO_SW10				9
	#define IO_SW11				10
	#define IO_SW12				11
	#define IO_SW13				12
	#define IO_SW14				13
	#define IO_SW15				14
	#define IO_SW16				15
	 
	#define INPUT_SW1 	 6
	#define INPUT_SW2 	 4
	#define INPUT_SW3 	 2
	#define INPUT_SW4 	 0
	#define INPUT_SW5 	 15
	#define INPUT_SW6 	 13
	#define INPUT_SW7 	 11
	#define INPUT_SW8 	 9
	 
#ifdef __cplusplus
 }
#endif
#endif
