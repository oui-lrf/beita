#include "pcf8575.h"
#include <math.h>
#include "i2c.h"

//#define IIC_Start i2c_Start
//#define IIC_Wait_Ack i2c_WaitAck
//#define IIC_Stop i2c_Stop
//#define IIC_NAck i2c_NAck
//#define IIC_Ack i2c_Ack

#define IIC_Send_Byte I2C1_Write
#define IIC_Read_Byte I2C1_Read
//等待应答信号到来
//返回值：1，接收应答失败
//			0，接收应答成功

uint8_t PCF8575_Check(u8 addr)
{
	uint8_t ack;
	u16 pin;
	ack = I2C_Receive((addr)|0X01,(uint8_t *)&pin,sizeof(pin));
	return ack;
}

void Set16BitPin(u8 addr,uint16_t pin)
{
	HAL_I2C_Transmit(addr,(uint8_t *)&pin,sizeof(pin));
}

void SwitchContrl(u8 addr,uint8_t num, bool ONorOFF)
{
	static uint8_t switchState[16];
	static uint8_t realState[16];
	uint16_t i,temp;
	if(num > 0 && num < 17)
	{
		switchState[num-1] = ONorOFF;
		if(num % 2)realState[16 - ((num + 1) / 2)] = ONorOFF;
		else realState[(num / 2) - 1] = ONorOFF;
	}
	for(i = 0; i < 16; i++)
	{
		temp += realState[i] * pow(2,i);
	}
	//Set16BitPin(~temp, ~(temp >> 8));
	Set16BitPin(addr,temp);
}
void CloseAll(u8 addr)
{
	Set16BitPin(addr,0x0000);
}


void OpenAll(u8 addr)
{
	Set16BitPin(addr,0xffff);
}

//读取PCF8574的8位IO值
//返回值:读到的数据
u16 PCF8575_ReadTwoByte(u8 addr)
{				  
	uint16_t pin;
	I2C_Receive((addr)|0X01,(uint8_t *)&pin,sizeof(uint16_t));
	return pin;
}

//读取PCF8574的某个IO的值
//bit：要读取的IO编号,0~7
//返回值:此IO的值,0或1
u8 PCF8574_ReadBit(u8 addr,u16 bit)
{
    u16 data;
    data=PCF8575_ReadTwoByte(addr); //先读取这个8位IO的值 
    if(data&(1<<bit))return 1;
    else return 0;   
}

