/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */     
#include  <stdarg.h>
#include "DeviceRunWork.h"
#include "FreeEvent.h"
#include "iwdg.h"
#include "Debug.h"
#include "stm32f4xx_hal.h"
#include "GuiPort.h"
#include "qpc.h"
#include "qf_port.h"
#include "GuiPort.h"
#include "NetPort.h"
#include "SysConfig.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
static SemaphoreHandle_t  xMutex = NULL;
SemaphoreHandle_t  iicMutex = NULL;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
Q_DEFINE_THIS_MODULE("freertos")
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
/* USER CODE END Variables */
osThreadId GuiTaskHandle;
osThreadId DeviceRunTaskHandle;
osThreadId freeTaskHandle;
osThreadId TcpRouteTaskHandle;
osThreadId TcpWorkTaskHandle;
osThreadId MqttTaskHandle;
osThreadId NetMangTaskHandle;
osMessageQId msgQueueUIHandle;
osMessageQId deviceRunQueueHandle;
osMessageQId freeQueueHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
   void printf_task(void);
/* USER CODE END FunctionPrototypes */

void StartGuiTask(void const * argument);
void StartDeviceRunTask(void const * argument);
void StartFreeTask(void const * argument);
void StartTcpRouteTask(void const * argument);
void StartTcpWorkTask(void const * argument);
void StartMqttTask(void const * argument);
void StartNetMangTask(void const * argument);

extern void MX_FATFS_Init(void);
extern void MX_GRAPHICS_Init(void);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Pre/Post sleep processing prototypes */
void PreSleepProcessing(uint32_t *ulExpectedIdleTime);
void PostSleepProcessing(uint32_t *ulExpectedIdleTime);

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* GetTimerTaskMemory prototype (linked to static allocation support) */
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize );

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);
void vApplicationIdleHook(void);
void vApplicationTickHook(void);
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName);
void vApplicationMallocFailedHook(void);
void vApplicationDaemonTaskStartupHook(void);

/* USER CODE BEGIN 1 */
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
__weak void configureTimerForRunTimeStats(void)
{

}


__weak unsigned long getRunTimeCounterValue(void)
{
return 0;
}
/* USER CODE END 1 */

/* USER CODE BEGIN 2 */


extern portBASE_TYPE IdleTaskHook(void* p);
 void vApplicationIdleHook( void )
{
   /* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
   to 1 in FreeRTOSConfig.h. It will be called on each iteration of the idle
   task. It is essential that code added to this hook function never attempts
   to block in any way (for example, call xQueueReceive() with a block time
   specified, or call vTaskDelay()). If the application makes use of the
   vTaskDelete() API function (as this demo application does) then it is also
   important that vApplicationIdleHook() is permitted to return to its calling
   function, because it is the responsibility of the idle task to clean up
   memory allocated by the kernel to any task that has since been deleted. */
		vTaskSetApplicationTaskTag(NULL, IdleTaskHook);
		QfvApplicationIdleHook();
}

/* USER CODE END 2 */

/* USER CODE BEGIN 3 */
__weak void vApplicationTickHook( void )
{
   /* This function will be called by each tick interrupt if
   configUSE_TICK_HOOK is set to 1 in FreeRTOSConfig.h. User code can be
   added here, but the tick hook is called from an interrupt context, so
   code must not attempt to block, and only the interrupt safe FreeRTOS API
   functions can be used (those that end in FromISR()). */
}
/* USER CODE END 3 */

/* USER CODE BEGIN 4 */
__weak void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName)
{
   /* Run time stack overflow checking is performed if
   configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2. This hook function is
   called if a stack overflow is detected. */
//	 Q_ERROR();
}
/* USER CODE END 4 */

/* USER CODE BEGIN 5 */
__weak void vApplicationMallocFailedHook(void)
{
   /* vApplicationMallocFailedHook() will only be called if
   configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h. It is a hook
   function that will get called if a call to pvPortMalloc() fails.
   pvPortMalloc() is called internally by the kernel whenever a task, queue,
   timer or semaphore is created. It is also called by various parts of the
   demo application. If heap_1.c or heap_2.c are used, then the size of the
   heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
   FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
   to query the size of free heap space that remains (although it does not
   provide information on how the remaining heap might be fragmented). */
}
/* USER CODE END 5 */

/* USER CODE BEGIN DAEMON_TASK_STARTUP_HOOK */
void vApplicationDaemonTaskStartupHook(void)
{
	
}
/* USER CODE END DAEMON_TASK_STARTUP_HOOK */

/* USER CODE BEGIN PREPOSTSLEEP */
__weak void PreSleepProcessing(uint32_t *ulExpectedIdleTime)
{
/* place for user code */ 
}

__weak void PostSleepProcessing(uint32_t *ulExpectedIdleTime)
{
/* place for user code */
}
/* USER CODE END PREPOSTSLEEP */

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];
  
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
	
	 /* Pass out the size of the array pointed to by *ppxIdleTaskStackBuffer.
    * Note that, as the array is necessarily of type StackType_t,
    * configMINIMAL_STACK_SIZE is specified in words, not bytes.
    */
}                   
/* USER CODE END GET_IDLE_TASK_MEMORY */

/* USER CODE BEGIN GET_TIMER_TASK_MEMORY */
static StaticTask_t xTimerTaskTCBBuffer;
static StackType_t xTimerStack[configTIMER_TASK_STACK_DEPTH];
  
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize )  
{
  *ppxTimerTaskTCBBuffer = &xTimerTaskTCBBuffer;
  *ppxTimerTaskStackBuffer = &xTimerStack[0];
  *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
  /* place for user code */
}

static void UserDataInit(void)
{
	SysConfigInit();
	//RecordInit();
}

/* USER CODE END GET_TIMER_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
  uint8_t n;
	UserDataInit();
	static QSubscrList subscrSto[MAX_PUB_SIG];
	static QF_MPOOL_EL(QpEvt) smlPoolSto[200]; /* small pool 6*/
	
	static StackType_t usartWorkStack[256];
	static QEvt const *usartWork_queueSto[20];
	
	static StackType_t tcpWorkStack[256];
	static QEvt const *tcpWork_queueSto[20];
	
	static StackType_t mqttWorkStack[256];
	static QEvt const *mqttWork_queueSto[20];
	
	static StackType_t netWorkStack[512];
	static QEvt const *netWork_queueSto[20];
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
	xMutex = xSemaphoreCreateMutex();
	iicMutex = xSemaphoreCreateMutex();
	CreateDebugMutex();
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of GuiTask */
  osThreadDef(GuiTask, StartGuiTask, osPriorityNormal, 0, 1024);
  GuiTaskHandle = osThreadCreate(osThread(GuiTask), NULL);

  /* definition and creation of DeviceRunTask */
  osThreadDef(DeviceRunTask, StartDeviceRunTask, osPriorityNormal, 0, 256);
  DeviceRunTaskHandle = osThreadCreate(osThread(DeviceRunTask), NULL);

  /* definition and creation of freeTask */
  osThreadDef(freeTask, StartFreeTask, osPriorityIdle, 0, 256);
  freeTaskHandle = osThreadCreate(osThread(freeTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */

  /* USER CODE END RTOS_THREADS */

  /* Create the queue(s) */
  /* definition and creation of msgQueueUI */
  osMessageQDef(msgQueueUI, 30, uint32_t);
  msgQueueUIHandle = osMessageCreate(osMessageQ(msgQueueUI), NULL);

  /* definition and creation of deviceRunQueue */
  osMessageQDef(deviceRunQueue, 10, uint32_t);
  deviceRunQueueHandle = osMessageCreate(osMessageQ(deviceRunQueue), NULL);

  /* definition and creation of freeQueue */
  osMessageQDef(freeQueue, 10, uint32_t);
  freeQueueHandle = osMessageCreate(osMessageQ(freeQueue), NULL);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
	  /* add threads, ... */
		
		UsartWork_ctor();
		//TCP对象，负责对象应用层的TCP需求
		TcpWork_ctor();
		//MQTT，TCP的其中一种应用，管理MQTT通讯逻辑
		MqttWork_ctor();	
//		
//		//网络应用协议层，设备注册，设备控制，数据上传等，对外提供设备管理的操作函数，上传数据进行打包等
		NetWork_ctor();
///////////////////////////////////////////////		
    QF_init();    /* initialize the framework and the underlying RT kernel */

    /* initialize publish-subscribe... */
    QF_psInit(subscrSto, Q_DIM(subscrSto));

    /* initialize event pools... */
    QF_poolInit(smlPoolSto, sizeof(smlPoolSto), sizeof(smlPoolSto[0]));

    /* initialize the Board Support Package
    * NOTE: BSP_init() is called *after* initializing publish-subscribe and
    * event pools, to make the system ready to accept SysTick interrupts.
    * Unfortunately, the STM32Cube code that must be called from BSP,
    * configures and starts SysTick.
    */
    BSP_init();

    /* start the active objects... */
			 
		QActive_setAttr(US_UsartWork, TASK_NAME_ATTR, "UsartWork");
		QACTIVE_START(US_UsartWork,          /* AO to start */
		(uint_fast8_t)(1U), /* QP priority of the AO */
		usartWork_queueSto,               /* event queue storage */
		Q_DIM(usartWork_queueSto),        /* queue length [events] */
		usartWorkStack,                  /* stack storage */
		sizeof(usartWorkStack),          /* stack size [bytes] */
		(QEvt *)0);                  /* initialization event (not used) */
///////////////////////////////////////////////////		

		
		QActive_setAttr(TP_TcpWork, TASK_NAME_ATTR, "TcpWork");
		QACTIVE_START(TP_TcpWork,          /* AO to start */
		(uint_fast8_t)(3U), /* QP priority of the AO */
		tcpWork_queueSto,               /* event queue storage */
		Q_DIM(tcpWork_queueSto),        /* queue length [events] */
		tcpWorkStack,                  /* stack storage */
		sizeof(tcpWorkStack),          /* stack size [bytes] */
		(QEvt *)0);                  /* initialization event (not used) */
		
		QActive_setAttr(MQ_MqttWork, TASK_NAME_ATTR, "MqttWork");
		QACTIVE_START(MQ_MqttWork,          /* AO to start */
		(uint_fast8_t)(4U), /* QP priority of the AO */
		mqttWork_queueSto,               /* event queue storage */
		Q_DIM(mqttWork_queueSto),        /* queue length [events] */
		mqttWorkStack,                  /* stack storage */
		sizeof(mqttWorkStack),          /* stack size [bytes] */
		(QEvt *)0);                  /* initialization event (not used) */
	
		QActive_setAttr(WF_NetWork, TASK_NAME_ATTR, "NetWork");
		QACTIVE_START(WF_NetWork,          /* AO to start */
		(uint_fast8_t)(5U), /* QP priority of the AO */
		netWork_queueSto,               /* event queue storage */
		Q_DIM(netWork_queueSto),        /* queue length [events] */
		netWorkStack,                  /* stack storage */
		sizeof(netWorkStack),          /* stack size [bytes] */
		(QEvt *)0);                  /* initialization event (not used) */
			QF_run();	
			 /* instantiate and start the Blinky active object */	 
			 
  /* USER CODE END RTOS_QUEUES */
}

/* USER CODE BEGIN Header_StartGuiTask */
/**
  * @brief  Function implementing the GuiTask thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_StartGuiTask */
void StartGuiTask(void const * argument)
{
  /* init code for FATFS */
  MX_FATFS_Init();

/* Graphic application */  
  GRAPHICS_MainTask();

  /* USER CODE BEGIN StartGuiTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(10);
  }
  /* USER CODE END StartGuiTask */
}

/* USER CODE BEGIN Header_StartDeviceRunTask */
/**
* @brief Function implementing the DeviceRunTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartDeviceRunTask */
void StartDeviceRunTask(void const * argument)
{
  /* USER CODE BEGIN StartDeviceRunTask */
  /* Infinite loop */
//  ControlInit();
  for(;;)
  {
//		ControlLoop();
//		static uint16_t idl =0;
//		if(idl++ >= 10000)
//		{
//			idl =0;
//		}
//		if(idl%20 == 0)
//		{
//			//IWDG_Refresh();
//		}
//		
//		if(idl%20 == 0)
//		{
//			printf_task();
//		}
		osDelay(1);
  }
  /* USER CODE END StartDeviceRunTask */
}

/* USER CODE BEGIN Header_StartFreeTask */
/**
* @brief Function implementing the freeTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartFreeTask */
void StartFreeTask(void const * argument)
{
  /* USER CODE BEGIN StartFreeTask */
  /* Infinite loop */
	FreeEventInit();
  for(;;)
  {
	FreePoll();
	
  }
  /* USER CODE END StartFreeTask */
}



/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
     
/*
*********************************************************************************************************
*	函 数 名: App_Printf
*	功能说明: 线程安全的printf方式		  			  
*	形    参: 同printf的参数。
*             在C中，当无法列出传递函数的所有实参的类型和数目时,可以用省略号指定参数表
*	返 回 值: 无
*********************************************************************************************************
*/
void  App_Printf(char *format, ...)
{
    char  buf_str[400 + 1];
    va_list   v_args;


    va_start(v_args, format);
   (void)vsnprintf((char       *)&buf_str[0],
                   (size_t      ) sizeof(buf_str),
                   (char const *) format,
                                  v_args);
    va_end(v_args);

	/* 互斥信号量 */
		xSemaphoreTake(xMutex, portMAX_DELAY);

    printf("%s", buf_str);

   	xSemaphoreGive(xMutex);
}

void printf_task(void)
{
	uint8_t ucKeyCode;
	uint8_t pcWriteBuffer[400];
	printf("=================================================\r\n");
	printf("任务名      任务状态 优先级   剩余栈 任务序号\r\n");
	vTaskList((char *)&pcWriteBuffer);
	printf("%s\r\n", pcWriteBuffer);

//	printf("\r\n任务名       运行计数         使用率\r\n");
//	vTaskGetRunTimeStats((char *)&pcWriteBuffer);
//	printf("%s\r\n", pcWriteBuffer);
}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
