#include <STM32F4TouchController.hpp>

/* USER CODE BEGIN BSP user includes */
extern "C" void GT811_Init(void);
extern "C" bool GT811_ReadOnePiont(uint16_t *px, uint16_t *py);
/* USER CODE END BSP user includes */

extern "C"
{

uint32_t LCD_GetXSize();
uint32_t LCD_GetYSize();
}

using namespace touchgfx;

void STM32F4TouchController::init()
{
   /* USER CODE BEGIN F4TouchController_init */

    /* Add code for touch controller Initialization*/
    //BSP_TS_Init(LCD_GetXSize(), LCD_GetYSize());
	GT811_Init();
  /* USER CODE END  F4TouchController_init  */
}

bool STM32F4TouchController::sampleTouch(int32_t& x, int32_t& y)
{
  /* USER CODE BEGIN  F4TouchController_sampleTouch  */
    
    /*TS_StateTypeDef state;
    BSP_TS_GetState(&state);
    if (state.TouchDetected)
    {
        x = state.x;
        y = state.y;
        return true;
    }*/
	uint16_t sx,sy;
  	bool ok = GT811_ReadOnePiont(&sx, &sy);
	x = sx;
	y = sy;
    return ok;

 /* USER CODE END F4TouchController_sampleTouch */    
}
