#include "usart.h"

/* USER CODE BEGIN 0 */
#include "tim.h"

u8 u1rx_data;
u8 u2rx_data;
u8 u4rx_data;
u8 u6rx_data;
/* USER CODE END 0 */

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
UART_HandleTypeDef huart4;
UART_HandleTypeDef huart6;


void (*Usart2BufRecvCallback)(u8 *data,u16 size);
void (*Usart4BufRecvCallback)(u8 *data,u16 size);
void (*Usart6BufRecvCallback)(u8 *data,u16 size);

u8 U2_BUF[MAX_USART2_SIZE+1];
u8 U4_BUF[MAX_USART4_SIZE+1];
u8 U6_BUF[MAX_USART6_SIZE+1];

static uint8_t* u2RecvBuff=U2_BUF;
static uint8_t* u4RecvBuff=U4_BUF;
static uint8_t* u6RecvBuff=U6_BUF;

static uint16_t u2Ctn=0; 
static uint16_t u4Ctn=0; 
static uint16_t u6Ctn=0;

/* USART1 init function */

void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }

}

void MX_USART2_UART_Init(void)
{
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
}

void MX_USART4_UART_Init(void)
{
  huart4.Instance = UART4;
  huart4.Init.BaudRate = 9600;
  huart4.Init.WordLength = UART_WORDLENGTH_8B;
  huart4.Init.StopBits = UART_STOPBITS_1;
  huart4.Init.Parity = UART_PARITY_NONE;
  huart4.Init.Mode = UART_MODE_TX_RX;
  huart4.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart4.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart4) != HAL_OK)
  {
    Error_Handler();
  }
}
/* USART6 init function */

void MX_USART6_UART_Init(void)
{
  huart6.Instance = USART6;
  huart6.Init.BaudRate = 115200;
  huart6.Init.WordLength = UART_WORDLENGTH_8B;
  huart6.Init.StopBits = UART_STOPBITS_1;
  huart6.Init.Parity = UART_PARITY_NONE;
  huart6.Init.Mode = UART_MODE_TX_RX;
  huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart6.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart6) != HAL_OK)
  {
    Error_Handler();
  }
}

void GPIO_INIT_485(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	
	__HAL_RCC_GPIOC_CLK_ENABLE();
	 HAL_GPIO_WritePin(SW485_Port, SW485_Pin, GPIO_PIN_RESET);
	
	GPIO_InitStruct.Pin = SW485_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(SW485_Port, &GPIO_InitStruct);

}

void HAL_UART_MspInit(UART_HandleTypeDef* uartHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(uartHandle->Instance==USART1)
  {
  /* USER CODE BEGIN USART1_MspInit 0 */

  /* USER CODE END USART1_MspInit 0 */
    /* USART1 clock enable */
    __HAL_RCC_USART1_CLK_ENABLE();
  
    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**USART1 GPIO Configuration    
    PA9     ------> USART1_TX
    PA10     ------> USART1_RX 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_9|GPIO_PIN_10;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* USART1 interrupt Init */
    HAL_NVIC_SetPriority(USART1_IRQn, 1, 0);
    HAL_NVIC_EnableIRQ(USART1_IRQn);
  /* USER CODE BEGIN USART1_MspInit 1 */
		
  /* USER CODE END USART1_MspInit 1 */
  }
	else if(uartHandle->Instance==USART2)
  {
    __HAL_RCC_USART2_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    GPIO_InitStruct.Pin = USART2_RX_Pin|USART2_TX_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* USART6 interrupt Init */
		
		__HAL_UART_ENABLE_IT(uartHandle,UART_IT_IDLE);		//开启接收中断
		__HAL_UART_ENABLE_IT(uartHandle,UART_IT_RXNE);		//开启接收中断
		
    HAL_NVIC_SetPriority(USART2_IRQn, 1, 1);
  }
	else if(uartHandle->Instance==UART4)
  {
		
		GPIO_INIT_485();
    __HAL_RCC_UART4_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    GPIO_InitStruct.Pin = USART4_RX_Pin|USART4_TX_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF8_UART4;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* USART6 interrupt Init */
		
		__HAL_UART_ENABLE_IT(uartHandle,UART_IT_IDLE);		//开启接收中断
		__HAL_UART_ENABLE_IT(uartHandle,UART_IT_RXNE);		//开启接收中断
		
    HAL_NVIC_SetPriority(UART4_IRQn, 1, 1);
  }	
  else if(uartHandle->Instance==USART6)
  {
  /* USER CODE BEGIN USART6_MspInit 0 */

  /* USER CODE END USART6_MspInit 0 */
    /* USART6 clock enable */
    __HAL_RCC_USART6_CLK_ENABLE();
  
    __HAL_RCC_GPIOG_CLK_ENABLE();
    /**USART6 GPIO Configuration    
    PG9     ------> USART6_RX
    PG14     ------> USART6_TX 
    */
    GPIO_InitStruct.Pin = USART6_RX_Pin|USART6_TX_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF8_USART6;
    HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

    /* USART6 interrupt Init */
		
		//__HAL_UART_ENABLE_IT(uartHandle,UART_IT_IDLE);		//开启接收中断
		__HAL_UART_ENABLE_IT(uartHandle,UART_IT_RXNE);		//开启接收中断
		
    HAL_NVIC_SetPriority(USART6_IRQn, 2, 2);
    HAL_NVIC_EnableIRQ(USART6_IRQn);
  /* USER CODE BEGIN USART6_MspInit 1 */

  /* USER CODE END USART6_MspInit 1 */
  }
	
}



void HAL_UART_MspDeInit(UART_HandleTypeDef* uartHandle)
{

  if(uartHandle->Instance==USART1)
  {
  /* USER CODE BEGIN USART1_MspDeInit 0 */

  /* USER CODE END USART1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_USART1_CLK_DISABLE();
  
    /**USART1 GPIO Configuration    
    PA9     ------> USART1_TX
    PA10     ------> USART1_RX 
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_9|GPIO_PIN_10);

    /* USART1 interrupt Deinit */
    HAL_NVIC_DisableIRQ(USART1_IRQn);
  /* USER CODE BEGIN USART1_MspDeInit 1 */

  /* USER CODE END USART1_MspDeInit 1 */
  }
  else if(uartHandle->Instance==USART6)
  {
  /* USER CODE BEGIN USART6_MspDeInit 0 */

  /* USER CODE END USART6_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_USART6_CLK_DISABLE();
  
    /**USART6 GPIO Configuration    
    PG9     ------> USART6_RX
    PG14     ------> USART6_TX 
    */
    HAL_GPIO_DeInit(GPIOG, USART6_RX_Pin|USART6_TX_Pin);

    /* USART6 interrupt Deinit */
    HAL_NVIC_DisableIRQ(USART6_IRQn);
  /* USER CODE BEGIN USART6_MspDeInit 1 */

  /* USER CODE END USART6_MspDeInit 1 */
  }
	else if(uartHandle->Instance==USART2)
  {
    __HAL_RCC_USART2_CLK_DISABLE();
    HAL_GPIO_DeInit(GPIOA, USART2_RX_Pin|USART2_TX_Pin);
    HAL_NVIC_DisableIRQ(USART2_IRQn);
  }
	else if(uartHandle->Instance==UART4)
  {
    __HAL_RCC_UART4_CLK_DISABLE();
    HAL_GPIO_DeInit(GPIOA, USART4_RX_Pin|USART4_TX_Pin);
    HAL_NVIC_DisableIRQ(UART4_IRQn);
  }
} 

void Uart_Receive_Init(void)
{
    HAL_UART_Receive_IT(&huart1, &u1rx_data, 1);
		
//    SET_BIT(huart1.Instance->CR1, USART_CR1_PEIE | USART_CR1_RXNEIE);
//    SET_BIT(huart2.Instance->CR1, USART_CR1_PEIE | USART_CR1_RXNEIE);
//    SET_BIT(huart6.Instance->CR1, USART_CR1_PEIE | USART_CR1_RXNEIE);
}


 

void SetUsartRecvBufCallBack(uint8_t id,void (*call)(u8 *data,u16 size))
{
	switch(id)
	{
		case 2:
			Usart2BufRecvCallback = call;
		break;
		case 4:
			Usart4BufRecvCallback = call;
		break;
		case 6:
			Usart6BufRecvCallback = call;
		break;
	}
}



void UsartStop(uint8_t id)
{
	switch(id)
	{
		case 2:
			u2Ctn=0;
			memset(U2_BUF,0,MAX_USART2_SIZE+1);
			HAL_NVIC_DisableIRQ(USART2_IRQn);			
		break;
		case 4:
			u4Ctn=0;
			memset(U4_BUF,0,MAX_USART4_SIZE+1);
			HAL_NVIC_DisableIRQ(UART4_IRQn);			
		break;
		case 6:
			u6Ctn=0;
			memset(U6_BUF,0,MAX_USART6_SIZE+1);
			HAL_NVIC_DisableIRQ(USART6_IRQn);			
		break;
	
	}

}
void UsartStart(uint8_t id)
{
	switch(id)
	{
		case 2:
			memset(U2_BUF,0,MAX_USART2_SIZE+1);
			HAL_NVIC_EnableIRQ(USART2_IRQn);
			HAL_UART_Receive_IT(&huart2, &u2rx_data, 1);						
		break;
		case 4:
			memset(U4_BUF,0,MAX_USART4_SIZE+1);
			HAL_NVIC_EnableIRQ(UART4_IRQn);
			HAL_UART_Receive_IT(&huart4, &u4rx_data, 1);						
		break;
		case 6:
			memset(U6_BUF,0,MAX_USART6_SIZE+1);
			HAL_NVIC_EnableIRQ(USART6_IRQn);
			HAL_UART_Receive_IT(&huart6, &u6rx_data, 1);			
		break;		
	}
}

void USART2_IRQHandler(void)
{
  /* USER CODE BEGIN USART6_IRQn 0 */

    if(__HAL_UART_GET_FLAG(&huart2, UART_FLAG_RXNE) != RESET)
    {	
			uint8_t ch=(uint16_t) READ_REG(huart2.Instance->DR);
			if(u2Ctn<MAX_USART2_SIZE)
			{
				*(u2RecvBuff++)=ch;
				u2Ctn++;
			}
			__HAL_UART_CLEAR_FLAG(&huart2,UART_FLAG_RXNE);
    }
    if(__HAL_UART_GET_FLAG(&huart2, UART_FLAG_IDLE) != RESET)
    {				
      u2RecvBuff=U2_BUF;
			if(Usart2BufRecvCallback)
			{
				Usart2BufRecvCallback(U2_BUF,u2Ctn);
				memset(U2_BUF,0,u2Ctn);
			}
			u2Ctn=0;
      __HAL_UART_CLEAR_IDLEFLAG(&huart2);
    }
}


void UART4_IRQHandler(void)
{
  /* USER CODE BEGIN USART6_IRQn 0 */

    if(__HAL_UART_GET_FLAG(&huart4, UART_FLAG_RXNE) != RESET)
    {	
			uint8_t ch=(uint16_t) READ_REG(huart4.Instance->DR);
			if(u4Ctn<MAX_USART4_SIZE)
			{
				*(u4RecvBuff++)=ch;
				u4Ctn++;
			}
			__HAL_UART_CLEAR_FLAG(&huart4,UART_FLAG_RXNE);
    }
    if(__HAL_UART_GET_FLAG(&huart4, UART_FLAG_IDLE) != RESET)
    {				
      u4RecvBuff=U4_BUF;
			if(Usart4BufRecvCallback)
			{
				Usart4BufRecvCallback(U4_BUF,u4Ctn);
				memset(U4_BUF,0,u4Ctn);
			}
			u4Ctn=0;
      __HAL_UART_CLEAR_IDLEFLAG(&huart4);
    }
}
#include "Debug.h"
void Usart6TimerCallback(void)
{
	if(Usart6BufRecvCallback)
	{
		Usart6BufRecvCallback(u6RecvBuff,u6Ctn);
	}
	//memset(u6RecvBuff,0,u6Ctn);
	u6Ctn=0;
	//u6RecvBuff=U6_BUF;
}

void USART6_IRQHandler(void)
{	
	  if(__HAL_UART_GET_FLAG(&huart6, UART_FLAG_RXNE) != RESET)
    {	
			uint8_t ch=(uint16_t) READ_REG(huart6.Instance->DR);
			if(u6Ctn<MAX_USART6_SIZE)
			{
				u6RecvBuff[u6Ctn++]=ch;
				TIM5_Start(50,Usart6TimerCallback);
			}
			__HAL_UART_CLEAR_FLAG(&huart6,UART_FLAG_RXNE);
    }
//    if(__HAL_UART_GET_FLAG(&huart6, UART_FLAG_IDLE) != RESET)
//    {				
//			TIM5_Start(10,Usart6TimerCallback);
//      __HAL_UART_CLEAR_IDLEFLAG(&huart6);
//    }
}

void UsartSendBuf(uint8_t id ,uint8_t *buffer, uint16_t size)
{
	switch(id)
	{
		case 1:
			HAL_UART_Transmit(&huart1, buffer, size,5000);
		break;
		case 2:
			HAL_UART_Transmit(&huart2, buffer, size,5000);
		break;
		case 4:
			HAL_UART_Transmit(&huart4, buffer, size,5000);
		break;
		case 6:
			HAL_UART_Transmit(&huart6, buffer, size,5000);
		break;
	}
}

void Uart1SendByte(uint8_t byte)
{
    //判断是否发送完
    while(__HAL_UART_GET_FLAG(&huart1, UART_FLAG_TXE) == RESET);
    //也可以使用寄存器
    USART1->DR = byte;
}

int fputc(int ch, FILE *f)
{
	Uart1SendByte((uint8_t)ch);
	return(ch);
}
/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
