#include <touchgfx/widgets/QRCode.hpp>
#include <stdlib.h>
#include "stdio.h"
#include "Debug.h"

//NOTE: in a real application you should override this and utilize a qr code generator

//QRCode::QRCode()
//{
//	Log("QRCode");

//}

//QRCode::~QRCode()
//{
//	Log("~QRCode");

//}
bool QRCode::at(uint16_t x, uint16_t y) const
{
    // a deterministic random value for x,y
    srand(x*123+y*getWidth()*234);
    for(int i = 0; i < 40; i++)
    {
        srand(rand());
    }
    return ((rand() / (float)RAND_MAX) > 0.5);
}

uint16_t QRCode::getWidth() const
{
    return 16;
}

uint16_t QRCode::getHeight() const
{
    return 16;
}

