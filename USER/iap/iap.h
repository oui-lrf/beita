#ifndef __IAP_H__
#define __IAP_H__
#include "sys.h"  
#include "stmflash.h"
#ifdef __cplusplus
 extern "C" {
#endif
typedef  void (*iapfun)(void);				//定义一个函数类型的参数.   
#define FLASH_APP1_ADDR		ADDR_FLASH_SECTOR_6  	//第一个应用程序起始地址(存放在FLASH)
											//保留0X08000000~0X0800FFFF的空间为Bootloader使用(共64KB)	   
void iap_load_app(u32 appxaddr);			//跳转到APP程序执行
void iap_write_appbin(u32 appxaddr,u8 *appbuf,u32 applen);	//在指定地址开始,写入bin

#define UPGRADE_PARAM_ADDR		ADDR_FLASH_SECTOR_5 //128K
typedef enum{
	UP_START_IAP = 0,//准备升级
	UP_FIRST_RUN=1,//正在更新Flash
	UP_RUNING=0xff,//升级完成，正在运行
}UP_STA;

#define SOFT_NAME_LEN 40
#define URL_LEN 100

typedef struct{
	UP_STA upSta;
	u32 download;
	char fileName[SOFT_NAME_LEN+1];
	u16 crc;
	u8 updating;//升级前把标志设置为1，升级后运行监测到为1则表示第一次运行，设置为0
}UpgradedParam;



u8 WritParamToFlash(void);
void UpdataFlashToParam(void);
u32 GetLownLoad(void);
u8 GetUpSta(void);
void SetDownLoad(u32 download);
void SetUpsta(u8 upSta);
void PrintUpgrateParam(void);
u8 WriteBufToFlash(u32 pos,u8 *buf,u32 size);
void RunToIap(char *fileName,u16 crc);
u16 GetSoftId(void);
u8 GetUpdating(void);
void SetUpdating(u8 value);
#ifdef __cplusplus
 }
#endif
#endif







































