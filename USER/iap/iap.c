#include "iap.h" 
#include "sys.h"
#include "bsp.h"
#include "Debug.h"
#include "string.h"
#include "mbCrc.h"
#include "sysConfig.h"
#include "cmsis_os.h"
#include "FreeEvent.h"
iapfun jump2app; 
UpgradedParam upgradedParam;


//THUMB指令不支持汇编内联
//采用如下方法实现执行汇编指令WFI  
__asm void WFI_SET(void)
{
	WFI;		  
}
//关闭所有中断(但是不包括fault和NMI中断)
__asm void INTX_DISABLE(void)
{
	CPSID   I
	BX      LR	  
}
//开启所有中断
__asm void INTX_ENABLE(void)
{
	CPSIE   I
	BX      LR  
}
//设置栈顶地址
//addr:栈顶地址
__asm void MSR_MSP(u32 addr) 
{
	MSR MSP, r0 			//set Main Stack value
	BX r14
}


//跳转到应用程序段
//appxaddr:用户代码起始地址.
void iap_load_app(u32 appxaddr)
{ 
	if(((*(vu32*)appxaddr)&0x2FF00000)==0x20000000)	//检查栈顶地址是否合法.
	{ 
		jump2app=(iapfun)*(vu32*)(appxaddr+4);		//用户代码区第二个字为程序开始地址(复位地址)		
		MSR_MSP(*(vu32*)appxaddr);					//初始化APP堆栈指针(用户代码区的第一个字用于存放栈顶地址)
		jump2app();									//跳转到APP.
	}
}		 

void RunToIap(char *fileName,u16 crc)
{
	//把升级状态设置为0xff
	//然后重启	
	Log("RunToIap fileName:%s,crc:%d",fileName,crc);
	
	if(strlen(fileName)>4 && strlen(fileName) <SOFT_NAME_LEN)
	{
		upgradedParam.upSta = UP_START_IAP;
		strncpy(upgradedParam.fileName,fileName,SOFT_NAME_LEN);
		upgradedParam.crc=crc;
		upgradedParam.updating=0;
		WritParamToFlash();
		osDelay(3000);
		//重启
		NVIC_SystemReset();
	}
	else
	{
		Log("RunToIap err");
	}
}

//把升级文件写入到指定地址
u8 WriteBufToFlash(u32 pos,u8 *buf,u32 size)
{
	//STMFLASH_Write(pos,(u32*)buf,SIZE(size));
	STMFLASH_Write_Byte(pos,(u8*)buf,size);
	return 1;
}

u8 WritParamToFlash(void)
{
	//STMFLASH_Write(UPGRADE_PARAM_ADDR,(u32*)&upgradedParam,SIZE(sizeof(UpgradedParam)));
	STMFLASH_Write_Byte(UPGRADE_PARAM_ADDR,(u8*)&upgradedParam,sizeof(UpgradedParam));
	return 1;
}


void UpdataFlashToParam(void)
{
	bsp_ReadCpuFlash(UPGRADE_PARAM_ADDR,(uint8_t *)&upgradedParam, sizeof(UpgradedParam));
	//STMFLASH_Read(UPGRADE_PARAM_ADDR,(uint32_t *)&upgradedParam, sizeof(UpgradedParam));
}

u32 GetLownLoad(void)
{
	return upgradedParam.download;
}

u8 GetUpSta(void)
{
	return upgradedParam.upSta;
}


void SetDownLoad(u32 download)
{
	upgradedParam.download = download;
}

void SetUpsta(u8 upSta)
{
	upgradedParam.upSta = upSta;
}


void SetUpdating(u8 value)
{
	upgradedParam.updating = value;
}

u8 GetUpdating(void)
{
	return upgradedParam.updating;
}

void PrintUpgrateParam(void)
{
	printf("Sta:%d\r\n",upgradedParam.upSta);
	printf("down:%d\r\n",upgradedParam.download);
	printf("name:%s\r\n",upgradedParam.fileName);
	printf("crc:%d\r\n",upgradedParam.crc);
	printf("updating:%d\r\n",upgradedParam.updating);
}


u16 GetFlashCrc16(void)
{
	return upgradedParam.crc;
}
