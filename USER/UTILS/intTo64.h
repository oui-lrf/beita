#pragma once
#include "sys.h"
#ifdef __cplusplus
 extern "C" {
#endif
int intTo64(char str[], const u32 num);
int _64strToInt(unsigned long long *num, const char str[]);
#ifdef __cplusplus
 }
#endif
