#pragma once
#include "stdio.h"
#include "sys.h"
#ifdef __cplusplus
 extern "C" {
#endif

#if 1
	#define Log(format,args...) \
	 WaitDebugMutex();\
	 MyDebug(format,##args);\
	 printf("[%s:%d]\r\n",__FILE__,__LINE__);\
	 ReleaseDebugMutex()
#else
    #define Log(format,args...) 
#endif
	 
#if 1
	 
	#define LogHex(name,pdata,size) \
	 WaitDebugMutex();\
	 PrintHex(name,pdata,size);\
	 printf("[%s:%d]\r\n",__FILE__,__LINE__);\
	 ReleaseDebugMutex()
#else
		#define LogHex(...)
#endif

void MyDebug(const char *format,...);
void WaitDebugMutex(void);
void ReleaseDebugMutex(void);	 
void CreateDebugMutex(void);	 
void DelayUs(unsigned int us);
void PrintHex(char *name,u8 *data,u16 size);
	 
	 #ifdef __cplusplus
 }
#endif
