#include "StringUtils.h"
#include "string.h"

// 返回子串t在母串s中出现时的首字符位置，不出现时返回0
int substr(char s[], char t[]) 
{
    int i = 0,j,k,pos,flag;
    while(s[i]) {
        if(s[i] == t[0]) {
            flag = 1;
            k = pos = i + 1;
            for(j = 1; t[j] && flag; ++k,++j)
                flag = s[k] == t[j];
            if(flag) return pos;
        }
        ++i;
    }
    return 0;
}

//total 要查找的源数组
//left 左边字符串
//right
//buffer  返回数组

char *picked(char total[], char left[], char right[],char *buffer) 
	{
    int i,j,hasleft,hasright;
    hasleft = substr(total,left);
    hasright = substr(total,right);
    printf("left = %d, right = %d\n",hasleft,hasright);
    if(hasleft == 0 && hasright == 0) // 前后串都没找到时，返回全部
        strcpy(buffer,total);
    else if(hasleft == 0) { // 只找到后串时，返回后串的前面部分
        for(i = 0; i < hasright - 1; ++i)
            buffer[i] = total[i];
        buffer[i] = 0;
    }
    else if(hasright == 0) { // 只找到前串时，返回前串后面部分
        for(i = hasleft + strlen(left) - 1,j = 0; total[i]; ++i,++j)
            buffer[j] = total[i];
        buffer[j] = 0;
    }
    else { // 前后串都找到时，返回两串的中间部分
        for(i = hasleft + strlen(left) - 1,j = 0; i < hasright - 1; ++i,++j)
            buffer[j] = total[i];
        buffer[j] = 0;
    }
    return buffer;
}
	
char*  strfind(char *find,char *start,char *end,char *resBuf,u16 resBufSize)
{
	char *findStart = strstr(find,start);
	if(findStart <=0)
	{
		return 0;
	}
	findStart += strlen(start);
	//如果能找到结束字符串，就把改位置设置为结束位置，否则就以源字符串的结束为end
	char *findEnd = strstr(findStart,end);
	int findLen ;
	if(findEnd)
	{
		findLen = findEnd -findStart;
	}
	else
	{
		findLen = strlen(findStart);
	}

	if(findLen >resBufSize-1)
	{
		findLen = resBufSize-1;
	}
	
	memcpy(resBuf,findStart,findLen);
	resBuf[findLen]=0;
	return findStart;
}
	

void U8ToString(char *Stringbuf,u8 *u8Data,u16 DataLen)
{
	memcpy(Stringbuf,u8Data,DataLen);
	Stringbuf[DataLen]=0;
}

