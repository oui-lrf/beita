#include "utils.h"
#include "stdio.h"

u8 IntToStr(char *str,u8 size,int value,int len)
{
	char spf[10];
	if(size <1)
	{
		return 0;
	}
	if(len>0)
	{
		sprintf(spf,"%s%dd","%0",len);
		sprintf(str,spf,value);
	}
	else
	{
		sprintf(str,"%d",value);
	}
	return 1;
}

u8 FloatToStr(char *str,u8 size,float value,int len)
{
	char text[20];
	char spf[10];
	if(size <1)
	{
		return 0;
	}
	if(len>0)
	{
		sprintf(spf,"%s%df","%.",len);
		sprintf(text,spf,value);
	}
	else
	{
		sprintf(text,"%f",value);
	}
	return 1;
}

