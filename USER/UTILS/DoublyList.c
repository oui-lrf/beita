#include "DoublyList.h"
#include "malloc.h"
#include "debug.h"
#include "string.h"

//要定义一个应用结构体，用于保存头结点，尾部结点，删除回调函数

#define ListMalloc(n) mymalloc(SRAMIN,n)
#define ListFree(n) myfree(SRAMIN,n)



void* OmciGetNodeData(T_OMCI_LIST_NODE *pNode);

/**********************************************************************
* 函数名称： CreateListNode
* 功能描述： 创建新的链表结点
* 输入参数： T_OMCI_LIST *pList :链表指针
*           void *pvNodeData  :待插入的链表结点数据指针
* 输出参数： NA
* 返 回 值： T_OMCI_LIST_NODE*
***********************************************************************/
static T_OMCI_LIST_NODE *CreateListNode(T_OMCI_LIST *pList, void *pvNodeData)
{
    T_OMCI_LIST_NODE *pInsertNode = (T_OMCI_LIST_NODE*)ListMalloc(sizeof(T_OMCI_LIST_NODE)+pList->dwNodeDataSize);
    if(NULL == pInsertNode)
    {
        printf("[%s]pList(%p) failed to alloc for pInsertNode!\n", FUNC_NAME, pList);
        return NULL;
    }

    pInsertNode->pvNodeData = (uint8_t *)pInsertNode + sizeof(T_OMCI_LIST_NODE);
    if(NULL != pvNodeData)
    {   //创建非头结点时
        memmove(pInsertNode->pvNodeData, pvNodeData, pList->dwNodeDataSize);
    }

    return pInsertNode;
}

/**********************************************************************
* 函数名称： CreateListNode
* 功能描述： 创建新的链表结点
* 输入参数： T_OMCI_LIST *pList :链表指针
*           void *pvNodeData  :待插入的链表结点数据指针
* 输出参数： NA
* 返 回 值： T_OMCI_LIST_NODE*
***********************************************************************/
T_OMCI_LIST_NODE *CreateListNodeDataSize(void *pvNodeData,uint32_t pvNodeDataSize)
{
    T_OMCI_LIST_NODE *pInsertNode = (T_OMCI_LIST_NODE*)ListMalloc(sizeof(T_OMCI_LIST_NODE)+pvNodeDataSize);
    if(NULL == pInsertNode)
    {
        printf("[%s]pList failed to alloc for pInsertNode!\n", FUNC_NAME);
        return NULL;
    }

    pInsertNode->pvNodeData = (uint8_t *)pInsertNode + sizeof(T_OMCI_LIST_NODE);
    if(NULL != pvNodeData)
    {   //创建非头结点时
        memmove(pInsertNode->pvNodeData, pvNodeData, pvNodeDataSize);
    }

    return pInsertNode;
}


/**********************************************************************
* 函数名称： RemoveListNode
* 功能描述： 删除指定的链表结点(释放结点内存并置其前驱后继指针为NULL)
* 输入参数： T_OMCI_LIST *pList :链表指针
*           void *pvNode       :待删除的链表结点指针
* 输出参数： NA
* 返 回 值： LIST_STATUS
* 注意事项： 本函数未置待删除结点指针为NULL，请避免访问已删除结点
***********************************************************************/
LIST_STATUS RemoveListNode(T_OMCI_LIST *pList, T_OMCI_LIST_NODE *pNode)
{
		if(pList->deleteFunc)pList->deleteFunc(OmciGetNodeData(pNode));
    ListFree(pNode);  //释放链表结点
    return OMCI_LIST_OK;
}

/**********************************************************************
* 函数名称： DestroyListNode
* 功能描述： 销毁指定的链表结点(释放结点内存并置结点指针为NULL)
* 输入参数： T_OMCI_LIST *pList :链表指针
*           void **pNode       :待销毁的链表结点指针的指针
* 输出参数： NA
* 返 回 值： LIST_STATUS
* 注意事项： 当指向待销毁结点的指针存在多份拷贝且散布程序各处时(尤其当
*           调用链未能保证**pNode指向原始结点时)，无法彻底销毁该结点
***********************************************************************/
static LIST_STATUS DestroyListNode(T_OMCI_LIST *pList, T_OMCI_LIST_NODE **pNode)
{
		if(pList->deleteFunc && *pNode != pList->pHead )pList->deleteFunc(GET_NODE_DATA(*pNode));
	
    ListFree(*pNode);  //释放链表结点
    *pNode = NULL;

    return OMCI_LIST_OK;
}



/**********************************************************************
* 函数名称： GetListOccupation
* 功能描述： 获取链表占用情况
* 输入参数： T_OMCI_LIST *pList :链表指针
* 输出参数： NA
* 返 回 值： LIST_OCCUPATION
* 注意事项： 本函数仅用于内部测试。
***********************************************************************/
static LIST_OCCUPATION GetListOccupation(T_OMCI_LIST *pList)
{
    CHECK_SINGLE_POINTER(pList, OMCI_LIST_NULL);
    CHECK_SINGLE_POINTER(pList->pHead, OMCI_LIST_NULL);

    return (0 == pList->dwNodeNum) ? OMCI_LIST_EMPTY : OMCI_LIST_OCCUPIED;
}

/**********************************************************************
* 函数名称： OmciInitList
* 功能描述： 链表初始化，产生空的双向循环链表
* 输入参数： T_OMCI_LIST *pList    :链表指针
*           uint32_t dwNodeDataSize :链表结点保存的数据字节数
* 输出参数： NA
* 返 回 值： LIST_STATUS
***********************************************************************/
LIST_STATUS OmciInitList(T_OMCI_LIST *pList, uint32_t dwNodeDataSize,uint32_t maxNodeNum,void (*deleteFunc)(void *))
{
    CHECK_SINGLE_POINTER(pList, OMCI_LIST_ERROR);

    if(0 == dwNodeDataSize)
    {
        printf("[%s]pList=%p, dwNodeDataSize=%uBytes, undesired initialization!\n",
               FUNC_NAME, pList, dwNodeDataSize);
        return OMCI_LIST_ERROR;
    }
    pList->dwNodeDataSize = dwNodeDataSize;  //给予重新修改结点数据大小的机会

    if(NULL != pList->pHead)
    {
        printf("[%s]pList(%p) has been initialized!\n", FUNC_NAME, pList);
        return OMCI_LIST_OK;
    }

    T_OMCI_LIST_NODE *pHeadNode = CreateListNode(pList, NULL);
    if(NULL == pHeadNode)
    {
        printf("[%s]pList(%p) failed to create pHeadNode!\n", FUNC_NAME, pList);
        return OMCI_LIST_ERROR;
    }

    OMCI_INIT_NODE(pHeadNode);
    pList->pHead = pList->pTail = pHeadNode;
    pList->dwNodeNum = 0;
		pList->maxNodeNum =maxNodeNum;
		pList->deleteFunc = deleteFunc;
    return OMCI_LIST_OK;
}

/**********************************************************************
* 函数名称： OmciClearList
* 功能描述： 清空双向循环链表除头结点外的结点
* 输入参数： T_OMCI_LIST *pList :链表指针
* 输出参数： NA
* 返 回 值： LIST_STATUS
* 注意事项： 清空链表结点后，再次插入结点时不需要初始化链表。
***********************************************************************/
LIST_STATUS OmciClearList(T_OMCI_LIST *pList)
{
    LIST_ITER_CHECK(pList, OMCI_LIST_ERROR);

    T_OMCI_LIST_NODE *pNextNode, *pListNode = pList->pHead->pNext;
    while(pListNode != pList->pHead)
    {
        pNextNode = pListNode->pNext;
        RemoveListNode(pList, pListNode);
        pListNode = pNextNode;
    }

    OMCI_INIT_NODE(pList->pHead);
    pList->pTail = pList->pHead;
    pList->dwNodeNum = 0;

    return OMCI_LIST_OK;
}


/**********************************************************************
* 函数名称： OmciDestroyList
* 功能描述： 销毁双向循环链表，包括头结点
* 输入参数： T_OMCI_LIST *pList :链表指针
* 输出参数： NA
* 返 回 值： LIST_STATUS
* 注意事项： 销毁链表后，再次插入结点时需要初始化链表。
***********************************************************************/
LIST_STATUS OmciDestroyList(T_OMCI_LIST *pList)
{		
    LIST_ITER_CHECK(pList, OMCI_LIST_ERROR);

    T_OMCI_LIST_NODE *pNextNode, *pListNode = pList->pHead->pNext;
    while(pListNode != pList->pHead)
    {
        pNextNode = pListNode->pNext;
        DestroyListNode(pList, &pListNode);
        pListNode = pNextNode;
    }

    DestroyListNode(pList, &(pList->pHead)); //销毁头结点
    pList->pTail = NULL;                     //尾结点指针置空
    pList->dwNodeNum = 0;
    pList->dwNodeDataSize = 0;

    return OMCI_LIST_OK;
}


/**********************************************************************
* 函数名称： OmciIsListEmpty
* 功能描述： 判断链表是否为空(仅含头结点或不含任何结点)
* 输入参数： T_OMCI_LIST *pList :链表指针
* 输出参数： NA
* 返 回 值： BOOL
***********************************************************************/
BOOL OmciIsListEmpty(T_OMCI_LIST *pList)
{
    CHECK_SINGLE_POINTER(pList, OMCI_LIST_TRUE);
    CHECK_SINGLE_POINTER(pList->pHead, OMCI_LIST_TRUE);

    T_OMCI_LIST_NODE *pHeadNode = pList->pHead;
    if((0 == pList->dwNodeNum) &&
       (pHeadNode->pPrev == pHeadNode) && //冗余校验以加强安全性
       (pHeadNode->pNext == pHeadNode))
    {
        return OMCI_LIST_TRUE;
    }
    else
    {
        return OMCI_LIST_FALSE;
    }
}

/**********************************************************************
* 函数名称： OmciPrependListNode
* 功能描述： 在链表头结点后逆序增加结点，尾结点恒为头结点
*           在头结点指针pHead所指向结点和pHead->pNext所指向结点
*           之间插入新结点，先插入的结点向右移动。遍历链表时
*           从pHead开始向右依次访问至最先插入的结点，类似于栈。
* 输入参数： T_OMCI_LIST *pList :链表指针
*           void *pvNodeData   :待插入的链表结点数据指针
* 输出参数： NA
* 返 回 值： LIST_STATUS
***********************************************************************/
LIST_STATUS OmciPrependListNode(T_OMCI_LIST *pList, void *pvNodeData)
{
		if(pList->dwNodeNum >= pList->maxNodeNum)
		{
			Log("list full err");
			return OMCI_LIST_ERROR;
		}
		if(pList->dwNodeNum >= pList->maxNodeNum)
		{
			Log("list full err");
			return OMCI_LIST_ERROR;
		}
    CHECK_DOUBLE_POINTER(pList, pvNodeData, OMCI_LIST_ERROR);
		
    if(0 == pList->dwNodeDataSize)
    {
        printf("[%s]pList=%p, dwNodeDataSize=0Bytes, probably uninitialized or initialized improperly. See 'OmciInitList'!\n",
               FUNC_NAME, pList);
        return OMCI_LIST_ERROR;
    }
    T_OMCI_LIST_NODE *pInsertNode = CreateListNode(pList, pvNodeData);
    if(NULL == pInsertNode)
    {
        printf("[%s]pList(%p) failed to create pInsertNode!\n", FUNC_NAME, pList);
        return OMCI_LIST_ERROR;
    }

    OMCI_INSERT_NODE(pList->pHead, pInsertNode); //在链表头结点后增加一个结点

    pList->dwNodeNum++;

    return OMCI_LIST_OK;
}

/**********************************************************************
* 函数名称： OmciAppendListNode
* 功能描述： 在链表头结点后顺序增加结点，新结点作为尾结点
*           在头结点指针pHead所指向结点前(即尾结点后)插入新结点，
*           先插入的结点向左移动。遍历链表时从pHead开始向右依次
*           访问至最后插入的结点，类似于队列。
*           双向循环链表已保证pList->pTail(即pHead->pPrev)非空。
* 输入参数： T_OMCI_LIST *pList :链表指针
*           void *pvNodeData   :待插入的链表结点数据指针
* 输出参数： NA
* 返 回 值： LIST_STATUS
***********************************************************************/
LIST_STATUS OmciAppendListNode(T_OMCI_LIST *pList, void *pvNodeData)
{
		if(pList->dwNodeNum >= pList->maxNodeNum)
		{
			Log("list full err");
			return OMCI_LIST_ERROR;
		}
    CHECK_DOUBLE_POINTER(pList, pvNodeData, OMCI_LIST_ERROR);

    if(0 == pList->dwNodeDataSize)
    {
        printf("[%s]pList=%p, dwNodeDataSize=0Bytes, probably uninitialized or initialized improperly. See 'OmciInitList'!\n",
               FUNC_NAME, pList);
        return OMCI_LIST_ERROR;
    }

    T_OMCI_LIST_NODE *pInsertNode = CreateListNode(pList, pvNodeData);
    if(NULL == pInsertNode)
    {
        printf("[%s]pList(%p) failed to create pInsertNode!\n", FUNC_NAME, pList);
        return OMCI_LIST_ERROR;
    }

    OMCI_INSERT_NODE(pList->pTail, pInsertNode); //在链表尾结点后增加一个结点
    pList->pTail = pInsertNode;                  //新的尾结点指向当前添加的结点

    pList->dwNodeNum++;

    return OMCI_LIST_OK;
}


/**********************************************************************
* 函数名称： OmciAppendListPackNode
* 功能描述： 在链表头结点后顺序增加结点，新结点作为尾结点
*           在头结点指针pHead所指向结点前(即尾结点后)插入新结点，
*           先插入的结点向左移动。遍历链表时从pHead开始向右依次
*           访问至最后插入的结点，类似于队列。
*           双向循环链表已保证pList->pTail(即pHead->pPrev)非空。
* 输入参数： T_OMCI_LIST *pList :链表指针
*           void *pvNodeData   :待插入的链表结点数据指针
* 输出参数： NA
* 返 回 值： LIST_STATUS
***********************************************************************/
LIST_STATUS OmciAppendListPackNode(T_OMCI_LIST *pList, T_OMCI_LIST_NODE *pInsertNode)
{
		if(pList->dwNodeNum >= pList->maxNodeNum)
		{
			Log("list full err");
			return OMCI_LIST_ERROR;
		}
    if(NULL == pInsertNode)
    {
        printf("[%s]pList(%p) failed to create pInsertNode!\n", FUNC_NAME, pList);
        return OMCI_LIST_ERROR;
    }

    OMCI_INSERT_NODE(pList->pTail, pInsertNode); //在链表尾结点后增加一个结点
    pList->pTail = pInsertNode;                  //新的尾结点指向当前添加的结点

    pList->dwNodeNum++;

    return OMCI_LIST_OK;
}
/**********************************************************************
* 函数名称： OmciInsertListNode
* 功能描述： 在链表中任意位置插入结点
* 输入参数： T_OMCI_LIST *pList          :链表指针
*           T_OMCI_LIST_NODE *pPrevNode :待插入结点的前驱结点指针
*           void *pvNodeData            :待插入结点的数据域指针
* 输出参数： NA
* 返 回 值： LIST_STATUS
* 注意事项： 若pPrevNode恒为头结点或尾结点，请使用OmciPrependListNode
*           或OmciAppendListNode函数
***********************************************************************/
LIST_STATUS OmciInsertListNode(T_OMCI_LIST *pList, T_OMCI_LIST_NODE *pPrevNode, void *pvNodeData)
{
		if(pList->dwNodeNum >= pList->maxNodeNum)
		{
			Log("list full err");
			return OMCI_LIST_ERROR;
		}
    CHECK_TRIPLE_POINTER(pList, pPrevNode, pvNodeData, OMCI_LIST_ERROR);

    if(0 == pList->dwNodeDataSize)
    {
        printf("[%s]pList=%p, dwNodeDataSize=0Bytes, probably uninitialized or initialized improperly. See 'OmciInitList'!\n",
               FUNC_NAME, pList);
        return OMCI_LIST_ERROR;
    }

    T_OMCI_LIST_NODE *pInsertNode = CreateListNode(pList, pvNodeData);
    if(NULL == pInsertNode)
    {
        printf("[%s]pList(%p) failed to create pInsertNode!\n", FUNC_NAME, pList);
        return OMCI_LIST_ERROR;
    }

    OMCI_INSERT_NODE(pPrevNode, pInsertNode);
    if(pPrevNode == pList->pTail)
        pList->pTail = pInsertNode;

    pList->dwNodeNum++;

    return OMCI_LIST_OK;
}

/**********************************************************************
* 函数名称： RemoveListNodeNoFree
* 功能描述： 移出某结点但不释放内存
* 输入参数： T_OMCI_LIST *pList      :链表指针
*           T_OMCI_LIST_NODE *pNode :待删除的链表结点指针
* 输出参数： NA
* 返 回 值： LIST_STATUS
***********************************************************************/
LIST_STATUS RemoveListNodeNoFree(T_OMCI_LIST *pList, T_OMCI_LIST_NODE *pNode)
{
    CHECK_DOUBLE_POINTER(pList, pNode, OMCI_LIST_ERROR);
    CHECK_DOUBLE_POINTER(pNode->pPrev, pNode->pNext, OMCI_LIST_ERROR);

    if(0 == pList->dwNodeNum)
    {
        printf("[%s]pList(%p) has no node to be Removed!\n", FUNC_NAME, pList);
        return OMCI_LIST_ERROR;
    }
    OMCI_REMOVE_NODE(pNode);
    if(pNode->pNext == pList->pHead)
    {
        pList->pTail = pNode->pPrev; //删除尾结点
    }
    pList->dwNodeNum--;

    return OMCI_LIST_OK;
}

/**********************************************************************
* 函数名称： OmciRemoveListNode
* 功能描述： 删除双向循环链表中除头结点外的某一结点
* 输入参数： T_OMCI_LIST *pList      :链表指针
*           T_OMCI_LIST_NODE *pNode :待删除的链表结点指针
* 输出参数： NA
* 返 回 值： LIST_STATUS
***********************************************************************/
LIST_STATUS OmciRemoveListNode(T_OMCI_LIST *pList, T_OMCI_LIST_NODE *pNode)
{
    CHECK_DOUBLE_POINTER(pList, pNode, OMCI_LIST_ERROR);
    CHECK_DOUBLE_POINTER(pNode->pPrev, pNode->pNext, OMCI_LIST_ERROR);

    if(0 == pList->dwNodeNum)
    {
        printf("[%s]pList(%p) has no node to be Removed!\n", FUNC_NAME, pList);
        return OMCI_LIST_ERROR;
    }
    OMCI_REMOVE_NODE(pNode);
    if(pNode->pNext == pList->pHead)
    {
        pList->pTail = pNode->pPrev; //删除尾结点
    }

    RemoveListNode(pList, pNode);
    pList->dwNodeNum--;

    return OMCI_LIST_OK;
}
/**********************************************************************
* 函数名称： OmciDestroyListNode
* 功能描述： 销毁双向循环链表中除头结点外的某一结点
* 输入参数： T_OMCI_LIST *pList       :链表指针
*           T_OMCI_LIST_NODE **pNode :待销毁的链表结点二级指针
* 输出参数： NA
* 返 回 值： LIST_STATUS
***********************************************************************/
LIST_STATUS OmciDestroyListNode(T_OMCI_LIST *pList, T_OMCI_LIST_NODE **pNode)
{
    CHECK_DOUBLE_POINTER(pList, pNode, OMCI_LIST_ERROR);
    CHECK_SINGLE_POINTER(*pNode, OMCI_LIST_ERROR);

    if(0 == pList->dwNodeNum)
    {
        printf("[%s]pList(%p) has no node to be Removed!\n", FUNC_NAME, pList);
        return OMCI_LIST_ERROR;
    }

    OMCI_REMOVE_NODE(*pNode);
    if((*pNode)->pNext == pList->pHead)
    {
        pList->pTail = (*pNode)->pPrev; //删除尾结点
    }

    DestroyListNode(pList, pNode);
    pList->dwNodeNum--;

    return OMCI_LIST_OK;
}

/**********************************************************************
* 函数名称： OmciGetListNodeByIndex
* 功能描述： 获取链表中指定序号的结点(按头结点后继方向排序)
* 输入参数： T_OMCI_LIST* pList :链表指针
*           uint32_t dwNodeIndex :结点序号(从1开始)
* 输出参数： NA
* 返 回 值： T_OMCI_LIST_NODE* 链表结点指针(空表返回NULL)
***********************************************************************/
T_OMCI_LIST_NODE* OmciGetListNodeByIndex(T_OMCI_LIST *pList, uint32_t dwNodeIndex)
{
    CHECK_SINGLE_POINTER(pList, NULL);
    
    if(0 == dwNodeIndex)
        return pList->pHead;  //也可返回NULL
    if(dwNodeIndex >= pList->dwNodeNum)
        return pList->pTail;

    uint32_t dwNodeIdx = 1;
    T_OMCI_LIST_NODE *pListNode = pList->pHead;
    for(; dwNodeIdx <= dwNodeIndex; dwNodeIdx++)
        pListNode = pListNode->pNext;

    return pListNode;
}
/**********************************************************************
* 函数名称： OmciLocateListNode
* 功能描述： 查找链表中首个与pData满足函数fpCompareNode判定关系的结点
* 输入参数： T_OMCI_LIST* pList            :链表指针
*           void* pvData                  :待比较数据指针
*           CompareNodeFunc fpCompareNode :比较回调函数指针
* 输出参数： NA
* 返 回 值： T_OMCI_LIST_NODE* 链表结点指针(未找到时返回NULL)
***********************************************************************/
/* 比较回调函数原型，用来自定义链表节点比较 */
T_OMCI_LIST_NODE* OmciLocateListNode(T_OMCI_LIST *pList, void *pvData, CompareNodeFunc fpCompareNode)
{
    CHECK_TRIPLE_POINTER(pList, pvData, fpCompareNode, NULL);
    CHECK_SINGLE_POINTER(pList->pHead, NULL);
    CHECK_SINGLE_POINTER(pList->pHead->pNext, NULL);

    T_OMCI_LIST_NODE *pListNode = pList->pHead->pNext;
    while(pListNode != pList->pHead)
    {
        if(0 == fpCompareNode(pListNode->pvNodeData, pvData, pList->dwNodeDataSize))
            return pListNode;

        pListNode = pListNode->pNext;
    }

    return NULL;
}

/**********************************************************************
* 函数名称： OmciTraverseListNode
* 功能描述： 链表结点遍历函数，遍历操作由fpTravNode指定
* 输入参数： T_OMCI_LIST* pList      :链表指针
*           void* pvTravInfo        :遍历操作回调函数所需信息
*                                    也可为空，取决于回调函数具体实现
*           TravNodeFunc fpTravNode :遍历操作回调函数指针
* 输出参数： NA
* 返 回 值： LIST_STATUS
* 注意事项： 本函数可间接实现Print等操作，但不建议代替后者。
*           fpTravNode返回非0(OMCI_LIST_OK)值时中止遍历
***********************************************************************/
LIST_STATUS OmciTraverseListNode(T_OMCI_LIST *pList, void *pvTravInfo, TravNodeFunc fpTravNode)
{
    CHECK_DOUBLE_POINTER(pList, fpTravNode, OMCI_LIST_ERROR);
    CHECK_SINGLE_POINTER(pList->pHead, OMCI_LIST_ERROR);
    CHECK_SINGLE_POINTER(pList->pHead->pNext, OMCI_LIST_ERROR);

    T_OMCI_LIST_NODE *pListNode = pList->pHead->pNext;
    while(pListNode != pList->pHead)
    {
        T_OMCI_LIST_NODE *pTmpNode = pListNode->pNext; //fpTravNode内可能会销毁结点pListNode
        if(OMCI_LIST_OK != fpTravNode(pListNode, pvTravInfo, pList->dwNodeDataSize))
            break;

        pListNode = pTmpNode;
    }

    return OMCI_LIST_OK;
}

/**********************************************************************
* 函数名称： OmciPrintListNode
* 功能描述： 打印输出链表结点的数据域内容
* 输入参数： T_OMCI_LIST* pList        :链表指针
*           PrintListFunc fpPrintList :打印回调函数指针
* 输出参数： NA
* 返 回 值： LIST_STATUS
***********************************************************************/
/* 打印回调函数原型，用来自定义链表内容打印 */

LIST_STATUS OmciPrintListNode(T_OMCI_LIST *pList, PrintListFunc fpPrintList)
{
    CHECK_DOUBLE_POINTER(pList, fpPrintList, OMCI_LIST_ERROR);
    CHECK_SINGLE_POINTER(pList->pHead, OMCI_LIST_ERROR);
    CHECK_SINGLE_POINTER(pList->pHead->pNext, OMCI_LIST_ERROR);

    T_OMCI_LIST_NODE *pListNode = pList->pHead->pNext;
    while(pListNode != pList->pHead)
    {
        //具体打印格式交给回调函数灵活处理(可直接打印也可拷贝至本地处理后打印)
        fpPrintList(pListNode->pvNodeData, pList->dwNodeNum);
        pListNode = pListNode->pNext;
    }
    printf("\n");

    return OMCI_LIST_OK;
}

/**********************************************************************
* 函数名称： CompareNodeGeneric
* 功能描述： 通用链表结点内存比较
* 输入参数： void *pvNodeData      :链表结点数据指针
*           void *pvData          :待比较外部数据指针
*           uint32_t dwNodeDataSize :链表结点数据大小
* 输出参数： NA
* 返 回 值： 0：Equal; !0:Unequal
* 注意事项： 比较长度为结点数据字节数，即默认与外部数据大小一致
***********************************************************************/
uint8_t CompareNodeGeneric(void *pvNodeData, void *pvData, uint32_t dwNodeDataSize)
{
    CHECK_DOUBLE_POINTER(pvNodeData, pvData, 1);
    return memcmp(pvNodeData, pvData, dwNodeDataSize);
}
/**********************************************************************
* 函数名称： PrintListWord
* 功能描述： 打印链表结点，结点数据域为两字节整数
* 输入参数： void *pvNodeData   :链表节点数据指针
*           uint32_t dwNodeNum  :链表节点数目
* 输出参数： NA
* 返 回 值： void
* 注意事项： 仅作示例，未考虑字节序等问题。
***********************************************************************/
void PrintListWord(void *pvNodeData, uint32_t dwNodeNum)
{
    CHECK_SINGLE_POINTER(pvNodeData, RETURN_void);
    printf("%d ", *((uint16_t *)pvNodeData));
}

/**********************************************************************
* 函数名称： OmciGetListNodeNum
* 功能描述： 获取链表结点数目
* 输入参数： T_OMCI_LIST *pList :链表指针
* 输出参数： NA
* 返 回 值： uint32_t 链表结点数目
***********************************************************************/
uint32_t OmciGetListNodeNum(T_OMCI_LIST *pList)
{
    CHECK_SINGLE_POINTER(pList, 0);
    return (pList->dwNodeNum);
}

/**********************************************************************
* 函数名称： OmciGetListHead/OmciGetListTail
* 功能描述： 获取链表头结点/尾结点指针
* 输入参数： T_OMCI_LIST *pList :链表指针
***********************************************************************/
T_OMCI_LIST_NODE* OmciGetListHead(T_OMCI_LIST *pList)
{
    CHECK_SINGLE_POINTER(pList, NULL);
    return (pList->pHead);
}
T_OMCI_LIST_NODE* OmciGetListTail(T_OMCI_LIST *pList)
{
    CHECK_SINGLE_POINTER(pList, NULL);
    return (pList->pTail);
}

/**********************************************************************
* 函数名称： OmciGetPrevNode/OmciGetNextNode
* 功能描述： 获取链表指定结点的前驱结点/后继结点指针
* 输入参数： T_OMCI_LIST_NODE *pNode :指定结点的指针
***********************************************************************/
T_OMCI_LIST_NODE* OmciGetPrevNode(T_OMCI_LIST_NODE *pNode)
{
    CHECK_SINGLE_POINTER(pNode, NULL);
    return (pNode->pPrev);
}
T_OMCI_LIST_NODE* OmciGetNextNode(T_OMCI_LIST_NODE *pNode)
{
    CHECK_SINGLE_POINTER(pNode, NULL);
    return (pNode->pNext);
}

/**********************************************************************
* 函数名称： OmciGetNodeData
* 功能描述： 获取链表指定结点的数据域
* 输入参数： T_OMCI_LIST_NODE *pNode :指定结点的指针
***********************************************************************/
void* OmciGetNodeData(T_OMCI_LIST_NODE *pNode)
{
    CHECK_DOUBLE_POINTER(pNode, pNode->pvNodeData, NULL);
    return (pNode->pvNodeData);
}



static LIST_STATUS TravPrintWord(void *pvNode, void *pvTravInfo, uint32_t dwNodeDataSize)
{
    CHECK_SINGLE_POINTER(pvNode, OMCI_LIST_ERROR);
    T_OMCI_LIST_NODE *pNode = (T_OMCI_LIST_NODE *)pvNode;
    printf("%d ", *((uint16_t *)GET_NODE_DATA(pNode)));
    return OMCI_LIST_OK;
}

void DeleteNodeCall(void * node)
{

	Log("DeleteNodeCall:%d",*(u16 *)node);
//	if()
//	{
//	
//	}
}


void ListTestExample(void)
{   
		//本函数并非严格意义上的测试函数，主要用作示例，且示例并非最佳用法。
//    uint8_t ucTestIndex = 1;
//    uint16_t aTestListData[] = {11, 22, 33, 44, 55, 66};
//		T_OMCI_LIST gExampleList = {0};
//    printf("\n<Test Case %u>: Initialization!\n", ucTestIndex++);
//    OmciInitList(&gExampleList, sizeof(uint16_t),10,DeleteNodeCall);
//    printf("gExampleList=%p, pHead=%p, pTail=%p\n", &gExampleList,
//           OmciGetListHead(&gExampleList), OmciGetListTail(&gExampleList));

//    printf("\n<Test Case %u>: Append Node to List!\n", ucTestIndex++);
//    OmciAppendListNode(&gExampleList, &aTestListData[0]);
//    OmciAppendListNode(&gExampleList, &aTestListData[1]);
//    OmciAppendListNode(&gExampleList, &aTestListData[2]);
//    printf("OmciIsListEmpty=%u(0-Occupied; 1-Empty)\n", OmciIsListEmpty(&gExampleList));
//    printf("gExampleList NodeNum=%u\n", OmciGetListNodeNum(&gExampleList));
//    OmciPrintListNode(&gExampleList, PrintListWord);

//    printf("\n<Test Case %u>: Insert Node to List!\n", ucTestIndex++);
//    T_OMCI_LIST_NODE *pPrevNode = OmciGetListNodeByIndex(&gExampleList, 2);
//    printf("NodeData2=%d\n", *((uint16_t *)OmciGetNodeData(pPrevNode)));
//    OmciInsertListNode(&gExampleList, pPrevNode, &aTestListData[4]);
//    printf("gExampleList NodeNum=%u\n", OmciGetListNodeNum(&gExampleList));
//    OmciPrintListNode(&gExampleList, PrintListWord);

//    printf("\n<Test Case %u>: Remove Node from List!\n", ucTestIndex++);
//    T_OMCI_LIST_NODE *pDeleteNode = OmciLocateListNode(&gExampleList, &aTestListData[1], CompareNodeGeneric);
//    OmciRemoveListNode(&gExampleList, pDeleteNode);
//    printf("gExampleList NodeNum=%u\n", OmciGetListNodeNum(&gExampleList));
//    OmciPrintListNode(&gExampleList, PrintListWord);

//    printf("\n<Test Case %u>: Clear List!\n", ucTestIndex++);
//    OmciClearList(&gExampleList);
//    printf("gExampleList=%p, pHead=%p, pTail=%p\n", &gExampleList,
//           GET_HEAD_NODE(&gExampleList), GET_TAIL_NODE(&gExampleList));
//    printf("OmciIsListEmpty=%u(0-Occupied; 1-Empty)\n", OmciIsListEmpty(&gExampleList));
//    printf("gExampleList NodeNum=%u\n", OmciGetListNodeNum(&gExampleList));

//    printf("\n<Test Case %u>: Prepend Node to List!\n", ucTestIndex++);
//    OmciPrependListNode(&gExampleList, &aTestListData[3]);
//    OmciPrependListNode(&gExampleList, &aTestListData[4]);
//    OmciPrependListNode(&gExampleList, &aTestListData[5]);
//    printf("OmciIsListEmpty=%u(0-Occupied; 1-Empty)\n", OmciIsListEmpty(&gExampleList));
//    printf("gExampleList NodeNum=%u\n", OmciGetListNodeNum(&gExampleList));
//    OmciPrintListNode(&gExampleList, PrintListWord);

//    T_OMCI_LIST_NODE *pListNode = NULL;
//    LIST_ITER_LOOP(&gExampleList, pListNode)
//    {
//        printf("%d ", *((uint16_t *)GET_NODE_DATA(pListNode)));
//    }
//    printf("\n");

//    OmciTraverseListNode(&gExampleList, NULL, TravPrintWord);
//    printf("\n");

//    printf("\n<Test Case %u>: Destory List!\n", ucTestIndex++);
//    OmciDestroyList(&gExampleList);
//    printf("gExampleList=%p, pHead=%p, pTail=%p\n", &gExampleList,
//           GET_HEAD_NODE(&gExampleList), GET_TAIL_NODE(&gExampleList));
//    printf("gExampleList NodeNum=%u\n", OmciGetListNodeNum(&gExampleList));
//    printf("GetListOccupation=%u(0-Occupied; 1-Empty; 2-Null)\n", GetListOccupation(&gExampleList));
//    return;
}
