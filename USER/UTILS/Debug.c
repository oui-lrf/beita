#include "Debug.h"
#include "stdio.h"
#include "string.h"
#include "stdarg.h"
#include "cmsis_os.h"
#include "malloc.h"

//每行打印字符数最多
#define LINE_MAX (3*20)

#define DEBUG_STRING_MAX (300)

//Debug("debug test:%d",321);
osMutexId  debugMutexId;

void CreateDebugMutex(void)
{
	static 	osMutexDef_t  debugMutex;
	debugMutexId = osMutexCreate(&debugMutex);
}

void DeleteDebugMutex(void)
{
	osMutexDelete(debugMutexId);
}

void WaitDebugMutex(void)
{
	if(debugMutexId && xTaskGetSchedulerState() !=taskSCHEDULER_NOT_STARTED)osMutexWait(debugMutexId, portMAX_DELAY);//
}

void ReleaseDebugMutex(void)
{
	if(debugMutexId && xTaskGetSchedulerState() !=taskSCHEDULER_NOT_STARTED)osMutexRelease(debugMutexId);//
}

void MyDebug(const char* format,...)
{
	char *dataBuf=mymalloc(SRAMIN,DEBUG_STRING_MAX);
	if(dataBuf)
	{
		memset(dataBuf,0,DEBUG_STRING_MAX);
		va_list ap;
		va_start(ap,format);
		vsnprintf(dataBuf,DEBUG_STRING_MAX,format, ap);
		va_end( ap );
		int dataLen = strlen(dataBuf);
		//查找换行最后一次出现的位置
		//不打印.的个数=总长度-最后换成
		char* rpos = strrchr(dataBuf,'\n');
		int noutLen=0;
		int outLen=0;
		if(rpos)
		{
			noutLen = dataLen -(rpos+1 - dataBuf); 
			outLen= 60-noutLen;
		}
		else
		{
			noutLen=dataLen;
			outLen= 60-noutLen-3;//要减去前面的-->
		}
		if(outLen<0)outLen=0;
		printf("-->%s",dataBuf);
		for(int i=0;i<outLen;i++){putchar('.');}	
		myfree(SRAMIN,dataBuf);
	}
	else
	{
		printf("debug malloc err\r\n");
	}
}



void PrintHex(char *name,u8 *buf,u16 size)
{
	u8 *data = buf;
	//占用字符个数
	int chSize=size*3;
	printf("-->%s:",name);
	u16 allRow = chSize/LINE_MAX;
	u16 reminRow = chSize%LINE_MAX;
	
	for(u16 row=0;row<allRow;row++)
	{
		printf("\r\n");
		for(u16 i=0;i<LINE_MAX/3;i++)
		{
			printf("%02X ",*data++);
		}
	}
	//打印整行余下的
	printf("\r\n");
	for(u16 i=0;i<reminRow/3;i++)
	{
		printf("%02X ",*data++);
	}
	//每个字节打印要占3个字符
	for(int i=0;i<(LINE_MAX - reminRow);i++)
	{
		putchar('.');
	}	

}


void DelayUs(unsigned int us)
{
	for(unsigned int i=0;i<us;i++)
	{
		for(u16 j=0;j<3;j++);
	}
}