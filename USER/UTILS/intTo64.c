#include "IntTo64.h"


int intTo64(char str[], const u32 num)
{
    char tmp_str[20] = {0};
    static char digits[] ="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$_";  /* 64进制数字的字符 #换成-*/ 
    unsigned int i = 10;
    int j = 0;
    u32 tmp_num= num;
//    printf("tmp_num = %x\n", num);
//		printf("tmp_num = %d\n", num);
    while(tmp_num)
    {
        tmp_str[--i] = digits[tmp_num % 64];
        tmp_num /= 64;
       // printf("tmp_num = %d i = %d , tmp_str[%d] = %c\n", tmp_num, i, i, tmp_str[i]);
    }
   // printf("tmp_str = %s\n", tmp_str);
    for(j=0;(str[j]=tmp_str[i])!='\0';j++,i++); /*将译出在工作数组中的字符串复制到s */
    return 1;

}



int _64strToInt(unsigned long long *num, const char str[])
{
    int i = 0;
    unsigned long long tmp_num = 0;
    for (; str[i] != '\0';++i)
    {
        printf("str[i] = %c, tmp_num = %lld\n", str[i], tmp_num);
        if(str[i] == '$')
        {
            tmp_num = 64 * tmp_num + 62;
        }
        else if(str[i] == '#')
        {
            tmp_num = 64 * tmp_num + 63;

        }
        else if (str[i] > '9')
        {
            if(str[i] > 'A')
            {
                tmp_num = 64 * tmp_num + (36 + str[i] - 'A');
            }
            else
            {
                tmp_num = 64 * tmp_num + (10 + str[i] - 'a');
            }

        }
        else if(str[i] <= '9')
        {
            tmp_num = 64 * tmp_num + (str[i] - '0');
        }
    }
    printf("tmp_num = %lld\n", tmp_num);
    *num = tmp_num;
    return 1;
}

