#include "VaveFit.h"
#include "malloc.h"
#include "Debug.h"
#include "math.h"




float WaveInti(Wave *vave,float data)
{
	vave->last =data;
	vave->now =data;
	vave->next =data;
}

static float GetDiff(float v1,float v2)
{
	if(v1 >= v2)
	{
		return v1-v2;
	}
	else
	{
		return v2-v1;
	}
}

float WaveFit(Wave *vave,float data,float fit)
{
	float t0=vave->last;	
	vave->last=vave->now;//当前->上一次
	if(GetDiff(t0,vave->now)>=fit || GetDiff(vave->next,vave->now)>=fit)
	{
	 vave->now = (t0 + vave->next)/2;
	}
	else
	{	
	 vave->now = vave->next;
	}		
	vave->next=data;//下一个
	return vave->now;
}

OrderWaveFit * CreateVaveFit(u8 order,float acc)
{
	OrderWaveFit *oderFit = mymalloc(SRAMIN,sizeof(OrderWaveFit));
	if(!oderFit)
	{
		Log("mallocErr");
		return 0;
	}
	oderFit->order = order;
	oderFit->fitAcc =acc;
	//数组
	Wave **vave = mymalloc(SRAMIN,(sizeof(Wave))*order*2);
	if(!vave)
	{
		myfree(SRAMIN,oderFit);
		Log("mallocErr");
		return 0;
	}
	oderFit->vave =vave;
	return oderFit;
}

void PrintWave(OrderWaveFit *oderFit)
{
	Wave *vave=(Wave *)oderFit->vave;
	for(u8 i=0;i<oderFit->order;i++)
	{
		vave += sizeof(Wave);
		printf("Wave >%d last>%f,now>%f,next>%f\r\n",i,vave->last,vave->now,vave->next);
	}
}

void VaveFitInit(OrderWaveFit *oderFit,float data)
{
	Wave *vave=(Wave *)oderFit->vave;
	for(u8 i=0;i<oderFit->order;i++)
	{
		vave += sizeof(Wave);
		WaveInti(vave,data);
	}
}

float VaveFitCalcul(OrderWaveFit *oderFit,float data)
{
	float res = data;
	float acc;
	Wave *vave=(Wave *)oderFit->vave;
	for(u8 i=0;i<oderFit->order;i++)
	{
		acc = oderFit->fitAcc*pow(2,(oderFit->order-1-i));
		vave += sizeof(Wave);
		res = WaveFit(vave,res,acc);
	}
	return res;
}


void VaveFitDelete(OrderWaveFit *oderFit)
{
	myfree(SRAMIN,oderFit->vave);
	myfree(SRAMIN,oderFit);
}
