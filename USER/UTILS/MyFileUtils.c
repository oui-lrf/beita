#include "MyFileUtils.h"
#include "malloc.h"
#include "MbCrc.h"

#if 0
#define	MyfileLog  Log
#else 
#define	MyfileLog(...)
#endif
#include "ff.h"
#include "Debug.h"

u16 GetFileCrc(const char * fileName,u32 chacheSize)
{
	u16 oneCrc = 0;
	FIL file; 
	u8 res=f_open(&file,(const TCHAR*)fileName,FA_READ);//模式0,或者尝试打开失败,则创建新文件	 
	if(res==0)
	{
		u32 bwr=0;
		u32 size = f_size(&file);
		Log("fileSize:%d",size);
		u8 * tempbuf = mymalloc(SRAMIN,chacheSize);
		if(!tempbuf)
		{
			Log("LoadFontLib tempbuf mymalloc failed\r\n");
		}
		
		u32 pos=0;
		while(1)
		{
			u8 readRes=0;
			u32 readSize;
			readRes = f_read(&file,tempbuf,chacheSize,&readSize);//读取一个字节		
			if(readRes||readSize==0)break;
			//LogHex("down ok file:",(u8 *)tempbuf,readSize);
			oneCrc =  usMBLongCRC16(oneCrc,pos++,(u8 *)tempbuf,readSize);
		}
		f_close(&file);
	}
	return oneCrc;
}

