#pragma once
#include "sys.h"
#ifdef __cplusplus
 extern "C" {
#endif
typedef struct ListNode{
	struct ListNode * prior;
	int value;
	struct ListNode * next;
}ListNode;

typedef struct {
	ListNode *head;
	ListNode *end;
	void (*deleteCall)(int);
	u16 maxNodeCount;
}List;


List * CreateList(int nodeValue,int maxCount,void (*deleteCall)(int));
void DisList(List * list);
#ifdef __cplusplus
}
#endif
