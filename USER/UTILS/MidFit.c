#include "MidFit.h"
#include "malloc.h"
#include "Debug.h"
#include "string.h"

MidFit * CreateMidFit(u16 maxCount)
{
	MidFit *fit = mymalloc(SRAMIN,sizeof(MidFit));
	if(!fit)
	{
		Log("MallocErr");
		return 0;
	}
	fit->maxFitCount = maxCount;
	float *fitData = mymalloc(SRAMIN,sizeof(float)*maxCount);
	
	if(!fitData)
	{
		Log("MallocErr");
		return 0;
	}
	fit->nowCount=0;
	memset(fitData,0,sizeof(float)*maxCount);
	fit->fitData = fitData;
	return fit;
}


void DeleteMidFit(MidFit *fit)
{
	myfree(SRAMIN,fit->fitData);
	myfree(SRAMIN,fit);
}

void MidFitInit(MidFit *fit)
{
	fit->nowCount=0;
	memset(fit->fitData,0,sizeof(float)*fit->maxFitCount);
}

void InsertMidFitData(MidFit *fit,float data)
{
	fit->fitData[fit->nowCount%fit->maxFitCount]=data;	
	fit->nowCount++;
}
void PrintData(MidFit *fit)
{
	for(u16 i=0;i<fit->maxFitCount;i++)
	{
		printf(" mid%d > %f",i,fit->fitData[i]);
	}
	printf("\r\n");
}
void Reorder(MidFit *fit)
{
	// 采样值从小到大排列（冒泡法）
	PrintData(fit);
	for(u16 j = 0; j < fit->nowCount - 1; j++) 
	{
		for(u16 i = 0; i < fit->nowCount - 1 - j; i++) 
		{
			if(fit->fitData[i] > fit->fitData[i+1]) 
			{
				float data  = fit->fitData[i];
				fit->fitData[i] = fit->fitData[i+1];
				fit->fitData[i + 1] = data;
			}
		}
	}
	PrintData(fit);
}



float GetMidValue(MidFit *fit)
{
	//冒泡排序
	//如果是奇数，取中值
	//偶数，取中间2个的平均值
	Reorder(fit);
	//奇数
	float res;
	if(fit->nowCount %2)
	{
		res = fit->fitData[fit->nowCount/2 + fit->nowCount%2];
	}
	else
	{
		res = (fit->fitData[fit->nowCount/2] + fit->fitData[fit->nowCount/2-1])/2.0f;
	}
	return res;
}

