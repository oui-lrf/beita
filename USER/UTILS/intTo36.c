#include "intConvert.h"
#include "stdio.h"

int intTo36(char str[], const u32 num)
{
    char tmp_str[20] = {0};
    static char digits[] ="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";  /* 36进制数字的字符 */
    unsigned int i = 10;
    int j = 0;
    u32 tmp_num;
    tmp_num = num;
    //printf("tmp_num = %lld\n", tmp_num);
    while(tmp_num)
    {
			tmp_str[--i] = digits[tmp_num % 36];
			tmp_num /= 36;
			//printf("tmp_num = %lld i = %d , tmp_str[%d] = %c\n", tmp_num, i, i, tmp_str[i]);
    }
    printf("tmp_str = %s\n", tmp_str);
    for(j=0;(str[j]=tmp_str[i])!='\0';j++,i++); /*将译出在工作数组中的字符串复制到s */
    return 1;

}

int _36strToInt(unsigned long long *num, const char str[])
{
    int i = 0;
    unsigned long long tmp_num = 0;
    for (; (str[i] >= '0' && str[i] <= '9') || (str[i] >= 'a' && str[i] <= 'z') || (str[i] >='A' && str[i] <= 'Z');++i)
    {
            printf("str[i] = %c\n", str[i]);
        if (str[i] > '9')
        {
            tmp_num = 36 * tmp_num + (10 + str[i] - 'A');
        }
        else
        {
            tmp_num = 36 * tmp_num + (str[i] - '0');
        }
    }
    *num = tmp_num;
    return 1;
}
