#pragma once
#include "sys.h"

typedef struct{
	u16 maxFitCount;
	u16 nowCount;
	float *fitData;
}MidFit;
MidFit * CreateMidFit(u16 maxCount);
void MidFitInit(MidFit *fit);
void InsertMidFitData(MidFit *fit,float data);
void DeleteMidFit(MidFit *fit);
float GetMidValue(MidFit *fit);
void PrintData(MidFit *fit);
