#include "DoublyList.h"
#include "malloc.h"
#include "debug.h"

//要定义一个应用结构体，用于保存头结点，尾部结点，删除回调函数

#define ListMalloc(n) mymalloc(SRAMIN,n)
#define ListFree(n) myfree(SRAMIN,n)

#define LINE_MALLOC_ERR Log("list malloc err")
#define LINE_ERR Log("list err")


List * CreateList(int headNodeValue,int maxCount,void (*deleteCall)(int))
{
	List *list = ListMalloc(sizeof(List));
	ListNode *node = ListMalloc(sizeof(ListNode));
	Log("CreateList:%d",node);
	if(!node)
	{
		LINE_MALLOC_ERR;
		ListFree(list);
	}
	node->next =node->prior= node;
	node->value = headNodeValue;
	list->head = list->end = node;
	list->deleteCall = deleteCall;
	list->maxNodeCount=maxCount;
	return list;
}

u16 ListLength(List *list)
{
	/* 初始条件：L已存在。操作结果： */
	int len=0;
	ListNode *p=list->head; /* p指向第一个结点 */
	while(p != list->head) /* p没到表头 */
	{
		len++;
		p=p->next;
	}
	return len;
}

//获取指定位置的结点地址
ListNode * GetNode(List *list,int i) /* 另加 */
{ 
	/* 在双向链表L中返回第i个元素的地址。i为0，返回头结点的地址。若第i个元素不存在，*/
	/* 返回NULL */
	ListNode *p=list->head; /* p指向头结点 */
	if(i<0||i>ListLength(list)) /* i值不合法 */
	return 0;
	for(int j=1;j<=i;j++)
	{
		p=p->next;
	}
	return p;
}

u8 AddNode(List *list,int i,int value)
{
	//判断当前结点数量是否达到最大
	if(ListLength(list) >= list->maxNodeCount)
	{
		LINE_ERR;
		return 0;
	}
	ListNode *priorNode = GetNode(list,i-1);
	
	//创建一个新结点，从头部插入
	ListNode *node = ListMalloc(sizeof(ListNode));
	if(!node)
	{
		LINE_MALLOC_ERR;
		return 0;
	}
	node->value = value;
	node->prior = priorNode;
	node->next = priorNode->next;
	priorNode->next->prior=node;
	priorNode->next=node;
	return 1;
}

//输出链表的功能函数
void DisList(List * list)
{
	ListNode *p=list->head->next; /* p指向第一个结点 */
	printf("list len:%d\r\n->%d",ListLength(list),p->value);
	while(p != list->head) /* p没到表头 */
	{
		printf("->%d",p->value);
		p=p->next;
	}
	printf("\r\n");
}
void DeleteList(List *list)
{
	//遍历删除每一个结点
	
//	if(list->deleteCall)
//	{
//		list->deleteCall();
//	}

}


