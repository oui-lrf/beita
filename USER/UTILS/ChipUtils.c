#include "ChipUtils.h"
#include "IntTo64.h"
#include "sys.h"
#include "Debug.h"
#include "string.h"

void GetChipId(char *des,unsigned int size)
{
	if(size >30)
	{
		static u32 CpuID[3];
		CpuID[0]=*(vu32*)(0x1FFF7A10);//0x1FFF 7A10
		CpuID[1]=*(vu32*)(0x1FFF7A14);
		CpuID[2]=*(vu32*)(0x1FFF7A18);
		intTo64(des,CpuID[0]);
		intTo64(&des[strlen(des)],CpuID[1]);
		intTo64(&des[strlen(des)],CpuID[2]);
	}
	else
	{
		Log("ERR,Size Too Little");
	}
	
}

