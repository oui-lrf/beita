#pragma once
#include "stdint.h"
#ifdef __cplusplus
 extern "C" {
#endif
#include "sys.h"	 
	 	 
#define LINE_MALLOC_ERR Log("list malloc err")
#define LINE_ERR Log("list err")

#define   FUNC_NAME    __FILE__ //(__func__)

/* 指针校验宏 */
//若无返回值则retVal置RETURN_void
#define RETURN_void


#define CHECK_SINGLE_POINTER(ptr1, retVal) do{\
	if(NULL == (ptr1))\
	{\
			printf("[%s(%d)]Null Pointer: "#ptr1"!\n\r", FUNC_NAME, __LINE__); \
			return retVal; \
	} \
}while(0)


#define CHECK_DOUBLE_POINTER(ptr1, ptr2, retVal) do{\
    if((NULL == (ptr1)) || (NULL == (ptr2))) \
    { \
        printf("[%s(%d)]Null Pointer: "#ptr1"(%p), "#ptr2"(%p)!\n\r", FUNC_NAME, __LINE__, ptr1, ptr2); \
        return retVal; \
    } \
}while(0)


#define CHECK_TRIPLE_POINTER(ptr1, ptr2, ptr3, retVal) do{\
    if((NULL == (ptr1)) || (NULL == (ptr2)) || (NULL == (ptr3))) \
    { \
        printf("[%s(%d)]Null Pointer: "#ptr1"(%p), "#ptr2"(%p), "#ptr3"(%p)!\n\r", FUNC_NAME, __LINE__, ptr1, ptr2, ptr3); \
        return retVal; \
    } \
}while(0)



//创建结点为作为链表头以生成双向循环空链表
#define OMCI_INIT_NODE(pNode) do{ \
    (pNode)->pNext = (pNode)->pPrev = (pNode); \
}while(0)
//"孤立"链表结点，避免通过该结点访问其前驱和后继结点(进而遍历链表)
#define OMCI_ISOL_NODE(pNode) do{ \
    (pNode)->pNext = (pNode)->pPrev = NULL; \
}while(0)
//判断链表是否仅含头结点
#define OMCI_LIST_WITH_HEAD(pHeadNode) do{ \
    (((pHeadNode)->pPrev == (pHeadNode)) && ((pHeadNode->pNext == pHeadNode))); \
}while(0)

//插入链表结点
#define OMCI_INSERT_NODE(prevNode, insertNode) do{ \
    (insertNode)->pNext      = (prevNode)->pNext;  \
    (insertNode)->pPrev      = (prevNode);         \
    (prevNode)->pNext->pPrev = (insertNode);       \
    (prevNode)->pNext        = (insertNode);       \
}while(0)
//删除链表结点
#define OMCI_REMOVE_NODE(removeNode) do{ \
    (removeNode)->pPrev->pNext = (removeNode)->pNext;  \
    (removeNode)->pNext->pPrev = (removeNode)->pPrev;  \
}while(0)

//获取链表结点及其数据(不做安全性检查)
#define GET_NODE_NUM(pList)      ((pList)->dwNodeNum)
#define GET_HEAD_NODE(pList)     ((pList)->pHead)
#define GET_TAIL_NODE(pList)     ((pList)->pTail)
#define GET_PREV_NODE(pNode)     ((pNode)->pPrev)
#define GET_NEXT_NODE(pNode)     ((pNode)->pNext)
#define GET_NODE_DATA(pNode)     ((pNode)->pvNodeData)

//双向循环链表遍历校验宏
#define LIST_ITER_CHECK(pList, retVal) do{\
    CHECK_SINGLE_POINTER((pList), retVal); \
    CHECK_SINGLE_POINTER((pList)->pHead, retVal); \
    CHECK_SINGLE_POINTER((pList)->pHead->pNext, retVal); \
}while(0)
//双向循环链表遍历宏
//pList: 链表指针；pLoopNode: 链表结点，用作循环计数器；
//pTmpNode: 链表结点，用作删除pLoopNode时临时保存pLoopNode->pNext
//某些情况下采用遍历宏代替OmciLocateListNode或OmciTraverseListNode函数可提高执行效率。
//如外部数据和结点数据需按共同的规则转换时，采用遍历宏可使外部数据不必重复转换。
#define LIST_ITER_LOOP(pList, pLoopNode) \
  for(pLoopNode = (pList)->pHead->pNext; \
      pLoopNode != (pList)->pHead; \
      pLoopNode = pLoopNode->pNext)
#define LIST_ITER_LOOP_SAFE(pList, pLoopNode, pTmpNode) \
  for(pLoopNode = (pList)->pHead->pNext, pTmpNode = pLoopNode->pNext; \
      pLoopNode != (pList)->pHead; \
      pLoopNode = pTmpNode, pTmpNode = pLoopNode->pNext)


	 
	 
	 
	 
	 
	////////////////////////////////////// 
typedef struct T_OMCI_LIST_NODE{
     struct T_OMCI_LIST_NODE  *pPrev;      /* 指向链表直接前驱结点的指针 */
     struct T_OMCI_LIST_NODE  *pNext;      /* 指向链表直接后继结点的指针 */
     void                     *pvNodeData; /* 指向链表数据的指针。获取具体数据时需显式转换该指针类型为目标类型 */
 }T_OMCI_LIST_NODE;


 typedef struct{
     T_OMCI_LIST_NODE   *pHead;          /* 指向链表头结点的指针 */
     T_OMCI_LIST_NODE   *pTail;          /* 指向链表尾结点的指针 */
     uint32_t             dwNodeNum;       /* 链表结点数目 */
		 uint32_t							maxNodeNum;
     uint32_t             dwNodeDataSize;  /* 链表结点保存的数据字节数 */		 
		 void (*deleteFunc)(void *);
 }T_OMCI_LIST;
 
 //链表函数返回状态枚举值
typedef enum{
    OMCI_LIST_OK    = (uint8_t)0,
    OMCI_LIST_ERROR = (uint8_t)1
}LIST_STATUS;

//链表结点空闲情况枚举值
typedef enum{
    OMCI_LIST_OCCUPIED = (uint8_t)0,
    OMCI_LIST_EMPTY    = (uint8_t)1,
    OMCI_LIST_NULL     = (uint8_t)2
}LIST_OCCUPATION;

//BOOL型常量，适用于'Is'前缀函数
#define  OMCI_LIST_TRUE   (BOOL)1
#define  OMCI_LIST_FALSE  (BOOL)0

void ListTestExample(void);
LIST_STATUS OmciInitList(T_OMCI_LIST *pList, uint32_t dwNodeDataSize,uint32_t maxNodeNum,void (*deleteFunc)(void *));
LIST_STATUS OmciAppendListNode(T_OMCI_LIST *pList, void *pvNodeData);
T_OMCI_LIST_NODE* OmciGetListHead(T_OMCI_LIST *pList);
T_OMCI_LIST_NODE* OmciGetListTail(T_OMCI_LIST *pList);
/**********************************************************************
* 函数名称： OmciGetListNodeByIndex
* 功能描述： 获取链表中指定序号的结点(按头结点后继方向排序)
* 输入参数： T_OMCI_LIST* pList :链表指针
*           uint32_t dwNodeIndex :结点序号(从1开始)
* 输出参数： NA
* 返 回 值： T_OMCI_LIST_NODE* 链表结点指针(空表返回NULL)
***********************************************************************/
T_OMCI_LIST_NODE* OmciGetListNodeByIndex(T_OMCI_LIST *pList, uint32_t dwNodeIndex);

/**********************************************************************
* 函数名称： OmciGetNodeData
* 功能描述： 获取链表指定结点的数据域
* 输入参数： T_OMCI_LIST_NODE *pNode :指定结点的指针
***********************************************************************/
void* OmciGetNodeData(T_OMCI_LIST_NODE *pNode);

/**********************************************************************
* 函数名称： CreateListNodeDataSize
* 功能描述： 创建新的链表结点
***********************************************************************/
T_OMCI_LIST_NODE *CreateListNodeDataSize(void *pvNodeData,uint32_t pvNodeDataSize);


/**********************************************************************
* 函数名称： OmciAppendListPackNode
* 功能描述： 在链表头结点后顺序增加结点，新结点作为尾结点
*           在头结点指针pHead所指向结点前(即尾结点后)插入新结点，
*           先插入的结点向左移动。遍历链表时从pHead开始向右依次
*           访问至最后插入的结点，类似于队列。
*           双向循环链表已保证pList->pTail(即pHead->pPrev)非空。
* 输入参数： T_OMCI_LIST *pList :链表指针
*           void *pvNodeData   :待插入的链表结点数据指针
* 输出参数： NA
* 返 回 值： LIST_STATUS
***********************************************************************/
LIST_STATUS OmciAppendListPackNode(T_OMCI_LIST *pList, T_OMCI_LIST_NODE *pInsertNode);

/**********************************************************************
* 函数名称： RemoveListNode
* 功能描述： 删除指定的链表结点(释放结点内存并置其前驱后继指针为NULL)
* 输入参数： T_OMCI_LIST *pList :链表指针
*           void *pvNode       :待删除的链表结点指针
* 输出参数： NA
* 返 回 值： LIST_STATUS
* 注意事项： 本函数未置待删除结点指针为NULL，请避免访问已删除结点
***********************************************************************/
LIST_STATUS RemoveListNode(T_OMCI_LIST *pList, T_OMCI_LIST_NODE *pNode);


/**********************************************************************
* 函数名称： OmciRemoveListNode
* 功能描述： 删除双向循环链表中除头结点外的某一结点
* 输入参数： T_OMCI_LIST *pList      :链表指针
*           T_OMCI_LIST_NODE *pNode :待删除的链表结点指针
* 输出参数： NA
* 返 回 值： LIST_STATUS
***********************************************************************/
LIST_STATUS OmciRemoveListNode(T_OMCI_LIST *pList, T_OMCI_LIST_NODE *pNode);


/**********************************************************************
* 函数名称： OmciPrintListNode
* 功能描述： 打印输出链表结点的数据域内容
* 输入参数： T_OMCI_LIST* pList        :链表指针
*           PrintListFunc fpPrintList :打印回调函数指针
* 输出参数： NA
* 返 回 值： LIST_STATUS
***********************************************************************/
/* 打印回调函数原型，用来自定义链表内容打印 */
typedef void (*PrintListFunc)(void *pNodeData, uint32_t dwNodeNum);
LIST_STATUS OmciPrintListNode(T_OMCI_LIST *pList, PrintListFunc fpPrintList);


/**********************************************************************
* 函数名称： OmciLocateListNode
* 功能描述： 查找链表中首个与pData满足函数fpCompareNode判定关系的结点
* 输入参数： T_OMCI_LIST* pList            :链表指针
*           void* pvData                  :待比较数据指针
*           CompareNodeFunc fpCompareNode :比较回调函数指针
* 输出参数： NA
* 返 回 值： T_OMCI_LIST_NODE* 链表结点指针(未找到时返回NULL)
***********************************************************************/
/* 比较回调函数原型，用来自定义链表节点比较 */
typedef uint8_t (*CompareNodeFunc)(void *pvNodeData, void *pvData, uint32_t dwNodeDataSize);
T_OMCI_LIST_NODE* OmciLocateListNode(T_OMCI_LIST *pList, void *pvData, CompareNodeFunc fpCompareNode);
 
/**********************************************************************
* 函数名称： OmciTraverseListNode
* 功能描述： 链表结点遍历函数，遍历操作由fpTravNode指定
* 输入参数： T_OMCI_LIST* pList      :链表指针
*           void* pvTravInfo        :遍历操作回调函数所需信息
*                                    也可为空，取决于回调函数具体实现
*           TravNodeFunc fpTravNode :遍历操作回调函数指针
* 输出参数： NA
* 返 回 值： LIST_STATUS
* 注意事项： 本函数可间接实现Print等操作，但不建议代替后者。
*           fpTravNode返回非0(OMCI_LIST_OK)值时中止遍历
***********************************************************************/
typedef LIST_STATUS (*TravNodeFunc)(void *pvNode, void *pvTravInfo, uint32_t dwNodeDataSize);
LIST_STATUS OmciTraverseListNode(T_OMCI_LIST *pList, void *pvTravInfo, TravNodeFunc fpTravNode);

/**********************************************************************
* 函数名称： OmciDestroyList
* 功能描述： 销毁双向循环链表，包括头结点
* 输入参数： T_OMCI_LIST *pList :链表指针
* 输出参数： NA
* 返 回 值： LIST_STATUS
* 注意事项： 销毁链表后，再次插入结点时需要初始化链表。
***********************************************************************/
LIST_STATUS OmciDestroyList(T_OMCI_LIST *pList);

/**********************************************************************
* 函数名称： OmciGetListNodeNum
* 功能描述： 获取链表结点数目
* 输入参数： T_OMCI_LIST *pList :链表指针
* 输出参数： NA
* 返 回 值： uint32_t 链表结点数目
***********************************************************************/
uint32_t OmciGetListNodeNum(T_OMCI_LIST *pList);

/**********************************************************************
* 函数名称： OmciClearList
* 功能描述： 清空双向循环链表除头结点外的结点
* 输入参数： T_OMCI_LIST *pList :链表指针
* 输出参数： NA
* 返 回 值： LIST_STATUS
* 注意事项： 清空链表结点后，再次插入结点时不需要初始化链表。
***********************************************************************/
LIST_STATUS OmciClearList(T_OMCI_LIST *pList);

/**********************************************************************
* 函数名称： RemoveListNodeNoFree
* 功能描述： 移出某结点但不释放内存
* 输入参数： T_OMCI_LIST *pList      :链表指针
*           T_OMCI_LIST_NODE *pNode :待删除的链表结点指针
* 输出参数： NA
* 返 回 值： LIST_STATUS
***********************************************************************/
LIST_STATUS RemoveListNodeNoFree(T_OMCI_LIST *pList, T_OMCI_LIST_NODE *pNode);

/**********************************************************************
* 函数名称： OmciIsListEmpty
* 功能描述： 判断链表是否为空(仅含头结点或不含任何结点)
* 输入参数： T_OMCI_LIST *pList :链表指针
* 输出参数： NA
* 返 回 值： BOOL
***********************************************************************/
BOOL OmciIsListEmpty(T_OMCI_LIST *pList);

#ifdef __cplusplus
}
#endif
