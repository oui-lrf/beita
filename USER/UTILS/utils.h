#ifndef __UTILS_H__
#define __UTILS_H__
#include "sys.h"

u8 IntToStr(char *str,u8 size,int value,int len);
u8 FloatToStr(char *str,u8 size,float value,int len);

#endif
