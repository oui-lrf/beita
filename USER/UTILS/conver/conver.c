#include "conver.h"
#include "gbk_to_unicode.h"
#include "unicode_to_gbk.h"

//int dprintf(char *format, ... )
//{
//#if 0
//	char buf[1024];
//	va_list marker;
//	int n;

//	va_start(marker, format);				// 开始分析字符串
//	n = vsprintf(buf, format, marker);		// 格式化输出字符串
//	printf("%s", buf);
//	va_end(marker);							// 结束分析
//	return 0;
//#else
//	return 0;
//#endif
//}




/*-----------------+-------------------------+------------------------
                   |    endian = 1 大端      |
				   |    endian = 0 小端      |
-------------------+-------------------------+-----------------------*/

int GbkToUnicode(unsigned char *gbk_buf, unsigned short *unicode_buf, int max_unicode_buf_size, int endian)
{
		unsigned short word;
		unsigned char *gbk_ptr = gbk_buf;
		unsigned short *uni_ptr = unicode_buf;
		unsigned int uni_ind = 0, gbk_ind = 0, uni_num = 0;
		unsigned char ch;
		int word_pos;

    if( !gbk_buf || !unicode_buf )
        return -1;

    while(1)
    {
    	ch = *(gbk_ptr + gbk_ind);
			if(ch == 0x00)
			break;	
			if( ch > 0x80 )
			{
				if(endian == 1)  //大端
				{
					word = *(gbk_ptr + gbk_ind);
					word <<= 8;
					gbk_ind++;
					word += *(gbk_ptr + gbk_ind);
					gbk_ind++;
				}
				else
				{
					word = *(gbk_ptr + gbk_ind + 1);
					word <<= 8;
					word += *(gbk_ptr + gbk_ind);
					gbk_ind += 2;
				}
				word_pos = word - g_gbk_first_code;
				if(word >= g_gbk_first_code && word <= g_gbk_last_code  && (word_pos < g_gbk_to_unicode_buf_size))
				{
					*(uni_ptr + uni_ind) = g_gbk_to_unicode_buf[word_pos];
					uni_ind++;
					uni_num++;
				}
			}
			else
			{
				gbk_ind++;
				*(uni_ptr + uni_ind) = ch;
				uni_ind++;
				uni_num++;
			}
        
      if(uni_num > max_unicode_buf_size - 1)
			break;
    }
		*(uni_ptr + uni_ind)=0;
    return uni_num;
}
