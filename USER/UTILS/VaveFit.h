#pragma once
#include "sys.h"
typedef struct
{
	float last;
	float now;
	float next;
}Wave;

typedef struct {
	u8 order;//阶数
	Wave **vave;//阶数组指针
	float fitAcc;//滤波精度
}OrderWaveFit;

OrderWaveFit * CreateVaveFit(u8 order,float acc);
void VaveFitInit(OrderWaveFit *oderFit,float data);
float VaveFitCalcul(OrderWaveFit *oderFit,float data);
void VaveFitDelete(OrderWaveFit *oderFit);
void PrintWave(OrderWaveFit *oderFit);
