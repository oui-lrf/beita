#ifndef __ECC_H__
#define __ECC_H__
#include "sys.h"
typedef unsigned char   u_char;
typedef unsigned char   uint8_t;  
typedef unsigned int   uint32_t;  
int TestEcc(void);
int calculate_ecc(const u_char *dat,uint32_t datlen, u_char *ecc_code);
int correct_data( u_char *dat, u16 datlen, u_char *read_ecc, u_char *calc_ecc);
#endif
