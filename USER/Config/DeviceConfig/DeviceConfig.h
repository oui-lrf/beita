#ifndef __DEVICECONFIG_H__
#define __DEVICECONFIG_H__

#ifdef __cplusplus
 extern "C" {
#endif
#include "sys.h" 

#define DEVICE_VERSION  0x01
#define DEVICE_TYPE_NUM  0x10 
#define PRODUC_CODE "b9817"	
	 
	 
extern const char SOFT_CODE_BUFFER[10];
#define SOFT_CODE SOFT_CODE_BUFFER	 
	 
#define USER_CODE_LEN 40	 
//#define HTTP_IP "47.97.155.219"
//#define	HTTP_PORT 8888

#define HTTP_IP "114.215.69.92"
#define	HTTP_PORT 8090

typedef struct {  

	float coatQua;	
	u16 sampSpaceTime;
	u16 airOutTime;
	
	u16 openTime1;
	u16 openTime2;
	u16 openTime3;
	u16 openTime4;
	
	u16 closeTime1;
	u16 closeTime2;
	u16 closeTime3;
	u16 closeTime4;
	
	u16 timerSta1;
	u16 timerSta2;
	u16 timerSta3;
	u16 timerSta4;
	
	
	u16 timerUserData1;
	u16 timerUserData2;
	u16 timerUserData3;
	u16 timerUserData4;
	char userCode[40];
	float pidP;
	float pidI;
	float pidD;	
	
	float smoke1;
	float pm1;
	float nmhc1;

	float smoke2;
	float pm2;
	float nmhc2;

	float smoke3;
	float pm3;
	float nmhc3;

	u8 sensorType;
}DeviceParam;	 
void GetDeviceParam(DeviceParam * param);	 

u8 GetSensorType(void);

float  GetCoatQua(void);
u16  GetAirOutTime(void);
u16  GetSampSpaceTime(void);


u16  GetOpenTime1(void);
u16  GetOpenTime2(void);
u16  GetOpenTime3(void);
u16  GetOpenTime4(void);
u16  GetCloseTime1(void);
u16  GetCloseTime2(void);
u16  GetCloseTime3(void);
u16  GetCloseTime4(void);
u16  GetTimerSta1(void);
u16  GetTimerSta2(void);
u16  GetTimerSta3(void);
u16  GetTimerSta4(void);
u16  GetTimeStopTemp1(void);
u16  GetTimeStopTemp2(void);
u16  GetTimeStopTemp3(void);
u16  GetTimeStopTemp4(void);
float  GetPidP(void);
float  GetPidI(void);
float  GetPidD(void);
u8 GetUserCode(char * res, u16 size);



float GetPm1(void);
float GetPm2(void);
float GetPm3(void);

float GetSmoke1(void);
float GetSmoke2(void);
float GetSmoke3(void);

float GetNmhc1(void);
float GetNmhc2(void);
float GetNmhc3(void);


////////////////////////////////////////
u8 SetSensorType(u8 value);
u8 SetSmoke1(float vlaue);
u8 SetPm1(float vlaue);
u8 SetNmhc1(float vlaue);

u8 SetSmoke2(float vlaue);
u8 SetPm2(float vlaue);
u8 SetNmhc2(float vlaue);

u8 SetSmoke3(float vlaue);
u8 SetPm3(float vlaue);
u8 SetNmhc3(float vlaue);


u8  SetPidP(float data);
u8  SetPidI(float data);
u8  SetPidD(float data);
u8 SetUserCode(char* data);
u8  SetSampSpaceTime(u16 data);
u8  SetAirOutTime(u16 data);
u8  SetCoatQua(float data);

u8  SetCloseTime1(u16 data);
u8  SetCloseTime2(u16 data);
u8  SetCloseTime3(u16 data);
u8  SetCloseTime4(u16 data);
u8  SetOpenTime1(u16 data);
u8  SetOpenTime2(u16 data);
u8  SetOpenTime3(u16 data);
u8  SetOpenTime4(u16 data);

u8  SetTimerSta1(u16 data);
u8  SetTimerSta2(u16 data);
u8  SetTimerSta3(u16 data);
u8  SetTimerSta4(u16 data);
	 
u8  SetTimeStopTemp1(u16 data);
u8  SetTimeStopTemp2(u16 data);
u8  SetTimeStopTemp3(u16 data);
u8  SetTimeStopTemp4(u16 data);

void ResetDeviceParam(void); 
void DeviceConfigInit(void);
void SaveDeviceParam(void);
u8 ParamModify(u16 value,u16* modParamPoint);
#ifdef __cplusplus
}
#endif

#endif
