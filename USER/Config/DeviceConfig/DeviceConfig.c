#include "DeviceConfig.h"
#include "FlashPort.h"
#include "SysConfig.h"
#include "cmsis_os.h"
#include "string.h"
#include "Debug.h"

const char SOFT_CODE_BUFFER[10] = "b1117";

DeviceParam deviceParam;
osMutexId  deviceConfigMutexId=0;


void PrintDeviceParam(void);

static void CreateMutx(void)
{
	static osMutexDef_t  mutex;
	deviceConfigMutexId = osMutexCreate(&mutex);
}
static void GetMutx(void)
{
	if(deviceConfigMutexId)osMutexWait(deviceConfigMutexId, portMAX_DELAY);
}

static void ReleaseMutx(void)
{
	if(deviceConfigMutexId)osMutexRelease(deviceConfigMutexId);
}

void SaveDeviceParam(void)
{
  save_para_flash(BOILER_FLASH,sizeof(deviceParam),&deviceParam);
}

void DeviceConfigInit(void)
{
	CreateMutx();
	read_para_flash(BOILER_FLASH,sizeof(DeviceParam),&deviceParam);
	PrintDeviceParam();
}

void GetDeviceParam(DeviceParam * param)
{
	GetMutx();
	memcpy(param,&deviceParam,sizeof(DeviceParam));
	ReleaseMutx();
}

float  GetCoatQua(void)
{
	float res;
	GetMutx();
	res = deviceParam.coatQua; 
	ReleaseMutx();
	return res;
}


float  GetPidP(void)
{
	float res;
	GetMutx();
	res = deviceParam.pidP; 
	ReleaseMutx();
	return res;
}
float  GetPidI(void)
{
	float res;
	GetMutx();
	res = deviceParam.pidI; 
	ReleaseMutx();
	return res;
}
float  GetPidD(void)
{
	u16 res;
	GetMutx();
	res = deviceParam.pidD; 
	ReleaseMutx();
	return res;
}

float GetPm1(void)
{
	float res;
	GetMutx();
	res = deviceParam.pm1; 
	ReleaseMutx();
	return res;
}
float GetPm2(void)
{
	float res;
	GetMutx();
	res = deviceParam.pm2; 
	ReleaseMutx();
	return res;
}
float GetPm3(void)
{
	float res;
	GetMutx();
	res = deviceParam.pm3; 
	ReleaseMutx();
	return res;
}

float GetSmoke1(void)
{
	float res;
	GetMutx();
	res = deviceParam.smoke1; 
	ReleaseMutx();
	return res;
}
float GetSmoke2(void)
{
	float res;
	GetMutx();
	res = deviceParam.smoke2; 
	ReleaseMutx();
	return res;
}
float GetSmoke3(void)
{
	float res;
	GetMutx();
	res = deviceParam.smoke3; 
	ReleaseMutx();
	return res;
}

float GetNmhc1(void)
{
	float res;
	GetMutx();
	res = deviceParam.nmhc1; 
	ReleaseMutx();
	return res;
}
float GetNmhc2(void)
{
	float res;
	GetMutx();
	res = deviceParam.nmhc2; 
	ReleaseMutx();
	return res;
}
float GetNmhc3(void)
{
	float res;
	GetMutx();
	res = deviceParam.nmhc3; 
	ReleaseMutx();
	return res;
}


u16  GetSampSpaceTime(void)
{
	u16 res;
	GetMutx();
	res = deviceParam.sampSpaceTime; 
	ReleaseMutx();
	return res;
}


u16  GetAirOutTime(void)
{
	u16 res;
	GetMutx();
	res = deviceParam.airOutTime; 
	ReleaseMutx();
	return res;
}


u16  GetOpenTime1(void)
{
	u16 res;
	GetMutx();
	res = deviceParam.openTime1; 
	ReleaseMutx();
	return res;
}
u16  GetOpenTime2(void)
{
	u16 res;
	GetMutx();
	res = deviceParam.openTime2; 
	ReleaseMutx();
	return res;
}
u16  GetOpenTime3(void)
{
	u16 res;
	GetMutx();
	res = deviceParam.openTime3; 
	ReleaseMutx();
	return res;
}
u16  GetOpenTime4(void)
{
	u16 res;
	GetMutx();
	res = deviceParam.openTime4; 
	ReleaseMutx();
	return res;
}
u16  GetCloseTime1(void)
{
	u16 res;
	GetMutx();
	res = deviceParam.closeTime1; 
	ReleaseMutx();
	return res;
}
u16  GetCloseTime2(void)
{
	u16 res;
	GetMutx();
	res = deviceParam.closeTime2; 
	ReleaseMutx();
	return res;
}
u16  GetCloseTime3(void)
{
	u16 res;
	GetMutx();
	res = deviceParam.closeTime3; 
	ReleaseMutx();
	return res;
}
u16  GetCloseTime4(void)
{
	u16 res;
	GetMutx();
	res = deviceParam.closeTime4; 
	ReleaseMutx();
	return res;
}

u16  GetTimerSta1(void)
{
	u16 res;
	GetMutx();
	res = deviceParam.timerSta1; 
	ReleaseMutx();
	return res;
}
u16  GetTimerSta2(void)
{
	u16 res;
	GetMutx();
	res = deviceParam.timerSta2; 
	ReleaseMutx();
	return res;
}
u16  GetTimerSta3(void)
{
	u16 res;
	GetMutx();
	res = deviceParam.timerSta3; 
	ReleaseMutx();
	return res;
}
u16  GetTimerSta4(void)
{
	u16 res;
	GetMutx();
	res = deviceParam.timerSta4; 
	ReleaseMutx();
	return res;
}

u16  GetTimeStopTemp1(void)
{
	u16 res;
	GetMutx();
	res = deviceParam.timerUserData1; 
	ReleaseMutx();
	return res;
}
u16  GetTimeStopTemp2(void)
{
	u16 res;
	GetMutx();
	res = deviceParam.timerUserData2; 
	ReleaseMutx();
	return res;
}
u16  GetTimeStopTemp3(void)
{
	u16 res;
	GetMutx();
	res = deviceParam.timerUserData3; 
	ReleaseMutx();
	return res;
}
u16  GetTimeStopTemp4(void)
{
	u16 res;
	GetMutx();
	res = deviceParam.timerUserData4; 
	ReleaseMutx();
	return res;
}


u8 GetSensorType(void)
{
	u8 res;
	GetMutx();
	res = deviceParam.sensorType; 
	ReleaseMutx();
	return res;
}

u8  SetPidP(float data)
{
	GetMutx();
	if(data >=1000.0f)
	{
		data = 999.9f;
	}
	deviceParam.pidP =data; 
	ReleaseMutx();
	return 1;
}
u8  SetPidI(float data)
{
	GetMutx();
	if(data >=1000.0f)
	{
		data = 999.9f;
	}
	deviceParam.pidI =data; 
	ReleaseMutx();
	return 1;
}
u8  SetPidD(float data)
{
	GetMutx();
	if(data >=1000.0f)
	{
		data = 999.9f;
	}
	deviceParam.pidD =data; 
	ReleaseMutx();
	return 1;
}

u8 SetSmoke1(float vlaue)
{
	GetMutx();
	if(vlaue >=1000.0f)
	{
		vlaue = 999.9f;
	}
	deviceParam.smoke1 =vlaue; 
	ReleaseMutx();
	return 1;
}
u8 SetSmoke2(float vlaue)
{
	GetMutx();
	if(vlaue >=1000.0f)
	{
		vlaue = 999.9f;
	}
	deviceParam.smoke2 =vlaue; 
	ReleaseMutx();
	return 1;
}
u8 SetSmoke3(float vlaue)
{
	GetMutx();
	if(vlaue >=1000.0f)
	{
		vlaue = 999.9f;
	}
	deviceParam.smoke3 =vlaue; 
	ReleaseMutx();
	return 1;
}

u8 SetPm1(float vlaue)
{
	GetMutx();
	if(vlaue >=1000.0f)
	{
		vlaue = 999.9f;
	}
	deviceParam.pm1 =vlaue; 
	ReleaseMutx();
	return 1;
}
u8 SetPm2(float vlaue)
{
	GetMutx();
	if(vlaue >=1000.0f)
	{
		vlaue = 999.9f;
	}
	deviceParam.pm2 =vlaue; 
	ReleaseMutx();
	return 1;

}

u8 SetPm3(float vlaue)
{
	GetMutx();
	if(vlaue >=1000.0f)
	{
		vlaue = 999.9f;
	}
	deviceParam.pm3 =vlaue; 
	ReleaseMutx();
	return 1;
}
u8 SetNmhc1(float vlaue)
{
	GetMutx();
	if(vlaue >=1000.0f)
	{
		vlaue = 999.9f;
	}
	deviceParam.nmhc1 =vlaue; 
	ReleaseMutx();
	return 1;
}
u8 SetNmhc2(float vlaue)
{
	GetMutx();
	if(vlaue >=1000.0f)
	{
		vlaue = 999.9f;
	}
	deviceParam.nmhc2 =vlaue; 
	ReleaseMutx();
	return 1;
}
u8 SetNmhc3(float vlaue)
{
	GetMutx();
	if(vlaue >=1000.0f)
	{
		vlaue = 999.9f;
	}
	deviceParam.nmhc3 =vlaue; 
	ReleaseMutx();
	return 1;
}





u8  SetAirOutTime(u16 data)
{
	if( data <200)
	{
		GetMutx();
		deviceParam.airOutTime =data; 
		ReleaseMutx();	
		return 1;
	}
	return 0;
}


u8  SetSampSpaceTime(u16 data)
{
	if( data <200)
	{
		GetMutx();
		deviceParam.sampSpaceTime =data; 
		ReleaseMutx();	
		return 1;
	}
	return 0;
}

u8  SetCoatQua(float data)
{
	if( data <100.0f)
	{
		GetMutx();
		deviceParam.coatQua =data; 
		ReleaseMutx();	
	}
	return 1;
}

u8  SetCloseTime1(u16 data)
{
	GetMutx();
	deviceParam.closeTime1 = data; 
	ReleaseMutx();
	return 1;
}

u8  SetCloseTime2(u16 data)
{
	GetMutx();
	deviceParam.closeTime2 = data; 
	ReleaseMutx();
	return 1;
}
u8  SetCloseTime3(u16 data)
{
	GetMutx();
	deviceParam.closeTime3 = data; 
	ReleaseMutx();
	return 1;
}
u8  SetCloseTime4(u16 data)
{
	GetMutx();
	deviceParam.closeTime4 = data; 
	ReleaseMutx();
	return 1;
}

u8  SetOpenTime1(u16 data)
{
	GetMutx();
	deviceParam.openTime1 = data; 
	ReleaseMutx();
	return 1;
}
u8  SetOpenTime2(u16 data)
{
	GetMutx();
	deviceParam.openTime2 = data; 
	ReleaseMutx();
	return 1;
}
u8  SetOpenTime3(u16 data)
{
	GetMutx();
	deviceParam.openTime3 = data; 
	ReleaseMutx();
	return 1;
}
u8  SetOpenTime4(u16 data)
{
	GetMutx();
	deviceParam.openTime4 = data; 
	ReleaseMutx();
	return 1;
}

u8  SetTimerSta1(u16 data)
{
	GetMutx();
	deviceParam.timerSta1 = data; 
	ReleaseMutx();
	return 1;
}
u8  SetTimerSta2(u16 data)
{
	GetMutx();
	deviceParam.timerSta2 = data; 
	ReleaseMutx();
	return 1;
}
u8  SetTimerSta3(u16 data)
{
	GetMutx();
	deviceParam.timerSta3 = data; 
	ReleaseMutx();
	return 1;
}
u8  SetTimerSta4(u16 data)
{
	GetMutx();
	deviceParam.timerSta4 = data; 
	ReleaseMutx();
	return 1;
}

u8  SetTimeStopTemp1(u16 data)
{
	GetMutx();
	deviceParam.timerUserData1 = data; 
	ReleaseMutx();
	return 1;
}

u8  SetTimeStopTemp2(u16 data)
{
	GetMutx();
	deviceParam.timerUserData2 = data; 
	ReleaseMutx();
	return 1;
}
u8  SetTimeStopTemp3(u16 data)
{
	GetMutx();
	deviceParam.timerUserData3 = data; 
	ReleaseMutx();
	return 1;
}
u8  SetTimeStopTemp4(u16 data)
{
	GetMutx();
	deviceParam.timerUserData4 = data; 
	ReleaseMutx();
	return 1;
}



u8 GetUserCode(char * res, u16 size)
{
	if(size<40)
	{
		return 0;
	}
	if(!res)
	{
		return 0;
	}
	GetMutx();
	char *top = strncpy(res,deviceParam.userCode,size);
	ReleaseMutx();
	u16 len =strlen(top);
	if(len <= 40)
	{
		res[len]=0;
		return 1;
	}
	else
	{
		return 0;
	}
}
u8 SetUserCode(char* data)
{
	u8 len = strlen(data);
	if(len >= 40)
	{
		return 0;
	}
	GetMutx();
	char *top  =strncpy(deviceParam.userCode,data,40);
	ReleaseMutx();
	u16 ln =strlen(top);
	deviceParam.userCode[ln]=0;
	return 1;
}
#include "ModPort.h"
u8 SetSensorType(u8 value)
{
	GetMutx();
	deviceParam.sensorType=value;
	ReleaseMutx();
	return 1;
}


void ResetDeviceParam(void)
{
	deviceParam.coatQua = 0.888f;
	deviceParam.sampSpaceTime =5;
	deviceParam.airOutTime=5;

	deviceParam.openTime1 =0;
	deviceParam.openTime2 =0;
	deviceParam.openTime3 =0;
	deviceParam.openTime4 =0;
	deviceParam.closeTime1 =0;
	deviceParam.closeTime2 =0;
	deviceParam.closeTime3 =0;
	deviceParam.closeTime4 =0;
	deviceParam.timerSta1 =0;
	deviceParam.timerSta2 =0;
	deviceParam.timerSta3 =0;
	deviceParam.timerSta4 =0;
	deviceParam.timerUserData1 =0;
	deviceParam.timerUserData2 =0;
	deviceParam.timerUserData3 =0;
	deviceParam.timerUserData4 =0;
	
	strcpy(deviceParam.userCode,"00000000");

	deviceParam.pidP = 0.0f;
	deviceParam.pidI = 0.0f;
	deviceParam.pidD = 0.0f;
	deviceParam.smoke1=1.0f;
	deviceParam.smoke2=1.0f;
	deviceParam.smoke3=1.0f;
	deviceParam.pm1=5.0f;
	deviceParam.pm2=5.0f;
	deviceParam.pm3=5.0f;
	deviceParam.nmhc1=10.0f;
	deviceParam.nmhc2=10.0f;
	deviceParam.nmhc3=10.0f;
	
	deviceParam.sensorType = 0;
	save_para_flash(BOILER_FLASH,sizeof(DeviceParam),&deviceParam);
}


void PrintDeviceParam(void)
{
	printf("--------------PrintDeviceParam-----------------");
	printf("coatQua:%f\r\n",deviceParam.coatQua);
	printf("sampSpaceTime:%d\r\n",deviceParam.sampSpaceTime);
	printf("airOutTime:%d\r\n",deviceParam.airOutTime);
}
