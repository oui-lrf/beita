#ifndef __SYSCONFIG_H__
#define __SYSCONFIG_H__

#ifdef __cplusplus
 extern "C" {
#endif
#include "sys.h"
#include "DeviceConfig.h"

typedef struct { 
	u32 eqpId;
	u8 screenSw;
	char screenPass[20];
	u16 firstUse;
}SysParam;

void SaveSysParam(void);
u32 GetEqpId(void);
void SetEqpId(u8 value);
u8 GetScreenSw(void);
void SetScreenSw(u8 value);
void SysConfigInit(void);
void ResetSysParam(void);
void SetFirstUse(void);
u8 GetScreenPass(char * res, u16 size);
u8 SetScreenPass(char* data);
#ifdef __cplusplus
}
#endif
#endif
