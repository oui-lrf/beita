#include "SysConfig.h"
#include "string.h"
#include "FlashPort.h"
//#include "Record.h"
#include "NetConfig.h"
#include "Debug.h"
#include "cmsis_os.h"

SysParam sysParam;
osMutexId  sysConfigMutexId=0;
static void CreateMutx(void)
{
	static osMutexDef_t  mutex;
	sysConfigMutexId = osMutexCreate(&mutex);
}
static void GetMutx(void)
{
	if(sysConfigMutexId)osMutexWait(sysConfigMutexId, portMAX_DELAY);
}

static void ReleaseMutx(void)
{
	if(sysConfigMutexId)osMutexRelease(sysConfigMutexId);
}
//检测第一次使用设备，设置初始参数
void CheckFirstUser() 
{
	if(sysParam.firstUse != ((DEVICE_TYPE_NUM<<8)))//|DEVICE_VERSION
	{
		printf("CheckFirstUser Reset\r\n");
		ResetSysParam();
		ResetNetParam();
		ResetDeviceParam();
		//DeleteAllRecord();
	}
}

void ResetSysParam(void)
{
	printf("ResetSysParam\r\n");
	sysParam.eqpId=0;
	sysParam.firstUse=((DEVICE_TYPE_NUM<<8));//|DEVICE_VERSION
	sysParam.screenSw=1;
	strncpy(sysParam.screenPass,"000000",10);
	SaveSysParam();
}

void PrintSysParam(void)
{
	printf("sysParam size:%d\r\n",sizeof(SysParam));
	printf("screenSw:%d\r\n",sysParam.screenSw);
	printf("FirstUse:%x\r\n",sysParam.firstUse);
	printf("EqpId:%d\r\n",sysParam.eqpId);
}

void SetEqpId(u8 value)
{
	GetMutx();
	sysParam.eqpId = value;
	ReleaseMutx();
}

void SetScreenSw(u8 value)
{
	GetMutx();
	sysParam.screenSw = value;
	ReleaseMutx();
}

u32 GetEqpId(void)
{
	u32 eqpId;
	GetMutx();
	eqpId = sysParam.eqpId;
	ReleaseMutx();
	return eqpId;
}

u8 GetScreenSw(void)
{
	u8 rt;
	GetMutx();
	rt = sysParam.screenSw;
	ReleaseMutx();
	return rt;
}




u8 GetScreenPass(char * res, u16 size)
{
	if(size<20)
	{
		return 0;
	}
	if(!res)
	{
		return 0;
	}
	GetMutx();
	char *top = strncpy(res,sysParam.screenPass,size);
	ReleaseMutx();
	u16 len =strlen(top);
	if(len <= 20)
	{
		res[len]=0;
		return 1;
	}
	else
	{
		return 0;
	}
}


void SetFirstUse(void)
{
	GetMutx();
	sysParam.firstUse=0;
	ReleaseMutx();
}

u8 SetScreenPass(char* data)
{
	u8 len = strlen(data);
	if(len >= 20)
	{
		return 0;
	}
	GetMutx();
	char *top  =strncpy(sysParam.screenPass,data,20);
	ReleaseMutx();
	u16 ln =strlen(top);
	sysParam.screenPass[ln]=0;
	return 1;
}

void SysConfigInit(void)
{
	CreateMutx();
	read_para_flash(SYS_FLASH,sizeof(sysParam),&sysParam);//更新Flash参数到结构体
	PrintSysParam();
	
	u8 app_sys_buf[28];
	read_para_flash(SF_MAX_PAGE_SIZE-4*4096,28,app_sys_buf);
	printf("app_sys_buf:\r\n");
	for(u16 i=0;i<28;i++)
	{
		printf("%02x ",app_sys_buf[i]);
	}
	printf("\r\n");
	CheckFirstUser();
	NetConfigInit();
	DeviceConfigInit();
}

void SaveSysParam(void)
{
	save_para_flash(SYS_FLASH,sizeof(SysParam),&sysParam);
}

