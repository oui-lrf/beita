#include "HdPort.h"
#include "adc.h"
#include "SysConfig.h"
#include "temp.h"
#include "cmsis_os.h"
#include "string.h"



RunSensorInput sensor;

SemaphoreHandle_t  sensorMutexId=0;
static void CreateMutx(void)
{
	static osMutexDef_t  mutex;
	sensorMutexId = osMutexCreate(&mutex);
}
static void GetMutx(void)
{
	if(sensorMutexId)osMutexWait(sensorMutexId, portMAX_DELAY);
}

static void ReleaseMutx(void)
{
	if(sensorMutexId)osMutexRelease(sensorMutexId);
}

int GetDiff(int v1,int v2)
{

	if(v1 >v2)
	{
		return v1 -v2;
	}
	else
	{
		return v2 -v1;
	}
}

void UpdataSensorData()
{
	RunSensorInput newSensor;
	UpdataIoPinStaForomHd();
	UpdataInputSwStaForomHd();
	
	newSensor.hotSta=GetHotSta();
	newSensor.waterSta=GetWaterSta();
	newSensor.roomTemp=GetNtcTemp(Get_Adc_Average(NTC1_ADC,20));
	newSensor.inWaterTemp=GetNtcTemp(Get_Adc_Average(NTC2_ADC,60));//GetNtcTemp(Get_Adc_Average(NTC2_ADC,10));
	//记录上一次的数据，本次和上次没有大差距差更新
	int newTemp	=GetPt100Temp(Get_Adc_Average(PT1_ADC,20));
	newSensor.outTemp =newTemp;
//	printf("nt1:%d,nt2:%f,pt:%d\r\n",newSensor.roomTemp,newSensor.inWaterTemp,newSensor.outTemp);
	GetMutx();
	memcpy(&sensor,&newSensor,sizeof(RunSensorInput));
	ReleaseMutx();
}

void GetSensor(RunSensorInput *resSensor)
{
	GetMutx();
	memcpy(resSensor,&sensor,sizeof(RunSensorInput));
	ReleaseMutx();
}	

float GetNtcTemp1(void)
{
	float temp=0;
	GetMutx();
	temp = sensor.roomTemp;
	ReleaseMutx();
	return temp;
}

float GetNtcTemp2(void)
{
	float temp=0.0f;
	GetMutx();
	temp = sensor.inWaterTemp;
	ReleaseMutx();
	return temp;
}

int GetPtTemp1(void)
{
	int temp=0;
	GetMutx();
	temp = sensor.outTemp;
	ReleaseMutx();
	return temp;
}

u8 GetRunHotSta(void)
{
	u8 sta =0;
	GetMutx();
	sta = sensor.hotSta;
	ReleaseMutx();
	return sta;
}

u8 GetRunWaterSta(void)
{
	u8 sta =0;
	GetMutx();
	sta = sensor.waterSta;
	ReleaseMutx();
	return sta;
}


//初始化信号互斥量
//系统启动前调用
void HdDataInit(void)
{
	CreateMutx();
}
