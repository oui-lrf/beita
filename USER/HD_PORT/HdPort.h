#pragma once
#ifdef __cplusplus
 extern "C" {
#endif
#include "IoSwHd.h"
#include "InputSwHd.h"
#include "HotSw.h"
#include "Water.h"
#include "temp.h" 

typedef struct{
	float inWaterTemp;
	int roomTemp;
	int outTemp;
	u16 waterSta;
	HOT_STA hotSta;
}RunSensorInput;	
	 
void UpdataSensorData(void);	 
void GetSensor(RunSensorInput *resSensor);
float GetNtcTemp1(void);
float GetNtcTemp2(void);
int GetPtTemp1(void);

u8 GetRunHotSta(void);

u8 GetRunWaterSta(void);

//初始化信号互斥量
//系统启动前调用
void HdDataInit(void);
#ifdef __cplusplus
 }
#endif

