#include "IoSwHd.h"
#include "pcf8575.h"
#include "Debug.h"
#include "cmsis_os.h"
	
volatile uint16_t ioSwOutState=0;
volatile uint16_t ioSwInputState=0;

//更新开关状态到外设
void UpdataPinStaToHd(u16 data)
{
	Set16BitPin(IO_PCF8575_ADDR,data);
}
//从外设更新状态
void UpdataIoPinStaForomHd()
{
	u16 read;
	read = PCF8575_ReadTwoByte(IO_PCF8575_ADDR);
	taskENTER_CRITICAL();
	ioSwInputState = read;
	ioSwOutState = read;
	taskEXIT_CRITICAL();
//	printf("-->ioSwInputState:%x\r\n",ioSwInputState);
}

void OutPinInit(void)
{
	taskENTER_CRITICAL();
	ioSwOutState = 0;
	taskEXIT_CRITICAL();
	UpdataPinStaToHd(0);
}


void SetPinSta(u8 bit,BOOL value)
{
	taskENTER_CRITICAL();
	BOOL oldV = !!(ioSwOutState&(0x01<<bit));
	taskEXIT_CRITICAL();
//	Log("SetPinSta bit:%d,oldv:%d,v:%d",bit,oldV,value);
	if(oldV != value)//判断指定位是否为要修改的值
	{
		taskENTER_CRITICAL();
		ioSwOutState &= ~(0x01<<bit);//清除指定位
		ioSwOutState |=  (value<<bit);//设置指定位
		u16 write = ioSwOutState;
		taskEXIT_CRITICAL();
		UpdataPinStaToHd(write);
	}
}
//把输出状态和输入状态分离，如果是UI控制输出的，同时更新输入状态
void SetPinStaForUi(u8 bit,BOOL value)
{
	
	Log("SetPinStaForUi:%d,value:%d",bit,value);
	SetPinSta(bit,value);
	taskENTER_CRITICAL();
	ioSwInputState = ioSwOutState;
	taskEXIT_CRITICAL();
}

u16 GetIoSwInputSta(void)
{
	u16 rt =0;
	taskENTER_CRITICAL();
	rt =ioSwInputState;
	taskEXIT_CRITICAL();
	return rt;
}

u16 GetIoSwOutSta(void)
{
	u16 rt =0;
	taskENTER_CRITICAL();
	rt =ioSwOutState;
	taskEXIT_CRITICAL();
	return rt;
}


BOOL GetIoSwInputBit(u8 bit)
{
	BOOL rt =0;
	
	taskENTER_CRITICAL();
	rt =!!(ioSwInputState & (0x01<<bit));
	taskEXIT_CRITICAL();
	return rt;
}

BOOL GetIoSwOutBit(u8 bit)
{
	BOOL rt =0;
	
	taskENTER_CRITICAL();
	rt =!!(ioSwOutState & (0x01<<bit)); 
	taskEXIT_CRITICAL();
	return rt;
}
