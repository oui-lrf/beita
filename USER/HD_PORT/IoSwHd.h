#ifndef __OUT_DV_H__
#define __OUT_DV_H__

#ifdef __cplusplus
 extern "C" {
#endif

#include "sys.h"
#include "pcf8575.h"
#define SW_OPEN 1
#define SW_CLOSE 0

void OutPinInit(void);
void SetPinSta(u8 bit,BOOL value);
BOOL GetIoSwOutBit(u8 bit);
BOOL GetIoSwInputBit(u8 bit);
void UpdataIoPinStaForomHd();
void SetPinStaForUi(u8 bit,BOOL value);
u16 GetIoSwInputSta(void);
u16 GetIoSwOutSta(void);
#ifdef __cplusplus
}
#endif
#endif
