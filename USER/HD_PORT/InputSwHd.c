#include "InputSwHd.h"
#include "pcf8575.h"


volatile uint16_t inputSwState=0;

//从外设更新状态
void UpdataInputSwStaForomHd()
{
	inputSwState = PCF8575_ReadTwoByte(INPUT_PCF8575_ADDR);
//	printf("----------->inputSwState:%04x\r\n",inputSwState);
}


BOOL GetInputSwBit(uint8_t bit)
{
	return !!(inputSwState&(0x01<<bit));
}
