#ifndef __HOTSW_H__
#define __HOTSW_H__
#ifdef __cplusplus
 extern "C" {
#endif
	 
#include "pcf8575.h"	 
	 
typedef enum {
	HOT_OK,
	HOT_ERR,
}HOT_STA;

HOT_STA GetHotSta();

#define HOT_SW_BIT INPUT_SW1
#ifdef __cplusplus
}
#endif
#endif
