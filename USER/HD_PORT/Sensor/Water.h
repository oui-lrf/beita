#ifndef __WATER_H__
#define __WATER_H__
#include "sys.h"
#ifdef __cplusplus
 extern "C" {
#endif
#include "pcf8575.h"
typedef enum
{
	WATER_S = 0xff, //1111
	WATER_L = 0xfe, //1110
	WATER_M = 0xfc, //1100
	WATER_H = 0xf8, //1000
	WATER_GH= 0xf0, //0000
}WATER_STA;

#define WATER_SW_BIT INPUT_SW5

u8 GetWaterSta();
#ifdef __cplusplus
 }
#endif

#endif
