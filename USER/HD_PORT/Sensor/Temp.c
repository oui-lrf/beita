#include "Temp.h"
#include "math.h"
#include "SysConfig.h"
#include "FreeEvent.h"
#include "math.h"
#include "Debug.h"
#include "VaveFit.h"

//double GetNtcTemp(u16 adx)
//{
//	const float Rp=10000.0; //10K
//	const float T2 = (273.15+25.0);;//T2
//	const float Bx = 3435.0;//B
//	const float Ka = 273.15;
//	const float ResUp = 10000;
//	float Rt;
//	float temp;
//	//Rt = Get_TempResistor();
//	Rt = (double)(adx*ResUp/(4096-adx))-4300;
//	
//	if(Rt <0)
//	{
//		Rt =0;
//	}
//	
//	//printf("ntc Rt:%f\r\n",Rt);
//	//like this R=5000, T2=273.15+25,B=3470, RT=5000*EXP(3470*(1/T1-1/(273.15+25)),  
//	temp = Rt/Rp;
//	temp = log(temp);//ln(Rt/Rp)
//	temp/=Bx;//ln(Rt/Rp)/B
//	temp+=(1/T2);
//	temp = 1/(temp);
//	temp-=Ka;
//	return temp;
//} 


double GetNtcTemp(u16 adx)
{
	float vout =adx*3.0f/4096;
	if(vout>3.3f)vout=3.3f;
//	Log("ntc Vout:%f",vout);
	const float ResUp = 10000.0f;
	double Rt = 0.000000f;
	double temp=0.000000f;
	Rt = (double)(vout*ResUp/(3.3f-vout));//-6000
	if(Rt <0)
	{
		Rt =0;
	}
	temp = (double)(((1/(log(Rt/10000)/3435.0f + 1/(25.0f+273.15f))-273.15f)*10+0.5f)/10); 
	if(temp < -40.0f)
	{
		temp = -40;
	}
	return temp;
}

//Rt = (double)(adx*10000/(4096-adx));
//	
//temp = (double)(((1/(log(Rt/10000)/3435.0f + 1/(25.0f+273.15f))-273.15f)*10+0.5f)/10); 
void Pt100Aline(double ntTemp,double ptTemp)
{
//	u16 i=0;
//	//double ntTemp = GetNtcTemp(ntData);
//	printf("-->Pt100Aline ntTemp:%f\r\n",ntTemp);
//	printf("-->Pt100Aline tempAcc:%f\r\n",deviceParam.tempAcc);
//	while(++i<1000)
//	{
//		if(ptTemp > ntTemp +0.2f)
//		{
//			deviceParam.tempAcc +=0.001f;
//		}
//		else if(ptTemp +0.2f < ntTemp)
//		{
//			deviceParam.tempAcc -=0.001f;
//		}
//		else
//		{
//			PostFreeEvent(EVENT_SAVE_DEVICE_FLASH);
//			break;
//		}
//		//printf("-->Pt100Aline tempAcc:%f\r\n",deviceParam.tempAcc);
//	}
//	printf("-->Pt100Aline tempAcc:%f\r\n",deviceParam.tempAcc);
}

double GetPt100Temp(u16 data)
{
		#define VCC 2.84f
		#define VAL (3.29f)
	
//	#define VAL deviceParam.tempAcc
//	printf("val:%f",VAL);
	/*测量温度求解函数，已知R，VREF*/ 
//	 double Vpt;
	  double rt,rt1,t; 
		double Vout,Vcha,Vin,V82;
		V82= VAL*82/3082;
		Vout =VCC*data/4096;//运放输出的电压 //4096	0.34f;//
		Vcha = Vout/10.0f;//电桥两端的电压差 理想情况是 10倍
		Vin = V82+Vcha;//热电阻输入端电压
		rt =3000*Vin/(VAL-Vin);//热电阻值

		//Log("pt data:%d",data);
//		Log("Vout:%f",Vout);
//		Log("V82:%f",V82);
//		Log("Vcha:%f",Vcha);
//		Log("Vin:%f",Vin);
//		Log("rt:%f",rt);
		t=(rt-100)/0.36f; 
	/*首先按照400度量程线性估计当前温度*/ 
	/*采用试差法循环计算，刚才估计的t做初始值*/ 
		do 
		{ 
			if(rt<100) /*摄氏零度以下处理*/ 
			{ 
				rt1=100+t*(0.390802f-0.0000580195f*t-0.000000000427351f*(t-100)*t*t); 
			} 
			else /*摄氏零度以下处理*/ 
			{ 
				rt1=100+t*(0.390802f-0.0000580195f*t); 
			} 
			t=t+(rt-rt1)/0.36f; 
		} 
		while(((rt-rt1)>0.005f)||(rt1-rt)>0.005f); 
				/*计算余差最终小于0.005欧*/ 
		/*试差结束，返回浮点数温度值*/
//		printf("pt100:%f\r\n",t); 
		if(t>800)t=800;
		if(t<-40)t=-40;		
		return t;   
}

