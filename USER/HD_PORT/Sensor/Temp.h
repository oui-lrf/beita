#ifndef __TEMP_H__
#define __TEMP_H__
#include "sys.h"
#include "adc.h"

#ifdef __cplusplus
 extern "C" {
#endif
#define NTC1_ADC  AD1
#define NTC2_ADC  AD2
#define PT1_ADC   AD3
#define PT2_ADC   AD4

double GetNtcTemp(u16 adx);
double GetPt100Temp(u16 adx);
void Pt100Aline(double ntTemp,double ptTemp);
	 #ifdef __cplusplus
 }
#endif
#endif

