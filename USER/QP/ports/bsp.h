/*****************************************************************************
* Product: DPP example, FreeRTOS kernel
* Last Updated for Version: 5.4.0
* Date of the Last Update:  2015-03-07
*
*                    Q u a n t u m     L e a P s
*                    ---------------------------
*                    innovating embedded systems
*
* Copyright (C) Quantum Leaps, LLC. state-machine.com.
*
* This program is open source software: you can redistribute it and/or
* modify it under the terms of the GNU General Public License as published
* by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Alternatively, this program may be distributed and modified under the
* terms of Quantum Leaps commercial licenses, which expressly supersede
* the GNU General Public License and are specifically designed for
* licensees interested in retaining the proprietary status of their code.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* Contact information:
* Web  : http://www.state-machine.com
* Email: info@state-machine.com
*****************************************************************************/
#ifndef bsp_h
#define bsp_h
#include "sys.h"
#include "qassert.h"
#include "qep.h"



#define BSP_TICKS_PER_SEC    configTICK_RATE_HZ

void BSP_init(void);
void BSP_displayPaused(uint8_t paused);
void BSP_displayPhilStat(uint8_t n, char_t const *stat);
void BSP_terminate(int16_t result);

void BSP_randomSeed(uint32_t seed);   /* random seed */
uint32_t BSP_random(void);            /* pseudo-random generator */

/* define the event signals used in the application ------------------------*/

enum QpSignals {
		
    USART_WORK_RECV_SIG = Q_USER_SIG,/* offset the first signal by Q_USER_SIG */
	
		
		GPRS_OPEN_SUCCES_SIG,
		GPRS_OPEN_FAILED_SIG,
		GPRS_CONNECT_SUCCES_SIG,
		GPRS_CONNECT_FAILED_SIG,
		GPRS_SEND_FAILED_SIG,
		GPRS_RECV_SIG,
		GPRS_DOWN_SIG,
		GPRS_DOWN_RECV_SIG,
		USART_SWITCH_FREE,
		USART_SEND_SIG,
		USART_CLOSE_SIG,
		TCP_CLOSE_SIG,
		TCP_CLOSE_CALL_SIG,
		TCP_OPEN_SIG,
		SWITCH_NET_TYPE_WIFI_SIG,
		SWITCH_NET_TYPE_GPRS_SIG,
		TCP_SWITCH_CLOSE_SIG,
		TCP_CONNECT_SIG,
		TCP_CONNECT_FAILED_SIG,
		TCP_CONNECT_SUCCES_SIG,
		TCP_RECV_SIG,

		
		BARE_TCP_PUBLISH_SIG,
		BARE_TCP_ON_SIG,
		BARE_TCP_CLOSE_SIG,
		BARE_TCP_REON,

		TCP_PAUSE_SIG,
		TCP_RESUME_SIG,
		MQTT_PAUSE_SIG,
		MQTT_RESUME_SIG,
		MAX_PUB_SIG,
		
		NET_SEND_SIG,
		MQTT_REON,
		TCP_SEND_SIG,
		GPRS_SEND_SIG,
		GPRS_MQTT_CREATE_SIG,
		GPRS_MQTT_SUB_SIG,
		GPRS_MQTT_PUB_SIG,
		GPRS_CONNECT_SIG,
		GPRS_CLOSE_SIG,
		USART_RECV_SIG , 
		USART_SWITCH_WIFI,
		USART_SWITCH_GPRS,
		//DEVICE_START_STOP_SIG,
		DEVICE_START_SIG,
		DEVICE_START_PID_SIG,
		DEVICE_STOP_SIG,
		TIMEOUT_SIG, 
		LOOP_TIMEOUT_SIG,
		TIMEOUT2_SIG,
		TIMEOUT3_SIG,
		TIMEOUT4_SIG,
		TIMEOUT5_SIG,
		TIMEOUT6_SIG,
		TIMEOUT7_SIG,
		TIMEDELAY_SIG, 
		
		TICK_SIG, /* time tick event */
    TIMER_IN_SIG,  /* set the alarm */
		TIMER_OUT_SIG,
    ALARM_ON_SIG,   /* turn the alarm on */
    ALARM_OFF_SIG,  /* turn the alarm off */
		TIME_SIG,
		TIMER_OFF_SIG,
		HAND_WARING,
		DEVICE_HAND_CONTROL_SW,
		DEVICE_UPDATA_PARAM_SIG,
		
		COAT_ALIGN_CONTROL_SIG,
		FLOWRATE_ALIGN_CONTROL_SIG,
		DIFF_UP_CONTROL_SIG,
		DIFF_LOW_CONTROL_SIG,
		AUTO_CHECK_CONTROL_SIG,
		STOP_SAMP_CONTROL_SIG,
		MOD_SEND_SIG,
		DEVICE_MOD_RECV_SIG,
		FREE_EVENT_SIG,
    MAX_SIG 
};

typedef struct {
/* protected: */
    QEvt super;

/* public: */
    uint32_t v;
		uint32_t p;
}QpEvt;

#endif /* bsp_h */
