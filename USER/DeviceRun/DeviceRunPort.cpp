#include "DeviceRunPort.h"
#include "DeviceRunWork.h"
#include "DeviceRunParam.h"
#include "DeviceRunInclude.h"
#include "HttpPack.h"
#include "ModPort.h"
#include "DeviceRunUsartWork.h"
#include "GprsPort.h"
#include "HttpPort.h"
#include "DeviceRunHttpApp.h"
#include "DeviceRunMqttApp.h"
#include "iap.h"

void SavaRecdFile(char *name,u32 value,VALUE_TYPE  valueType,char *unit);
extern QActive * const DV_DeviceWork;

u8 DeviceRunControl(u8 value)
{
	u8 runSta = GetDeviceRunSta();
	Log("DeviceRunControl runSta:%d,value:%d",runSta,value);
	if(runSta != value )
	{
		if(value == DEVICE_RUN_START || value == DEVICE_RUN_NET_CONTROL_START)
		{
			QACTIVE_POST(DV_DeviceWork, &Q_NEW(QpEvt, DEVICE_START_SIG)->super,0);
		}
		else if(value == DEVICE_RUN_STOPING)
		{
			QACTIVE_POST(DV_DeviceWork, &Q_NEW(QpEvt, DEVICE_STOP_SIG)->super,0);
		}
		return value;
	}
	else
	{
		return runSta;
	}
}




void DeviceRecvModCallEx(u8 type,u16 addr,u16 reg,u16 value)
{
	//Log("DeviceRecvModCallEx type:%d addr:%d,bit:%d,value:%d",type,addr,reg,value);
	ModRecvObj *mod =  (ModRecvObj *)DeviceRunMalloc(sizeof(ModRecvObj)); 
	if(!mod)
	{
		Log("malloc err");
		return ;
	}
	mod->addr = addr;
	mod->type = (MOD_CMD_TYPE)type;
	mod->reg = reg;
	mod->value = value;
	QpEvt *te;
	te = Q_NEW(QpEvt, DEVICE_MOD_RECV_SIG);
	te->p = (uint32_t)mod;
	te->v = sizeof(ModRecvObj);
	QACTIVE_POST(DV_DeviceWork, &te->super, me);
}


void CamUsartSend(u8 *data,u16 size)
{
//	HAL_GPIO_WritePin(SW485_Port, SW485_Pin, GPIO_PIN_SET);
	//Log("CamUsartSend:%s",data);
	vTaskSuspendAll();
	UsartSendBuf(2,data,size);
	( void ) xTaskResumeAll();
//	HAL_GPIO_WritePin(SW485_Port, SW485_Pin, GPIO_PIN_RESET);
}

void ClearDeviceWaring(void)
{
	QACTIVE_POST(DV_DeviceWork, &Q_NEW(QpEvt, HAND_WARING)->super,0);
}
//外部调用
void HandControlSw(u8 bit,u8 value)
{
	u8 old = GetIoSwInputBit(bit);
	if(value != old)
	{
		QpEvt *te;
		te = Q_NEW(QpEvt, DEVICE_HAND_CONTROL_SW);
		te->p = bit;
		te->v = value;
		QACTIVE_POST(DV_DeviceWork, &te->super,0);	
	}
}

u8 GetDeviceRunSta(void)
{
	u8 rt ;
	QF_CRIT_STAT_
	QF_CRIT_ENTRY_();
	rt = ((DeviceWork *)DV_DeviceWork)->sysRunSta;
	QF_CRIT_EXIT_();
	return rt;
}


void RunStartCall(DeviceWork * const me)
{
//	return ;
	char chip[40]={0};
	GetChipId(chip,40);
	char *clientId = (char *)DeviceRunMalloc(101);
	if(!clientId)
	{
		return ;
	}
	 me->devSubTopic = (char *)DeviceRunMalloc(101);
	if(!me->devSubTopic)
	{
		DeviceRunFree(clientId);
		return ;
	}
	me->devPubTopic = (char *)DeviceRunMalloc(101);
	if(!me->devPubTopic)
	{
		DeviceRunFree(clientId);
		DeviceRunFree(me->devSubTopic);
		return ;
	}
	
	me->devSysSubTopic = (char *)DeviceRunMalloc(101);
	if(!me->devSysSubTopic)
	{
		DeviceRunFree(clientId);
		DeviceRunFree(me->devSubTopic);
		DeviceRunFree(me->devPubTopic);
		return ;
	}
	snprintf(clientId,100,"dev_%s_%s",PRODUC_CODE,chip);
	snprintf(me->devSubTopic,100,"dev/sub/%s/%s",PRODUC_CODE,chip);
	snprintf(me->devPubTopic,100,"dev/pub/%s/%s",PRODUC_CODE,chip);
	snprintf(me->devSysSubTopic,100,"dev/sys_sub/%s/%s",PRODUC_CODE,chip);
	
	me->mqttConnect = GprsCreateMqtt((char *)MQTT_IP,MQTT_PORT,clientId,(char *)"UserName",(char *)"UserPass",0,30,MqttRecvCall);
	GprsMqttSub(me->mqttConnect,me->devSubTopic);
	GprsMqttSub(me->mqttConnect,me->devSysSubTopic);
	
	char *send = (char *)DeviceRunMalloc(201);
	snprintf(send,200,"{\"messageType\":\"properties\",\"productCode\":\"%s\",\"deviceCode\":\"%s\",\"softCode\":\"%s\",\"data\":{\"chushui\":\"22\",\"huishui\":\"20\"}}",PRODUC_CODE,chip,SOFT_CODE);
	
	GprsMqttPub(me->mqttConnect,(char *)me->devPubTopic,(u8 *)send,strlen(send));
	DeviceRunFree(send);
	DeviceRunFree(clientId);

//	me->tcpConnect = CreateTcpConnect((char *)"114.215.69.92",8090,10,0);
	


	u8 upSta = GetUpSta();
	Log("upSta:%d",upSta);	
	if( upSta == UP_FIRST_RUN)
	{
		Log("UP_FIRST_RUN");
		char *fileDir = (char *)"0:/updata";
		mf_scan_files((u8 *)fileDir);	
		if(f_deldir(fileDir) !=0 )
		{
			u8 res=f_mkfs("0:",0,4096);
			if(res == 0)
			{
				Log("mkfs ok");
			}
			else
			{
				Log("mkfs failed");
			}
		}
		mf_mkdir((u8 *)fileDir);
		GprsHttpConnectObj *http = CreateUpdataSucCallBackHttpConnect();
		SendGprsHttpConnect(http,HttpUpdataSucCallBackRecvCall,20);	
	}
	else
	{
		GprsHttpConnectObj *http = CreateAskUpdataHttpConnect();
		SendGprsHttpConnect(http,HttpAskUpdataRecvCall,20);
	}
	
}



void HandControl(u8 num)
{
	Log("HandControl num:%d value:%d",num);
	QpEvt *te;
	te = Q_NEW(QpEvt, DEVICE_HAND_CONTROL_SW);
	te->p = num;
	QACTIVE_POST(DV_DeviceWork, &te->super,0);	
}



void ModAsk(DeviceWork * const me)
{
	static u8 i=0;
	if(i++>1)
	{
		static u8 n=0;
		switch(n++)
		{
			case 0:
				ModSendReadHoldRegist(SENDOR1_ADDR,VOL1_H_REG,14);
			break;		
			case 1:
				ModSendReadHoldRegist(SENDOR1_ADDR,CONSUME_H_REG,2);
				n = 0;
			break;
		}
		i=0;
	}
}

void SavaRecdMinFile(char *name,u32 value,VALUE_TYPE  valueType,char *unit)
{
		RTC_TimeTypeDef RTC_TimeStruct;
		RTC_DateTypeDef RTC_DateStruct;
		GetTimeStruct(&RTC_TimeStruct);
		GetDateStruct(&RTC_DateStruct);
		char path[40];
		sprintf(path,"0:/data/%04d%02d%02d",RTC_DateStruct.Year+2000,RTC_DateStruct.Month,RTC_DateStruct.Date);
		FIL file; 
		u8 res=f_open(&file,(const TCHAR*)path,FA_WRITE|FA_OPEN_ALWAYS);//模式0,或者尝试打开失败,则创建新文件	 
		if(res==0)
		{
			u32 bwr=0;
			u32 size = f_size(&file);			
			if(size <MAX_ONE_FILE_RECORD*sizeof(FileRecord))
			{				
				FileRecord rcd = {0};
				strcpy(rcd.name,name);
				strcpy(rcd.unit,unit);
				rcd.value =value;
				rcd.time = GetIntTime();
				rcd.valueType = valueType;
				f_lseek(&file,size);
				res=f_write(&file,&rcd,sizeof(FileRecord),&bwr);
				static u32 count =0;	
				//printf("count:%d bwr:%d res:%d\r\n",count++,bwr,res);
			}
			f_close(&file);
			if(res !=0 || bwr != sizeof(FileRecord))
			{
				u32 total,free;
				exf_getfree((u8 *)"0:",&total,&free);
				Log("total:%d,free:%d",total,free);
				if(free <= 1)
				{
					//删除日期最前的文件
					mf_scan_files((u8 *)"0:/data");
					mf_scan_delete_files((u8 *)"0:/data");
					mf_scan_files((u8 *)"0:/data");
				}
			}			

		}
}

void SaveMinData(SendsorData *sensor,u8 sensorNum)
{
	char name[20]={0};
	sprintf(name,"VOCs%d",sensorNum);
	SavaRecdMinFile(name,*(u32 *)&(sensor->smoke),VALUE_FLOAT,(char *)"mg/m3");
	sprintf(name,"风机%d",sensorNum);
	SavaRecdMinFile(name,*(u32 *)&(sensor->fanCurrent),VALUE_FLOAT,(char *)"A");
	sprintf(name,"净化器%d",sensorNum);
	SavaRecdMinFile(name,*(u32 *)&(sensor->fanCurrent),VALUE_FLOAT,(char *)"A");
}


void TcpRecvCallBack(u8 *data,u16 size)
{
	Log("TcpRecvCallBack");
}


//如果是多探头 MN为设备编号加探头号（固定为3字节），如设备编号为DH123456，探头编号为1，则MN=DH123456001
//##0221QN=20180427125809000;ST=32;CN=2051;PW=123456;MN=DH123456001;Flag=7;CP=&&DataTime=20180427125809;
//a24088-Avg=75.00;a34005-Avg=140.000;LB-Avg=180.00;FA-Avg=0.5;PA-Avg=0.3;FS-Avg=0;PS-Avg=0&&30c0\r\n

void SendData(u8 connect ,SendsorData *sendsensor,u8 num,u16 CN)
{
	RTC_TimeTypeDef RTC_TimeStruct;
	RTC_DateTypeDef RTC_DateStruct;
	GetTimeStruct(&RTC_TimeStruct);
	GetDateStruct(&RTC_DateStruct);

	char * sendStr = (char *)mymalloc(SRAMIN,200);
	if(!sendStr)return;				
	char * tmpStr =  (char *)mymalloc(SRAMIN,200);
	if(!tmpStr)
	{
		myfree(SRAMIN,sendStr);
		return;
	}	
	char  *userCode  =  (char *)mymalloc(SRAMIN,40);
	if(!userCode)
	{
		myfree(SRAMIN,tmpStr);
		myfree(SRAMIN,sendStr);
		return;
	}
	u8 res = GetUserCode(userCode,40);
	sprintf(tmpStr,"##0221QN=%04d%02d%02d%02d%02d%02d000;ST=32;CN=%04d;PW=123456;MN=%s%03d;Flag=5;CP=&&DataTime=%04d%02d%02d%02d%02d%02d;\
VOC-Avg=%.2f;FA-Avg=%.1f;PA-Avg=%.1f;FS-Avg=%d;PS-Avg=%d",\
	RTC_DateStruct.Year+2000,RTC_DateStruct.Month,RTC_DateStruct.Date,RTC_TimeStruct.Hours,RTC_TimeStruct.Minutes,RTC_TimeStruct.Seconds,\
	CN,userCode,num,
	RTC_DateStruct.Year+2000,RTC_DateStruct.Month,RTC_DateStruct.Date,RTC_TimeStruct.Hours,RTC_TimeStruct.Minutes,RTC_TimeStruct.Seconds,\
	sendsensor->smoke,sendsensor->fanCurrent,sendsensor->clearCurrent,sendsensor->fanSta,sendsensor->clearSta
	);
	unsigned int crc = usMBCRC16((u8 *)tmpStr,strlen(tmpStr));
	sprintf(sendStr,"%s&&%04X\r\n",tmpStr,crc);
	char ip[41];
	u16 port=0;
	GetIp(ip,41);
	port = GetPort();			
	if(strlen(ip)>4 && strlen(ip) <40 && port >0)
	{
		u8 tcp = CreateTcpConnect((char *)"114.215.69.92",8090,10,0);
		GprsTcpSend(tcp,(u8 *)sendStr,strlen(sendStr),0,0);
	}
	//ip不同或者端口不同
//	if(strstr(ip,"114.115.150.248")  || port != 18570)
//	{
//		NetSendData((u8 *)sendStr,strlen(sendStr),"114.115.150.248",18570,0,0,0);
//	}
	printf("%s",sendStr);
	myfree(SRAMIN,sendStr);
	myfree(SRAMIN,tmpStr);
	myfree(SRAMIN,userCode);

}

#include "conver.h"
#include <STM32F4TouchController.hpp>
#include <gui/containers/OverSetContain.hpp>
#define MAX_CUR_CH 1//显示屏电流通道组数3组

using namespace touchgfx;

void SendNetSta(u8 ch,u32 allSta)
{
	char send[100];
	send[0]=0;
	snprintf(send,100,"SetName=%d,\"车间1 ",ch);
	if(allSta&0x38)
	{
		//风机开
		strcat(send,"风机:开 ");
	}
	else
	{
		strcat(send,"风机:关 ");
	}
	
	if(allSta&0x07)
	{
		strcat(send,"净化:开 ");
	}
	else
	{
		strcat(send,"净化:关");
	}
	strcat(send,"\"");
	
	u16 unicodeBuf[100];
	GbkToUnicode((unsigned char *)send,unicodeBuf,100,1);
	Unicode::toUTF8(unicodeBuf,(unsigned char *)send,100);
	Log("cam name:%s",send);
	CamUsartSend((u8 *)send,strlen(send));
}


//save = save+new/count
void UpdataSaveData(SendsorData *save,SendsorData *nw,u8 count)
{
	save->pm = (save->pm + nw->pm)/count;
	save->smoke = (save->smoke + nw->smoke)/count;
	save->nmhc = (save->nmhc + nw->nmhc)/count;
	save->fanCurrent = (save->fanCurrent + nw->fanCurrent)/count;
	save->clearCurrent = (save->clearCurrent+ nw->clearCurrent)/count;
}
 


//内部调用
void RunLoopCall(DeviceWork * const me)
{
	static u8 sendFun = 0;
	
	RTC_TimeTypeDef RTC_TimeStruct;
	RTC_DateTypeDef RTC_DateStruct;
	GetTimeStruct(&RTC_TimeStruct);
	GetDateStruct(&RTC_DateStruct);
	
	
	static u8 start =0;
	if(start < 20)start++;
	if(start == 5)
	{
		RunStartCall(me);
	}
	
	ModAsk(me);
	static	u16 sv = 0;
	if(sv++ >20)
	{
		printf("------------->SendMqtt start mem:%d\r\n",my_mem_perused(SRAMIN));		
		u32 static sendCount=0;
		char *send = (char *)DeviceRunMalloc(200);
		sprintf(send,"%02d:%02d:%02d,pub:%d mem:%d",RTC_TimeStruct.Hours,RTC_TimeStruct.Minutes,RTC_TimeStruct.Seconds,sendCount++,my_mem_perused(SRAMIN));
//		char chip[40]={0};
//		GetChipId(chip,40);
//		sprintf(send,"{\"time\":\"%02d:%02d:%02d\",\"messageType\":\"properties\",\"productCode\":\"%s\",\"deviceCode\":\"%s\",\"softCode\":\"%s\",\"data\":{\"vol\":\"%d\",\"id\":%d}}",\
//		RTC_TimeStruct.Hours,RTC_TimeStruct.Minutes,RTC_TimeStruct.Seconds,PRODUC_CODE,chip,SOFT_CODE,my_mem_perused(SRAMIN),sendCount++);
		GprsMqttPub(me->mqttConnect,me->devPubTopic,(u8 *)send,strlen(send)+1);
		DeviceRunFree(send);	
		sv =0;
	}
	return ;
	
	
	if(sendFun == 0)
	{
		// 0 -5秒内
		if(RTC_TimeStruct.Seconds > 0 && RTC_TimeStruct.Seconds <5)
		{
			sendFun = 1;
			
			
			

			
			
			
//			GprsHttpConnectObj *http = CreateAskUpdataHttpConnect();
//			SendGprsHttpConnect(http,HttpAskUpdataRecvCall);
			
//			char *send = (char *)DeviceRunMalloc(201);
//			snprintf(send,200,"{\"messageType\":\"properties\",\"productCode\":\"%s\",\"deviceCode\":\"%s\",\"softCode\":\"%s\",\"data\":{\"chushui\":\"22\",\"huishui\":\"20\"}}",PRODUC_CODE,"700SOdB47Ue3kO",SOFT_CODE);
//	
//			DeviceRunFree(send);
			/////////////
			//没小时的第5分钟
			if(RTC_TimeStruct.Minutes == 5)
			{
				//HttpAskUpdata();
			}
			if(RTC_TimeStruct.Minutes == 15)
			{
				//HttpAskParam();
			}
			if(RTC_TimeStruct.Minutes%1 == 0)//10
			{
				
//				GprsHttpConnectObj *http = CreateAskUpdataHttpConnect();
//				SendGprsHttpConnect(http,HttpAskUpdataRecvCall,20);
				
//				printf("SendData my_mem_perused:%d\r\n",my_mem_perused(SRAMIN));		
//				SendData(me->tcpConnect,&(me->sensor1),1,2051);
//				SendData(&(me->sensor2),2,2051);		
//				SendData(&(me->sensor3),3,2051);	
				if(me->sensorAlive1)SaveMinData(&(me->sensor1),1);
				if(me->sensorAlive2)SaveMinData(&(me->sensor2),2);		
				if(me->sensorAlive3)SaveMinData(&(me->sensor3),3);	
				
				me->hourCount++;
				UpdataSaveData(&(me->hourSensor1),&(me->sensor1),me->hourCount);
				UpdataSaveData(&(me->hourSensor2),&(me->sensor2),me->hourCount);
				UpdataSaveData(&(me->hourSensor3),&(me->sensor3),me->hourCount);
					//小时数据
				if(RTC_TimeStruct.Minutes == 0)
				{
					me->hourCount=0;
					//先发送完数据，再清除
					SendData(me->tcpConnect,&(me->hourSensor1),1,2061);
					SendData(me->tcpConnect,&(me->hourSensor2),2,2061);		
					SendData(me->tcpConnect,&(me->hourSensor3),3,2061);
					memset(&(me->hourSensor1),0,sizeof(me->hourSensor1));
					memset(&(me->hourSensor2),0,sizeof(me->hourSensor2));
					memset(&(me->hourSensor3),0,sizeof(me->hourSensor3));
					
					me->dayCount++;
					UpdataSaveData(&(me->daySensor1),&(me->hourSensor1),me->dayCount);
					UpdataSaveData(&(me->daySensor2),&(me->hourSensor2),me->dayCount);
					UpdataSaveData(&(me->daySensor3),&(me->hourSensor3),me->dayCount);
					if(RTC_TimeStruct.Hours == 0)
					{
						me->dayCount=0;
						//先发送完数据，再清除
						SendData(me->tcpConnect,&(me->daySensor1),1,2031);
						SendData(me->tcpConnect,&(me->daySensor2),2,2031);		
						SendData(me->tcpConnect,&(me->daySensor3),3,2031);						
						memset(&(me->daySensor1),0,sizeof(me->daySensor1));
						memset(&(me->daySensor2),0,sizeof(me->daySensor2));
						memset(&(me->daySensor3),0,sizeof(me->daySensor3));
					}				
				}			
			}
		}
	}
	// 大于10 秒小于50秒
	else if(RTC_TimeStruct.Seconds >10 && RTC_TimeStruct.Seconds < 50)
	{
		sendFun = 0;
	}
	/////////////////////
	
	u32 allCurSta = me->sensor3.fanSta<<5|me->sensor2.fanSta<<4|me->sensor1.fanSta<<3|me->sensor3.clearSta<<2|me->sensor2.clearSta<<1|me->sensor1.clearSta;
	static u8 sendCount = 0;
	
	static u32 svAllCurSta = 0;
	//Log("svAllCurSta:%04X allCurSta:%04X",svAllCurSta,allCurSta);
	if(svAllCurSta != allCurSta)
	{
		
		svAllCurSta = allCurSta;
		sendCount = me->maxCam;
	}
	
	if(sendCount >0 && me->firstCamAsk == FIRST_ASK_END)
	{
		//每发送一次，发送数量减1
		//每发送一次，要等10次循环之后才能发送第二次
		
		if(me->sendWait == 0)
		{
			SendNetSta(me->maxCam-sendCount,svAllCurSta);
			sendCount--;
			me->sendWait =20;
		}
		else
		{
			me->sendWait--;
		}
	}
	if(me->firstCamAsk == FIRST_CAM_SET)
	{
		me->firstCamAsk = FIRST_ASK_END;
		sendCount = me->maxCam;
	}
	
}

