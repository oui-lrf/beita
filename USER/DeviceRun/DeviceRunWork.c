#include "DeviceRunDrive.h"
#include "DeviceRunPort.h"
#include "DeviceRunWork.h"
#include "DeviceRunDrive.h"
#include "arm_math.h"
#include "VaveFit.h"
#include "MidFit.h"
#include "DeviceRunUsartWork.h"
#include "DeviceRunCmd.h"

#include "ModPort.h"
#include "HttpPort.h"
#include "circular_queue.h"
 

#define OPEN_CURRENT 0.01f  

//秒
#define CMD_WAIT_TIME 4


static QState DeviceWork_initial(DeviceWork * const me, QEvt const * const e);

static QState DEVICE_TOP_TEST(DeviceWork * const me, QEvt const * const e);
static QState DEVICE_TOP(DeviceWork * const me, QEvt const * const e);
static QState DEVICE_ON(DeviceWork * const me, QEvt const * const e);
static QState DEVICE_CLOSED(DeviceWork * const me, QEvt const * const e);
static QState DEVICE_WARING(DeviceWork * const me, QEvt const * const e);
static QState DEVICE_CLOSING(DeviceWork * const me, QEvt const * const e);


static QState DEVICE_PID_SET_PRETEMP_RUN(DeviceWork * const me, QEvt const * const e);

static DeviceWork deviceWork;
QActive * const DV_DeviceWork = &deviceWork.super;
Q_DEFINE_THIS_MODULE("DevicerRunWork")

arm_pid_instance_f32 S;

void DeviceWork_ctor(void) {
    DeviceWork *me = (DeviceWork *)DV_DeviceWork;
    QActive_ctor(&me->super, Q_STATE_CAST(&DeviceWork_initial));
		QTimeEvt_ctorX(&me->timeEvt1, &me->super, TIMEOUT_SIG, 0U);
		QTimeEvt_ctorX(&me->timeEvtLoop, &me->super, LOOP_TIMEOUT_SIG, 0U);
}

static QState DeviceWork_initial(DeviceWork * const me, QEvt const * const e) {
    /*${Device::DeviceWork::SM::initial} */

	me->sw1=0;
	me->sw2=0;
	me->sw3=0;
	
	me->sensorAlive1=0;
	me->sensorAlive2=0;
	me->sensorAlive3=0;	
	
	me->align1=0;
	me->align2=0;
	me->align3=0;
	me->hourCount=0;
	me->dayCount=0;
	
	me->sensor1.fanSta=0;
	me->sensor2.fanSta=0;
	me->sensor3.fanSta=0;

	me->sensor1.clearSta=0;
	me->sensor2.clearSta=0;
	me->sensor3.clearSta=0;
	me->maxCam=MAX_CAM;
	me->sendWait=0;
	me->firstCamAsk=FIRST_LOC_IP;
  //return Q_TRAN(&DEVICE_ON);
	return Q_TRAN(&DEVICE_TOP_TEST);
}


extern void SavaRecdFile(char *name,u32 value,VALUE_TYPE  valueType,char *unit);
static float cacul_smoke(float src)
{
	float res= src;
	if(src <0.05f)
	{
		res = src*10.0f;
	}
	else if(src > 1.5f)
	{
		res = 1.5f;
	}
	return res;
}

static float cacul_pm(float src)
{
	float res= src;
	if(src <0.05f)
	{
		res = src*13.0f;
	}
	else if(src > 5.5f)
	{
		res = 5.5f;
	}
	return res;
}
extern  cycleQueue runUsartQue;

static QState DEVICE_TOP_TEST(DeviceWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Device::DeviceWork::SM::DEVICE_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
						Log("+++DEVICE_TOP");
						QTimeEvt_armX(&me->timeEvtLoop,BSP_TICKS_PER_SEC,BSP_TICKS_PER_SEC);
						status_ = Q_HANDLED();
            break;
        }
				case LOOP_TIMEOUT_SIG:{
					RunLoopCall(me);
					status_ = Q_HANDLED();	
				break;
				}
				case Q_EXIT_SIG:{
					Log("~~~ DEVICE_TOP");
					QTimeEvt_disarm(&me->timeEvtLoop);
					status_ = Q_HANDLED();	
					break;
				}
        default: {
            status_ = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status_;
}

static QState DEVICE_TOP(DeviceWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Device::DeviceWork::SM::DEVICE_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
						Log("+++DEVICE_TOP");
						QTimeEvt_armX(&me->timeEvtLoop,BSP_TICKS_PER_SEC,BSP_TICKS_PER_SEC);
						UsartWorkInit();
						status_ = Q_HANDLED();
            break;
        }
				case USART_RECV_SIG:{
						status_ = Q_HANDLED();
						u16 size = (Q_EVT_CAST(QpEvt)->v);
						if(size)
						{
							u8 *data = DeviceRunMalloc(size+1);
							if(data)
							{
								ReadQueBuf(&runUsartQue,data,size);
								data[size]=0;
								Log("recv:%s,size:%d",(char *)data,size);
								//
								if(strstr((char *)data,"SetName OK"))
								{
									me->sendWait = 0;
								}
		
								else if(strstr((char *)data,"GetCam OK")||strstr((char *)data,"FindCam OK") || strstr((char *)data,"CH="))
								{
									char *findPos = (char *)data;
									for(u8 i=0;i<MAX_CAM;i++)
									{
										char *chStrFind = strstr((char *)findPos,"CH=");
										
										if(chStrFind)
										{
											me->firstCamAsk=FIRST_CAM_SET;
											char *chStr = chStrFind+3;
											int ch= atoi(chStr);
											me->maxCam	=ch+1;
											findPos = chStr;	
											Log("maxCam:%d",me->maxCam);
											SendUiMsg(SCREEN_SET,SET_REMOTE_IP_STA,(u32)1);
										}
										else
										{
												break;
										}									
									}								
								}
								
								else if(strstr((char *)data,"DhcpIp="))
								{
									char * ipFind = strstr((char *)data,"DhcpIp=\"");
									if(ipFind)
									{
										char *ipStart = ipFind + strlen("DhcpIp=\"");
										char * portFind = strstr(ipStart,"\",");
										int ipLen = portFind - ipStart;
										if(portFind && ipLen>0 && ipLen <20)
										{
											int port = atoi(portFind+2);	
											char *ip = DeviceRunMalloc(30);
											if(ip && port >0 && port <65535)
											{
												strncpy(ip,ipStart,ipLen);
												ip[ipLen] = 0;
												SendUiMsg(SCREEN_SET,SET_LOCAL_IP,(u32)ip);
												SendUiMsg(SCREEN_SET,SET_LOCAL_PORT,(u32)port);
											}
										}
									}
								}
								else if(strstr((char *)data,"LocalIp="))
								{
									char * ipFind = strstr((char *)data,"LocalIp=\"");
									if(ipFind)
									{
										char *ipStart = ipFind + strlen("LocalIp=\"");
										char * portFind = strstr(ipStart,"\",");
										int ipLen = portFind - ipStart;
										if(portFind && ipLen>0 && ipLen <20)
										{
											int port = atoi(portFind+2);	
											char *ip = DeviceRunMalloc(30);
											if(ip && port >0 && port <65535)
											{
												me->firstCamAsk=FIRST_REMOTE_IP;
												strncpy(ip,ipStart,ipLen);
												ip[ipLen] = 0;
												
												SendUiMsg(SCREEN_SET,SET_LOCAL_IP,(u32)ip);
												SendUiMsg(SCREEN_SET,SET_LOCAL_PORT,(u32)port);
												//DeviceRunFree(ip);
											}
										}
									}
								}
								else if(strstr((char *)data,"RemoteIp="))
								{
									char * ipFind = strstr((char *)data,"RemoteIp=\"");
									if(ipFind)
									{
										char *ipStart = ipFind + strlen("RemoteIp=\"");
										char * portFind = strstr(ipStart,"\",");
										int ipLen = portFind - ipStart;
										if(portFind && ipLen>0 && ipLen <20)
										{
											int port = atoi(portFind+2);	
											char *ip = DeviceRunMalloc(30);
											if(ip && port >0 && port <65535)
											{
												me->firstCamAsk=FIRST_USER;
												strncpy(ip,ipStart,ipLen);
												ip[ipLen] = 0;
												
												SendUiMsg(SCREEN_SET,SET_REMOTE_IP,(u32)ip);
												SendUiMsg(SCREEN_SET,SET_REMOTE_PORT,(u32)port);
												//DeviceRunFree(ip);
											}
										}
									}
								}
								//UserName="admin",UserPass="DHzn2017"
								else if(strstr((char *)data,"UserName="))
								{
									char * nameFind = strstr((char *)data,"UserName=\"");
									if(nameFind)
									{
										char *nameStart = nameFind + strlen("UserName=\"");
										char * passFind = strstr(nameStart,"\",UserPass=\"");
										int nameLen = passFind - nameStart;
										if(passFind && nameLen>0 && nameLen <20)
										{										
											char *passStart = passFind + strlen("\",UserPass=\"");
											char *passEnd = strstr(passStart,"\"");
											int passLen = passEnd - passStart;
											char *name = DeviceRunMalloc(30);
											if(name)
											{											
												char *pass = DeviceRunMalloc(30);
												if(pass)
												{
													if(passLen>0 && passLen <20)
													{
														me->firstCamAsk=FIRST_CAM_LIST;
														strncpy(name,nameStart,nameLen);
														strncpy(pass,passStart,passLen);
														Log("name:%s pass:%s\r\n",name,pass);
														SendUiMsg(SCREEN_SET,SET_NAME,(u32)name);
														SendUiMsg(SCREEN_SET,SET_PASS,(u32)pass);
													}													
												}
												else
												{
													DeviceRunFree(name);
												}

											}
										}
									}
								}
								else if(strstr((char *)data,"ERROR"))
								{
									me->sendWait = 0;
								}
								NetFree(data);							
							}						
						}
						break;
					}
				case DEVICE_HAND_CONTROL_SW:{
							status_ = Q_HANDLED();
							u8 num = (u8)(Q_EVT_CAST(QpEvt)->p);
							Log("DEVICE_HAND_CONTROL_SW num:%d,value:%d",num);
							u8 sentype = GetSensorType();
							break;
				}
			 case DEVICE_MOD_RECV_SIG:{
						status_ = Q_HANDLED();
            ModRecvObj *mod = (ModRecvObj *)(Q_EVT_CAST(QpEvt)->p);
						if(mod)
						{
							//Log("addr:%x,fun:%x,reg:%x,value:%x",mod->addr,mod->type,mod->reg,mod->value);
							
							switch(mod->type)
							{
								case MOD_CMD_READ_HOLDREGIST:{
									float frd;
									switch(mod->addr)
									{
										case SENDOR1_ADDR:{
												//LogHex("reg value",(u8 *)&mod->value,2);
												switch(mod->reg)
												{
													//电压
													case VOL1_H_REG :{ 
															me->vol1H = mod->value;
													break;
													}
													case VOL1_L_REG :{
															me->vol1L = mod->value;
															u32 data = me->vol1H<<16 | me->vol1L;
															SendUiMsg(SCREEN_MAIN,VOL1,data);
															me->vol1 = *(float *)&data;
													break;
													}

													case VOL2_H_REG :{
															me->vol2H = mod->value;
													break;
													}
													case VOL2_L_REG :{
															me->vol2L = mod->value;
															u32 data = me->vol2H<<16 | me->vol2L;
															SendUiMsg(SCREEN_MAIN,VOL2,data);
															me->vol2 = *(float *)&data;
													break;
													}

													case VOL3_H_REG :{

														me->vol3H = mod->value;
													break;
													}
													case VOL3_L_REG :{
															me->vol3L = mod->value;
															u32 data = me->vol3H<<16 | me->vol3L;
															SendUiMsg(SCREEN_MAIN,VOL3,data);
															me->vol3 = *(float *)&data;
													break;
													}

													//电流
													case CUR1_H_REG :{
														me->cur1H = mod->value;
													break;
													}
													case CUR1_L_REG :{
															me->cur1L = mod->value;
															u32 data = me->cur1H<<16 | me->cur1L;
															SendUiMsg(SCREEN_MAIN,CUR1,data);
															me->cur1 = *(float *)&data;
													break;
													}

													case CUR2_H_REG :{

														me->cur2H = mod->value;
													break;
													}
													case CUR2_L_REG :{
															me->cur2L = mod->value;
															u32 data = me->cur2H<<16 | me->cur2L;
															SendUiMsg(SCREEN_MAIN,CUR2,data);
															me->cur2 = *(float *)&data;
													break;
													}

													case CUR3_H_REG :{

														me->cur3H = mod->value;
													break;
													}
													case CUR3_L_REG :{
															me->cur3L = mod->value;
															u32 data = me->cur3H<<16 | me->cur3L;
															SendUiMsg(SCREEN_MAIN,CUR3,data);
															me->cur3 = *(float *)&data;
													break;
													}

													//功率

													case POW_H_REG :{

														me->powerH = mod->value;
													break;
													}
													
													case POW_L_REG :{
															me->powerL = mod->value;
															u32 data = me->powerH<<16 | me->powerL;
															SendUiMsg(SCREEN_MAIN,NOW_POW,data);
															me->power = *(float *)&data;
													break;
													}
													//电能

													case CONSUME_H_REG :{
														me->consumeH = mod->value;
													break;
													}
													
													case CONSUME_L_REG :{
															me->consumeL = mod->value;
															u32 data = me->consumeH<<16 | me->consumeL;
															SendUiMsg(SCREEN_MAIN,ALL_CONSUME,data);
															me->consume = *(float *)&data;
															Log("consume:%f",me->consume);
													break;
													}													
													
												}
										break;
										}
										
									}											
									break;
								}
														
							}
							DeviceRunFree(mod);
						}				 
			 break;
			 }
				case LOOP_TIMEOUT_SIG:{
					RunLoopCall(me);
					status_ = Q_HANDLED();	
				break;
				}
				case Q_EXIT_SIG:{
					Log("~~~ DEVICE_TOP");
					QTimeEvt_disarm(&me->timeEvtLoop);
					status_ = Q_HANDLED();	
					break;
				}
        default: {
            status_ = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status_;
}

static QState DEVICE_ON(DeviceWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        /*${Device::DeviceWork::SM::DEVICE_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
						Log("+++DEVICE_ON");
						status_ = Q_HANDLED();	
            break;
        }
				case LOOP_TIMEOUT_SIG:{
					RunLoopCall(me);
					status_ = Q_UNHANDLED();	
				break;
				}
				case DEVICE_STOP_SIG:{
					Log("DEVICE_CLOSE DEVICE_STOP_SIG");
					status_ = Q_TRAN(&DEVICE_CLOSING);
				break;
				}
        default: {
            status_ = Q_SUPER(&DEVICE_TOP);
            break;
        }
    }
    return status_;
}



//接受手动控制 该信号一定要被接收
static QState DEVICE_CLOSED(DeviceWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        /*${Device::DeviceWork::SM::DEVICE_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
						Log("+++DEVICE_CLOSE");
						status_ = Q_HANDLED();	
            break;
        }
				case COAT_ALIGN_CONTROL_SIG:{
					;float qua = GetCoatQua();
					SendCoatAlign(qua);
				break;
				}
				case FLOWRATE_ALIGN_CONTROL_SIG:{
					DeviceSendCmd((u8 *)FlowRateAlignSendCmd,sizeof(FlowRateAlignSendCmd));
				break;
				}
				case DIFF_UP_CONTROL_SIG:{
					DeviceSendCmd((u8 *)LiftUpSendCmd,sizeof(LiftUpSendCmd));
				break;
				}
				case DIFF_LOW_CONTROL_SIG:{
					DeviceSendCmd((u8 *)LiftLowSendCmd,sizeof(LiftLowSendCmd));
				break;
				}
				case AUTO_CHECK_CONTROL_SIG:{
					DeviceSendCmd((u8 *)AutoCheckSendCmd,sizeof(AutoCheckSendCmd));
				break;
				}
				case STOP_SAMP_CONTROL_SIG:{
					DeviceSendCmd((u8 *)StopSampSendCmd,sizeof(StopSampSendCmd));
				break;
				}
				case DEVICE_START_SIG:{
					Log("DEVICE_CLOSE DEVICE_START_SIG");				
					status_ = Q_TRAN(&DEVICE_ON);
				break;
				}
        default: {
            status_ = Q_SUPER(&DEVICE_TOP);
            break;
        }
    }
    return status_;
}

static QState DEVICE_WARING(DeviceWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        /*${Device::DeviceWork::SM::DEVICE_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
						Log("+++DEVICE_WARING");
						RunWaringCall(me);
						status_ = Q_HANDLED();	
            break;
        }			
				case HAND_WARING:{
					Log("DEVICE_WARING HAND_WARING");				
					status_ = Q_TRAN(&DEVICE_CLOSED);
				break;
				}
				case Q_EXIT_SIG:{
					Log("~~~~DEVICE_WARING");
					me->err = 0;
					RunWaringCall(me);
					status_ = Q_HANDLED();	
					break;
				}
        default: {
            status_ = Q_SUPER(&DEVICE_TOP);
            break;
        }
    }
    return status_;
}


static QState DEVICE_CLOSING(DeviceWork * const me, QEvt const * const e) {
      QState status_;
    switch (e->sig) {
        /*${Device::DeviceWork::SM::DEVICE_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
					Log("+++DEVICE_CLOSING");
						DeviceSendCmd((u8 *)StopSampSendCmd,sizeof(StopSampSendCmd));
						QTimeEvt_armX(&me->timeEvt1,BSP_TICKS_PER_SEC*CMD_WAIT_TIME,0);
						status_ = Q_HANDLED();	
            break;
        }
				//三次停止
				case TIMEOUT_SIG:{
					Log("DEVICE_START TIMEOUT_SIG");
					status_ = Q_HANDLED();	
					//发送失败则重新发送
					DeviceSendCmd((u8 *)StopSampSendCmd,sizeof(StopSampSendCmd));
					QTimeEvt_armX(&me->timeEvt1,BSP_TICKS_PER_SEC*CMD_WAIT_TIME,0);
				break;
				}
				case Q_EXIT_SIG:{
					Log("~~~~DEVICE_START");
					QTimeEvt_disarm(&me->timeEvt1);
					status_ = Q_HANDLED();	
					break;
				}
        default: {
            status_ = Q_SUPER(&DEVICE_ON);
            break;
        }
    }
    return status_;
}
