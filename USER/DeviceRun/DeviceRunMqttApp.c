#include "DeviceRunMqttApp.h"
#include "DeviceRunHttpApp.h"
#include "cJson.h"
#include "PbPort.h"
//被外部线程调用
void MqttRecvCall(MqttRecvObj *recvObj)
{
	PbDataObj  *dataObj = (PbDataObj  *)recvObj->data.arg;		
	PbDataObj  *topicObj = (PbDataObj  *)recvObj->topic.arg;		
	Log("MqttRecvCall topic:%s,",topicObj->data);
	Log("MqttRecvCall data:%s",dataObj->data);
	if(strstr((char *)topicObj->data,( char *)"ev/sys_sub"))
	{
		cJSON *cmdJson ={0};
		cmdJson= cJSON_Parse((char *)dataObj->data);
		if(!cmdJson)
		{
			printf("cmdJson is null:%s\r\n",dataObj->data);
			return ;
		}		
		if(strstr(cJSON_GetObjectItem(cmdJson, "messageType")->string,"messageType"))
		{
			char *messageType = cJSON_GetObjectItem(cmdJson, "messageType")->valuestring;
			if(strstr(messageType,"upgradeSoft"))
			{
				char *deviceCode = cJSON_GetObjectItem(cmdJson, "deviceCode")->valuestring;
				char chip[40]={0};
				GetChipId(chip,40);
							
				char *upgradeSoftCode = cJSON_GetObjectItem(cmdJson, "upgradeSoftCode")->valuestring;
				if(strcmp(deviceCode,chip) == 0 && strcmp(upgradeSoftCode,SOFT_CODE) != 0)
				{
					cJSON *fileArry = cJSON_GetObjectItem(cmdJson,"files");
					if(fileArry)
					{
						int size = cJSON_GetArraySize(fileArry);
						for(int i=0;i<size;i++)
						{
							cJSON *file=cJSON_GetArrayItem(fileArry,i);	
							if(file)
							{
								char* crc16Str = cJSON_GetObjectItem(file,"crc16")->valuestring;
								char* name = cJSON_GetObjectItem(file,"name")->valuestring;
								char *url =  cJSON_GetObjectItem(file,"url")->valuestring;
								char *fileSizeStr = cJSON_GetObjectItem(file,"size")->valuestring;
								Log("crc16:%s,name:%s,url:%s",crc16Str,name,url);
								char *ipStart = strstr(url,"://")+3;
								char *ipEnd=0;
								ipEnd = strstr(ipStart,":");
								if(!ipEnd)
								{
									ipEnd = strstr(ipStart,"/");
								}
								int ipLen = ipEnd - ipStart;
								u16 crc16 = atoi(crc16Str);
								if(ipLen >0 && crc16 >0)
								{
									u32 fileSize = atoi(fileSizeStr);
									Log("fileSizeStr:%d",fileSize);
									if( fileSize > 0 && fileSize < (2*1024*1024))
									{
										char *softIdPos = strstr(url,"fileId=");
										if(softIdPos)
										{
											char *softIdStart = softIdPos+7;
											
											u16 softId = atoi(softIdStart);
											if(softId)
											{
												char *catUrl = (char *)DeviceRunMalloc(201);
												if(catUrl)
												{
													char chip[40];
													GetChipId(chip,40);
													snprintf(catUrl,200,"%s&&productCode=%s&&softCode=%s&&deviceCode=%s",url,PRODUC_CODE,SOFT_CODE,chip);			
													Log("DownLoad info: crc16:%d,softId:%d",crc16,softId);
													Log("url:%s",catUrl);
													u32 total,free;
													exf_getfree((u8 *)"0:",&total,&free);
													Log("total:%d,free:%d",total,free);
													if(free > 1024*2)
													{
														GprsDownLoad((char *)"0:/updata",(char *)"soft.bin",catUrl,crc16,fileSize,(1024*5),DownLoadEndCall);		//fileSize
													}
													else
													{
														Log("Flash Size err");
													}
													GprsFree(catUrl);
												}
											}										
										}
									}
								}
							}
						}
					}				
				}		
			}
		}			
	}
}

