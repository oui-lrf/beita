#ifndef __DEVICERUNWORK_H__
#define __DEVICERUNWORK_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "DeviceRunInclude.h"
#include "DeviceRunTimer.h"
#include "DeviceRunParam.h"
typedef struct{
	float pm;
	float smoke;
	float nmhc;
	float fanCurrent;
	float clearCurrent;
	u8 fanSta;
	u8 clearSta;
}SendsorData;

#define MAX_CAM 4

typedef enum{
	FIRST_LOC_IP,
	FIRST_REMOTE_IP,
	FIRST_USER,
	FIRST_CAM_LIST,
	FIRST_CAM_SET,
	FIRST_ASK_END,
}FIRST_ASK_CAM;

typedef struct {
/* protected: */
    QActive super;
		QTimeEvt timeEvtLoop;
		QTimeEvt timeEvt1;
	
		 float svPm10;
		 float svOutTemp;
		 float svOutHum;
		 float svRoomTemp;
		 float svRoomHum;
		 float svAtmos;
		 float svFlowRate;
		DEVICE_RUN_STA  sysRunSta;
		u8 err;
	
	 SendsorData sensor1;
	 SendsorData sensor2;
	 SendsorData sensor3;	
	
	
	 //每小时统计一次，为分钟数据的平均值
	 SendsorData hourSensor1;
	 SendsorData hourSensor2;
	 SendsorData hourSensor3;	
	  u8 hourCount;//小时数据的次数
	 
	 //每天统计一次，为小时数据的平均值
	 SendsorData daySensor1;
	 SendsorData daySensor2;
	 SendsorData daySensor3;	
	  u8 dayCount;//小时数据的次数
	
		bool sw1;
		bool sw2;
		bool sw3;
		
		u8 maxCam;
		bool sensorAlive1;
		bool sensorAlive2;
		bool sensorAlive3;

		
		bool align1;
		bool align2;
		bool align3;
		u8 sendWait;
		FIRST_ASK_CAM firstCamAsk;
		
		
		float vol1;
		float vol2;		
		float vol3;		
		
		float cur1;
		float cur2;		
		float cur3;			

		float power;
		float consume;
		
		u16 vol1H;
		u16 vol1L;		
		
		u16 vol2H;		
		u16 vol2L;			
		
		u16 vol3H;		
		u16 vol3L;				
		
		u16 cur1H;
		u16 cur1L;		
		
		u16 cur2H;		
		u16 cur2L;				
		
		u16 cur3H;			
		u16 cur3L;			

		u16 powerH;		
		u16 powerL;		
		
		u16 consumeH;		
		u16 consumeL;		
		
		u8 tcpConnect;
		u8 mqttConnect;
		char *devPubTopic;
		char *devSubTopic;
		char *devSysSubTopic;
}DeviceWork;
void DeviceWork_ctor(void);


extern  void RunLoopCall(DeviceWork * const me);
extern  void RunStartCall(DeviceWork * const me);
extern  void RunWaringCall(DeviceWork * const me);
extern  void HttpAskUpdata(void);
extern  void HttpAskParam(void);

extern  void ModSendWrightHoldRegist(uint8_t _addr, uint16_t _reg, uint16_t value);	
extern  void ModSendReadHoldRegist(uint8_t _addr, uint16_t _reg, uint16_t _num);
extern  void ModSendReadInputRegist(uint8_t _addr, uint16_t _reg, uint16_t _num);
#ifdef __cplusplus
}
#endif
#endif
