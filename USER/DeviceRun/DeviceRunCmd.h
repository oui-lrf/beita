#pragma once
#include "DeviceRunInclude.h"

 extern const u8 VersionSendCmd[3] ; 
 extern const u8 SetSampTimeSpaceSendCmd[4];//采集间隔
 extern const u8 SetAirOutTimeSpaceSendCmd[4];//抽气间隔 
 extern const u8 StartSampSendCmd[3]; //开始采集
 extern const u8 StopSampSendCmd[3]; //停止采集
 extern const u8 SampingReadPm10SendCmd[3];	//获取PM10
 extern const u8 SampingReadParamSendCmd[3];	//获取参数
 extern const u8 AutoCheckSendCmd[3];
 extern const u8 ReadParamSendCmd[3];
 extern const u8 LiftUpSendCmd[3];
 extern const u8 LiftLowSendCmd[3];
 extern const u8 FlowRateAlignSendCmd[3];

 extern const u8 VersionRecvCmd[8];
 extern const u8 StopSampRecvCmd[4];
 extern const u8 AutoCheckRecvHeadCmd[3];
 extern const u8 SetSampTimeSpaceRecvCmd[3];
 extern const u8 SetAirOutTimeSpaceRecvCmd[3]; //猜测的
 extern const u8 SampingReadPm10RecvHeadCmd[3];//
 extern const u8 SampingReadParamRecvHeadCmd[3];


void DeviceSendCmd(u8 *data,u16 size);
//设置采集时间间隔
void SendSampTimeSpaceCmd(u16 time);
//设置膜片校准
void SendCoatAlign(float value);
u16 BufCmp(u8 *data1,u16 size1,u8 *data2,u16 size2);
u8 AnalysisPm10(u8 *data,u16 size,float *res);
u8 AnalysisParam(u8 *data,u16 size,float *outTemp,float *outHum,float *roomTemp,float *roomHum,float *atmos,float *flowRate);
u8 GetAutoCheckCode(u8 *data ,u16 size);
void SendAirOutTimeSpaceCmd(u16 time);
