#pragma once
#include "air.pb.h"
#include "PbPort.h"
#ifdef __cplusplus
extern "C" {
#endif
void HttpAskUpdataRecvCall(HttpRecvObj *httpRecvObj);
void HttpUpdataSucCallBackRecvCall(HttpRecvObj *httpRecvObj);
void DownLoadEndCall(u8 sta,DownFile *downObj);
#ifdef __cplusplus
}
#endif