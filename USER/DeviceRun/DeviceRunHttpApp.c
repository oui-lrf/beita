#include "DeviceRunHttpApp.h"
#include "MyFileUtils.h"

//void AnalyzeHttpReturn(char *jsonStr)
//{
//	cJSON *cmdJson ={0};
//	cmdJson= cJSON_Parse(jsonStr);
//	
//	if(cmdJson && strstr(cJSON_GetObjectItem(cmdJson, "func")->string,"func"))
//	{
//		char *func = cJSON_GetObjectItem(cmdJson, "func")->valuestring;
//		
//		if(strstr(func,"getUpgradeSoft"))
//		{
//			char *code = cJSON_GetObjectItem(cmdJson, "code")->valuestring;
//			if(strstr((char *)code,"0"))
//			{
//				cJSON *fileArry = cJSON_GetObjectItem(cmdJson,"files");
//				if(fileArry)
//				{
//					int size = cJSON_GetArraySize(fileArry);
//					for(int i=0;i<size;i++)
//					{
//						cJSON *file=cJSON_GetArrayItem(fileArry,i);	
//						if(file)
//						{
//							char* crc16Str = cJSON_GetObjectItem(file,"crc16")->valuestring;
//							char* name = cJSON_GetObjectItem(file,"name")->valuestring;
//							char *url =  cJSON_GetObjectItem(file,"url")->valuestring;
//							Log("crc16:%s,name:%s,url:%s",crc16Str,name,url);
//							char *ipStart = strstr(url,"://")+3;
//							char *sortUrl = strstr(ipStart,"/");
//							char *ipEnd=0;
//							ipEnd = strstr(ipStart,":");
//							if(!ipEnd)
//							{
//								ipEnd = strstr(ipStart,"/");
//							}
//							int ipLen = ipEnd - ipStart;
//							u16 crc16 = atoi(crc16Str);
//							if(ipLen >0 && crc16 >0)
//							{
//								char *ip = mymalloc(SRAMIN,ipLen+1);
//								u16 port = 80;
//								strncpy(ip,ipStart,ipLen);
//								ip[ipLen]=0;
//								char *portStart = strstr(ipStart,":")+1;
//								port = atoi(portStart);
//								Log("ip:%s,port:%d",ip,port);
//								if(port)
//								{
//									char *softIdPos = strstr(url,"fileId=");
//									if(softIdPos)
//									{
//										char *softIdStart = softIdPos+7;
//										
//										u16 softId = atoi(softIdStart);
//										if(softId)
//										{
//											RunToIap(ip,port,sortUrl,crc16,softId);
//										}
//										
//									}
//								}
//								myfree(SRAMIN,ip);
//							}
//						}
//					}
//				}				
//			}
//	
//		}
//		else if(strstr(func,"upgradeSoftSuccess"))
//		{
//			char *code = cJSON_GetObjectItem(cmdJson,"code")->valuestring;
//			if(strstr((char *)code,"0"))
//			{
//				SetUpdating(1);
//				WritParamToFlash();
//			}		
//			return ;
//		}
//		//{"33":"33","44":"559","name":"33","peizhi2":"22","ip":"97/"}
//		else if(strstr(func,"getExtraInfoByCode"))
//		{
//			Log("recv getExtraInfoByCode");
//			cJSON *extraInfo = cJSON_GetObjectItem(cmdJson,"extraInfo");
//			if(extraInfo)
//			{
//				
//				///////////////////////////////////////////////////////////////////
//				if(strstr(cJSON_GetObjectItem(extraInfo,"userDeviceCode")->string,"userDeviceCode"))
//				{
//					char *deviceCode =  cJSON_GetObjectItem(extraInfo,"userDeviceCode")->valuestring;
//					Log("deviceCode:%s",deviceCode);
//					if(strlen(deviceCode) >0 && strlen(deviceCode) < USER_CODE_LEN)
//					{
//						if(SetUserCode(deviceCode))
//						{
//							//更新到界面
//							char *code = DeviceRunMalloc(USER_CODE_LEN+1);
//							if(code)
//							{
//								strncpy(code,deviceCode,USER_CODE_LEN);
//								code[USER_CODE_LEN] = 0;
//								SendUiMsg(SCREEN_SET,SET_USER_CODE,(u32)code);
//							}
//							PostFreeEvent(EVENT_SAVE_DEVICE_FLASH);
//						}				
//					}
//				}

//				///////////////////////////////////////////////////////////////////
//				if(strstr(cJSON_GetObjectItem(extraInfo,"userDeviceIp")->string,"userDeviceIp"))
//				{
//					char *deviceIp = cJSON_GetObjectItem(extraInfo,"userDeviceIp")->valuestring;
//					if(strlen(deviceIp) >0 && strlen(deviceIp) < MAX_IP_LEN)
//					{
//						Log("deviceIp:%s",deviceIp);
//						if(SetIp(deviceIp))
//						{
//							//更新到界面
//							char *ip = DeviceRunMalloc(MAX_IP_LEN+1);
//							if(ip)
//							{
//								strncpy(ip,deviceIp,MAX_IP_LEN);
//								ip[MAX_IP_LEN] = 0;
//								SendUiMsg(SCREEN_SET,SET_USER_IP,(u32)ip);
//							}
//							PostFreeEvent(EVENT_SAVE_NET_FLASH);
//						}				
//					}
//				}
//				////////////////////////////////////////////////////////////
//				///////////////////////////////////////////////////////////////////
//				if(strstr(cJSON_GetObjectItem(extraInfo,"userDevicePort")->string,"userDevicePort"))
//				{
//					char *devicePort = cJSON_GetObjectItem(extraInfo,"userDevicePort")->valuestring;
//					Log("devicePort:%s",devicePort);
//					u16 port = atoi(devicePort);
//					if(port)
//					{
//						if(SetPort(port))
//						{
//							SendUiMsg(SCREEN_SET,SET_USER_PORT,(u32)port);
//							PostFreeEvent(EVENT_SAVE_NET_FLASH);
//						}				
//					}
//				}
//			}
//		}

//	}
//	else
//	{
//		printf("cmdJson is null:%s\r\n",jsonStr);
//	}
//	cJSON_Delete(cmdJson);
//}
#include "cJson.h"
#include "HttpPack.h"
#include "iap.h"
//下载完成后进行整体文件的crc16校验，

void DownLoadEndCall(u8 sta,DownFile *downObj)
{
	Log("DownLoadEndCall name:%s",downObj->fileName);
	FIL file; 
	char *wholePath = GprsMalloc(strlen(downObj->filePath)+strlen(downObj->fileName)+10);
	sprintf(wholePath,"%s/%s",downObj->filePath,downObj->fileName);	
	u16 calculCrc = GetFileCrc(wholePath,1024);
	GprsFree(wholePath);	
	Log("fileCrc:%d,calculCrc:%d",downObj->fileCrc16,calculCrc);
	//如果校验成功，重启进入bootload
	if(downObj->fileCrc16 == calculCrc)
	{
		Log("校验成功，重启更新");
		RunToIap(downObj->fileName,calculCrc);
	}
	//失败需要重新请求并下载
	else
	{
		Log("CRC失败,需要重新请求升级");
		char *fileDir = (char *)"0:/updata";
		mf_scan_files((u8 *)fileDir);	
		if(f_deldir(fileDir) !=0 )
		{
			u8 res=f_mkfs("0:",0,4096);
			if(res == 0)
			{
				Log("mkfs ok");
			}
			else
			{
				Log("mkfs failed");
			}
		}
		mf_mkdir((u8 *)fileDir);		
		
		GprsHttpConnectObj *http = CreateAskUpdataHttpConnect();
		SendGprsHttpConnect(http,HttpAskUpdataRecvCall,20);	
	}
}


void HttpUpdataSucCallBackRecvCall(HttpRecvObj *httpRecvObj)
{
	Log("HttpUpdataSucCallBackRecvCall id:%d,body:%s",httpRecvObj->id,((PbDataObj  *)(httpRecvObj->body.arg))->data);
	char * jsonStr = (char *)((PbDataObj  *)(httpRecvObj->body.arg))->data;
	if(!jsonStr)
	{
		Log("jsonStr Null");
		return ;
	}

	cJSON *cmdJson ={0};
	cmdJson= cJSON_Parse(jsonStr);
	if(!cmdJson)
	{
		printf("cmdJson is null:%s\r\n",jsonStr);
		return ;
	}
	
	if(strstr(cJSON_GetObjectItem(cmdJson, "func")->string,"func"))
	{
		char *func = cJSON_GetObjectItem(cmdJson, "func")->valuestring;
		if(strstr(func,"upgradeSoftSuccess"))
		{
			char *code = cJSON_GetObjectItem(cmdJson,"code")->valuestring;
			if(strstr((char *)code,"0"))
			{
				SetUpdating(UP_RUNING);
				WritParamToFlash();
			}		
		}
	}
	cJSON_Delete(cmdJson);
}

#include "GprsCmdUnPack.h"
const char monthCode[12][6]={"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

#include "time.h"

void HttpAskUpdataRecvCall(HttpRecvObj *httpRecvObj)
{
	Log("HttpRecvObj id:%d,body:%s",httpRecvObj->id,((PbDataObj  *)(httpRecvObj->body.arg))->data);
	
	//Date: Thu, 21 Nov 2019 03:30:01 GMT
	static u8 updataTime = 0;
	if(updataTime == 0)
	{
		HttpHead *head = GetHeadByName(httpRecvObj->head.arg,"Date");
		char *date =(char *)((PbDataObj  *)(head->value.arg))->data;
		if(date)
		{
			Log("Date Head:%s,size:%d",date,((PbDataObj  *)(head->value.arg))->size);
			char res[40]={0};
			strncpy(res,date,40);
			char restr[20]={0};
			char *dayStr = strfind((char *)res,", "," ",restr,20);
			u8 day = atoi(restr);
			
			char *monthStr = strfind((char *)dayStr," "," ",restr,20);
			u8 month = 0;
			for(month=0;month<12;month++)
			{
				//Log("cmp month:%s",monthCode[month]);
				if(strstr(restr,monthCode[month]))
				{
					break;
				}
			}
			month ++;
			char *yearStr = strfind(monthStr," "," ",restr,20);
			u16 year = atoi(restr);
			char *hourStr = strfind(yearStr," ",":",restr,20);
			u8 hour = atoi(restr);
			char *minStr = strfind(hourStr,":",":",restr,20);
			u8 min = atoi(restr);
			char *secStr = strfind(minStr,":"," ",restr,20);
			u8 sec = atoi(restr);	
							
			
			if(year >0 &&& month >0 & day >0)
			{
				time_t iTime;
				struct tm lt;
				lt.tm_year=100+(year -2000); 
				lt.tm_mon=month-1;
				lt.tm_mday=day;
				lt.tm_hour=hour;
				lt.tm_min=min;
				lt.tm_sec=sec;
				
				iTime = mktime(&lt);
				
				iTime = iTime + 8*3600;
				Log("iTime:%d",iTime);

				char nowtime[40];   
				localtime_r(&iTime,&lt);
				
				//Log("local time:%d-%d-%d %d:%d:%d",lt2->tm_year+1900,lt2->tm_mon+1,lt2->tm_mday,lt2->tm_hour,lt2->tm_min,lt2->tm_sec);
				strftime(nowtime, 24, "%Y-%m-%d %H:%M:%S",&lt);  
				Log("nowtime:%s",nowtime);
				year = lt.tm_year+1900;
				month = lt.tm_mon+1;
				day = lt.tm_mday;
				
				hour = lt.tm_hour;
				min = lt.tm_min;
				sec = lt.tm_sec;

				RTC_Set_Date(year-48,month,day,0);
				RTC_Set_Time(hour,min,sec,RTC_HOURFORMAT12_PM);
				updataTime = 1;
			}
			
		}		
		
	}
	char * jsonStr = (char *)((PbDataObj  *)(httpRecvObj->body.arg))->data;
	if(!jsonStr)
	{
		Log("jsonStr Null");
		return ;
	}

	cJSON *cmdJson ={0};
	cmdJson= cJSON_Parse(jsonStr);
	if(!cmdJson)
	{
		printf("cmdJson is null:%s\r\n",jsonStr);
		return ;
	}
	
		//dev/sys_sub/A111/a1NpEmshElO
		//{
		// "messageType": "upgradeSoft",
		// "sender","wangwu",//用户
		// "productCode": "1111",
		// "deviceCode": "a1NpEmshElO",
		// "softCode": "1111",//当前固件
		// "upgradeSoftCode": "2222",//要升级固件
		// "files":[
		//        {
		//            "crc16":"49113",
		//            "name":"smoke_xincheng-upgrade20191112104518.bin",
		//            "url":"http://47.97.155.219:8888/file/downloadFile?fileId=58",
		//            "fileId":"58"
		//        }
		//    ]
		//}
	
	if(strstr(cJSON_GetObjectItem(cmdJson, "func")->string,"func"))
	{
		char *func = cJSON_GetObjectItem(cmdJson, "func")->valuestring;
		if(strstr(func,"getUpgradeSoft"))
		{
			char *code = cJSON_GetObjectItem(cmdJson, "code")->valuestring;
			char *upgradeSoftCode = cJSON_GetObjectItem(cmdJson, "upgradeSoftCode")->valuestring;
			if(strstr((char *)code,"0") && strstr(upgradeSoftCode,SOFT_CODE) == 0)
			{
				cJSON *fileArry = cJSON_GetObjectItem(cmdJson,"files");
				if(fileArry)
				{
					int size = cJSON_GetArraySize(fileArry);
					for(int i=0;i<size;i++)
					{
						cJSON *file=cJSON_GetArrayItem(fileArry,i);	
						if(file)
						{
							char* crc16Str = cJSON_GetObjectItem(file,"crc16")->valuestring;
							char* name = cJSON_GetObjectItem(file,"name")->valuestring;
							char *url =  cJSON_GetObjectItem(file,"url")->valuestring;
							char *fileSizeStr = cJSON_GetObjectItem(file,"size")->valuestring;
							Log("crc16:%s,name:%s,url:%s",crc16Str,name,url);
							u16 crc16 = atoi(crc16Str);
							if(crc16 >0)
							{
								u32 fileSize = atoi(fileSizeStr);
								Log("fileSizeStr:%d",fileSize);
								if(fileSize > 0 && fileSize < (2*1024*1024))
								{									
									char *softIdPos = strstr(url,"fileId=");
									if(softIdPos)
									{
										char *softIdStart = softIdPos+7;
										
										u16 softId = atoi(softIdStart);
										if(softId)
										{
											char *catUrl = GprsMalloc(201);
											if(catUrl)
											{
												char chip[40];
												GetChipId(chip,40);
												snprintf(catUrl,200,"%s&&productCode=%s&&softCode=%s&&deviceCode=%s",url,PRODUC_CODE,SOFT_CODE,chip);			
												Log("DownLoad info crc16:%d,softId:%d",crc16,softId);
												Log("url:%s",catUrl);
												u32 total,free;
												exf_getfree((u8 *)"0:",&total,&free);
												Log("total:%d,free:%d",total,free);
												if(free > 1024*2)
												{
													GprsDownLoad("0:/updata","soft.bin",catUrl,crc16,fileSize,(1024*5),DownLoadEndCall);		//fileSize
												}
												else
												{
													Log("Flash Size err");
												}
												GprsFree(catUrl);
											}
										}										
									}
								}
							}
						}
					}
				}				
			}
	
		}
	}
	cJSON_Delete(cmdJson);
}
