#pragma once

#ifdef __cplusplus
 extern "C" {
#endif	
	#include "DeviceRunParam.h"
	 
	#define GetOutWaterTemp GetNtcTemp1
	#define GetInWaterTemp  GetNtcTemp2
	#define GetMaterTemp    GetPtTemp1
	 
	 u8 DeviceRunControl(u8 value);
	 u8 DeviceRunModeControl(u8 value);
	 void ClearDeviceWaring(void);
	 void HandControlSw(u8 bit,u8 value);
	 u8 GetDeviceRunSta(void);
	 void HandControl(u8 num);
	 void AlignControl(u8 num);
	 u8 DevicePidRunControl(u8 value);
	 void DeviceRecvModCallEx(u8 type,u16 addr,u16 reg,u16 value);
	  void CamUsartSend(u8 *data,u16 size);	
#ifdef __cplusplus
 }
#endif	


