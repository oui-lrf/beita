#include "DeviceRunCmd.h"
#include "DeviceRunUsartWork.h"

 const u8 VersionSendCmd[3] = 					{0x1B,0x41,0x01}; 
 const u8 SetSampTimeSpaceSendCmd[4]=   {0x1B,0x42,0x02,0x01};//采集间隔
 const u8 SetAirOutTimeSpaceSendCmd[4]= {0x1B,0x4E,0x02,0x01};//抽气间隔 //猜测的
 const u8 StartSampSendCmd[3] = 				{0x1B,0x43,0x01}; //开始采集
 const u8 StopSampSendCmd[3] = 					{0x1B,0x44,0x01}; //停止采集
 const u8 SampingReadPm10SendCmd[3] = 	{0x1B,0x45,0x01};	//获取PM10
 const u8 SampingReadParamSendCmd[3] = 	{0x1B,0x46,0x01};	//获取参数
 const u8 AutoCheckSendCmd[3] = 				{0x1B,0x47,0x01};
 const u8 FlowRateAlignSendCmd[3] =     {0x1B,0x48,0x01};//流速校准
 
 const u8 ReadParamSendCmd[3] = 				{0x1B,0x49,0x01};
 const u8 LiftUpSendCmd[3] = 						{0x1B,0x4A,0x01};//抬管上升
 const u8 LiftLowSendCmd[3] = 					{0x1B,0x4B,0x01};//抬管下降


 const u8 VersionRecvCmd[8] =							{0x1B,0x61,0x05,0x56,0x31,0x2E,0x30,0x66};
 const u8 StopSampRecvCmd[4] =						{0x1B,0x64,0x01,0x80};
 const u8 AutoCheckRecvHeadCmd[3] =				{0x1B,0x67,0x02};
 const u8 SetSampTimeSpaceRecvCmd[3]=			{0x1B,0x62,0x02};
 const u8 SetAirOutTimeSpaceRecvCmd[3]=		{0x1B,0x6E,0x02}; 
 const u8 SampingReadPm10RecvHeadCmd[3]=	{0x1B,0x65,0x06};//
 const u8 SampingReadParamRecvHeadCmd[3]= {0x1B,0x66,0x1A};
 
 

 
void DeviceSendCmd(u8 *data,u16 size)
{
//	u8 *cmd = DeviceRunMalloc(size +1);
//	if(!cmd)
//	{
//		return ;
//	}
//	u16 sum=0;
//	for(u16 i=0;i<size;i++)
//	{
//		sum+=data[i];
//	}
//	memcpy(cmd,data,size);
//	cmd[size]=(u8)(sum&0xff);
//	CamUsartSend(cmd,size+1);
//	DeviceRunFree(cmd);
}

u16 GetMin(u16 v1,u16 v2)
{
	if(v1 <= v2)
	{
		return v1;
	}
	else 
	{
		return v2;
	}
}

static void PrintfBuf(u8 *data,u16 size)
{
//	printf("cmp:");
//	for(u16 i=0;i<size;i++)
//	{
//		printf("%02X ",data[i]);
//	}
//	printf("\r\n");
}

//返回一致的字节数
u16 BufCmp(u8 *data1,u16 size1,u8 *data2,u16 size2)
{
	u16 res =0;
	u16 i=0;
	for(i=0;i<size1;i++)
	{
		if(data1[i] != data2[i])
		{
			break;
		}
	}
	res =i;
	return res;
}

//设置采集时间间隔
void SendSampTimeSpaceCmd(u16 time)
{
	u16 size = sizeof(SetSampTimeSpaceSendCmd);
	u8 *cmd = DeviceRunMalloc(size);
	if(!cmd)
	{
		return ;
	}
	memcpy(cmd,SetSampTimeSpaceSendCmd,size);
	cmd[3]=time;
	DeviceSendCmd(cmd,size);
	DeviceRunFree(cmd);
}



//设置抽气间隔
void SendAirOutTimeSpaceCmd(u16 time)
{
	u16 size =sizeof(SetAirOutTimeSpaceSendCmd);
	u8 *cmd = DeviceRunMalloc(size);
	if(!cmd)
	{
		return ;
	}
	memcpy(cmd,SetAirOutTimeSpaceSendCmd,size);
	cmd[3]=time;
	DeviceSendCmd(cmd,size);
	DeviceRunFree(cmd);
}

//设置膜片校准
void SendCoatAlign(float value)
{
//	u8 *cmd = DeviceRunMalloc(6);
//	if(!cmd)
//	{
//		return ;
//	}
//	cmd[0]=0x1b;
//	cmd[1]=0x4c;
//	memcpy(&cmd[2],&value,4);
//	CamUsartSend(cmd,8);
//	DeviceRunFree(cmd);
}




u8 AnalysisPm10(u8 *data,u16 size,float *res)
{
	if(size >= sizeof(SampingReadPm10RecvHeadCmd) +sizeof(float))
	{
		*res =  *(float*)(data+4);
		return 1;
	}
	else
	{
		return 0;
	}
}

u8 AnalysisParam(u8 *data,u16 size,float *outTemp,float *outHum,float *roomTemp,float *roomHum,float *atmos,float *flowRate)
{
	if(size >= sizeof(SampingReadParamRecvHeadCmd) +sizeof(float))
	{
		*outTemp =  *(float*)(data+4);
		*outHum  =  *(float*)(data+8);
		*roomTemp = *(float*)(data+16);
		*roomHum =  *(float*)(data+20);	
		*atmos = *(float*)(data+12);
		*flowRate = *(float*)(data+24);
		return 1;
	}
	else
	{
		return 0;
	}
}


u8 GetAutoCheckCode(u8 *data ,u16 size)
{
	u8 res = 0xff;
	if(size < 3)
	{
		return res;
	}
	if(BufCmp(data,size,(u8 *)AutoCheckRecvHeadCmd,sizeof(AutoCheckRecvHeadCmd)) == sizeof(AutoCheckRecvHeadCmd))
	{
		res = data[3];
//		switch(data[3])
//		{
//			case 0:
//				//自检通过
//			  runParam.bCmdType = 0;
//				runParam.ErrCode = 0;
//			break;
//			case 1:
//				//抬管单元异常
//				runParam.ErrCode = 1;
//			break;
//			case 2:
//				//纸带传送单元异常
//				runParam.ErrCode = 2;
//			break;
//			case 3:
//				//探测器单元异常
//				runParam.ErrCode = 3;
//			break;
//			case 4:
//				//气象单元异常
//				runParam.ErrCode = 4;
//			break;
//			case 5:
//				//气室温度检测单元异常
//				runParam.ErrCode = 5;
//			break;
//			case 6:
//				//流量传感器单元异常
//				runParam.ErrCode = 6;
//			break;
//		}
	}
	return res;
}
