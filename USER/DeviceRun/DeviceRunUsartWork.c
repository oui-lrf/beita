#include "DeviceRunUsartWork.h"
#include "DeviceRunInclude.h"


 cycleQueue runUsartQue;
extern QActive * const DV_DeviceWork;
//次函数被中断调用，不可执行耗时长的事务
static void UsartWorkRecvCallback(u8 *dat,u16 size)
{
	u16 rcvSize;
	if(size <= MAX_USART2_SIZE)
	{
	 rcvSize= size;
	}
	else
	{
		rcvSize= MAX_USART2_SIZE;
	}
	if(DV_DeviceWork->super.state.fun)
	{
		BaseType_t xHigherPriorityTaskWoken = pdFALSE;
		InsertQueBuf(&runUsartQue,dat,rcvSize);
		QpEvt *te;
		te = Q_NEW_FROM_ISR(QpEvt, USART_RECV_SIG);
		te->v = rcvSize;
		QACTIVE_POST_FROM_ISR(DV_DeviceWork,
																	&te->super,
																	&xHigherPriorityTaskWoken,
																	&l_TickHook);
		portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
	}
}

void UsartWorkInit(void)
{
	CreateQueue(&runUsartQue,MAX_USART2_SIZE*2);
	SetUsartRecvBufCallBack(2,UsartWorkRecvCallback);
	UsartStart(2);
}

void UsartWorkDeInit(void)
{
	SetUsartRecvBufCallBack(2,0);
	UsartStop(2);
}



