#include "DeviceRunTimer.h"
#include "DeviceRunParam.h"


#include "qpc.h"
#include "bsp.h"
#include "DeviceRunWork.h"




//Q_DEFINE_THIS_FILE
//extern void RunModeChangeCall(CONTROL_RUN_MODE sta);
//extern void TimerRunStaChangeCall(TIMER_RUN_STA sta);

extern QActive * const DV_DeviceWork;


/* protected: */
QState Alarm_initial(Alarm * const me, QEvt const * const e);
QState Alarm_off(Alarm * const me, QEvt const * const e);
QState Alarm_on(Alarm * const me, QEvt const * const e);

QState Alarm_TimerPartIn(Alarm * const me, QEvt const * const e);
QState Alarm_TimerPartOut(Alarm * const me, QEvt const * const e);
/* Alarm component --------------------*/
/*$skip${QP_VERSION} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv*/
/* Check for the minimum required QP version */
#if (QP_VERSION < 650U) || (QP_VERSION != ((QP_RELEASE^4294967295U) % 0x3E8U))
#error qpc version 6.5.0 or higher required
#endif

static u8 IsTimerIn(u32 time)
{
	if(time >=GetOpenTime1()  &&time <GetCloseTime1() && GetTimerSta1())
	{
		return TIMER_RUN_IN1;
	}
	if(time >=GetOpenTime2()  &&time <GetCloseTime2()&& GetTimerSta1())
	{
		return TIMER_RUN_IN2;
	}
	if(time >=GetOpenTime3()  &&time <GetCloseTime3()&& GetTimerSta1())
	{
		return TIMER_RUN_IN3;
	}
	if(time >=GetOpenTime4()  &&time <GetCloseTime4()&& GetTimerSta1())
	{
		return TIMER_RUN_IN4;
	}
	return TIMER_RUN_OUT;
}
/*$endskip${QP_VERSION} ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
/*$define${Components::Alarm} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv*/
/*${Components::Alarm} .....................................................*/
/*${Components::Alarm::ctor} ...............................................*/
void Alarm_ctor(Alarm * const me) {
    QHsm_ctor(&me->super, Q_STATE_CAST(&Alarm_initial));
}

/*${Components::Alarm::SM} .................................................*/
QState Alarm_initial(Alarm * const me, QEvt const * const e) {
    /*${Components::Alarm::SM::initial} */
    (void)e; /* avoid compiler warning about unused parameter */
    return Q_TRAN(&Alarm_off);
}
/*${Components::Alarm::SM::off} ............................................*/
QState Alarm_off(Alarm * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        /*${Components::Alarm::SM::off} */
        case Q_ENTRY_SIG: {
            me->runMode = RUN_MODE_AUTO;
						Log("+++Alarm_off");
            status_ = Q_HANDLED();
//						RunModeChangeCall(RUN_MODE_AUTO);
						me->runTimePart = TIMER_RUN_NULL;
//						TimerRunStaChangeCall(TIMER_RUN_NULL);
            break;
        }
        /*${Components::Alarm::SM::off} */
        case Q_EXIT_SIG: {
            /* upon exit, the alarm is converted to binary format */
            status_ = Q_HANDLED();
            break;
        }
        /*${Components::Alarm::SM::off::ALARM_ON} */
        case ALARM_ON_SIG:{
							RTC_TimeTypeDef RTC_TimeStruct;
							GetTimeStruct(&RTC_TimeStruct);
							u32 time = RTC_TimeStruct.Hours*60+RTC_TimeStruct.Minutes;
							Log("time out:%d",time);
							u8 res = IsTimerIn(time);
							me->runTimePart = res;
							if(res != TIMER_RUN_OUT)
							{
								 status_ = Q_TRAN(&Alarm_TimerPartIn);
							}
							else
							{
								status_ = Q_TRAN(&Alarm_TimerPartOut);
							}
            break;
        }
        default: {
            status_ = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status_;
}



/*${Components::Alarm::SM::on} .............................................*/
QState Alarm_on(Alarm * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        /*${Components::Alarm::SM::on} */
        case Q_ENTRY_SIG: {
            Log("+++Alarm ON");
						me->runMode = RUN_MODE_TIMER;
//						RunModeChangeCall(RUN_MODE_TIMER);
            status_ = Q_HANDLED();
            break;
        }
        /*${Components::Alarm::SM::on::ALARM_OFF} */
        case ALARM_OFF_SIG: {
            status_ = Q_TRAN(&Alarm_off);
            break;
        }
        /*${Components::Alarm::SM::on::TIME} */
        case TICK_SIG:{
           status_ = Q_HANDLED();
            break;
        }
        default: {
            status_ = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status_;
}

QState Alarm_TimerPartIn(Alarm * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        /*${Components::Alarm::SM::on} */
        case Q_ENTRY_SIG: {
            Log("+++Alarm_TimerPartIn");
							if(me->runTimePart != TIMER_RUN_OUT && me->runTimePart !=  TIMER_RUN_NULL)
							{
								switch(me->runTimePart)
								{
									case TIMER_RUN_IN1:
										me->runStopTemp = GetTimeStopTemp1();
									break;
									case TIMER_RUN_IN2:
										me->runStopTemp = GetTimeStopTemp2();
									break;
									case TIMER_RUN_IN3:
										me->runStopTemp = GetTimeStopTemp3();
									break;
									case TIMER_RUN_IN4:
										me->runStopTemp = GetTimeStopTemp4();
									break;
									default :
										
									break;
								}
								QpEvt *te;
								te = Q_NEW(QpEvt, TIMER_IN_SIG);
								te->p = me->runTimePart;
								te->v = me->runStopTemp;
								QACTIVE_POST(DV_DeviceWork, &te->super, me);
//								TimerRunStaChangeCall(me->runTimePart);							
							}
            status_ = Q_HANDLED();
            break;
        }
        /*${Components::Alarm::SM::on::ALARM_OFF} */
        case ALARM_OFF_SIG: {
						QACTIVE_POST(DV_DeviceWork, &Q_NEW(QpEvt,TIMER_OFF_SIG)->super, me);
            status_ = Q_TRAN(&Alarm_off);
            break;
        }
        /*${Components::Alarm::SM::on::TIME} */
        case TICK_SIG:{
						status_ = Q_HANDLED();
						RTC_TimeTypeDef RTC_TimeStruct;
						GetTimeStruct(&RTC_TimeStruct);
						u32 time = RTC_TimeStruct.Hours*60+RTC_TimeStruct.Minutes;
						u8 res = IsTimerIn(time);
						if(res == TIMER_RUN_OUT)
						{	
							me->runTimePart = res;
							status_ = Q_TRAN(&Alarm_TimerPartOut);
						}
            break;
        }
        default: {
            status_ = Q_SUPER(&Alarm_on);
            break;
        }
    }
    return status_;
}

QState Alarm_TimerPartOut(Alarm * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        /*${Components::Alarm::SM::on} */
        case Q_ENTRY_SIG: {
            Log("+++Alarm_TimerPartOut");
//						TimerRunStaChangeCall(TIMER_RUN_OUT);
						QACTIVE_POST(DV_DeviceWork, &Q_NEW(QpEvt,TIMER_OUT_SIG)->super, me);
            status_ = Q_HANDLED();
            break;
        }
        /*${Components::Alarm::SM::on::TIME} */
        case TICK_SIG:{
						status_ = Q_HANDLED();
						RTC_TimeTypeDef RTC_TimeStruct;
						GetTimeStruct(&RTC_TimeStruct);
						u32 time = RTC_TimeStruct.Hours*60+RTC_TimeStruct.Minutes;
						u8 res = IsTimerIn(time);
						if(res != TIMER_RUN_OUT)
						{
							me->runTimePart = res;
							status_ = Q_TRAN(&Alarm_TimerPartIn);
						}
            break;
        }
        default: {
            status_ = Q_SUPER(&Alarm_on);
            break;
        }
    }
    return status_;
}


