#ifndef __DEVICERUNTIMER_H__
#define __DEVICERUNTIMER_H__

#include "DeviceRunInclude.h"
/*$declare${Components::Alarm} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv*/
/*${Components::Alarm} .....................................................*/
typedef struct {
/* protected: */
    QHsm super;

/* private: */
	//	TIMER_RUN_STA runTimePart;
		int runStopTemp;
} Alarm;

/* public: */
void Alarm_ctor(Alarm * const me);
#endif
