#ifndef __DEVICERUNDRIVE_H__
#define __DEVICERUNDRIVE_H__
#include "DeviceRunInclude.h"

typedef enum {
	FEED_SW = IO_SW1,
	FIRE_SW = IO_SW2,
	BLOW_IN_SW = IO_SW3,
	BLOW_OUT_SW = IO_SW4,
	GRATE_SW = IO_SW5,
	CIRCUL_PUMP_SW1 =IO_SW6, 
	WARING_OUT_SW = IO_SW7,
	BACK_UP_SW = IO_SW8,
}SWPin;
void OutPutSw(u8 bit,BOOL value);
#endif

