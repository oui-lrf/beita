/*
*********************************************************************************************************
*
*	模块名称 : 电容触摸芯片GT811驱动程序
*	文件名称 : bsp_ct811.c
*	版    本 : V1.0
*	说    明 : GT811触摸芯片驱动程序。
*	修改记录 :
*		版本号   日期        作者     说明
*		V1.0    2014-12-25  armfly   正式发布
*
*	Copyright (C), 2014-2015, 安富莱电子 www.armfly.com
*********************************************************************************************************
*/
#include "gt811.h"
#include "usart.h"
#include "i2c.h"
#include "Debug.h"
#include "FreeEvent.h"
//#include "GUI.h"

#define GT811_WriteReg I2C1_Write
#define GT811_ReadReg I2C1_Read

GT811_T g_GT811;

#define GT811_READ_XY_REG    0x814E /* 坐标寄存器 */ 

#define GT811_CLEARBUF_REG   0x814E /* 清除坐标寄存器 */ 

#define GT811_CONFIG_REG     0x8047 /* 配置参数寄存器 */ 

#define GT811_COMMAND_REG    0x8040 /* 实时命令 */ 

#define GT811_PRODUCT_ID_REG 0x8140 /* 芯片ID */ 

#define GT811_VENDOR_ID_REG  0x814A /* 当前模组选项信息 */ 

#define GT811_CONFIG_VERSION_REG   0x8047 /* 配置文件版本号 */ 

#define GT811_CONFIG_CHECKSUM_REG  0x80FF /* 配置文件校验码 */ 

#define GT811_FIRMWARE_VERSION_REG 0x8144 /* 固件版本号 */ 

/* GT811单个触点配置参数，一次性写入 */ 
const uint8_t s_GT811_CfgParams[]= 
{ 

#if 1	/* 1024 * 600 */
	0x00,0x00,0x04,0x58,0x02,0x0A,0x0D,0x00,
	0x01,0x08,0x28,0x05,0x50,0x32,0x03,0x05,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x8C,0x2E,0x0E,0x27,0x24,
	0xD0,0x07,0x00,0x00,0x01,0x99,0x04,0x1D,
	0x00,0x00,0x00,0x00,0x00,0x03,0x64,0x32,
	0x00,0x00,0x00,0x0F,0x23,0x94,0xC5,0x02,
	0x07,0x00,0x00,0x04,0xA2,0x10,0x00,0x8C,
	0x13,0x00,0x7C,0x16,0x00,0x68,0x1B,0x00,
	0x5C,0x20,0x00,0x5C,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x01,0x25,0x14,0x04,0x14,
	0x00,0x00,0x02,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x02,0x04,0x06,0x08,0x0A,0x0C,0x0E,0x10,
	0x12,0x14,0x16,0x18,0x1A,0x1C,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x02,
	0x04,0x06,0x08,0x0A,0x0C,0x0F,0x10,0x12,
	0x13,0x14,0x16,0x18,0x1C,0x1D,0x1E,0x1F,
	0x20,0x21,0x22,0x24,0x26,0x28,0x29,0x2A,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x43,0x01	
#else

	0x00, //0x8047 版本号 

	0xE0,0x01, //0x8048/8049 X坐标输出最大值480 

	0x56,0x03, //0x804a/804b Y坐标输出最大值854 

	0x01, //0x804c 输出触点个数上限 

	0x35, //0x804d 软件降噪，下降沿触发 

	0x00, //0x804e reserved 

	0x02, //0x804f 手指按下去抖动次数 

	0x08, //0x8050 原始坐标窗口滤波值 

	0x28, //0x8051 大面积触点个数 

	0x0A, //0x8052 噪声消除值 

	0x5A, //0x8053 屏上触摸点从无到有的阈值 

	0x46, //0x8054 屏上触摸点从有到无的阈值 

	0x03, //0x8055 进低功耗时间 s 

	0x05, //0x8056 坐标上报率 

	0x00, //0x8057 X坐标输出门上限 

	0x00, //0x8058 Y坐标输出门上限 

	0x00,0X00, //0x8059-0x805a reserved 

	0x00, //0x805b reserved 

	0x00, //0x805c reserved 

	0x00, //0x805d 划线过程中小filter设置 

	0x18, //0x805e 拉伸区间 1 系数 

	0x1A, //0x805f 拉伸区间 2 系数 

	0x1E, //0x8060 拉伸区间 3 系数 

	0x14, //0x8061 各拉伸区间基数 

	0x8C, //0x8062 、、 

	0x28, //0x8063 、、 

	0x0C, //0x8064 、、 

	0x71, //0x8065 驱动组A的驱动频率倍频系数 

	0x73, //0x8066 驱动组B的驱动频率倍频系数 

	0xB2, //0x8067 驱动组A、B的基频 

	0x04, //0x8068 

	0x00, //0x8069 相邻两次驱动信号输出时间间隔 

	0x00, //0x806a 

	0x00, //0x806b 、、 

	0x02, //0x806c 、、 

	0x03, //0x806d 原始值放大系数 

	0x1D, //0x806e 、、 

	0x00, //0x806f reserved 

	0x01, //0x8070 、、 

	0x00,0x00, //reserved 

	0x00, //0x8073 、、 

	0x00,0x00,0x00,0x00,0x00,0x00, //0x8071 - 0x8079 reserved 

	0x50, //0x807a 跳频范围的起点频率 

	0xA0, //0x807b 跳频范围的终点频率 

	0x94, //0x807c 多次噪声检测后确定噪声量，1-63有效 

	0xD5, //0x807d 噪声检测超时时间 

	0x02, //0x807e 、、 

	0x07, //0x807f 判别有干扰的门限 

	0x00,0x00, //0x8081 reserved 

	0x04, //0x8082 跳频检测区间频段1中心点基频（适用于驱动A、B） 

	0xA4, //0x8083 

	0x55, //0x8084 跳频检测区间频段1中心点倍频系数 

	0x00, //0x8085 跳频检测区间频段2中心点基频(驱动A、B在此基础上换算) 

	0x91, //0x8086 

	0x62, //0x8087 跳频检测区间频段2中心点倍频系数 

	0x00, //0x8088 跳频检测区间频段3中心点基频（适用于驱动A、B） 

	0x80, //0x8089 

	0x71, //0x808a 跳频检测区间频段3中心点倍频系数 

	0x00, //0x808b 跳频检测区间频段4中心点基频（适用于驱动A、B） 

	0x71, //0x808c 

	0x82, //0x808d 跳频检测区间频段4中心点倍频系数 

	0x00, //0x808e 跳频检测区间频段5中心点基频（适用于驱动A、B） 

	0x65, //0x808f 

	0x95, //0x8090 跳频检测区间频段5中心点倍频系数 

	0x00, 0x65, //reserved 

	0x00, //0x8093 key1位置 0：无按键 

	0x00, //0x8094 key2位置 0：无按键 

	0x00, //0x8095 key3位置 0：无按键 

	0x00, //0x8096 key4位置 0：无按键 

	0x00, //0x8097 reserved 

	0x00, //0x8098 reserved 

	0x00, //0x8099 reserved 

	0x00, //0x809a reserved 

	0x00, //0x809b reserved 

	0x00, //0x809c reserved 

	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, 

	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, //0x809d-0x80b2 reserved 

	0x00, //0x80b3 合框距离 

	0x00, //0x80b4 

	0x00,0x00, //0x80b6 reserved 

	0x06, //0x80b7 

	0x08, //0x80b8 

	0x0A, //0x80b9 

	0x0C, //0x80ba 

	0x0E, //0x80bb 

	0x10, //0x80bc 

	0x12, //0x80bd 

	0x14, //0x80be 

	0x16, //0x80bf 
+	
	0x18, //0x80c0 

	0x1A, //0x80c1 

	0x1C, //0x80c2 

	0xFF, //0x80c3 

	0xFF, //0x80c4 

	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, 

	0x00,0x00,0x00 



	,0x00, //0x80c5-0x80d4 reserved 

	0x00, //0x80d5 

	0x02, //0x80d6 

	0x04, //0x80d7 

	0x06, //0x80d8 

	0x08, //0x80d9 

	0x0A, //0x80da 

	0x0C, //0x80db 

	0x0F, //0x80dc 

	0x10, //0x80dd 

	0x12, //0x80de 

	0x13, //0x80df 

	0x14, //0x80e0 

	0x16, //0x80e1 

	0x18, //0x80e2 

	0x1C, //0x80e3 

	0x1D, //0x80e4 

	0x1E, //0x80e5 

	0x1F, //0x80e6 

	0x20, //0x80e7 

	0x21, //0x80e8 

	0xFF, //0x80e9 

	0xFF, //0x80ea 

	0xFF, //0x80eb 

	0xFF, //0x80ec 

	0xFF, //0x80ed 

	0xFF, //0x80ee 

	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00, 

	0x00,0x00,0x00,0x00, //0x80ef-0x80fe reserved 

	0x0B, //0x80ff 配置信息校验 

	0x01 //0x8100 配置以更新标记 
#endif
}; 

/*
*********************************************************************************************************
*	函 数 名: GT811_InitHard
*	功能说明: 配置触摸芯片.  在调用该函数前，请先执行 bsp_touch.c 中的函数 bsp_DetectLcdType() 识别id
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void GT811_Init(void)
{
	
	#if 1
		/* 定义触笔中断INT的GPIO端口 */
	{
		 GPIO_InitTypeDef GPIO_InitStruct = {0};

//		/* 第1步：打开GPIO时钟 */
//		  __HAL_RCC_GPIOH_CLK_ENABLE();

//		 HAL_GPIO_WritePin(PORT_TP_INT,PIN_TP_INT,GPIO_PIN_RESET);
//		 
//		GPIO_InitStruct.Pin = PIN_TP_INT;
//		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//		GPIO_InitStruct.Pull = GPIO_PULLDOWN;
//		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
//		HAL_GPIO_Init(PORT_TP_INT, &GPIO_InitStruct);
		
//		DelayUs(10000);
//		HAL_GPIO_WritePin(PORT_TP_INT,PIN_TP_INT,GPIO_PIN_SET);
//		DelayUs(10000);					/* 产生2ms脉冲唤醒 */
//		HAL_GPIO_WritePin(PORT_TP_INT,PIN_TP_INT,GPIO_PIN_RESET);	/* INT脚拉低。使GT911能正常工作 */		
//		DelayUs(200);				
//		/* 第2步：配置所有的按键GPIO为浮动输入模式。*/	
//		 GPIO_InitStruct.Pin = PIN_TP_INT;
//		GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
//		GPIO_InitStruct.Pull = GPIO_PULLUP;
//		HAL_GPIO_Init(PORT_TP_INT, &GPIO_InitStruct);
	}	
	#endif
	//////////////////////////
	g_GT811.i2c_addr = GT811_I2C_ADDR1;
	#if 1
	uint16_t ver;
	ver = GT811_ReadVersion();
	
	printf("Version : %04X\r\n", ver);
	#else
	
	GT811_WriteReg(g_GT811.i2c_addr, GT811_CONFIG_REG, (uint8_t *)s_GT811_CfgParams, sizeof(s_GT811_CfgParams));
	
	/* 根据模组类型重置分辨率寄存器 */
	if (g_GT811.i2c_addr == GT811_I2C_ADDR1)
	{
		const uint8_t tab[4] = {0xe0, 0x01, 0x20, 0x03};
		GT811_WriteReg(g_GT811.i2c_addr, 0x6DD, (uint8_t *)tab, 4);
	}
	else	/* GT811_I2C_ADDR3 */
	{
		const uint8_t tab[4] = {0x58, 0x02, 0x00, 0x04};
		GT811_WriteReg(g_GT811.i2c_addr, 0x6DD, (uint8_t *)tab, 4);
	}	
	
	#endif
}

/*
*********************************************************************************************************
*	函 数 名: GT811_ReadVersion
*	功能说明: 获得GT811的芯片版本
*	形    参: 无
*	返 回 值: 16位版本
*********************************************************************************************************
*/
uint16_t GT811_ReadVersion(void)
{
	uint8_t buf[2];
	
	GT811_ReadReg(g_GT811.i2c_addr, GT811_FIRMWARE_VERSION_REG, buf, 2);

	return ((uint16_t)buf[1] << 8) + buf[0]; 
}


/*
*********************************************************************************************************
*	函 数 名: TOUCH_PenInt
*	功能说明: 判断触摸按下
*	形    参: 无
*	返 回 值: 0表示无触笔按下，1表示有触笔按下
*********************************************************************************************************
*/
uint8_t TOUCH_PenInt(void)
{
	if ((PORT_TP_INT->IDR & PIN_TP_INT) == 0)
	{
		return 1;
	}
	return 0;
}
/*
*********************************************************************************************************
*	函 数 名: GT811_OnePiontScan
*	功能说明: 读取GT811触摸数据，这里仅读取一个触摸点。
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
bool GT811_ReadOnePiont(uint16_t *px, uint16_t *py)
{
	uint8_t buf[10] = {0}; /* 将其都初始化为 0 */
	uint16_t x, y;
	uint8_t res;
	uint8_t clear_flag = 0;
	/* 读取寄存器：0x721  R  TouchpointFlag  Sensor_ID  key  tp4  tp3  tp2  tp1  tp0 */
	res = GT811_ReadReg(g_GT811.i2c_addr, GT811_READ_XY_REG, buf, 1);
	if(res != 0)
	{
		printf("1.failed\r\n");
		return false;
	}
	
	/* 判断是否按下，没有按下，直接退出 */
	if ((buf[0] & 0x0f) == 0)
	{
		GT811_WriteReg(g_GT811.i2c_addr,GT811_READ_XY_REG,&clear_flag,1);	
		//printf("release\r\n");
		return false;
	}
	
	
//	if (TOUCH_PenInt() == 0)
//	{		
//		return false;
//	}
	/* 读取第一个触摸点0 */
	res = GT811_ReadReg(g_GT811.i2c_addr, GT811_READ_XY_REG + 1, &buf[1], 6);
	GT811_WriteReg(g_GT811.i2c_addr,GT811_READ_XY_REG,&clear_flag,1);	
	if(res != 0)
	{
		return false;
	}
	
	/*
	0x721  R  TouchpointFlag  Sensor_ID  key  tp4  tp3  tp2  tp1  tp0
	0x722  R  Touchkeystate     0  0  0  0  key4  key3  key2  key1

	0x723  R  Point0Xh  触摸点 0，X 坐标高 8 位
	0x724  R  Point0Xl  触摸点 0，X 坐标低 8 位
	0x725  R  Point0Yh  触摸点 0，Y 坐标高 8 位
	0x726  R  Point0Yl  触摸点 0，Y 坐标低 8 位
	0x727  R  Point0Atmos  触摸点 0，触摸压力
	*/
//	g_GT811.X0 = ((uint16_t)buf[2] << 8) + buf[3];
//	g_GT811.Y0 = ((uint16_t)buf[4] << 8) + buf[5];
//	g_GT811.P0 = buf[6];
	
	
	g_GT811.X0 = ((uint16_t)buf[3] << 8) + buf[2];
	g_GT811.Y0 = ((uint16_t)buf[5] << 8) + buf[4];
	g_GT811.P0 = ((uint16_t)buf[7] << 8) + buf[6];
	
	/* 检测按下 */
	/* 坐标转换 :
		电容触摸板左下角是 (0，0);  右上角是 (479，799)
		需要转到LCD的像素坐标 (左上角是 (0，0), 右下角是 (799，479)
	*/
//	uprintf("x0:%5d,y0:%5d\r\n", g_GT811.X0, g_GT811.Y0);
	x = g_GT811.X0*800/1024;
	y = g_GT811.Y0*480/600;
//	x = g_GT811.X0;
//	y = g_GT811.Y0;
	if (x > 799)
	{
		x = 799;
	}
	if (x > 799)
	{
		x = 799;
	}
	
	if (y > 479)
	{
		y = 479;
	}
	/*
		有时候I2C通信出问题，有触摸值，但是压力参数是0，通过这种方法可以将其排除。
		x     y   p
		0,    0,  0
		0,    0,  0
	   24,    0,  0
		0,    0,  0
	   11,  323,  0
		0,    0,  0
		0,    0,  0
		6,  235,  0
	   38,  230,  0
		0,    0,  0
//	*/
	if(g_GT811.P0 == 0)
	{
		//uprintf("%5d,%5d,%3d\r\n",  g_GT811.X0, g_GT811.Y0, g_GT811.P0);
		printf("press failed\r\n");
		return false;
	}

	//如果已经灭屏了
	if(LCD_LED == 0)
	{
		PostFreeEvent(EVENT_OPEN_LCD_LED);
	}
	*px = x;
	*py = y;
//	uprintf("%5d,%5d\r\n", x, y);
	return true;
}

/*12
*********************************************************************************************************
*	函 数 名: GT811_ReadSensorID
*	功能说明: 识别显示模块类别。读取GT811 SensorID引脚状态，有3个状态，悬空，接电源，接地。
*	形    参: 无
*	返 回 值: 显示模块类别, 0, 1, 2
*********************************************************************************************************
*/
uint8_t GT811_ReadSensorID(void)
{
	uint8_t value;
	
	/* 	0x721  R  TouchpointFlag      Sensor_ID  key  tp4  tp3  tp2  tp1  tp0 */
	GT811_ReadReg(g_GT811.i2c_addr, 0x721, &value, 1);
	
	return (value >> 6);
}

//1234
//bt22
//bt2
//swz
