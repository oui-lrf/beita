#ifndef __GT811_H
#define __GT811_H

#include <stdint.h>
#include <stdbool.h>
/*
	GT811 的从设备地址有三组可选，以方便主控调配。
	三组地址分别为：0xBA/0xBB、0x6E/0x6F和0x28/0x29
	
	由Sensor ID 引脚的状态决定.
	
	Sensor ID = NC   时，模组1   安富莱用于 800 * 480 
	Sensor ID = VDD  时，模组2   
	Sensor ID = GND  时，模组3   安富莱用于 1024 * 600 
*/
#define GT811_I2C_ADDR1	0xBA
#define GT811_I2C_ADDR2	0x6E
#define GT811_I2C_ADDR3	0x28

typedef struct
{
	uint8_t i2c_addr;
	uint8_t P0;
	uint16_t X0;
	uint16_t Y0;
}GT811_T;

void GT811_Init(void);
uint16_t GT811_ReadVersion(void);
uint8_t GT811_ReadSensorID(void);
bool GT811_ReadOnePiont(uint16_t *px, uint16_t *py);

extern GT811_T g_GT811;

#endif

/***************************** 安富莱电子 www.armfly.com (END OF FILE) *********************************/
