#ifndef __FLASHPORT_H__
#define __FLASHPORT_H__

#include "bsp_spi_flash.h"
#include "sys.h"

u8 save_para_flash(u32 FLASH_ADDR,u16 size,void *param);
u8 read_para_flash(u32 FLASH_ADDR,u16 size,void *param);

#endif
