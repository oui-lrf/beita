#include "GuiPort.h"
#include "GuiInclude.h"
extern osMessageQId msgQueueUIHandle;

void SendUiMsg(uint8_t screenId,uint16_t WidGetId,uint32_t data)
{
	//Log("SendUiMsg screenId:%d,WidGetId:%d,data:%d",screenId,WidGetId,data);
	UiMsg *msg = (UiMsg *)pvPortMalloc(sizeof(UiMsg));
	if(!msg)
	{
		Log("UpdataSwToUi malloc failed");
		return ;
	}
	msg->screenId = (SCREEN_ID)screenId;
	msg->widGetId = WidGetId;
	msg->data = data;
	osStatus err = osMessagePut(msgQueueUIHandle,(uint32_t)msg,0);	
	if(err != osOK)
	{
		Log("UpdataSwInput osMessagePut error");
		 vPortFree(msg);
	}
}

