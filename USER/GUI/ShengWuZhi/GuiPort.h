#pragma once

#include "sys.h"
#include "cmsis_os.h"
#include "malloc.h"
#ifdef __cplusplus
 extern "C" {
#endif	  
	#include "portmacro.h"
	 
	 #define GuiFree(n) myfree(SRAMIN,n)
	 #define GuiMalloc(n) mymalloc(SRAMIN,n)
	 
	 
typedef enum{
	
	 NOW_POW,
	 MONTH_CONSUME,
	 ALL_CONSUME,

	 CUR1,
	 CUR2,
	 CUR3,
	
	 VOL1,
	 VOL2,
	 VOL3,

	HEAD_NET_STA,
	HEAD_NET_TYPE,//23
	HEAD_NET_SIGNAL,
	
	SET_START_TEMP,
	SET_STOP_TEMP,
	SET_ROOM_START_TEMP,
	SET_ROOM_STOP_TEMP,
	SET_OPEN_PUMB_TEMP,
	SET_CLOSE_PUMB_TEMP,
	SET_LOCAL_IP,
	SET_REMOTE_IP,
	SET_REMOTE_IP_STA,
	SET_NAME,
	SET_PASS,
	SET_LOCAL_PORT,
	SET_REMOTE_PORT,
	SET_USER_CODE,
	SET_ICCID,
	SET_USER_IP,
	SET_USER_PORT,	
	SET_SW1,
	SET_SW2,
	SET_SW3,
	
	HEAD_TIME,
}MAIN_WIDGET_ID;	 
	 
	 
typedef enum{
	SCREEN_MAIN,
	SCREEN_SET,
	SCREEN_SYS,
	SCREEN_TIMER,
}SCREEN_ID;


typedef struct{
	SCREEN_ID screenId;
	uint16_t widGetId;
	uint32_t data;
}UiMsg;
void SendUiMsg(uint8_t screenId,uint16_t WidGetId,uint32_t data);

extern portBASE_TYPE IdleTaskHook(void* p);
#ifdef __cplusplus
 }
#endif	 
