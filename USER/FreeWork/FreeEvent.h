#ifndef __FREEEVENT_H
#define __FREEEVENT_H

#ifdef __cplusplus
 extern "C" {
#endif
#include "sys.h"
#include "cmsis_os.h"
#include "qpc.h"
typedef enum
{
	EVENT_OPEN_BEEP				=1,
	EVENT_SAVE_SYS_FLASH			,
	EVENT_SAVE_DEVICE_FLASH,
	EVENT_SAVE_NET_FLASH,
	EVENT_OPEN_LCD_LED,
	EVENT_OPEN_FACTORY_RESET,
}FREE_EVENT_TYPE; 

osStatus PostFreeEvent(FREE_EVENT_TYPE event);

extern QActive * const US_FreeWork;
void FreeWork_ctor(void);
#ifdef __cplusplus
}
#endif

#endif
