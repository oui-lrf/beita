#include "FreeEvent.h"
#include "beep.h"
#include "SysConfig.h"
#include "NetConfig.h"
#include "FileRecord.h"
#include "main.h"

#if 1
	#define FreeLog Log
#else
	#define FreeLog(...)
#endif


typedef struct {
    QActive super;
		QTimeEvt timeEvt;
		QTimeEvt timeEvt2;
//		QTimeEvt timeEvt3;
		QTimeEvt timeEvt4;
//		QEQueue requestQueue;
//		QEvt const *requestQSto[10];
} FreeWork;

#define LCD_BL_TIME 5*60*10  //5Min

static QState FreeWork_initial(FreeWork * const me, QEvt const * const e);
static QState FREE_TOP(FreeWork * const me, QEvt const * const e);

static FreeWork freeWork;
QActive * const US_FreeWork = &freeWork.super;


void FreeWork_ctor(void) {
	FreeWork *me = (FreeWork *)US_FreeWork;
  QActive_ctor(&me->super, Q_STATE_CAST(&FreeWork_initial));
	QTimeEvt_ctorX(&me->timeEvt, &me->super, TIMEOUT_SIG, 0U);
	QTimeEvt_ctorX(&me->timeEvt2, &me->super, TIMEOUT2_SIG, 0U);
//	QTimeEvt_ctorX(&me->timeEvt3, &me->super, TIMEOUT3_SIG, 0U);
	QTimeEvt_ctorX(&me->timeEvt4, &me->super, TIMEOUT4_SIG, 0U);
//	QEQueue_init(&me->requestQueue,me->requestQSto, Q_DIM(me->requestQSto));
}

static QState FreeWork_initial(FreeWork * const me, QEvt const * const e) {
		FreeLog("FreeWork_initial");
    return Q_TRAN(&FREE_TOP);
}

static QState FREE_TOP(FreeWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
					 case Q_ENTRY_SIG: {
						status_ = Q_HANDLED();	
						FreeLog("+++FREE_TOP");
						 BEEP =BEEP_CLOSE;
						QTimeEvt_armX(&me->timeEvt2,BSP_TICKS_PER_SEC*LCD_BL_TIME,0);
            break;
					}
					case TIMEOUT_SIG: {
						status_ = Q_HANDLED();
						FreeLog("BEEP_CLOSE");
						BEEP =BEEP_CLOSE;
					break;
					} 
					case TIMEOUT2_SIG: {
						status_ = Q_HANDLED();
						LCD_LED = 0;
					break;
					} 
					//预留
					case TIMEOUT3_SIG: {
						status_ = Q_HANDLED();
					
					break;
					} 
					case TIMEOUT4_SIG: {
						status_ = Q_HANDLED();
						HAL_NVIC_SystemReset();
					break;
					} 
					 case FREE_EVENT_SIG:{
						 status_ = Q_HANDLED();	
						 
						 FREE_EVENT_TYPE FrType = (FREE_EVENT_TYPE)(Q_EVT_CAST(QpEvt)->v);
						 FreeLog("FREE_EVENT_SIG:%d",FrType);
						switch(FrType)
						{
							case EVENT_OPEN_BEEP:
								BEEP =BEEP_OPEN;
								QTimeEvt_disarm(&me->timeEvt); 
								QTimeEvt_armX(&me->timeEvt,300,0);
							break;
							
							case EVENT_OPEN_LCD_LED:
								LCD_LED = 1;
								QTimeEvt_disarm(&me->timeEvt2); 
								QTimeEvt_armX(&me->timeEvt2,BSP_TICKS_PER_SEC*LCD_BL_TIME,0);
							break;
							case EVENT_SAVE_SYS_FLASH:
								SaveSysParam();
							break;
							case EVENT_SAVE_DEVICE_FLASH:
								SaveDeviceParam();
							break;
						case EVENT_SAVE_NET_FLASH:
							SaveNetParam();
						break;
							case EVENT_OPEN_FACTORY_RESET:
								BEEP =BEEP_OPEN;
								u8 res=f_mkfs("0:",0,4096);
								if(res == 0)
								{
									Log("mkfs ok");
								}
								else
								{
									Log("mkfs failed");
								}
								ResetSysParam();
								ResetDeviceParam();
								ResetNetParam();
								QTimeEvt_disarm(&me->timeEvt4); 
								QTimeEvt_armX(&me->timeEvt4,BSP_TICKS_PER_SEC*2,0);
								
							break;
						}
					 break;
					}
				case Q_EXIT_SIG:{
					status_ = Q_HANDLED();	
					QTimeEvt_disarm(&me->timeEvt);
					QTimeEvt_disarm(&me->timeEvt2);
//					QTimeEvt_disarm(&me->timeEvt3);
					QTimeEvt_disarm(&me->timeEvt4);
					break;
				}
        default: {
            status_ = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status_;
}

osStatus PostFreeEvent(FREE_EVENT_TYPE event)
{
	QpEvt *te;
	te = Q_NEW(QpEvt, FREE_EVENT_SIG);
	te->v = event;
	QACTIVE_POST(US_FreeWork,&te->super,0);
}
