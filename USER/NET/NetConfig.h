#ifndef __NETCONFIG_H__
#define __NETCONFIG_H__
#include "sys.h"
#ifdef __cplusplus
extern "C" {
#endif

#define SERVER_TOPIC "boiler/server/recv"
#define MAX_IP_LEN 40

typedef enum {
	NET_TYPE_CLOSE,
	NET_TYPE_WIFI=1,
	NET_TYPE_GPRS,
}NET_TYPE;

#define MAX_TOPIC_LEN 40 
#define MAX_WIFI_NAME_LEN 40
#define MAX_WIFI_PASS_LEN 40

void NetConfigInit(void);
void ResetNetParam(void);
void PrintfNetParam(void);

u8 GetIp(char * res, u16 size);
u16 GetPort(void);
u8 SetIp(char* data);
u8 SetPort(u16 data);

u8 GetEmpPermiss(void);
u8 GetNetType(void);
u8 GetMangTopic(char * topic, u16 size);
u8 GetEmpTopic(char * topic, u16 size);
u8 SetEmpTopic(char *topic);
u8 SetMangTopic(char *topic);
u8 SetNetType(u8 type);
void SetEmpPermiss(u8 per);
void SaveNetParam(void);
u8 SetWifiPass(char *pass);
u8 SetWifiName(char *name);
u8 GetWifiName(char * name, u16 size);
u8 GetWifiPass(char * pass, u16 size);

#define DEFAULT_SERVER_IP "114.115.150.248"
#define DEFAULT_SERVER_PORT 18572

#define MQTT_IP "49.235.100.116"
#define MQTT_PORT 1883

#ifdef __cplusplus
}
#endif
#endif
