/**
  ******************************************************************************
  * @file    mqtt.h
  * $Author: �ɺ�̤ѩ $
  * $Revision: 17 $
  * $Date:: 2012-07-06 11:16:48 +0800 #$
  * @brief   MQTTӦ�ò㺯��.
  ******************************************************************************
  * @attention
  *
  *<h3><center>&copy; Copyright 2009-2012, EmbedNet</center>
  *<center><a href="http:\\www.embed-net.com">http://www.embed-net.com</a></center>
  *<center>All Rights Reserved</center></h3>
  * 
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MQTTPORT_H
#define __MQTTPORT_H
#include "MqttInclude.h"
#include "MqttWork.h"

/* Includes ------------------------------------------------------------------*/

/* Exported Functions --------------------------------------------------------*/

int mqtt_connect_server(int wait_time);
int mqtt_publish(char *pTopic,u8 *data,u16 size);

int MqttConnectServer(MqttWork * const me);
int MqttSubServer(char *pTopic);
void MqttSendHeart(void);
u8 MqttPubHeart(void);

#endif /* __MAIN_H */

/*********************************END OF FILE**********************************/
