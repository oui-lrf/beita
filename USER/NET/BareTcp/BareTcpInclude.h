#pragma once
#include "debug.h"
#include "malloc.h"
#include "NetInclude.h"
#include "sys.h"
#include "qpc.h"
#include "cmsis_os.h"
#include "bsp.h"
#include "NetPort.h"
#include "circular_queue.h"
#include "string.h"
#include "TcpPort.h"

#if 1
	#define BareTcpLog Log
#else
	#define BareTcpLog 
#endif
