#include "BareTcpLogic.h"
#include "BareTcpPort.h"
#include "BareTcpWork.h"

extern QActive * const TP_TcpWork;

static QState BareTcpWork_initial(BareTcpWork * const me, QEvt const * const e);
static QState BARE_TCP_ON(BareTcpWork * const me, QEvt const * const e);
static QState BARE_TCP_CLOSE(BareTcpWork * const me, QEvt const * const e);
static QState BARE_TCP_LINK(BareTcpWork * const me, QEvt const * const e);
static QState BARE_TCP_SUB(BareTcpWork * const me, QEvt const * const e);
static QState BARE_TCP_SUBED(BareTcpWork * const me, QEvt const * const e);
static QState BARE_TCP_PAUSE(BareTcpWork * const me, QEvt const * const e);

static BareTcpWork bareTcpWork;
QActive * const MQ_BareTcpWork = &bareTcpWork.super;
Q_DEFINE_THIS_MODULE("BareTcpWork")

void BareTcpWork_ctor(void) {
    BareTcpWork *me = (BareTcpWork *)MQ_BareTcpWork;
    QActive_ctor(&me->super, Q_STATE_CAST(&BareTcpWork_initial));
		QTimeEvt_ctorX(&me->timeEvt, &me->super, TIMEOUT_SIG, 0U);
}

static QState BareTcpWork_initial(BareTcpWork * const me, QEvt const * const e) {
    /*${BareTcp::BareTcpWork::SM::initial} */
		QActive_subscribe(&me->super, TCP_CONNECT_SUCCES_SIG);
		QActive_subscribe(&me->super, TCP_CONNECT_FAILED_SIG);
		QActive_subscribe(&me->super, TCP_RECV_SIG);
		QActive_subscribe(&me->super, BARE_TCP_PUBLISH_SIG);
		QActive_subscribe(&me->super, TCP_PAUSE_SIG);
		QActive_subscribe(&me->super, TCP_RESUME_SIG);
    return Q_TRAN(&BARE_TCP_CLOSE);
}


static QState BARE_TCP_ON(BareTcpWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        /*${BareTcp::BareTcpWork::SM::NET_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
						BareTcpLog("+++BARE_TCP_ON");
						TcpAppConnect(me->msg.ip,me->msg.port);
						status_ = Q_HANDLED();	
            break;
        }
				case BARE_TCP_REON:
						status_ =Q_TRAN(&BARE_TCP_ON);
				break;
				case TCP_PAUSE_SIG:{
						status_ =Q_TRAN(&BARE_TCP_PAUSE);
					break;
				}
				case BARE_TCP_CLOSE_SIG:
						status_ =Q_TRAN(&BARE_TCP_CLOSE);
				break;
				case TCP_CONNECT_SUCCES_SIG:
					BareTcpLog("BARE_TCP_ON TCP_CONNECT_SUCCES_SIG");
					status_ =Q_TRAN(&BARE_TCP_LINK);
				break;
				case TCP_CONNECT_FAILED_SIG:
					BareTcpLog("BARE_TCP_ON TCP_CONNECT_FAILED_SIG");
					//给上层发送连接失败
					QF_PUBLISH(&Q_NEW(QpEvt, BARE_TCP_LINK_FAILED_SIG)->super, me);
					status_ =Q_TRAN(&BARE_TCP_CLOSE);
				break;
				case BARE_TCP_PUBLISH_SIG:
						BareTcpLog("BARE_TCP_ON BARE_TCP_PUBLISH_SIG");
						status_ = Q_HANDLED();
						BareTcpPubMsg *msg = (BareTcpPubMsg *)(Q_EVT_CAST(QpEvt)->p);
						if(msg)
						{
							NetFree( msg->data);
							NetFree(msg);
						}
					break;
					case TCP_RECV_SIG:{
						BareTcpLog("BARE_TCP_ON TCP_RECV_SIG");
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							NetFree(data);
						}
						status_ = Q_HANDLED();	
          break;
        }
        default: {
            status_ = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status_;
}


//复写接收函数，由bareTcp内部调用
cycleQueue bareTcp_que;
static int transport_getdata(unsigned char* buf, int count)
{
	int len = 0;
	len = ReadQueBuf(&bareTcp_que,buf,count);
	return len;
}

//给服务器发送连接指令
static QState BARE_TCP_LINK(BareTcpWork * const me, QEvt const * const e)
{
		QState status_;
    switch (e->sig) {
        /*${BareTcp::BareTcpWork::SM::NET_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
						BareTcpLog("+++BARE_TCP_LINK");
						BareTcpConnectServer(me);
						//有时候服务器不会回复，应该加入超时处理
						QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*60,0);
						status_ = Q_HANDLED();	
            break;
        }
				case TCP_SEND_FAILED_SIG:
					Log("BareTcp Link Failed");
					QTimeEvt_disarm(&me->timeEvt);
					//给上层报告失败
					QF_PUBLISH(&Q_NEW(QpEvt, BARE_TCP_LINK_FAILED_SIG)->super, me);
					status_ =Q_TRAN(&BARE_TCP_CLOSE);
				break;
				case TIMEOUT_SIG:
					status_ = Q_HANDLED();	
					QF_PUBLISH(&Q_NEW(QpEvt, BARE_TCP_LINK_FAILED_SIG)->super, me);
					status_ =Q_TRAN(&BARE_TCP_CLOSE);
				break;
				case TCP_RECV_SIG: {
						status_ = Q_HANDLED();
            u8 *data = (u8 *)(Q_EVT_CAST(QpEvt)->p);
						u16 len = Q_EVT_CAST(QpEvt)->v;
						if(data && len)
						{
							QTimeEvt_disarm(&me->timeEvt);
							WifiLog("bareTcp tcp size:%d",len);
							int res = 0; 
							res = CreateQueue(&bareTcp_que,len);
							if(res) 
							{
								InsertQueBuf(&bareTcp_que,data,len);
								int msgType=0;
								msgType =BARE_TCPPacket_read(data,len,transport_getdata);
								if(msgType == CONNACK)
								{
									;unsigned char sessionPresent, connack_rc;
									if (BARE_TCPDeserialize_connack(&sessionPresent, &connack_rc,data,len) != 1 || connack_rc != 0)
									{
										Log("BareTcp Link Failed");
										//重新连接
										status_ = Q_TRAN(&BARE_TCP_LINK);
									}
									else
									{
										Log("BareTcp Link Success");
										status_ = Q_TRAN(&BARE_TCP_SUB);
									}
								}
								else
								{
									Log("BareTcp Link Failed");
									//重新连接
									status_ = Q_TRAN(&BARE_TCP_LINK);
								}
							}
							else
							{
								Log("BareTcpRecvCall CreateQueue failed");
								//重新连接
								status_ = Q_TRAN(&BARE_TCP_LINK);
							}
							DestroyQueue(&bareTcp_que);
							NetFree(data);
						}
          break;
        }
				case Q_EXIT_SIG: {
						QTimeEvt_disarm(&me->timeEvt);
						status_ = Q_HANDLED();	
            break;
        }
        default: {
            status_ = Q_SUPER(&BARE_TCP_ON);
            break;
        }
    }
    return status_;
}


static QState BARE_TCP_SUB(BareTcpWork * const me, QEvt const * const e)
{
		QState status_;
    switch (e->sig) {
        /*${BareTcp::BareTcpWork::SM::NET_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
						BareTcpLog("+++BARE_TCP_SUB");
						BareTcpSubServer(me->msg.subTopic);
						//有时候服务器不会回复，应该加入超时处理
						QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*60,0);
						status_ = Q_HANDLED();	
            break;
        }
				case TCP_SEND_FAILED_SIG:
					Log("BareTcp Link Failed");
					QTimeEvt_disarm(&me->timeEvt);
					//给上层报告失败
					QF_PUBLISH(&Q_NEW(QpEvt, BARE_TCP_LINK_FAILED_SIG)->super, me);
					status_ =Q_TRAN(&BARE_TCP_CLOSE);
				break;
				case TIMEOUT_SIG:
					QF_PUBLISH(&Q_NEW(QpEvt, BARE_TCP_LINK_FAILED_SIG)->super, me);
					status_ =Q_TRAN(&BARE_TCP_CLOSE);
				break;
				case TCP_RECV_SIG: {
						status_ = Q_HANDLED();
            u8 *data = (u8 *)(Q_EVT_CAST(QpEvt)->p);
						u16 len = Q_EVT_CAST(QpEvt)->v;
						if(data && len)
						{
							QTimeEvt_disarm(&me->timeEvt);
							int res = 0; 
							res = CreateQueue(&bareTcp_que,len);
							if(res) 
							{
								InsertQueBuf(&bareTcp_que,data,len);
								int msgType=0;
								msgType =BARE_TCPPacket_read(data,len,transport_getdata);
								if(msgType == SUBACK)
								{
									unsigned short submsgid;
									int subcount;
									int granted_qos;
									BARE_TCPDeserialize_suback(&submsgid, 1, &subcount, &granted_qos, data, len);
									if (granted_qos != 0)
									{
										Log("BareTcp sub Failed");
										//重新订阅
										status_ = Q_TRAN(&BARE_TCP_SUB);
									}
									else
									{
										Log("BareTcp sub Success");
										status_ = Q_TRAN(&BARE_TCP_SUBED);
									}
								}
								else
								{
									Log("BareTcp sub Failed");
									//重新订阅
									status_ = Q_TRAN(&BARE_TCP_SUB);
								}
							}
							else
							{
								Log("BareTcpRecvCall CreateQueue failed");
								//重新订阅
								status_ = Q_TRAN(&BARE_TCP_SUB);
							}
							DestroyQueue(&bareTcp_que);
							NetFree(data);
						}
          break;
        }
				case Q_EXIT_SIG: {
						QTimeEvt_disarm(&me->timeEvt);
						status_ = Q_HANDLED();	
            break;
        }
        default: {
            status_ = Q_SUPER(&BARE_TCP_ON);
            break;
        }
    }
    return status_;
}

//订阅成功后不停的发送心跳
//要对关闭消息进行处理，关闭定时器
//3次心跳失败则重连
static QState BARE_TCP_SUBED(BareTcpWork * const me, QEvt const * const e)
{
	QState status_;
	static u8 heartNum = 0;
	switch (e->sig) {
			/*${BareTcp::BareTcpWork::SM::NET_MODEL_OPEN} */
			case Q_ENTRY_SIG: {
					BareTcpLog("+++BARE_TCP_SUBED");
					QF_PUBLISH(&Q_NEW(QpEvt, BARE_TCP_LINK_SUCCES_SIG)->super, me);
					heartNum =0;
					//有时候服务器不会回复，应该加入超时处理
					QTimeEvt_disarm(&me->timeEvt); 
					QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*5,0);
					status_ = Q_HANDLED();	
					break;
			}
			case TIMEOUT_SIG:
				status_ = Q_HANDLED();	

				if(heartNum <5)
				{
					BareTcpSendHeart();
					QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*5,0);
				}
				else
				{	Log("heartNum:%d",heartNum);
					//向上层提交网络失败
					QF_PUBLISH(&Q_NEW(QpEvt, BARE_TCP_LINK_FAILED_SIG)->super, me);
					status_ =Q_TRAN(&BARE_TCP_CLOSE);
				}
				heartNum++;
			break;
			case BARE_TCP_PUBLISH_SIG:
				//BareTcpLog("BARE_TCP_SUBED BARE_TCP_PUBLISH_SIG");
				status_ = Q_HANDLED();
				BareTcpPubMsg *msg = (BareTcpPubMsg *)(Q_EVT_CAST(QpEvt)->p);
				if(msg)
				{
					u8 *pubData = msg->data;
					char *topic = msg->topic;
					bareTcp_publish(topic,pubData,msg->dataSize);
					NetFree(pubData);
					NetFree(topic);
					NetFree(msg);
				}
			break;
			case TCP_RECV_SIG: {
					status_ = Q_HANDLED();
					u8 *data = (u8 *)(Q_EVT_CAST(QpEvt)->p);
					u16 len = Q_EVT_CAST(QpEvt)->v;				
//					printf("bareTcp recv size:%d,data:%s\r\n",len,data);
					if(data && len)
					{
						int res = 0; 
						res = CreateQueue(&bareTcp_que,len);
						if(res) 
						{
							InsertQueBuf(&bareTcp_que,data,len);
							int msgType=0;
							msgType =BARE_TCPPacket_read(data,len,transport_getdata);
							switch(msgType)
							{
								case PINGREQ:
									//Log("send ping ok");
									heartNum =0;
								break;
								case PINGRESP:
									heartNum =0;
									//Log("ping PINGRESP");
								break;
								case PUBLISH:
										;
										 unsigned char dup;
										 int qos;
										 unsigned char retained;
										 unsigned short msgid;
										 int payloadlen_in; 
											u8 * payload_in;
											u8 * bareTcpTopic ;
											BARE_TCPString receivedTopic = BARE_TCPString_initializer; 
											payloadlen_in=0;
											receivedTopic.cstring = (char *)bareTcpTopic;
											BARE_TCPDeserialize_publish(&dup, &qos,&retained,&msgid,&receivedTopic,&payload_in,&payloadlen_in,data,len);
											if(payloadlen_in>0)
											{
												u8 *buf = mymalloc(SRAMIN,payloadlen_in);
												if(!buf) 
												{
													printf("BareTcpPublish malloc buf err\r\n");
													break ;
												}
												BareTcpPubMsg *msg = (BareTcpPubMsg *)mymalloc(SRAMIN,sizeof(BareTcpPubMsg));
												if(!msg) 
												{
													printf("BareTcpPublish malloc msg err\r\n");
													myfree(SRAMIN,buf);
													break ;
												}
												
												char *top = mymalloc(SRAMIN,strlen(receivedTopic.cstring)+1);
												if(!top)
												{
													myfree(SRAMIN,buf);
													myfree(SRAMIN,msg);
												}
												strcpy(top,receivedTopic.cstring);
												memcpy(buf,payload_in,payloadlen_in);
												msg->topic= top;
												msg->data=buf;
												msg->dataSize=payloadlen_in;
												QpEvt *te;
												te = Q_NEW(QpEvt, BARE_TCP_RECV_SIG);
												te->p = (u32)msg;
												te->v = sizeof(BareTcpPubMsg);
												QF_PUBLISH(&te->super, me);
											}
											heartNum =0;
								break;
								case DISCONNECT:
									Log("BARE_TCP DISCONNECT");
									QTimeEvt_disarm(&me->timeEvt);
									QF_PUBLISH(&Q_NEW(QpEvt, BARE_TCP_LINK_FAILED_SIG)->super, me);
									status_ =Q_TRAN(&BARE_TCP_CLOSE);
								break;
							}
						}
						DestroyQueue(&bareTcp_que);
						NetFree(data);
					}
				break;
			}
			case Q_EXIT_SIG:{
				BareTcpLog("***BARE_TCP_SUBED");
				QTimeEvt_disarm(&me->timeEvt);
				status_ = Q_HANDLED();	
			break;
			}
			default: {
					status_ = Q_SUPER(&BARE_TCP_ON);
					break;
			}
    }
    return status_;

}


static QState BARE_TCP_PAUSE(BareTcpWork * const me, QEvt const * const e)
{
    QState status_;
    switch (e->sig) {
        case Q_ENTRY_SIG: {
						BareTcpLog("+++BARE_TCP_PAUSE");
						QF_PUBLISH(&Q_NEW(QpEvt, BARE_TCP_PAUSE_SIG)->super, me);
						status_ = Q_HANDLED();	
            break;
        }
				case TCP_RESUME_SIG:{
						BareTcpLog("BARE_TCP_PAUSE TCP_RESUME_SIG");
						status_ = Q_HANDLED();	
						QF_PUBLISH(&Q_NEW(QpEvt, BARE_TCP_RESUME_SIG)->super, me);
				break;
				}
				case BARE_TCP_ON_SIG:{
					status_ = Q_HANDLED();	
					BareTcpLog("BARE_TCP_CLOSE BARE_TCP_ON_SIG");
					BARE_TCP_LINK_MSG *msg = (BARE_TCP_LINK_MSG *)(Q_EVT_CAST(QpEvt)->p);
					if(msg)
					{
						memcpy(&(me->msg),msg,sizeof(BARE_TCP_LINK_MSG));
						NetFree(msg);
						QACTIVE_POST(MQ_BareTcpWork, &Q_NEW(QpEvt, BARE_TCP_REON)->super,0);
					}
				break;
				}
        default: {
            status_ = Q_SUPER(&BARE_TCP_ON);
            break;
        }
    }
    return status_;
}

static QState BARE_TCP_CLOSE(BareTcpWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        /*${BareTcp::BareTcpWork::SM::NET_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
						BareTcpLog("+++BARE_TCP_CLOSE");
						QACTIVE_POST(TP_TcpWork, &Q_NEW(QpEvt, TCP_CLOSE_SIG)->super, me);
						status_ = Q_HANDLED();	
            break;
        }
				case BARE_TCP_ON_SIG:{
					status_ = Q_HANDLED();	
					BareTcpLog("BARE_TCP_CLOSE BARE_TCP_ON_SIG");
					BARE_TCP_LINK_MSG *msg = (BARE_TCP_LINK_MSG *)(Q_EVT_CAST(QpEvt)->p);
					if(msg)
					{
						status_ =Q_TRAN(&BARE_TCP_ON);
						memcpy(&(me->msg),msg,sizeof(BARE_TCP_LINK_MSG));
						NetFree(msg);
					}
				break;
				}
				case BARE_TCP_PUBLISH_SIG:{
						BareTcpLog("BARE_TCP_CLOSE BARE_TCP_PUBLISH_SIG");
						status_ = Q_HANDLED();
						BareTcpPubMsg *pubmsg = (BareTcpPubMsg *)(Q_EVT_CAST(QpEvt)->p);
						if(pubmsg)
						{
							NetFree( pubmsg->data);
							NetFree(pubmsg->topic);
							NetFree(pubmsg);
						}
					break;
					}
				case TCP_RECV_SIG: {
						BareTcpLog("BARE_TCP_ON TCP_RECV_SIG");
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							NetFree(data);
						}
						status_ = Q_HANDLED();	
          break;
        }
        default: {
            status_ = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status_;
}


