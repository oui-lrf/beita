/**
  ******************************************************************************
  * @file    main.c
  * $Author: 飞鸿踏雪 $
  * $Revision: 17 $
  * $Date:: 2014-10-25 11:16:48 +0800 #$
  * @brief   主函数.
  ******************************************************************************
  * @attention
  *
  *<h3><center>&copy; Copyright 2009-2012, EmbedNet</center>
  *<center><a href="http:\\www.embed-net.com">http://www.embed-net.com</a></center>
  *<center>All Rights Reserved</center></h3>
  * 
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/

#include "BARE_TCPPacket.h"
#include "transport.h"
#include "BareTcpLogic.h"
#include "BareTcpPort.h"


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/**
  * @brief  向代理（服务器）发送一个消息
  * @param  pTopic 消息主题
  * @param  pMessage 消息内容
  * @retval 小于0表示发送失败
  */


/*
	如果推送成功,则返回推送数据的长度
*/
int bareTcp_publish(char *pTopic,u8 *data,u16 size)
{
	int len=0;
	int rc=0;
  int buflen = size+100;
	u8 *send_buf;
	
	//Log("bareTcp_publish Topic:%s,data:%s size:%d",pTopic,data,size);
	BARE_TCPString topicString = BARE_TCPString_initializer;
	send_buf = mymalloc(SRAMIN,buflen);
	if(!send_buf) 
	{
		BareTcpLog("bareTcp_publish send_buf");
		return 0;
	}
  topicString.cstring = pTopic;
	len = BARE_TCPSerialize_publish(send_buf, buflen, 0, 0, 0, 0, topicString, data, size); 
  rc = transport_sendPacketBuffer(send_buf,len);
	myfree(SRAMIN,send_buf);
  return rc;
}

int BareTcpConnectServer(BareTcpWork * const me)
{
	int sendLen=0;
	u8 *buf;
	int MaxSendLen=200;
	int resLen=0;
	
	BARE_TCPPacket_connectData data = BARE_TCPPacket_connectData_initializer;
	data.clientID.cstring = (char *)me->msg.clientId;//<27
	
	data.keepAliveInterval = 100;//服务器保持连接时间，超过该时间后，服务器会主动断开连接，单位为秒
	data.cleansession = 1;
	data.username.cstring = me->msg.userName;
	data.password.cstring = me->msg.userPass;
	data.struct_version = 0;
	data.BARE_TCPVersion =3;
	
	buf = mymalloc(SRAMIN,MaxSendLen);
	if(!buf)
	{
		BareTcpLog("BareTcpConnectServer malloc buf err");
		return 1;
	}
	
	sendLen = BARE_TCPSerialize_connect(buf, MaxSendLen, &data);
	resLen = transport_sendPacketBuffer(buf,sendLen);
	myfree(SRAMIN,buf);
  if(resLen != sendLen){
   BareTcpLog("transport_sendPacketBuffer error");
    return 1;
  }
	else
	{
		BareTcpLog("transport_sendPacketBuffer OK len:%d",sendLen);
		return 0;
	}
}


int BareTcpSubServer(char *pTopic)
{
	
	u8 *buf;
	int MaxSendLen=100;
	
	int rc = 0xff;
//	OS_ERR err;
	int msgid = 1;
	BARE_TCPString topicString = BARE_TCPString_initializer;
	int req_qos = 0;
	int len = 0;
	
	BareTcpLog("bareTcp subscrib");
	/* subscribe */
	topicString.cstring = pTopic;
	buf = mymalloc(SRAMIN,MaxSendLen);
	if(!buf)
	{
		BareTcpLog("BareTcpSubServer malloc buf err");
		return 1;
	}
	len = BARE_TCPSerialize_subscribe(buf, MaxSendLen, 0, msgid, 1, &topicString, &req_qos);
	rc = transport_sendPacketBuffer(buf, len);
	myfree(SRAMIN,buf);
  if(rc != len){
    BareTcpLog("connect transport_sendPacketBuffer error2");
		rc =1;
    return 2;
  }else
	{
		return 0;
	}
}


void BareTcpSendHeart(void)
{
	int len=0;
	int MaxSendLen=30;
	u8 *buf;
	buf = mymalloc(SRAMIN,MaxSendLen);
	if(!buf)
	{
		BareTcpLog("BareTcpSendHeart malloc err");
		return ;
	}
	memset(buf,0,MaxSendLen);
	len = BARE_TCPSerialize_pingreq(buf,MaxSendLen);
	transport_sendPacketBuffer(buf,len);
	myfree(SRAMIN,buf);
}


