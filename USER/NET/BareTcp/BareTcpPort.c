#include "BareTcpPort.h"
#include "BareTcpWork.h"
#include "BareTcpInclude.h"

extern QActive * const MQ_BareTcpWork;

/*
把数据直接发送，成功返回0,可多线程调用
*/
void BareTcpPublish(const u8 *data,const u16 dataSize)
{
	u8 *buf = mymalloc(SRAMIN,dataSize);
	if(!buf) 
	{
		printf("BareTcpPublish malloc buf err\r\n");
		return ;
	}
	BareTcpPubMsg *msg = (BareTcpPubMsg *)mymalloc(SRAMIN,sizeof(BareTcpPubMsg));
	if(!msg) 
	{
		printf("BareTcpPublish malloc msg err\r\n");
		myfree(SRAMIN,buf);
		return ;
	}
	memcpy(buf,data,dataSize);
	msg->data=buf;
	msg->dataSize=dataSize;
	QpEvt *te;
	te = Q_NEW(QpEvt, BARE_TCP_PUBLISH_SIG);
	te->p = (u32)msg;
	te->v = sizeof(BareTcpPubMsg);
	QF_PUBLISH(&te->super,0);
}


void CreateBareTcp(char *sip,u16 port)
{
	Log("CreateBareTcp ip:%s,port:%d",sip,port);
	u16  size = strlen(sip)+1;
	char *ip= NetMalloc(size);
	if(ip)
	{
		memset(ip,0,size);
		strncpy(ip,sip,size);
		QpEvt *te;
		te = Q_NEW(QpEvt, BARE_TCP_ON_SIG);
		te->p = (uint32_t)ip;
		te->v = port;
		QACTIVE_POST(MQ_BareTcpWork, &te->super, me);
	}
}

void CloseBareTcp(void)
{
	QACTIVE_POST(MQ_BareTcpWork, &Q_NEW(QpEvt, BARE_TCP_CLOSE_SIG)->super, me);
}

