#pragma once
#include "BareTcpInclude.h"
typedef struct{
	u16 port;
	char ip[40];
}BARETCP_LINK_MSG;

typedef struct {
/* protected: */
    QActive super;
		QTimeEvt timeEvt;
		MQTT_LINK_MSG msg;
}BareTcpWork;
void BareTcpCtPoll(void);
void BareTcpWorkPoll(void);

void SubSucessCall(void);
void SubFailedCall(void);
void LinkFailedCall(void);
void LinkSucessCall(void);
extern QActive * const MQ_BareTcpWork;
void BareTcpWork_ctor(void);



