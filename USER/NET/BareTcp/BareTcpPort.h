#ifndef __BARETCPPORT_H__
#define __BARETCPPORT_H__
#include "BareTcpInclude.h"
typedef struct{
	u8* data;
	u16 dataSize;
}BareTcpPubMsg;


void BareTcpPublish(const u8 *data,const u16 dataSize);
void CreateBareTcp(char *ip,u16 port);
void CloseBareTcp(void);
#endif
