#include "HttpPort.h"
#include "string.h"
#include "malloc.h"
#include "HttpResultProcess.h"
#include "iap.h"
#include "FlashPort.h"
#include "GprsPort.h"

u8 HttpRequest(char *ip,u16 port,char *method,char *url,char *data)
{
	char *http;
//	http = CreateHttpPack(method,url,ip,port,data);
//	printf("http:%s\r\n",http);
//	GprsTcpSend(ip,port,(u8 *)http,strlen(http),0,0,10);
	myfree(SRAMIN,http);
	return 1;
}

//const char monthCode[12][6]={"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};


//如果是超时无响应也要返回改函数
void HttpRecvCall(u8 *data,u16 size)
{
	u8 *body=0;
	u16 bodyLen = GetBody(data,&body);
	Log("http bodyLen:%d",bodyLen);
	printf("data:%s",data);
	
//	{
//		static u8 firstTime = 0;
//		if(firstTime==0)
//		{
//			char res[40]={0};
//			char *date = strfind((char *)data,"Date:","GMT",res,40);
//			if(date)
//			{
//				Log("date:%s",date);
//				// Wed, 16 Oct 2019 11:34:21 GMT
//				char restr[20]={0};
//				char *dayStr = strfind((char *)res,", "," ",restr,20);
//				u8 day = atoi(restr);
//				
//				char *monthStr = strfind((char *)dayStr," "," ",restr,20);
//				u8 month = 0;
//				//Log("month:%s",restr);
//				for(month=0;month<12;month++)
//				{
//					//Log("cmp month:%s",monthCode[month]);
//					if(strstr(restr,monthCode[month]))
//					{
//						break;
//					}
//				}
//				char *yearStr = strfind(monthStr," "," ",restr,20);
//				u16 year = atoi(restr);
//				char *hourStr = strfind(yearStr," ",":",restr,20);
//				u8 hour = atoi(restr);
//				char *minStr = strfind(hourStr,":",":",restr,20);
//				u8 min = atoi(restr);
//				char *secStr = strfind(minStr,":"," ",restr,20);
//				u8 sec = atoi(restr);	
//				hour = (hour +8)%24;
//				month ++;
//				Log("%04d-%02d-%02d %02d:%02d:%02d",year,month,day,hour,min,sec);
//				if(year >0 &&& month >0 & day >0)
//				{
//					RTC_Set_Date(year-48,month,day,0);
//					RTC_Set_Time(hour,min,sec,RTC_HOURFORMAT12_PM);
//					firstTime = 1;
//				}
//			}
//		}	
//	}

	
	if(bodyLen)
	{
//		printf("body:%s",body);
//		AnalyzeHttpReturn((char *)body);
		myfree(SRAMIN,body);
	}
}




void HttpAskUpdata(void)
{
	char *ask = mymalloc(SRAMIN,100);
	if(!ask)
	{
		return ;
	}
	char *chip = mymalloc(SRAMIN,40);
	if(!chip)
	{
		myfree(SRAMIN,ask);
		return ;
	}
	GetChipId(chip,40);
	sprintf(ask,"{\"productCode\":\"%s\",\"deviceCode\":\"%s\"}",PRODUC_CODE,chip);
	HttpRequest(HTTP_IP,HTTP_PORT,"POST","device/getUpgradeSoft",ask);
	myfree(SRAMIN,ask);
	myfree(SRAMIN,chip);
}

void HttpAskParam(void)
{
	char *ask = mymalloc(SRAMIN,100);
	if(!ask)
	{
		return ;
	}
	char *chip = mymalloc(SRAMIN,40);
	if(!chip)
	{
		myfree(SRAMIN,ask);
		return ;
	}
	GetChipId(chip,40);
	//{"productCode":"a4480","deviceCode":"gM0R1piR0pwegln"}
	///extra/getExtraInfoByCode 
	sprintf(ask,"{\"productCode\":\"%s\",\"deviceCode\":\"%s\"}",PRODUC_CODE,chip);
	HttpRequest(HTTP_IP,HTTP_PORT,"POST","extra/getExtraInfoByCode",ask);
	myfree(SRAMIN,ask);
	myfree(SRAMIN,chip);
}


void HttpUploadUpdated(void)
{
//	char *ask = mymalloc(SRAMIN,100);
//	if(!ask)
//	{
//		return ;
//	}
//	char *chip = mymalloc(SRAMIN,40);
//	if(!chip)
//	{
//		myfree(SRAMIN,ask);
//		return ;
//	}
//	GetChipId(chip,40);
//	//{"productCode":"11111","deviceCode":"11111"} 
//	sprintf(ask,"{\"productCode\":\"%s\",\"deviceCode\":\"%s\",\"softCode\":\"%s\"}",PRODUC_CODE,chip,SOFT_CODE);
//	HttpRequest(HTTP_IP,HTTP_PORT,"POST","device/upgradeSoftSuccess",ask);
//	myfree(SRAMIN,ask);
//	myfree(SRAMIN,chip);
}

