#ifndef __HTTP_PORT_H__
#define __HTTP_PORT_H__
#include "sys.h"

typedef enum{
	HTTP_LINK_SUCESS=1,
	HTTP_LINK_FAILED,
	HTTP_REQUEST_SUCESS,
	HTTP_REQUEST_FAILED,
	HTTP_LINK_ERR,
}HTTP_MSG_TYPE;

typedef struct{
	u8 *data;
	u16 len;
}HTTP_DATA;

typedef struct{
	HTTP_MSG_TYPE type;
	HTTP_DATA *data;
}HTTP_MSG;


u8 CreateHttp(char *ip,u16 port,void (*call)(HTTP_MSG *msg));
u8 HttpRequest(char *ip,u16 port,char *method,char *url,char *data);
void HttpInit(void (*call)(void));
void HttpAskUpdata(void);
void HttpAskParam(void);
void HttpUploadUpdated(void);
void HttpRecvCall(u8 *data,u16 size);
#endif
