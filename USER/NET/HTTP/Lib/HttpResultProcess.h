#ifndef __HTTPRESULTPROCESS_H__
#define __HTTPRESULTPROCESS_H__
#include "sys.h"
u16 GetBody(u8 *RecvData,u8 **bodyBuf);
u8 GetStartCount(u8 *RecvData,u32 *startByte,u32 *endByte,u32 *totalByte);
u16 ReadBody(u8 *body,u16 bodySize,u8 *resBuf,u16 bufSize);
u8 GetHttpResCrc16(u8 *RecvData,u16 *crc);
u8 ConnectionClose(u8 *data);
#endif
