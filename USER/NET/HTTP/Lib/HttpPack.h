#ifndef __HTTPPACK_H__
#define __HTTPPACK_H__

#ifdef __cplusplus
extern "C" {
#endif
#include "sys.h"
#include "GprsPort.h"

char *CreateDownBinPack(char *url,char* host,char* fileName,float verSion,u16 startRange,u16 endRange);
char *CreateHttpPack(char *method,char *url,char* ip, u16 port,char* body);
char *CreateGetDownBinPack(char* ip, u16 port,char *url,u32 startRange,u32 endRange);
GprsHttpConnectObj *CreateAskUpdataHttpConnect(void);
GprsHttpConnectObj *CreateDownFileHttpConnect(char *url,u32 startRange,u32 endRange,u32 waiteTime);
GprsHttpConnectObj *CreateUpdataSucCallBackHttpConnect(void);
	#ifdef __cplusplus
}
#endif
#endif

