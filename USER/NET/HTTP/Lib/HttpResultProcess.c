#include "HttpResultProcess.h"
#include "string.h"
#include "malloc.h"
#include "stdlib.h"
#include "stdio.h"
#include "Debug.h"

u16 GetBody(u8 *RecvData,u8 **bodyBuf)
{
	u16 len =0;
	u8 *body;
	char *lenPos = strstr((char *)RecvData,"Content-Length:");
	len = atoi(lenPos+strlen("Content-Length:"));
	if(len <=0)
	{
		return 0;
	}
	char *bodyHead = strstr((char *)RecvData,"\r\n\r\n");
	if(!bodyHead)
	{
		return 0;
	}
	body = mymalloc(SRAMIN,len+1);
	if(!body)
	{
		Log("GetBody malloc body err");
		return 0;
	}
	memset(body,0,len);
	Log("len:%d",len);
	memcpy(body,bodyHead+4,len);
	*bodyBuf = body;
	return len;
}



//获取开始字节，结束字节，总字节
u8 GetStartCount(u8 *RecvData,u32 *startByte,u32 *endByte,u32 *totalByte)
{
	u8 rt = 0;
	char *startBytePos =  strstr((char *)RecvData,"Content-Range: bytes ")+strlen("content-range: bytes ");
	char *endBytePos = strstr(startBytePos,"-")+1;
	char *totalBytePos = strstr(endBytePos,"/")+1; 
	if(startBytePos)
	{
		*startByte = atoi(startBytePos);
		rt =1;
	}
	if(endBytePos)
	{
		*endByte  = atoi(endBytePos);
		rt =2;
	}
	if(totalBytePos)
	{ 
		*totalByte = atoi(totalBytePos);
		rt =3;
	}
	return rt;
}


u8 GetHttpResCrc16(u8 *RecvData,u16 *crc)
{
	char *crcPos = strstr((char *)RecvData,"Crc16:")+6;
	if(crcPos)
	{
		*crc = atoi(crcPos);
		return 1;
	}
	else
	{
		return 0;
	}
}

u16 ReadBody(u8 *body,u16 bodySize,u8 *resBuf,u16 bufSize)
{
	u16 resSize=0;
	if(bufSize < bodySize)
	{
		resSize = bufSize;
	}
	else
	{
		resSize = bodySize;
	}
	memcpy(resBuf,body,resSize);
	return resSize;
}

//

