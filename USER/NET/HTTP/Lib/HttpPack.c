#include "HttpPack.h"
#include "malloc.h"
#include "string.h"
#include "stdlib.h"
#include <stdio.h>
#include "utils.h"
#include "Debug.h"
#include "GprsUserSendData.h"

//GET http://www.boileriot.com/downLoad_device?boiler=ele_hot_water&version=1.0 HTTP/1.1
//cache-control: no-cache
//User-Agent: MCU
//Accept: */*
//Host: www.boileriot.com
//accept-encoding: gzip, deflate
//Connection: close
//Range: bytes=0-10

char *CreateDownBinPack(char *url,char* host,char* fileName,float verSion,u16 startRange,u16 endRange)
{
	char * http;
	char * file;
	char * range;
	
	range =mymalloc(SRAMIN,20);
	if(!range)
	{
		printf("CreateDownBinPack malloc range err\r\n");
		return 0;
	}
	file = mymalloc(SRAMIN,30);
	if(!file)
	{
		printf("CreateDownBinPack malloc file err\r\n");
		myfree(SRAMIN,range);
		return 0;
	}
	http = mymalloc(SRAMIN,1024);
	if(!http)
	{
		printf("CreateDownBinPack malloc range err\r\n");
		myfree(SRAMIN,range);
		myfree(SRAMIN,http);
		return 0;
	}
	
	
	sprintf(file,"boiler=%s&version=%.1f",fileName,verSion);
	sprintf(range,"Range: bytes=%u-%u",startRange,endRange);
	
	strcpy(http,"GET http://");
	strcat(http,url);
	strcat(http,"?");
	strcat(http,file);
	strcat(http," HTTP/1.1\r\n\
	Content-Type: application/json\r\n\
	cache-control: no-cache\r\n\
	User-Agent: MCU\r\n\
	Accept: */*\r\n\
	Host: ");
	strcat(http,host);
	strcat(http,"\r\n\
	accept-encoding: gzip, deflate\r\n\
	Connection: close\r\n");
	strcat(http,range);
	strcat(http,"\r\n\r\n");
	myfree(SRAMIN,file);
	myfree(SRAMIN,range);
	return http;
}

char *CreateHttpPack(char *method,char *url,char* ip, u16 port,char* body)
{
	char * http;
	
	http = mymalloc(SRAMIN,1024);
	if(!http)
	{
		return 0;
	}
	

	u16 hostSize = strlen(ip)+strlen(url)+20;
	char *host = mymalloc(SRAMIN,hostSize);
	if(!url)
	{
		return 0;
	}
	sprintf(host,"%s:%d",ip,port);
	
	strcpy(http,method);
	strcat(http," http://");//
	strcat(http,host);
	strcat(http,"/");
	strcat(http,url);
	strcat(http," HTTP/1.1\r\n\
Content-Type: application/json\r\n\
cache-control: no-cache\r\n\
User-Agent: MCU\r\n\
Accept: */*\r\n\
Host: ");
	strcat(http,host);
	strcat(http,"\r\n\
accept-encoding: gzip, deflate\r\n\
Content-Length: ");
	char  bodyLenStr[20];
	sprintf(bodyLenStr,"%d",strlen(body));
	strcat(http,bodyLenStr);
	//strcat(http,"\r\nConnection: close\r\n\r\n");
	strcat(http,"\r\nConnection: Keep-Alive\r\n\r\n");
	strcat(http,body);
	myfree(SRAMIN,host);
	return http;
}





//升级成功回调
GprsHttpConnectObj *CreateUpdataSucCallBackHttpConnect(void)
{
	char *ask = mymalloc(SRAMIN,100);
	if(!ask)
	{
		return 0;
	}
	char *chip = mymalloc(SRAMIN,40);
	if(!chip)
	{
		myfree(SRAMIN,ask);
		return 0;
	}	
	GprsHttpConnectObj *http =  CreateGprsHttpConnectObj(HttpMethod_POST,"49.235.100.116:8888/code/upgradeSoftSuccess",20);
	if(!http)
	{
		Log("http err");
		myfree(SRAMIN,ask);
		myfree(SRAMIN,chip);
		return 0;
	}
	AddHttpHead(http,"Content-Type","application/json");

	GetChipId(chip,40);
	sprintf(ask,"{\"productCode\":\"%s\",\"softCode\":\"%s\",\"deviceCode\":\"%s\"}",PRODUC_CODE,SOFT_CODE,chip);
	AddHttpBody(http,ask);
	myfree(SRAMIN,ask);
	myfree(SRAMIN,chip);
	return http;
}

//升级请求
GprsHttpConnectObj *CreateAskUpdataHttpConnect(void)
{
	char *ask = mymalloc(SRAMIN,100);
	if(!ask)
	{
		return 0;
	}
	char *chip = mymalloc(SRAMIN,40);
	if(!chip)
	{
		myfree(SRAMIN,ask);
		return 0;
	}	
	GprsHttpConnectObj *http =  CreateGprsHttpConnectObj(HttpMethod_POST,"49.235.100.116:8888/code/getUpgradeSoft",20);
	if(!http)
	{
		Log("http err");
		myfree(SRAMIN,ask);
		myfree(SRAMIN,chip);
		return 0;
	}
	AddHttpHead(http,"Content-Type","application/json");

	GetChipId(chip,40);
	sprintf(ask,"{\"productCode\":\"%s\",\"softCode\":\"%s\",\"deviceCode\":\"%s\"}",PRODUC_CODE,SOFT_CODE,chip);
	AddHttpBody(http,ask);
	myfree(SRAMIN,ask);
	myfree(SRAMIN,chip);
	return http;
}



GprsHttpConnectObj *CreateDownFileHttpConnect(char *url,u32 startRange,u32 endRange,u32 waiteTime)
{
	char *chip = mymalloc(SRAMIN,40);
	if(!chip)
	{
		return 0;
	}	
	GprsHttpConnectObj *http =  CreateGprsHttpConnectObj(HttpMethod_GET,url,waiteTime);
	if(!http)
	{
		Log("http err");
		myfree(SRAMIN,chip);
		return 0;
	}
	//AddHttpHead(http,"Connection","Keep-Alive");
	char range[30];
	snprintf(range,30,"bytes=%u-%u",startRange,endRange);
	AddHttpHead(http,"Range",range);
	GetChipId(chip,40);
	myfree(SRAMIN,chip);
	return http;
}
char *CreateGetDownBinPack(char* ip, u16 port,char *url,u32 startRange,u32 endRange)
{
	char * http;
	char * range;
	
	range =mymalloc(SRAMIN,40);
	if(!range)
	{
		Log("CreateDownBinPack malloc range err\r\n");
		return 0;
	}
	http = mymalloc(SRAMIN,1024);
	if(!http)
	{
		myfree(SRAMIN,range);
		return 0;
	}
	u16 hostSize = strlen(ip)+strlen(url)+20;
	char *host = mymalloc(SRAMIN,hostSize);
	if(!url)
	{
		myfree(SRAMIN,range);
		myfree(SRAMIN,http);
		return 0;
	}
	sprintf(host,"%s:%d",ip,port);
	sprintf(range,"Range: bytes=%u-%u",startRange,endRange);
	strcpy(http,"GET ");
	//strcat(http," http://");//
	strcat(http,url);
	strcat(http," HTTP/1.1\r\n\
cache-control: no-cache\r\n\
User-Agent: MCU\r\n\
Accept: */*\r\n\
Host: ");
	strcat(http,host);
	strcat(http,"\r\n");
	strcat(http,"accept-encoding: gzip, deflate\r\n");
	strcat(http,"Connection: Keep-Alive\r\n");
	strcat(http,range);
	strcat(http,"\r\n");
	strcat(http,"\r\n");
	myfree(SRAMIN,range);
	myfree(SRAMIN,host);
	return http;
}
