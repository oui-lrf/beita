#include "ServerCmdPack.h"


char * CreateRegistCmd(void)
{
	cJSON *json;
	cJSON *Data;
	char *res;
	json = cJSON_CreateObject();
	Data = cJSON_CreateObject();
	char clientId[40]={0};
	char deviceTopic[60]={0} ;
	GetChipId(clientId,40);
	sprintf(deviceTopic,"boiler/device/%s",clientId);
	cJSON_AddStringToObject(json, "Action", "RegistDevice");
	cJSON_AddStringToObject(Data, "EqpTopic",deviceTopic);
	cJSON_AddNumberToObject(Data, "DeviceType",DEVICE_TYPE_NUM);
	cJSON_AddNumberToObject(Data, "DEVICE_VERSION",DEVICE_VERSION);
	cJSON_AddItemToObject(json,"Data",Data);
	
	res = cJSON_PrintUnformatted(json);
	cJSON_Delete(json);
	return res;
}

char * CreateSubSeverDataCmd(Node *node)
{
	cJSON *json;
	cJSON *Data;
	char *res;
	json = cJSON_CreateObject();
	Data = cJSON_CreateObject();
	
	cJSON_AddStringToObject(json, "Action", "Submit");
	cJSON_AddNumberToObject(Data, "EqpId",GetEqpId());
	
	cJSON *reNodearray = cJSON_CreateArray();
	cJSON *reNode = cJSON_CreateObject();
	
	cJSON_AddNumberToObject(reNode, "Num",node->NodeNum);
	cJSON_AddNumberToObject(reNode, "Value",*(u16 *)node->NodePoint);
	cJSON_AddNumberToObject(reNode, "Time",(u32)GetIntTime());
	
	cJSON_AddItemToArray(reNodearray,reNode);
	
	cJSON_AddItemToObject(Data,"NodeArray",reNodearray);
	
	cJSON_AddItemToObject(json,"Data",Data);
	res = cJSON_PrintUnformatted(json);
	cJSON_Delete(json);
	return res;
}

//char *CreateServerWaringCmd(WaringObj *waring)
//{
////	int len=0;
//	cJSON *json;
//	cJSON *Data;
//	char *res;
//	json = cJSON_CreateObject();
//	Data = cJSON_CreateObject();
//	
//	cJSON_AddStringToObject(json, "Action", "SubWarning");
//	cJSON_AddNumberToObject(Data, "EqpId",GetEqpId());
//	
//	//cJSON *reNodearray = cJSON_CreateArray();
//	cJSON *reNode = cJSON_CreateObject();
//	cJSON_AddNumberToObject(reNode, "Num",waring->type);
//	cJSON_AddNumberToObject(reNode, "Value",waring->waringValue);
//	cJSON_AddNumberToObject(reNode, "Time",(int)waring->waringTime);
//	//cJSON_AddItemToArray(reNodearray,reNode);
//	cJSON_AddItemToObject(Data,"Msg",reNode);
//	cJSON_AddItemToObject(json,"Data",Data);
//	res = cJSON_PrintUnformatted(json);
//	cJSON_Delete(json);
//	return res;
//}


char * CreateAskDeviceDataCmd(int eqpId)
{
	cJSON *json;
	cJSON *Data;
	char *res;
	json = cJSON_CreateObject();
	Data = cJSON_CreateObject();
	
	cJSON_AddStringToObject(json, "Action", "AskDeviceData");
	cJSON_AddNumberToObject(Data, "EqpId",eqpId);
	
	u8 type = GetNetType();
	cJSON_AddNumberToObject(Data, "NetType",type);
	switch(type)
	{
		case NET_TYPE_WIFI:
			
		break;
		case NET_TYPE_GPRS:
//			cJSON_AddStringToObject(Data, "CCID",GetCCID());
		break;
	}
	
	cJSON_AddItemToObject(json,"Data",Data);
	
	res = cJSON_PrintUnformatted(json);
	cJSON_Delete(json);
	return res;
}


