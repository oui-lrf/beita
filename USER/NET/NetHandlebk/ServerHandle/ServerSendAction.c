#include "ServerSendAction.h"
#include "ServerCmdPack.h"

u8 RegistDevice(void)
{
		char *Buf;
		Buf = CreateRegistCmd();
		MqttPublish(SERVER_TOPIC,(u8 *)Buf,strlen(Buf));
		myfree(SRAMIN,Buf);
		return 1;
}

u8 AskDeviceData(void)
{
		char *Buf;
		Buf = CreateAskDeviceDataCmd(GetEqpId());
		MqttPublish(SERVER_TOPIC,(u8 *)Buf,strlen(Buf));
		myfree(SRAMIN,Buf);
	return 1;
}

u8 SubNodeServer(Node *node)
{
		char *Buf;
		Buf = CreateSubSeverDataCmd(node);
		MqttPublish(SERVER_TOPIC,(u8 *)Buf,strlen(Buf));
		myfree(SRAMIN,Buf);
	return 1;
}

//u8 SubWaringServer(WaringObj *waring)
//{
//	char *Buf;
//	Buf = CreateServerWaringCmd(waring);
//	MqttPublish(SERVER_TOPIC,(u8 *)Buf,strlen(Buf));
//	myfree(SRAMIN,Buf);
//	return 1;
//}

//void SubmitWaringServer(WARING_TYPE type,u16 waringValue)
//{
//	printf("--->SubmitWaringDataServer\r\n");
//	WaringObj *waring;
//	waring = mymalloc(SRAMIN,sizeof(WaringObj));
//	if(!waring)
//	{
//		printf("SubmitWaringServer malloc waring err\r\n");
//		return;
//	}
//	waring->type = type;
//	waring->waringValue=waringValue;
//	waring->waringTime = GetIntTime();
//	SubWaringServer(waring);
//	myfree(SRAMIN,waring);
//}

