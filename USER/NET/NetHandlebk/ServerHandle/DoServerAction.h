#pragma once
#include "NetHandleInclude.h"
void ReRegistDevice(cJSON *json);
void ReAskDeviceData(cJSON *json);
void AddMang(cJSON *json);
void AddStaff(cJSON *json);
void DeleteStaff(void);
void DeleteMang(void);
void SetStaffPermiss(cJSON *json);
void AnalyzeServerAction(cJSON *json);
void AnalyzeHttpReturn(char *jsonStr);
