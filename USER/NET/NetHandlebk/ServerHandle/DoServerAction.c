#include "DoServerAction.h"
#include "iap.h"
#include "NetConfig.h"

void ReRegistDevice(cJSON *json)
{
	char *Sta = cJSON_GetObjectItem(json, "Sta")->valuestring;
	if(strcmp(Sta,"EqpAlRegist") == 0||strcmp(Sta,"OK")==0) //已经完成生产的设备，重新烧程序会出现这种情况
	{
		cJSON *data =  cJSON_GetObjectItem(json,"Data");
		if(!data)
		{
			printf("data is null\r\n");
			return;
		}
		int eqpId = cJSON_GetObjectItem(data, "EqpId")->valueint;
		int StaffPermiss = cJSON_GetObjectItem(data, "StaffPermiss")->valueint;
		
		char *StaffTopic = cJSON_GetObjectItem(data, "StaffTopic")->valuestring;;
		char *MangTopic  = cJSON_GetObjectItem(data, "MangTopic")->valuestring;
		int Time = cJSON_GetObjectItem(data, "Time")->valueint;
		
		
		printf("eqpId:%d\r\n",eqpId);
		printf("StaffPermiss:%d\r\n",StaffPermiss);
		printf("StaffTopic:%s\r\n",StaffTopic);
		printf("MangTopic:%s\r\n",MangTopic);
		printf("Time:%d\r\n",Time);
		SetIntTime(Time);
		SetEqpId(eqpId);
		u8 mangTopicLen = strlen(MangTopic);
		u8 staffTopicLen = strlen(StaffTopic);
		
		printf("-->mangTopicLen:%d,staffTopicLen:%d\r\n",mangTopicLen,staffTopicLen);
		
		if(strstr(MangTopic,"boiler/"))
		{
			SetMangTopic(MangTopic);
		}
		else
		{
			SetMangTopic("");
		}
		if(strstr(StaffTopic,"boiler/"))
		{
			SetEmpTopic(StaffTopic);
		}else
		{
			SetEmpTopic("");
		}
		SetEmpPermiss(StaffPermiss);
		PostFreeEvent(EVENT_SAVE_SYS_FLASH);	
		PostFreeEvent(EVENT_SAVE_NET_FLASH);	
	}

}

void ReAskDeviceData(cJSON *json)
{
	char *Sta = cJSON_GetObjectItem(json, "Sta")->valuestring;
	
	if(strcmp(Sta,"OK")==0) //已经完成生产的设备，重新烧程序会出现这种情况
	{
		cJSON *data =  cJSON_GetObjectItem(json,"Data");
		if(!data)
		{
			printf("data is null\r\n");
			return;
		}
		int StaffPermiss = cJSON_GetObjectItem(data, "StaffPermiss")->valueint;
		int staffTopicType = cJSON_GetObjectItem(data, "StaffTopic")->type;
		int mangTopicType  = cJSON_GetObjectItem(data, "MangTopic")->type;
		char *StaffTopic = cJSON_GetObjectItem(data, "StaffTopic")->valuestring;;
		char *MangTopic  = cJSON_GetObjectItem(data, "MangTopic")->valuestring;
		int Time = cJSON_GetObjectItem(data, "Time")->valueint;
		
		printf("StaffPermiss:%d\r\n",StaffPermiss);
		printf("StaffTopic:%s\r\n",StaffTopic);
		printf("MangTopic:%s\r\n",MangTopic);
		printf("Time:%d\r\n",Time);
		SetIntTime(Time);
		printf("-->staffTopicType:%d,mangTopicType:%d\r\n",staffTopicType,mangTopicType);
		if(strstr(MangTopic,"boiler"))
		{
			SetMangTopic(MangTopic);
		}
		else
		{
			SetMangTopic("");
		}
		if(strstr(StaffTopic,"boiler"))
		{
			SetEmpTopic(StaffTopic);
		}else
		{
			SetEmpTopic("");
		}
		SetEmpPermiss(StaffPermiss);
		PostFreeEvent(EVENT_SAVE_NET_FLASH);	
		
		//cJSON_Delete(data);
	}
}

void AddMang(cJSON *json)
{
		cJSON *data =  cJSON_GetObjectItem(json,"Data");
		if(!data)
		{
			printf("data is null\r\n");
			return;
		}
		char *MangTopic  = cJSON_GetObjectItem(data, "MangTopic")->valuestring;	
		printf("MangTopic:%s\r\n",MangTopic);
		u8 mangTopicLen = strlen(MangTopic);
		printf("-->mangTopicLen:%d\r\n",mangTopicLen);
		
		if(strstr(MangTopic,"boiler"))
		{
			SetMangTopic(MangTopic);
		}
		else
		{
			SetMangTopic("");
		}
		PostFreeEvent(EVENT_SAVE_NET_FLASH);	
		//cJSON_Delete(data);
}

void AddStaff(cJSON *json)
{
	cJSON *data =  cJSON_GetObjectItem(json,"Data");
	if(!data)
	{
		printf("data is null\r\n");
		return;
	}
	char *StaffTopic  = cJSON_GetObjectItem(data, "StaffTopic")->valuestring;	
	printf("StaffTopic:%s\r\n",StaffTopic);
	u8 staffTopicLen = strlen(StaffTopic);
	printf("-->staffTopicLen:%d\r\n",staffTopicLen);
	if(strstr(StaffTopic,"boiler"))
	{
		SetEmpTopic(StaffTopic);
	}else
	{
		SetEmpTopic("");
	}
	PostFreeEvent(EVENT_SAVE_NET_FLASH);	
	//cJSON_Delete(data);
}

void DeleteMang(void)
{
	SetMangTopic("");
	PostFreeEvent(EVENT_SAVE_NET_FLASH);	
}

void DeleteStaff(void)
{
	SetEmpTopic("");
	PostFreeEvent(EVENT_SAVE_NET_FLASH);	
}

void SetStaffPermiss(cJSON *json)
{
	cJSON *data =  cJSON_GetObjectItem(json,"Data");
	if(!data)
	{
		printf("data is null\r\n");
		return;
	}
	u8 StaffPermiss  = (u8)cJSON_GetObjectItem(data, "StaffPermiss")->valueint;	
	printf("StaffPermiss:%d\r\n",StaffPermiss);
	SetEmpPermiss(StaffPermiss);
	PostFreeEvent(EVENT_SAVE_NET_FLASH);	
}

void AnalyzeServerAction(cJSON *json)
{
	char *Action = cJSON_GetObjectItem(json, "Action")->valuestring;
	Log("Action:%s",Action);
	if(!strcmp(Action,"ReSubmit"))
	{
	
	
	}else if(!strcmp(Action,"ReAskTime"))
	{
		
	}else if(!strcmp(Action,"ReRegistDevice"))
	{
		ReRegistDevice(json);
	}else if(!strcmp(Action,"ReAskDeviceData"))
	{
		ReAskDeviceData(json);
	}else if(!strcmp(Action,"AddMang"))
	{
		AddMang(json);
	}else if(!strcmp(Action,"DeleteMang"))
	{
		DeleteMang();
	}else if(!strcmp(Action,"AddStaff"))
	{
		AddStaff(json);
	}else if(!strcmp(Action,"DeleteStaff"))
	{
		DeleteStaff();
	}else if(!strcmp(Action,"SetStaffPermiss"))
	{
		SetStaffPermiss(json);
	}else if(!strcmp(Action,"ReAskNodeList"))
	{
	
	}else if(!strcmp(Action,"ReActivateNode"))
	{
	
	}else if(!strcmp(Action,"AddNode"))
	{
	
	}else if(!strcmp(Action,"DeleteNode"))
	{
	
	}
}

void AnalyzeHttpReturn(char *jsonStr)
{

	cJSON *cmdJson ={0};
	cmdJson= cJSON_Parse(jsonStr);
	if(cmdJson)
	{
		char *func = cJSON_GetObjectItem(cmdJson, "func")->valuestring;
		if(strstr(func,"getUpgradeSoft"))
		{
			char *code = cJSON_GetObjectItem(cmdJson, "code")->valuestring;
			if(strstr((char *)code,"0"))
			{
				cJSON *fileArry = cJSON_GetObjectItem(cmdJson,"files");
				if(fileArry)
				{
					int size = cJSON_GetArraySize(fileArry);
					for(int i=0;i<size;i++)
					{
						cJSON *file=cJSON_GetArrayItem(fileArry,i);	
						if(file)
						{
							char* crc16Str = cJSON_GetObjectItem(file,"crc16")->valuestring;
							char* name = cJSON_GetObjectItem(file,"name")->valuestring;
							char *url =  cJSON_GetObjectItem(file,"url")->valuestring;
							Log("crc16:%s,name:%s,url:%s",crc16Str,name,url);
							char *ipStart = strstr(url,"://")+3;
							char *sortUrl = strstr(ipStart,"/");
							char *ipEnd=0;
							ipEnd = strstr(ipStart,":");
							if(!ipEnd)
							{
								ipEnd = strstr(ipStart,"/");
							}
							int ipLen = ipEnd - ipStart;
							u16 crc16 = atoi(crc16Str);
							if(ipLen >0 && crc16 >0)
							{
								char *ip = mymalloc(SRAMIN,ipLen+1);
								u16 port = 80;
								strncpy(ip,ipStart,ipLen);
								ip[ipLen]=0;
								char *portStart = strstr(ipStart,":")+1;
								port = atoi(portStart);
								Log("ip:%s,port:%d",ip,port);
								if(port)
								{
									char *softIdPos = strstr(url,"fileId=");
									if(softIdPos)
									{
										char *softIdStart = softIdPos+7;
										
										u16 softId = atoi(softIdStart);
										if(softId)
										{
											RunToIap(ip,port,sortUrl,crc16,softId);
										}
										
									}
								}
								myfree(SRAMIN,ip);
							}
						}
					}
				}				
			}
	
		}
		else if(strstr(func,"upgradeSoftSuccess"))
		{
			char *code = cJSON_GetObjectItem(cmdJson,"code")->valuestring;
			if(strstr((char *)code,"0"))
			{
				SetUpdating(1);
				WritParamToFlash();
			}		
			return ;
		}
		//{"33":"33","44":"559","name":"33","peizhi2":"22","ip":"97/"}
		else if(strstr(func,"getExtraInfoByCode"))
		{
			Log("recv getExtraInfoByCode");
			cJSON *extraInfo = cJSON_GetObjectItem(cmdJson,"extraInfo");
			if(extraInfo)
			{
				
				///////////////////////////////////////////////////////////////////
				{
					char *deviceCode =  cJSON_GetObjectItem(extraInfo,"userDeviceCode")->valuestring;
					Log("deviceCode:%s",deviceCode);
					if(strlen(deviceCode) >0 && strlen(deviceCode) < USER_CODE_LEN)
					{
						if(SetUserCode(deviceCode))
						{
							//更新到界面
							char *code = DeviceRunMalloc(USER_CODE_LEN+1);
							if(code)
							{
								strncpy(code,deviceCode,USER_CODE_LEN);
								code[USER_CODE_LEN] = 0;
								SendUiMsg(SCREEN_SET,SET_USER_CODE,(u32)code);
							}
							PostFreeEvent(EVENT_SAVE_DEVICE_FLASH);
						}				
					}
				}

				///////////////////////////////////////////////////////////////////
				{
					char *deviceIp = cJSON_GetObjectItem(extraInfo,"userDeviceIp")->valuestring;
					Log("deviceIp:%s",deviceIp);
					if(strlen(deviceIp) >0 && strlen(deviceIp) < MAX_IP_LEN)
					{
						if(SetIp(deviceIp))
						{
							//更新到界面
							char *ip = DeviceRunMalloc(MAX_IP_LEN+1);
							if(ip)
							{
								strncpy(ip,deviceIp,MAX_IP_LEN);
								ip[MAX_IP_LEN] = 0;
								SendUiMsg(SCREEN_SET,SET_USER_IP,(u32)ip);
							}
							PostFreeEvent(EVENT_SAVE_NET_FLASH);
						}				
					}
				}
				////////////////////////////////////////////////////////////
				///////////////////////////////////////////////////////////////////
				{
					char *devicePort = cJSON_GetObjectItem(extraInfo,"userDevicePort")->valuestring;
					Log("devicePort:%s",devicePort);
					u16 port = atoi(devicePort);
					if(port)
					{
						if(SetPort(port))
						{
							SendUiMsg(SCREEN_SET,SET_USER_PORT,(u32)port);
							PostFreeEvent(EVENT_SAVE_NET_FLASH);
						}				
					}
				}
				
				
			}
		}

	}
	else
	{
		printf("cmdJson is null:%s\r\n",jsonStr);
	}
	cJSON_Delete(cmdJson);
}

