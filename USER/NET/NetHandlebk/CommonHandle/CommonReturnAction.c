#include "CommonReturnAction.h"
#include "NetHandleInclude.h"

u8 CommonReturn(char *UserTopic,char *Action,CMD_STA cmdSta,cJSON *reNodearray)
{
	char *res;
	cJSON *json = cJSON_CreateObject();
	cJSON_AddStringToObject(json, "Action", Action);
	cJSON_AddNumberToObject(json, "CmdSta", cmdSta);
	if(cmdSta==CMD_HANDLE_OK)
	{	
		cJSON *data;
		data = cJSON_CreateObject();
		cJSON_AddStringToObject(data, "NodeType", "LocalNode");
		cJSON_AddItemToObject(data,"NodeArray",reNodearray);
		cJSON_AddItemToObject(json,"Data",data);
	}
	res = cJSON_PrintUnformatted(json);
	cJSON_Delete(json);
	MqttPublish(UserTopic,(u8 *)res,strlen(res));
	myfree(SRAMIN,res);
	return 0;
}

