#pragma once
#include "sys.h"
#include "cJson.h"

typedef enum{
	CMD_HANDLE_OK=0,
	CMD_HANDLE_ERR=1,
	CMD_HANDLE_NOPERMISS,
	CMD_HANDLE_NOUSER,
}CMD_STA;

u8 CommonReturn(char *UserTopic,char *Action,CMD_STA cmdSta,cJSON *reNodearray);


