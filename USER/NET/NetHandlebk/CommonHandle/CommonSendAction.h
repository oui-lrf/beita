#ifndef __NETDOUSERACTION_H__
#define __NETDOUSERACTION_H__
#include "cJson.h"


void NetControl(cJSON *json);
void WriteParam(cJSON *json);
void NetAsk(cJSON *json);

#endif
