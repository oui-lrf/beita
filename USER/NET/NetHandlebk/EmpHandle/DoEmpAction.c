#include "DoEmpAction.h"
#include "EmpReturnAction.h"
#include "EmpMang.h"
#include "CommonReturnAction.h"


static CMD_STA ErrCheck(cJSON *json)
{
	CMD_STA rt=CMD_HANDLE_OK;
	char topic[MAX_TOPIC_LEN+1];
	GetEmpTopic(topic,MAX_TOPIC_LEN);
	if(strlen(topic) <10)
	{
		Log("ErrCheck CMD_HANDLE_NOUSER");
		return CMD_HANDLE_NOUSER;
	}
	if(GetEmpPermiss() == 0)
	{
		Log("ErrCheck CMD_HANDLE_NOPERMISS");
		EmpErrReturn("ReWriteParam",CMD_HANDLE_NOPERMISS);
		rt = CMD_HANDLE_NOPERMISS;
	}
	return rt;
}

void EmpNetAsk(cJSON *json)
{
	if(ErrCheck(json) != CMD_HANDLE_OK)
	{
		return ;
	}
	cJSON *data =  cJSON_GetObjectItem(json,"Data");
	if(data)
	{
		char* NodeType  = cJSON_GetObjectItem(data, "NodeType")->valuestring;
		if(!strcmp(NodeType,"LocalNode"))
		{
			cJSON *nodeArry = cJSON_GetObjectItem(data,"NodeArray");
			if(nodeArry)
			{
				int size = cJSON_GetArraySize(nodeArry);
				cJSON *reNodearray = cJSON_CreateArray();
				CMD_STA cmdSta = CMD_HANDLE_ERR;
				for(int i=0;i<size;i++)
				{
					cJSON *node=cJSON_GetArrayItem(nodeArry,i);			
					int nodeNum = cJSON_GetObjectItem(node,"Num")->valueint;
					cJSON *reNode;
					u16 value=0;
					u8 res = ReadValueByNum(nodeNum,&value);
					if(res)
					{
							reNode = cJSON_CreateObject();
							cJSON_AddNumberToObject(reNode, "Num",nodeNum);
							cJSON_AddNumberToObject(reNode,"Value",value);
							cJSON_AddItemToArray(reNodearray,reNode);
							cmdSta = CMD_HANDLE_OK;
					}
				}
				char topic[MAX_TOPIC_LEN+1];
				GetEmpTopic(topic,MAX_TOPIC_LEN);
				CommonReturn(topic,"ReAsk",cmdSta,reNodearray);
			}
		}
	}
}

void EmpNetControl(cJSON *json)
{
	if(ErrCheck(json) != CMD_HANDLE_OK)
	{
		return ;
	}
	cJSON *data =  cJSON_GetObjectItem(json,"Data");
	if(data)
	{
		char* NodeType  = cJSON_GetObjectItem(data, "NodeType")->valuestring;
		//printf("-->NodeType:%s\r\n",NodeType);
		if(!strcmp(NodeType,"LocalNode"))
		{
			cJSON *nodeArry = cJSON_GetObjectItem(data,"NodeArray");
			if(nodeArry)
			{
				int size = cJSON_GetArraySize(nodeArry);
				cJSON *reNodearray = cJSON_CreateArray();
				CMD_STA cmdSta=CMD_HANDLE_ERR;
				for(int i=0;i<size;i++)
				{
					cJSON *node=cJSON_GetArrayItem(nodeArry,i);			
					int nodeNum = cJSON_GetObjectItem(node,"Num")->valueint;
					int nodeValue = cJSON_GetObjectItem(node,"Value")->valueint;
					printf("--->NodeNum:%d,Value:%d\r\n",nodeNum,nodeValue);
					u8 res = ControlNodeByNum((NODENUM)nodeNum,nodeValue);
					cJSON *reNode = cJSON_CreateObject();
					cJSON_AddNumberToObject(reNode, "Num",nodeNum);
					cJSON_AddNumberToObject(reNode,"NodeSta",1);
					cJSON_AddNumberToObject(reNode,"Value",res);
					cJSON_AddItemToArray(reNodearray,reNode);
					cmdSta = CMD_HANDLE_OK;
				}
				char topic[MAX_TOPIC_LEN+1];
				GetEmpTopic(topic,MAX_TOPIC_LEN);
				CommonReturn(topic,"ReControl",cmdSta,reNodearray);
			}
			
		}
	}
	
}

void EmpWriteParam(cJSON *json)
{
	
	if(ErrCheck(json) != CMD_HANDLE_OK)
	{
		return ;
	}
	cJSON *data =  cJSON_GetObjectItem(json,"Data");
	int nodeNum=0;
	if(data)
	{
		char* NodeType  = cJSON_GetObjectItem(data, "NodeType")->valuestring;
		//printf("-->NodeType:%s\r\n",NodeType);
		if(!strcmp(NodeType,"LocalNode"))
		{
			cJSON *nodeArry = cJSON_GetObjectItem(data,"NodeArray");
			if(nodeArry)
			{
				int size = cJSON_GetArraySize(nodeArry);
				cJSON *reNodearray = cJSON_CreateArray();
				CMD_STA cmdSta =CMD_HANDLE_ERR;
				for(int i=0;i<size;i++)
				{
					cJSON *node=cJSON_GetArrayItem(nodeArry,i);			
					nodeNum = cJSON_GetObjectItem(node,"Num")->valueint;
					int nodeValue = cJSON_GetObjectItem(node,"Value")->valueint;
					printf("--->NodeNum:%d,Value:%d\r\n",nodeNum,nodeValue);
					u16 resValue;
					
					int res = SetNodeByNum((NODENUM)nodeNum,nodeValue);
					if(res)
					{
						printf("--->WriteParam OK\r\n");
						cJSON *reNode = cJSON_CreateObject();
						cJSON_AddNumberToObject(reNode, "Num",nodeNum);
						cJSON_AddNumberToObject(reNode,"NodeSta",1);
						cJSON_AddNumberToObject(reNode,"Value",nodeValue);
						cJSON_AddItemToArray(reNodearray,reNode);
						cmdSta =CMD_HANDLE_OK;
					}
					else
					{
						cJSON *reNode = cJSON_CreateObject();
						cJSON_AddNumberToObject(reNode, "Num",nodeNum);
						cJSON_AddNumberToObject(reNode, "NodeSta",0);
						cJSON_AddItemToArray(reNodearray,reNode);
						cmdSta =CMD_HANDLE_ERR;
					}
				}
				char topic[MAX_TOPIC_LEN+1];
				GetEmpTopic(topic,MAX_TOPIC_LEN);
				CommonReturn(topic,"ReWriteParam",cmdSta,reNodearray);
			}
		}
	}
	
	//更新到界面
	PostFreeEvent(EVENT_SAVE_DEVICE_FLASH);	
}


void AnalyzeEmpUserAction(cJSON *json)
{
	char *Action = cJSON_GetObjectItem(json, "Action")->valuestring;
	if(!strcmp(Action,"Ask"))
	{
		EmpNetAsk(json);
	
	}else if(!strcmp(Action,"Control"))
	{
		PostFreeEvent(EVENT_OPEN_BEEP);
		EmpNetControl(json);
	}else if(!strcmp(Action,"WriteParam"))
	{
		PostFreeEvent(EVENT_OPEN_BEEP);
		EmpWriteParam(json);
	}else if(!strcmp(Action,"Heart"))
	{
		EmpReturnHeart();
	}
	NetEmpLinkStaUpdata();	
}
