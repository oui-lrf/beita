#include "EmpMang.h"
#include "NetHandleInclude.h"


#define EMP_HEART_WAIT_TIME 10 //10没有请求数据，认为客户端离线


u8 empOnlineSta;


//建立一个定时器，长时间没有收到消息认为客户端离线，设备更新状态不给用户推送消息
//没收到一次启动一个定时器，在定时器结束之前再次受到命令就不会离线


static TimerHandle_t EmpWorkTimer = NULL;
static void StopTimer(void)
{
	if(EmpWorkTimer !=NULL)
	{
		osTimerDelete(EmpWorkTimer);	
		EmpWorkTimer =0;
	}
}

//被定时器调用
static void EmpTimerCall(xTimerHandle pxTimer)
{
	configASSERT(pxTimer);
	empOnlineSta=0;
}



static void StartTimerWait(u16 time)
{
	if(EmpWorkTimer !=0)
	{
		StopTimer();
	}
	EmpWorkTimer = xTimerCreate("EmpTimer",          
							       time*1000,   
							       pdFALSE,          
							       (void *) 0,      
							       EmpTimerCall);
	xTimerStart(EmpWorkTimer,0);
}



void NetEmpLinkStartCall(void)
{

}
//收到客户端的命令即为在线
void NetEmpLinkStaUpdata(void)
{
	empOnlineSta=1;
	StartTimerWait(EMP_HEART_WAIT_TIME);
}

void NetEmpLinkStopCall(void)
{
	empOnlineSta=0;
}




