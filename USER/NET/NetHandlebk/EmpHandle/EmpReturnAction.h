#ifndef __NETEMPRETURN_H__
#define __NETEMPRETURN_H__
#include "NetHandleInclude.h"
#include "CommonReturnAction.h"
u8 EmpErrReturn(char *Action,CMD_STA cmdSta);
u8 EmpReturnHeart(void);
u8 EmpActiveSend(char *Action,char *nodeType,cJSON *reNodearray);
#endif

