#include "EmpSendAction.h"
#include "NethandleNode.h"
#include "ServerSendAction.h"

extern u8 empOnlineSta;

u8 EmpActiveSend(char *Action,char *nodeType,cJSON *reNodearray)
{
	char *res;
	if(!empOnlineSta)return 1;
	cJSON *json = cJSON_CreateObject();
	cJSON *data = cJSON_CreateObject();
	cJSON_AddStringToObject(json, "Action", Action);
	cJSON_AddNumberToObject(json, "EqpId",GetEqpId());
	cJSON_AddStringToObject(data, "NodeType",nodeType);
	cJSON_AddItemToObject(data,"NodeArray",reNodearray);
	cJSON_AddItemToObject(json,"Data",data);

	res = cJSON_PrintUnformatted(json);
	cJSON_Delete(json);
	char topic[MAX_TOPIC_LEN+1];
	GetEmpTopic(topic,MAX_TOPIC_LEN);
	MqttPublish(topic,(u8 *)res,strlen(res));
	myfree(SRAMIN,res);
	return 0;
}

void EmpNetSubmitNode(u16 NodeNum,u16 value)
{
	if(!empOnlineSta)return;
	cJSON *reNodearray = cJSON_CreateArray();
	cJSON *reNode = cJSON_CreateObject();
	cJSON_AddNumberToObject(reNode, "Num",NodeNum);
	cJSON_AddNumberToObject(reNode, "Value",value);
	cJSON_AddItemToArray(reNodearray,reNode);
	EmpActiveSend("Submit","LocalNode",reNodearray);
}




