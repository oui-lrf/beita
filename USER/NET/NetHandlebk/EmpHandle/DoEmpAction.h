#ifndef __NETDOEMPACTION_H__
#define __NETDOEMPACTION_H__
#include "NethandleNode.h"
#include "NetHandleInclude.h"
void EmpNetControl(cJSON *json);
void EmpWriteParam(cJSON *json);
void EmpNetAsk(cJSON *json);
void AnalyzeEmpUserAction(cJSON *json);
#endif
