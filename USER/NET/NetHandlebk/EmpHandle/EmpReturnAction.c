#include "EmpReturnAction.h"
#include "CommonReturnAction.h"

u8 EmpErrReturn(char *Action,CMD_STA type)
{
	char *res;
		cJSON *json = cJSON_CreateObject();
		Log("EmpErrReturn");
		cJSON_AddStringToObject(json, "Action", Action);
		cJSON_AddNumberToObject(json, "CmdSta", type);
		res = cJSON_PrintUnformatted(json);
		cJSON_Delete(json);
		char topic[MAX_TOPIC_LEN+1];
		GetEmpTopic(topic,MAX_TOPIC_LEN);
		MqttPublish(topic,(u8 *)res,strlen(res));
		myfree(SRAMIN,res);
		return 0;
}

u8 EmpReturnHeart()
{
	char *res;
		cJSON *json = cJSON_CreateObject();
		Log("EmpReturnHeart");
		cJSON_AddStringToObject(json, "Action", "ReHeart");
		res = cJSON_PrintUnformatted(json);
		cJSON_Delete(json);
		char topic[MAX_TOPIC_LEN+1];
		GetEmpTopic(topic,MAX_TOPIC_LEN);
		MqttPublish(topic,(u8 *)res,strlen(res));
		myfree(SRAMIN,res);
		return 0;
}


