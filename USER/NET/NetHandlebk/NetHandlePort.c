#include "NetHandlePort.h"
#include "NethandleInclude.h"
#include "DoServerAction.h"
#include "DoMangAction.h"
#include "DoEmpAction.h"


//网络线程启动时调用
void NetHandleNetWorkInitCall(void)
{
	//NetNodeBind();
}

void NetHandleLinkSatrtCall(void)
{
	NetServerMangLinkStartCall();
  //NetEmpLinkStartCall();
}

void NetHandleLinkStopCall(void)
{
	NetServerMangLinkStopCall();
	//NetEmpLinkStopCall();
}


void NetHandleRecvCall(char *jsonStr)
{
	cJSON *cmdJson ={0};
	char *Idty;
	printf("NetHandleRecvCall:%s\r\n",jsonStr);
	cmdJson= cJSON_Parse(jsonStr);
	if(cmdJson)
	{
		Idty = cJSON_GetObjectItem(cmdJson, "Idty")->valuestring;
		if(!strcmp(Idty,"Server"))
		{
			AnalyzeServerAction(cmdJson);
		}
	}
	else
	{
		Log("cmdJson is null");
	}
	cJSON_Delete(cmdJson);
}


