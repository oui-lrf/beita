#include "DoMangAction.h"

extern Node *headNode;


static CMD_STA ErrCheck(cJSON *json)
{
	CMD_STA rt=CMD_HANDLE_OK;
	char topic[MAX_TOPIC_LEN+1];
	GetMangTopic(topic,MAX_TOPIC_LEN);
	if(strlen(topic) <10)
	{
		printf("--->ErrCheck CMD_HANDLE_NOUSER\r\n");
		return CMD_HANDLE_NOUSER;
	}
	return rt;
}

void MangNetAsk(cJSON *json)
{
//	if(ErrCheck(json) != CMD_HANDLE_OK)
//	{
//		return ;
//	}
//	cJSON *data =  cJSON_GetObjectItem(json,"Data");
//	if(data)
//	{
//		char* NodeType  = cJSON_GetObjectItem(data, "NodeType")->valuestring;
//		//printf("-->NodeType:%s\r\n",NodeType);
//		if(!strcmp(NodeType,"LocalNode"))
//		{
//			cJSON *nodeArry = cJSON_GetObjectItem(data,"NodeArray");
//			if(nodeArry)
//			{
//				int size = cJSON_GetArraySize(nodeArry);
//				cJSON *reNodearray = cJSON_CreateArray();
//				CMD_STA cmdSta = CMD_HANDLE_ERR;
//				for(int i=0;i<size;i++)
//				{
//					cJSON *node=cJSON_GetArrayItem(nodeArry,i);			
//					int nodeNum = cJSON_GetObjectItem(node,"Num")->valueint;
//					cJSON *reNode = cJSON_CreateObject();
//					cJSON_AddNumberToObject(reNode, "Num",nodeNum);
//					cJSON_AddNumberToObject(reNode, "Value",*(u16 *)findNode->NodePoint);
//					cJSON_AddItemToArray(reNodearray,reNode);
//					cmdSta = CMD_HANDLE_OK;
//				}
//				char topic[MAX_TOPIC_LEN+1];
//				GetMangTopic(topic,MAX_TOPIC_LEN);
//				CommonReturn(topic,"ReAsk",cmdSta,reNodearray);
//			}
//		}
//	}
}

void MangNetControl(cJSON *json)
{
//	if(ErrCheck(json) != CMD_HANDLE_OK)
//	{
//		return ;
//	}
//	cJSON *data =  cJSON_GetObjectItem(json,"Data");
//	if(data)
//	{
//		char* NodeType  = cJSON_GetObjectItem(data, "NodeType")->valuestring;
//		//printf("-->NodeType:%s\r\n",NodeType);
//		if(!strcmp(NodeType,"LocalNode"))
//		{
//			cJSON *nodeArry = cJSON_GetObjectItem(data,"NodeArray");
//			if(nodeArry)
//			{
//				int size = cJSON_GetArraySize(nodeArry);
//				cJSON *reNodearray = cJSON_CreateArray();
//				CMD_STA cmdSta=CMD_HANDLE_ERR;
//				for(int i=0;i<size;i++)
//				{
//					cJSON *node=cJSON_GetArrayItem(nodeArry,i);			
//					int nodeNum = cJSON_GetObjectItem(node,"Num")->valueint;
//					int nodeValue = cJSON_GetObjectItem(node,"Value")->valueint;
//					printf("--->NodeNum:%d,Value:%d\r\n",nodeNum,nodeValue);
//					if(ControlNodeByNum((NODENUM)nodeNum,nodeValue)==0)
//					{
//						printf("--->ControlNode OK\r\n");
//						cJSON *reNode = cJSON_CreateObject();
//						cJSON_AddNumberToObject(reNode, "Num",nodeNum);
//						cJSON_AddNumberToObject(reNode,"NodeSta",0);
//						cJSON_AddNumberToObject(reNode,"Value",nodeValue);
//						cJSON_AddItemToArray(reNodearray,reNode);
//						cmdSta = CMD_HANDLE_OK;
//					}
//					else
//					{
//						printf("--->ControlNode failed\r\n");
//						Node *node = FindNodeByNum(headNode,(NODENUM)nodeNum);
//						if(node)
//						{
//							cJSON *reNode = cJSON_CreateObject();
//							cJSON_AddNumberToObject(reNode, "Num",nodeNum);
//							cJSON_AddNumberToObject(reNode, "NodeSta",1);
//							cJSON_AddNumberToObject(reNode, "Value",*(u16 *)node->NodePoint);
//							cJSON_AddItemToArray(reNodearray,reNode);
//							cmdSta = CMD_HANDLE_OK;
//						}else
//						{
//							//printf("-->find node failed\r\n");
//						}
//					}
//				}
//				char topic[MAX_TOPIC_LEN+1];
//				GetMangTopic(topic,MAX_TOPIC_LEN);
//				CommonReturn(topic,"ReControl",cmdSta,reNodearray);
//			}
//			
//		}
//	
//	}
	
}

void MangWriteParam(cJSON *json)
{
//	if(ErrCheck(json) != CMD_HANDLE_OK)
//	{
//		return ;
//	}
//	cJSON *data =  cJSON_GetObjectItem(json,"Data");
//	int nodeNum=0;
//	if(data)
//	{
//		char* NodeType  = cJSON_GetObjectItem(data, "NodeType")->valuestring;
//		//printf("-->NodeType:%s\r\n",NodeType);
//		if(!strcmp(NodeType,"LocalNode"))
//		{
//			cJSON *nodeArry = cJSON_GetObjectItem(data,"NodeArray");
//			if(nodeArry)
//			{
//				int size = cJSON_GetArraySize(nodeArry);
//				cJSON *reNodearray = cJSON_CreateArray();
//				CMD_STA cmdSta =CMD_HANDLE_ERR;
//				for(int i=0;i<size;i++)
//				{
//					cJSON *node=cJSON_GetArrayItem(nodeArry,i);			
//					nodeNum = cJSON_GetObjectItem(node,"Num")->valueint;
//					int nodeValue = cJSON_GetObjectItem(node,"Value")->valueint;
//					printf("--->NodeNum:%d,Value:%d\r\n",nodeNum,nodeValue);
//					
//					int res =SetNodeByNum((NODENUM)nodeNum,nodeValue);
//					if(res==0)
//					{
//						printf("--->WriteParam OK\r\n");
//						cJSON *reNode = cJSON_CreateObject();
//						cJSON_AddNumberToObject(reNode, "Num",nodeNum);
//						cJSON_AddNumberToObject(reNode,"NodeSta",1);
//						cJSON_AddNumberToObject(reNode,"Value",nodeValue);
//						cJSON_AddItemToArray(reNodearray,reNode);
//						cmdSta =CMD_HANDLE_OK;
//					}
//					else
//					{
//						printf("--->WriteParam failed:%d\r\n",res);
//						Node *node = FindNodeByNum(headNode,(NODENUM)nodeNum);
//						if(node)
//						{
//							cJSON *reNode = cJSON_CreateObject();
//							cJSON_AddNumberToObject(reNode, "Num",nodeNum);
//							cJSON_AddNumberToObject(reNode, "NodeSta",0);
//							cJSON_AddNumberToObject(reNode, "Value",*(u16 *)node->NodePoint);
//							cJSON_AddItemToArray(reNodearray,reNode);
//							cmdSta =CMD_HANDLE_OK;
//						}else
//						{
//							//printf("-->find node failed\r\n");
//						}
//					}
//				}
//				char topic[MAX_TOPIC_LEN+1];
//				GetMangTopic(topic,MAX_TOPIC_LEN);
//				CommonReturn(topic,"ReWriteParam",cmdSta,reNodearray);
//			}
//		}
//	}
//	//更新并保存到flash
//	PostFreeEvent(EVENT_SAVE_NET_FLASH);	
}


void AnalyzeMangUserAction(cJSON *json)
{
	char *Action = cJSON_GetObjectItem(json, "Action")->valuestring;
	Log("Mang Action:%s",Action);
	if(!strcmp(Action,"Ask"))
	{
		MangNetAsk(json);
	
	}else if(!strcmp(Action,"Control"))
	{
		MangNetControl(json);
	}else if(!strcmp(Action,"WriteParam"))
	{
		MangWriteParam(json);
	}	
}

