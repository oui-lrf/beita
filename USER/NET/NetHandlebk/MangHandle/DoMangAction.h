#ifndef __NETDOMANGACTION_H__
#define __NETDOMANGACTION_H__
#include "cJson.h"
#include "NethandleNode.h"
#include "NetHandleInclude.h"

void MangNetControl(cJSON *json);
void MangWriteParam(cJSON *json);
void MangNetAsk(cJSON *json);
void AnalyzeMangUserAction(cJSON *json);

#endif

