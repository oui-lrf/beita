/**
  ******************************************************************************
  * @file    transport.c
  * $Author: 飞鸿踏雪 $
  * $Revision: 17 $
  * $Date:: 2014-10-25 11:16:48 +0800 #$
  * @brief   主函数.
  ******************************************************************************
  * @attention
  *
  *<h3><center>&copy; Copyright 2009-2012, EmbedNet</center>
  *<center><a href="http:\\www.embed-net.com">http://www.embed-net.com</a></center>
  *<center>All Rights Reserved</center></h3>
  * 
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "MQTTPacket.h"
#include "transport.h"
#include "MqttWork.h"


int transport_sendPacketBuffer(unsigned char* buf, int buflen)
{
  TcpAppSendData("114.215.69.92",1883,(u8 *)buf,buflen,0xffff,0,0);
	return buflen;
}
/**
  * @brief  阻塞方式接收TCP服务器发送的数据
  * @param  buf 数据存储首地址
  * @param  count 数据缓冲区长度
  * @retval 小于0表示接收数据失败
  */
__weak int transport_getdata(unsigned char* buf, int count)
{

}

/**
  * @brief  打开一个socket并连接到服务器
  * @param  无
  * @retval 小于0表示打开失败
  */
int transport_open(char *ip,u16 port)
{
	
}

/**
  * @brief  关闭socket
  * @param  无
  * @retval 小于0表示关闭失败
  */
u8 transport_close(void)
{
	
	return 1;
}
