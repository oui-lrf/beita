#pragma once
#include "MqttInclude.h"
typedef struct{
	u16 port;
	char ip[40];
	char clientId[40+1];
	char userName[40+1];
	char userPass[40+1];
	char subTopic[40+1];
}MQTT_LINK_MSG;

typedef struct {
/* protected: */
    QActive super;
		QTimeEvt timeEvt;
		MQTT_LINK_MSG msg;
}MqttWork;
void MqttCtPoll(void);
void MqttWorkPoll(void);

void SubSucessCall(void);
void SubFailedCall(void);
void LinkFailedCall(void);
void LinkSucessCall(void);
extern QActive * const MQ_MqttWork;
void MqttWork_ctor(void);



