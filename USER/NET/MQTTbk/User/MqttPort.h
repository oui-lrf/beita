#ifndef __MQTTPORT_H__
#define __MQTTPORT_H__
#include "MqttInclude.h"
typedef struct{
	char *topic;
	u8* data;
	u16 dataSize;
}MqttPubMsg;
 

#define SERVER_TOPIC "boiler/server/recv"

void MqttPublish(const char *topic,const u8 *data,const u16 dataSize);
void CreateMqtt(char *ip,u16 port,char *clientId,char *userName,char *userPass,char *subTopic);
void CloseMqtt(void);
#endif
