#include "MqttLogic.h"
#include "MqttPort.h"
#include "MqttWork.h"

extern QActive * const TP_TcpWork;

static QState MqttWork_initial(MqttWork * const me, QEvt const * const e);
static QState MQTT_ON(MqttWork * const me, QEvt const * const e);
static QState MQTT_CLOSE(MqttWork * const me, QEvt const * const e);
static QState MQTT_LINK(MqttWork * const me, QEvt const * const e);
static QState MQTT_SUB(MqttWork * const me, QEvt const * const e);
static QState MQTT_SUBED(MqttWork * const me, QEvt const * const e);
static QState MQTT_PAUSE(MqttWork * const me, QEvt const * const e);

static MqttWork mqttWork;
QActive * const MQ_MqttWork = &mqttWork.super;
Q_DEFINE_THIS_MODULE("MqttWork")

void MqttWork_ctor(void) {
    MqttWork *me = (MqttWork *)MQ_MqttWork;
    QActive_ctor(&me->super, Q_STATE_CAST(&MqttWork_initial));
		QTimeEvt_ctorX(&me->timeEvt, &me->super, TIMEOUT_SIG, 0U);
}

static QState MqttWork_initial(MqttWork * const me, QEvt const * const e) {
    /*${Mqtt::MqttWork::SM::initial} */
		QActive_subscribe(&me->super, TCP_CONNECT_SUCCES_SIG);
		QActive_subscribe(&me->super, TCP_CONNECT_FAILED_SIG);
		QActive_subscribe(&me->super, TCP_RECV_SIG);
		QActive_subscribe(&me->super, MQTT_PUBLISH_SIG);
		QActive_subscribe(&me->super, TCP_PAUSE_SIG);
		QActive_subscribe(&me->super, TCP_RESUME_SIG);
    return Q_TRAN(&MQTT_CLOSE);
}


static QState MQTT_ON(MqttWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        /*${Mqtt::MqttWork::SM::NET_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
						MqttLog("+++MQTT_ON");
						TcpAppConnect(me->msg.ip,me->msg.port);
						status_ = Q_HANDLED();	
            break;
        }
				case MQTT_REON:
						status_ =Q_TRAN(&MQTT_ON);
				break;
				case TCP_PAUSE_SIG:{
						status_ =Q_TRAN(&MQTT_PAUSE);
					break;
				}
				case MQTT_CLOSE_SIG:
						status_ =Q_TRAN(&MQTT_CLOSE);
				break;
				case TCP_CONNECT_SUCCES_SIG:
					MqttLog("MQTT_ON TCP_CONNECT_SUCCES_SIG");
					status_ =Q_TRAN(&MQTT_LINK);
				break;
				case TCP_CONNECT_FAILED_SIG:
					MqttLog("MQTT_ON TCP_CONNECT_FAILED_SIG");
					//给上层发送连接失败
					QF_PUBLISH(&Q_NEW(QpEvt, MQTT_LINK_FAILED_SIG)->super, me);
					status_ =Q_TRAN(&MQTT_CLOSE);
				break;
				case MQTT_PUBLISH_SIG:
						MqttLog("MQTT_ON MQTT_PUBLISH_SIG");
						status_ = Q_HANDLED();
						MqttPubMsg *msg = (MqttPubMsg *)(Q_EVT_CAST(QpEvt)->p);
						if(msg)
						{
							NetFree( msg->data);
							NetFree(msg->topic);
							NetFree(msg);
						}
					break;
					case TCP_RECV_SIG:{
						MqttLog("MQTT_ON TCP_RECV_SIG");
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							NetFree(data);
						}
						status_ = Q_HANDLED();	
          break;
        }
        default: {
            status_ = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status_;
}


//复写接收函数，由mqtt内部调用
cycleQueue mqtt_que;
static int transport_getdata(unsigned char* buf, int count)
{
	int len = 0;
	len = ReadQueBuf(&mqtt_que,buf,count);
	return len;
}

//给服务器发送连接指令
static QState MQTT_LINK(MqttWork * const me, QEvt const * const e)
{
		QState status_;
    switch (e->sig) {
        /*${Mqtt::MqttWork::SM::NET_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
						MqttLog("+++MQTT_LINK");
						MqttConnectServer(me);
						//有时候服务器不会回复，应该加入超时处理
						QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*60,0);
						status_ = Q_HANDLED();	
            break;
        }
				case TCP_SEND_FAILED_SIG:
					Log("Mqtt Link Failed");
					QTimeEvt_disarm(&me->timeEvt);
					//给上层报告失败
					QF_PUBLISH(&Q_NEW(QpEvt, MQTT_LINK_FAILED_SIG)->super, me);
					status_ =Q_TRAN(&MQTT_CLOSE);
				break;
				case TIMEOUT_SIG:
					status_ = Q_HANDLED();	
					QF_PUBLISH(&Q_NEW(QpEvt, MQTT_LINK_FAILED_SIG)->super, me);
					status_ =Q_TRAN(&MQTT_CLOSE);
				break;
				case TCP_RECV_SIG: {
						status_ = Q_HANDLED();
            u8 *data = (u8 *)(Q_EVT_CAST(QpEvt)->p);
						u16 len = Q_EVT_CAST(QpEvt)->v;
						if(data && len)
						{
							QTimeEvt_disarm(&me->timeEvt);
							WifiLog("mqtt tcp size:%d",len);
							int res = 0; 
							res = CreateQueue(&mqtt_que,len);
							if(res) 
							{
								InsertQueBuf(&mqtt_que,data,len);
								int msgType=0;
								msgType =MQTTPacket_read(data,len,transport_getdata);
								if(msgType == CONNACK)
								{
									;unsigned char sessionPresent, connack_rc;
									if (MQTTDeserialize_connack(&sessionPresent, &connack_rc,data,len) != 1 || connack_rc != 0)
									{
										Log("Mqtt Link Failed");
										//重新连接
										status_ = Q_TRAN(&MQTT_LINK);
									}
									else
									{
										Log("Mqtt Link Success");
										status_ = Q_TRAN(&MQTT_SUB);
									}
								}
								else
								{
									Log("Mqtt Link Failed");
									//重新连接
									status_ = Q_TRAN(&MQTT_LINK);
								}
							}
							else
							{
								Log("MqttRecvCall CreateQueue failed");
								//重新连接
								status_ = Q_TRAN(&MQTT_LINK);
							}
							DestroyQueue(&mqtt_que);
							NetFree(data);
						}
          break;
        }
				case Q_EXIT_SIG: {
						QTimeEvt_disarm(&me->timeEvt);
						status_ = Q_HANDLED();	
            break;
        }
        default: {
            status_ = Q_SUPER(&MQTT_ON);
            break;
        }
    }
    return status_;
}


static QState MQTT_SUB(MqttWork * const me, QEvt const * const e)
{
		QState status_;
    switch (e->sig) {
        /*${Mqtt::MqttWork::SM::NET_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
						MqttLog("+++MQTT_SUB");
						MqttSubServer(me->msg.subTopic);
						//有时候服务器不会回复，应该加入超时处理
						QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*60,0);
						status_ = Q_HANDLED();	
            break;
        }
				case TCP_SEND_FAILED_SIG:
					Log("Mqtt Link Failed");
					QTimeEvt_disarm(&me->timeEvt);
					//给上层报告失败
					QF_PUBLISH(&Q_NEW(QpEvt, MQTT_LINK_FAILED_SIG)->super, me);
					status_ =Q_TRAN(&MQTT_CLOSE);
				break;
				case TIMEOUT_SIG:
					QF_PUBLISH(&Q_NEW(QpEvt, MQTT_LINK_FAILED_SIG)->super, me);
					status_ =Q_TRAN(&MQTT_CLOSE);
				break;
				case TCP_RECV_SIG: {
						status_ = Q_HANDLED();
            u8 *data = (u8 *)(Q_EVT_CAST(QpEvt)->p);
						u16 len = Q_EVT_CAST(QpEvt)->v;
						if(data && len)
						{
							QTimeEvt_disarm(&me->timeEvt);
							int res = 0; 
							res = CreateQueue(&mqtt_que,len);
							if(res) 
							{
								InsertQueBuf(&mqtt_que,data,len);
								int msgType=0;
								msgType =MQTTPacket_read(data,len,transport_getdata);
								if(msgType == SUBACK)
								{
									unsigned short submsgid;
									int subcount;
									int granted_qos;
									MQTTDeserialize_suback(&submsgid, 1, &subcount, &granted_qos, data, len);
									if (granted_qos != 0)
									{
										Log("Mqtt sub Failed");
										//重新订阅
										status_ = Q_TRAN(&MQTT_SUB);
									}
									else
									{
										Log("Mqtt sub Success");
										status_ = Q_TRAN(&MQTT_SUBED);
									}
								}
								else
								{
									Log("Mqtt sub Failed");
									//重新订阅
									status_ = Q_TRAN(&MQTT_SUB);
								}
							}
							else
							{
								Log("MqttRecvCall CreateQueue failed");
								//重新订阅
								status_ = Q_TRAN(&MQTT_SUB);
							}
							DestroyQueue(&mqtt_que);
							NetFree(data);
						}
          break;
        }
				case Q_EXIT_SIG: {
						QTimeEvt_disarm(&me->timeEvt);
						status_ = Q_HANDLED();	
            break;
        }
        default: {
            status_ = Q_SUPER(&MQTT_ON);
            break;
        }
    }
    return status_;
}

//订阅成功后不停的发送心跳
//要对关闭消息进行处理，关闭定时器
//3次心跳失败则重连
static QState MQTT_SUBED(MqttWork * const me, QEvt const * const e)
{
	QState status_;
	static u8 heartNum = 0;
	switch (e->sig) {
			/*${Mqtt::MqttWork::SM::NET_MODEL_OPEN} */
			case Q_ENTRY_SIG: {
					MqttLog("+++MQTT_SUBED");
					QF_PUBLISH(&Q_NEW(QpEvt, MQTT_LINK_SUCCES_SIG)->super, me);
					heartNum =0;
					//有时候服务器不会回复，应该加入超时处理
					QTimeEvt_disarm(&me->timeEvt); 
					QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*5,0);
					status_ = Q_HANDLED();	
					break;
			}
			case TIMEOUT_SIG:
				status_ = Q_HANDLED();	

				if(heartNum <5)
				{
					MqttSendHeart();
					QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*5,0);
				}
				else
				{	Log("heartNum:%d",heartNum);
					//向上层提交网络失败
					QF_PUBLISH(&Q_NEW(QpEvt, MQTT_LINK_FAILED_SIG)->super, me);
					status_ =Q_TRAN(&MQTT_CLOSE);
				}
				heartNum++;
			break;
			case MQTT_PUBLISH_SIG:
				//MqttLog("MQTT_SUBED MQTT_PUBLISH_SIG");
				status_ = Q_HANDLED();
				MqttPubMsg *msg = (MqttPubMsg *)(Q_EVT_CAST(QpEvt)->p);
				if(msg)
				{
					u8 *pubData = msg->data;
					char *topic = msg->topic;
					mqtt_publish(topic,pubData,msg->dataSize);
					NetFree(pubData);
					NetFree(topic);
					NetFree(msg);
				}
			break;
			case TCP_RECV_SIG: {
					status_ = Q_HANDLED();
					u8 *data = (u8 *)(Q_EVT_CAST(QpEvt)->p);
					u16 len = Q_EVT_CAST(QpEvt)->v;				
//					printf("mqtt recv size:%d,data:%s\r\n",len,data);
					if(data && len)
					{
						int res = 0; 
						res = CreateQueue(&mqtt_que,len);
						if(res) 
						{
							InsertQueBuf(&mqtt_que,data,len);
							int msgType=0;
							msgType =MQTTPacket_read(data,len,transport_getdata);
							switch(msgType)
							{
								case PINGREQ:
									//Log("send ping ok");
									heartNum =0;
								break;
								case PINGRESP:
									heartNum =0;
									//Log("ping PINGRESP");
								break;
								case PUBLISH:
										;
										 unsigned char dup;
										 int qos;
										 unsigned char retained;
										 unsigned short msgid;
										 int payloadlen_in; 
											u8 * payload_in;
											u8 * mqttTopic ;
											MQTTString receivedTopic = MQTTString_initializer; 
											payloadlen_in=0;
											receivedTopic.cstring = (char *)mqttTopic;
											MQTTDeserialize_publish(&dup, &qos,&retained,&msgid,&receivedTopic,&payload_in,&payloadlen_in,data,len);
											if(payloadlen_in>0)
											{
												u8 *buf = mymalloc(SRAMIN,payloadlen_in);
												if(!buf) 
												{
													printf("MqttPublish malloc buf err\r\n");
													break ;
												}
												MqttPubMsg *msg = (MqttPubMsg *)mymalloc(SRAMIN,sizeof(MqttPubMsg));
												if(!msg) 
												{
													printf("MqttPublish malloc msg err\r\n");
													myfree(SRAMIN,buf);
													break ;
												}
												
												char *top = mymalloc(SRAMIN,strlen(receivedTopic.cstring)+1);
												if(!top)
												{
													myfree(SRAMIN,buf);
													myfree(SRAMIN,msg);
												}
												strcpy(top,receivedTopic.cstring);
												memcpy(buf,payload_in,payloadlen_in);
												msg->topic= top;
												msg->data=buf;
												msg->dataSize=payloadlen_in;
												QpEvt *te;
												te = Q_NEW(QpEvt, MQTT_RECV_SIG);
												te->p = (u32)msg;
												te->v = sizeof(MqttPubMsg);
												QF_PUBLISH(&te->super, me);
											}
											heartNum =0;
								break;
								case DISCONNECT:
									Log("MQTT DISCONNECT");
									QTimeEvt_disarm(&me->timeEvt);
									QF_PUBLISH(&Q_NEW(QpEvt, MQTT_LINK_FAILED_SIG)->super, me);
									status_ =Q_TRAN(&MQTT_CLOSE);
								break;
							}
						}
						DestroyQueue(&mqtt_que);
						NetFree(data);
					}
				break;
			}
			case Q_EXIT_SIG:{
				MqttLog("***MQTT_SUBED");
				QTimeEvt_disarm(&me->timeEvt);
				status_ = Q_HANDLED();	
			break;
			}
			default: {
					status_ = Q_SUPER(&MQTT_ON);
					break;
			}
    }
    return status_;

}


static QState MQTT_PAUSE(MqttWork * const me, QEvt const * const e)
{
    QState status_;
    switch (e->sig) {
        case Q_ENTRY_SIG: {
						MqttLog("+++MQTT_PAUSE");
						QF_PUBLISH(&Q_NEW(QpEvt, MQTT_PAUSE_SIG)->super, me);
						status_ = Q_HANDLED();	
            break;
        }
				case TCP_RESUME_SIG:{
						MqttLog("MQTT_PAUSE TCP_RESUME_SIG");
						status_ = Q_HANDLED();	
						QF_PUBLISH(&Q_NEW(QpEvt, MQTT_RESUME_SIG)->super, me);
				break;
				}
				case MQTT_ON_SIG:{
					status_ = Q_HANDLED();	
					MqttLog("MQTT_CLOSE MQTT_ON_SIG");
					MQTT_LINK_MSG *msg = (MQTT_LINK_MSG *)(Q_EVT_CAST(QpEvt)->p);
					if(msg)
					{
						memcpy(&(me->msg),msg,sizeof(MQTT_LINK_MSG));
						NetFree(msg);
						QACTIVE_POST(MQ_MqttWork, &Q_NEW(QpEvt, MQTT_REON)->super,0);
					}
				break;
				}
        default: {
            status_ = Q_SUPER(&MQTT_ON);
            break;
        }
    }
    return status_;
}

static QState MQTT_CLOSE(MqttWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        /*${Mqtt::MqttWork::SM::NET_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
						MqttLog("+++MQTT_CLOSE");
						QACTIVE_POST(TP_TcpWork, &Q_NEW(QpEvt, TCP_CLOSE_SIG)->super, me);
						status_ = Q_HANDLED();	
            break;
        }
				case MQTT_ON_SIG:{
					status_ = Q_HANDLED();	
					MqttLog("MQTT_CLOSE MQTT_ON_SIG");
					MQTT_LINK_MSG *msg = (MQTT_LINK_MSG *)(Q_EVT_CAST(QpEvt)->p);
					if(msg)
					{
						status_ =Q_TRAN(&MQTT_ON);
						memcpy(&(me->msg),msg,sizeof(MQTT_LINK_MSG));
						NetFree(msg);
					}
				break;
				}
				case MQTT_PUBLISH_SIG:{
						MqttLog("MQTT_CLOSE MQTT_PUBLISH_SIG");
						status_ = Q_HANDLED();
						MqttPubMsg *pubmsg = (MqttPubMsg *)(Q_EVT_CAST(QpEvt)->p);
						if(pubmsg)
						{
							NetFree( pubmsg->data);
							NetFree(pubmsg->topic);
							NetFree(pubmsg);
						}
					break;
					}
				case TCP_RECV_SIG: {
						MqttLog("MQTT_ON TCP_RECV_SIG");
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							NetFree(data);
						}
						status_ = Q_HANDLED();	
          break;
        }
        default: {
            status_ = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status_;
}


