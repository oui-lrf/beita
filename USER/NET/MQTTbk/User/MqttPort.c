#include "MqttPort.h"
#include "MqttWork.h"


extern QActive * const MQ_MqttWork;

/*
把数据直接发送，成功返回0,可多线程调用
*/
void MqttPublish(const char *topic,const u8 *data,const u16 dataSize)
{
//	int rt = 0xFF;
//	printf("MqttPublish topic:%s,data:%s\r\n",topic,data);
	int topicLen = strlen(topic);
	if(topicLen<=4 || topicLen >100)
	{
		Log("topic is err");
		return ;
	}
	
	u8 *buf = mymalloc(SRAMIN,dataSize);
	if(!buf) 
	{
		printf("MqttPublish malloc buf err\r\n");
		return ;
	}
	MqttPubMsg *msg = (MqttPubMsg *)mymalloc(SRAMIN,sizeof(MqttPubMsg));
	if(!msg) 
	{
		printf("MqttPublish malloc msg err\r\n");
		myfree(SRAMIN,buf);
		return ;
	}

	u16 topLen = strlen(topic);
	char *top = mymalloc(SRAMIN,topLen+1);
	if(!top)
	{
		myfree(SRAMIN,buf);
		myfree(SRAMIN,msg);
	}
	strncpy(top,topic,topLen+1);
	memcpy(buf,data,dataSize);
	msg->topic= top;
	msg->data=buf;
	msg->dataSize=dataSize;
	QpEvt *te;
	te = Q_NEW(QpEvt, MQTT_PUBLISH_SIG);
	te->p = (u32)msg;
	te->v = sizeof(MqttPubMsg);
	QF_PUBLISH(&te->super,0);
}


void CreateMqtt(char *ip,u16 port,char *clientId,char *userName,char *userPass,char *subTopic)
{
	Log("CreateMqtt ip:%s,port:%d",ip,port);
	Log("CreateMqtt clientId:%s,user:%s,pass:%s",clientId,userName,userPass);
	MQTT_LINK_MSG *msg= NetMalloc(sizeof(MQTT_LINK_MSG));
	if(msg)
	{
		msg->port = port;
		strncpy(msg->ip,ip,40);
		strncpy(msg->clientId,clientId,40);
		strncpy(msg->userName,userName,40);
		strncpy(msg->userPass,userPass,40);
		strncpy(msg->subTopic,subTopic,40);
		QpEvt *te;
		te = Q_NEW(QpEvt, MQTT_ON_SIG);
		te->p = (uint32_t)msg;
		te->v = sizeof(MQTT_LINK_MSG);
		QACTIVE_POST(MQ_MqttWork, &te->super, me);
	}
}

void CloseMqtt(void)
{
	QACTIVE_POST(MQ_MqttWork, &Q_NEW(QpEvt, MQTT_CLOSE_SIG)->super, me);
}

