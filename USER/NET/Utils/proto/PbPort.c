#include "PbPort.h"

/**
 数组编码的回调函数
 **/
bool EncodeBufferCallBack(pb_ostream_t *stream, const pb_field_t *field, void * const *arg)
{
    if (!pb_encode_tag_for_field(stream, field))
        return false;
		
		bool res=true;
		PbDataObj *obj = (PbDataObj *)*arg;
		if(obj)
		{
//			Log("EncodeBuffer obj:%s,size:%d",obj->data, obj->size);
		  res  =pb_encode_string( stream, obj->data, obj->size);
//			PbFree(obj->data);
//			PbFree(obj);		
		}
		return res;
}

/**
 数组解码回调函数
 **/
bool DecodeBufferCallBack(pb_istream_t *stream, const pb_field_t *field, void **arg)
{
	int i=0;
	PbDataObj *obj=*arg;
	u8 *data = PbMalloc(obj->size);
	if(!data)
	{
		Log("malloc err");
		return false;
	}
	uint8_t* tmp = (uint8_t *)data;
	while (stream->bytes_left )//&& i < obj->size
	{
			uint8_t value;
			//if (!pb_decode_varint(stream, &value))
			if (!pb_readbyte(stream, &value))
			{
				PbFree(data);
				return false;
			}
			//printf("%x ",value);

			*(tmp+i)=value;
			i++;
	}
	if(i > 0)
	{
		obj->data = data;
		obj->size = i;		
	}
	else
	{		
		PbFree(data);
	}
//	LogHex("Decode data",obj->data,obj->size);
	return true;
}


void DeletePbDataObj(PbDataObj *dataObj)
{
//	Log("DeletePbDataObj:%d",dataObj);
	if(dataObj)
	{
		if(dataObj->data)
		{
			PbFree(dataObj->data);
		}
		PbFree(dataObj);	
	}
}


