#pragma once

#include "pb_decode.h"
#include "pb_encode.h"
#include "air.pb.h"
#include "sys.h"
#include "cmsis_os.h"
#include "GprsPort.h"
#include "malloc.h"
#include "DoublyList.h"

#define PbMalloc(n) mymalloc(SRAMIN,n)
#define PbFree(n)  myfree(SRAMIN,n)

#define PB_MAX_BUF_SIZE MAX_USART6_SIZE

//通用数据体
typedef struct{
u8 *data;
u16 size;
}PbDataObj;

bool EncodeBufferCallBack(pb_ostream_t *stream, const pb_field_t *field, void * const *arg);
bool DecodeBufferCallBack(pb_istream_t *stream, const pb_field_t *field, void **arg);
void DeletePbDataObj(PbDataObj *dataObj);

