#ifndef __CIRCULAR_QUEUE_H__
#define __CIRCULAR_QUEUE_H__
#include "sys.h"
//#include "FreeRTOS.h"
//#include "task.h"

#define QUE_SIZE 512
typedef struct
{
	u8 *base; // 用于存放内存分配基地址
				// 这里你也可以用数组存放
	int front;
	int rear;
	int maxsize;
} cycleQueue;

int CreateQueue(cycleQueue *q,int maxsz);
int InsertQueue(cycleQueue *q, u8 e);
int InsertQueBuf(cycleQueue *q,u8 *data,int len);
int ReadQueBuf(cycleQueue *q,u8 *buf,int count);
int ReadQueue(cycleQueue *q, u8 *e);
int ShowQueue(cycleQueue *q);
int ReSetQueue(cycleQueue *q);
int DestroyQueue(cycleQueue *q);
int GetQueUseSize(cycleQueue *q);
#endif
