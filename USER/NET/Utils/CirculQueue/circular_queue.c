#include "circular_queue.h"
#include "malloc.h"
#include "stdio.h"



int CreateQueue(cycleQueue *q,int maxsz)
{
	q->maxsize=maxsz+1;
	q->base = (u8 *) mymalloc(SRAMIN,q->maxsize * sizeof(u8));
	if( !q->base )
	{
		printf("CreateQueue malloc err\r\n");
		return 0;
	}
	q->front = q->rear = 0;
	return 1;
}

int InsertQueBuf(cycleQueue *q,u8 *data,int len)
{
	int i = 0 ;
	int res;
	for(i=0;i<len;i++)
	{
		res = InsertQueue(q,data[i]);
		if(res == 0)
		{
			printf("que is full \r\n");
			break;
		}
	}
	return i;
}

int ReadQueBuf(cycleQueue *q,u8 *buf,int count)
{
	int i=0;
	for(i = 0;i<count;i++)
	{
		if(!ReadQueue(q,&buf[i]))
		{
			break;
		}
	}
	return i;
}
//被中断调用
int InsertQueue(cycleQueue *q, u8 e)
{
	if( (q->rear+1)%q->maxsize == q->front )
	return 0; // 队列已满
	q->base[q->rear] = e;
	q->rear = (q->rear+1) % q->maxsize;
	return 1;
}

int ReadQueue(cycleQueue *q, u8 *e)
{
	
	//中断收到数据后插入队列会改写rear
	int rear = 	q->rear;
	if( q->front == rear )
	{
		return 0; // 队列为空
	}
	*e = q->base[q->front];
	q->front = (q->front+1) % q->maxsize;
	return 1;
}

int DestroyQueue(cycleQueue *q)
{
	q->front = q->rear = 0;
	myfree(SRAMIN,q->base);
	return 1;
}

int ReSetQueue(cycleQueue *q)
{
	q->front = q->rear = 0;
	return 0;
}

int GetQueUseSize(cycleQueue *q)
{
	int rear = 	q->rear;
	return (rear -q->front +q->maxsize)%q->maxsize;
}

int ShowQueue(cycleQueue *q)
{
	int i = 0;
	printf("rear:%d\r\n",q->rear);
	printf("front:%d\r\n",q->front);
	for(i=0;i<q->maxsize;i++)
	{
		printf("%d ",q->base[i]);
	}
	
	return 1;
}
