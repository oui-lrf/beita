#ifndef __TCPPORT_H__
#define __TCPPORT_H__
#include "TcpInclude.h"
#define TcpDelayMs osDelay 

#ifdef __cplusplus
 extern "C" {
#endif

//返回给应用层的数据类型应该有打开连接状态（成功或失败），建立连接的状态（成功或失败）,接收到数据返回

typedef struct{
	u8 *data;
	u16 size;
	u16 closeDelay;
}TcpSendData;


typedef struct{
	u8 *data;
	u16 size;
}TcpBackAppRecvData;


typedef enum{	
	TCP_BACK_CLOSE,
	TCP_BACK_OPEN,
	TCP_BACK_CONNECT,
	TCP_BACK_RECV,
	TCP_BACK_CONNECT_CLOSE,
	TCP_BACK_CONFIG,
	TCP_SEND_FAILD,
}TCP_BACK_APP_TYPE;

typedef enum{
	TCP_APP_CLOSE=0,
	TCP_APP_OPENING,
	TCP_APP_CONNECTING,
	TCP_APP_CONNECT,
	TCP_APP_SEND,
	TCP_APP_STA_SEND_CLOSE,
	TCP_APP_NET_CONFIGING,
	TCP_APP_NET_CONFIG_OK,
	TCP_APP_CONFIG_FAILED,
}TCP_APP_ONLINE_STA;


typedef struct 
{
	char ip[34];
	u16 port;
	u8 *data;
	int size;
}TcpAppDataObj;
void TcpOpen(void);
void TcpAppSendData(const char* ip,const u16 port,u8 *dat,u16 size,u16 closeDelay,u8 block,u8 keepConnect);
void TcpCloseDerive(void);
void TcpOpenDerive(void);
void TcpClose(void);
extern TCP_APP_ONLINE_STA TcpAppSta;

void SetTcpAppCall(void (*call)(TCP_BACK_APP_TYPE type,u32 value));
#if 1
	#define TcpLog Log
#else
	#define TcpLog(...) 
#endif


#ifdef __cplusplus
 }
#endif

#endif

