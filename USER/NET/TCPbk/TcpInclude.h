#pragma once
#include "qpc.h"    /* QP/C framework API */
#include "bsp.h"
#include "NetConfig.h"
#include "NetPort.h"
#include "sys.h"
#include "Debug.h"
#include "malloc.h"
#include "cmsis_os.h"
#include "string.h"
#include "NetInclude.h"
#include "WifiPort.h"

#define TcpMalloc NetMalloc


