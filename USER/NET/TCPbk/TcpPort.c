#include "TcpPort.h"

extern QActive * const TP_TcpWork;
void TcpAppConnect(const char* ip,const u16 port)
{
	
  TcpLog("TcpAppConnect ip:%s,port:%d",ip,port);
	u16 size =strlen(ip);
	char *ipData = TcpMalloc(size+1);
	if(!ipData)
	{
		TcpLog("Usart Malloc err");
		return;
	}	
	strncpy(ipData,ip,size);
	QpEvt *te;
	te = Q_NEW(QpEvt, TCP_CONNECT_SIG);
	te->p = (u32)ipData;
	te->v = port;
	QACTIVE_POST(TP_TcpWork,&te->super,0);
}

void TcpLinkStaChangeCall(NET_STA sta)
{
	SendUiMsg(SCREEN_MAIN,HEAD_NET_STA,sta);
}


void TcpAppSendData(const char* sip,const u16 port,u8 *dat,u16 size,u16 closeDelay,u8 block,u8 keepConnect)
{	
	Log("TcpAppSendData ip:%s,port:%d,size:%d",sip,port,size);
	NET_SENG_MSG *msg =  TcpMalloc(sizeof(NET_SENG_MSG));
	if(!msg)
	{
		Log("Malloc err");
		return ;
	}
	
	u8 *data = TcpMalloc(size);
	if(!data)
	{
		Log("Malloc err");
		NetFree(msg);
		return;
	}
	char *ip =  TcpMalloc(strlen(sip)+1);
	
	if(!ip)
	{
		Log("Malloc err");
		NetFree(data);
		NetFree(msg);
		return;
	}
	
	strncpy(ip,sip,strlen(sip)+1);
	memcpy(data,dat,size);
	
	
	msg->data = data;
	msg->size = size;
	msg->ip = ip;
	msg->port = port;
	msg->closeDelay = closeDelay;
	msg->block =block;
	msg->keepConnect = keepConnect;
	QpEvt *te;
	te = Q_NEW(QpEvt, TCP_SEND_SIG);
	te->p = (u32)msg;
	te->v = sizeof(TCP_SEND_SIG);
	QACTIVE_POST(TP_TcpWork,&te->super,0);
}

void TcpClose(void)
{
	QACTIVE_POST(TP_TcpWork, &Q_NEW(QpEvt, TCP_CLOSE_SIG)->super, me);
}



