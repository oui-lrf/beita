#include "TcpWork.h"

#define TCP_QUE_SIZE 10

typedef struct {
/* protected: */
    QActive super;
		QTimeEvt timeEvt;
		QTimeEvt timeEvt2;
		QTimeEvt timeEvt3;	
		//QTimeEvt timeEvt4;	
		QEQueue requestQueue;
		QEvt const *requestQSto[TCP_QUE_SIZE];
		QEQueue requestQueue2;
		QEvt const *requestQSto2[TCP_QUE_SIZE];
		NET_TYPE netType;
		char ip[40];
		u16 port;
		u8 firstSaveIp;
		u8 recvFun;
		u8 keepConnect;
		u16 sendDelay;
		//QpEvt const *activeRequest; /* request event being processed */
}TcpWork;

static QState TcpWork_initial(TcpWork * const me, QEvt const * const e);

static QState TCP_TOP(TcpWork * const me, QEvt const * const e);
static QState TCP_ON(TcpWork * const me, QEvt const * const e);
static QState TCP_CLOSE(TcpWork * const me, QEvt const * const e);


static QState TCP_REOPEN(TcpWork * const me, QEvt const * const e);
static QState TCP_QUE2_REOPEN(TcpWork * const me, QEvt const * const e);

static QState TCP_WIFI_OPEN(TcpWork * const me, QEvt const * const e);
static QState TCP_WIFI_CONNECT(TcpWork * const me, QEvt const * const e);
static QState TCP_WIFI_CONNECTED(TcpWork * const me, QEvt const * const e);

static QState TCP_GPRS_OPEN(TcpWork * const me, QEvt const * const e);
static QState TCP_GPRS_CONNECT(TcpWork * const me, QEvt const * const e);
static QState TCP_DELETE_QUE_RECONNECT(TcpWork * const me, QEvt const * const e);

static QState TCP_GPRS_SEND(TcpWork * const me, QEvt const * const e);
static QState TCP_PAUSE(TcpWork * const me, QEvt const * const e);
static TcpWork tcpWork;
QActive * const TP_TcpWork = &tcpWork.super;

extern QActive * const WF_WifiWork;
//Q_DEFINE_THIS_MODULE("TcpWork")

void TcpWork_ctor(void) {
	
		
    TcpWork *me = (TcpWork *)TP_TcpWork;
    QActive_ctor(&me->super, Q_STATE_CAST(&TcpWork_initial));
		QTimeEvt_ctorX(&me->timeEvt, &me->super, TIMEOUT_SIG, 0U);
		QTimeEvt_ctorX(&me->timeEvt2, &me->super, TIMEOUT2_SIG, 0U);
		QTimeEvt_ctorX(&me->timeEvt3, &me->super, TIMEOUT3_SIG, 0U);
		//QTimeEvt_ctorX(&me->timeEvt4, &me->super, TIMEOUT4_SIG, 0U);
		QEQueue_init(&me->requestQueue,me->requestQSto, Q_DIM(me->requestQSto));
		QEQueue_init(&me->requestQueue2,me->requestQSto2, Q_DIM(me->requestQSto2));

}

void TcpLinkStaChangeCall(NET_STA sta);
static QState TcpWork_initial(TcpWork * const me, QEvt const * const e) {
    /*${Tcp::TcpWork::SM::initial} */
	
		QActive_subscribe(&me->super,TCP_CONNECT_SIG);
		QActive_subscribe(&me->super, TCP_CLOSE_SIG);
		QActive_subscribe(&me->super, TCP_OPEN_SIG);
	
	
	
		QActive_subscribe(&me->super, WIFI_OPEN_SUCCES_SIG);
		QActive_subscribe(&me->super, WIFI_OPEN_FAILED_SIG);
		QActive_subscribe(&me->super, WIFI_CONNECT_SUCCES_SIG);
		QActive_subscribe(&me->super, WIFI_CONNECT_FAILED_SIG);
		QActive_subscribe(&me->super, WIFI_RECV_SIG);
		QActive_subscribe(&me->super, WIFI_CONFIG_START);
	
		QActive_subscribe(&me->super, WIFI_CONFIG_SUCESS);
		QActive_subscribe(&me->super, WIFI_CONFIG_FAILED);
	
		QActive_subscribe(&me->super, GPRS_OPEN_SUCCES_SIG);
		QActive_subscribe(&me->super, GPRS_OPEN_FAILED_SIG);
		QActive_subscribe(&me->super, GPRS_CONNECT_SUCCES_SIG);
		QActive_subscribe(&me->super, GPRS_CONNECT_FAILED_SIG);
		QActive_subscribe(&me->super, GPRS_RECV_SIG);
    return Q_TRAN(&TCP_CLOSE);
}

static QState TCP_TOP(TcpWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        /*${Tcp::TcpWork::SM::NET_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
					status_ = Q_HANDLED();	
					TcpLinkStaChangeCall(NET_STA_CLOSE);
					Log("+++TCP_TOP");
            break;
        }
				//还没有连上就发送数据,如果网络未打开,延迟发布事件并,打开网络
				case TCP_SEND_SIG:{
					status_ = Q_HANDLED();	
					Log("TCP TCP_SEND_SIG");
					NET_SENG_MSG *msg = (NET_SENG_MSG  *)(Q_EVT_CAST(QpEvt)->p);
					if(msg)
					{	
						NetLog("NET_TOP NET_SEND_SIG ip:%s,port:%d",msg->ip,msg->port);
						Log("free:%d",me->requestQueue.nFree);
						if(me->requestQueue.nFree >=2)//保存延迟事件
						{
							if(QActive_defer(&me->super, &me->requestQueue, e))
							{
									//打开网络
									strncpy(me->ip,msg->ip,40);
									me->port = msg->port;
									TcpLog("save ok msg:%d",msg);
									me->netType = GetNetType();
									Log("netType:%d",me->netType);
									if(me->netType == NET_TYPE_WIFI)
									{
										status_ =Q_TRAN(&TCP_WIFI_OPEN);
									}
									else if(me->netType == NET_TYPE_GPRS)
									{
										status_ =Q_TRAN(&TCP_GPRS_OPEN);
									}
							}
							else 
							{
								NetFree(msg->data);
								NetFree(msg->ip);
								NetFree(msg);
								NetLog("save faild");
							}
						}
						else
						{
							NetFree(msg->data);
							NetFree(msg->ip);
							NetFree(msg);
							NetLog("no free");
						}
					}
				break;	
				}
				case WIFI_RECV_SIG:
				case GPRS_RECV_SIG:{
					status_ = Q_HANDLED();	
					Log("TCP RECV_SIG");
					u8 *rdata =(u8 *)(Q_EVT_CAST(QpEvt)->p);
					if(rdata)
					{
						NetFree(rdata);
					}
				break;
				}
				case WIFI_CONFIG_START:{
						status_ =Q_TRAN(&TCP_PAUSE);
					break;
				}
        default: {
            status_ = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status_;
}

static QState TCP_ON(TcpWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        /*${Tcp::TcpWork::SM::NET_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
					Log("+++TCP_ON");
					status_ = Q_HANDLED();	
					TcpLinkStaChangeCall(NET_STA_START);
					
					break;
        }
				case TCP_SEND_SIG:
					status_ = Q_HANDLED();	
					Log("TCP TCP_SEND_SIG");
					NET_SENG_MSG *msg = (NET_SENG_MSG  *)(Q_EVT_CAST(QpEvt)->p);
					if(msg)
					{	
						char *ip = msg->ip;
						u16 port = msg->port;
						NetLog("TCP_ON cache que free:%d ip:%s,port:%d",me->requestQueue.nFree,ip,port);
						if(me->requestQueue.nFree >=2)//保存延迟事件
						{
							if (QActive_defer(&me->super, &me->requestQueue, e))
							{
								TcpLog("TCP_ON ip:%s,port:%d",ip,port);				
							}
							else 
							{
								NetFree(msg->data);
								NetFree(msg->ip);
								NetFree(msg);
								NetLog("save faild");
							}
						}
						else
						{
							NetFree(msg->data);
							NetFree(msg->ip);
							NetFree(msg);
							NetLog("no free");
						}
					}
				break;	
				case TCP_CLOSE_SIG:
						status_ =Q_TRAN(&TCP_CLOSE);
				break;
        default: {
            status_ = Q_SUPER(&TCP_TOP);
            break;
        }
    }
    return status_;
}


static QState TCP_CLOSE(TcpWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        /*${Tcp::TcpWork::SM::NET_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
						Log("+++TCP_CLOSE");
						TcpLinkStaChangeCall(NET_STA_CLOSE);
						if(me->netType == NET_TYPE_WIFI)
						{
							WifiDeInit();
						}
						else if(me->netType == NET_TYPE_GPRS)
						{
							GprsDeInit();
						}
						status_ = Q_HANDLED();	
						QF_PUBLISH(&Q_NEW(QpEvt, TCP_CLOSE_CALL_SIG)->super, me);
            break;
        }
        default: {
            status_ = Q_SUPER(&TCP_TOP);
            break;
        }
    }
    return status_;
}


static QState TCP_WIFI_OPEN(TcpWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        case Q_ENTRY_SIG: {
						;static u8 i = 0;
						Log("+++TCP_WIFI_OPEN i:%d",i++);
						TcpLinkStaChangeCall(NET_STA_OPENING);
						WifiInit();
						status_ = Q_HANDLED();	
            break;
        }
				case WIFI_OPEN_SUCCES_SIG:{
						Log("TCP_WIFI_OPEN WIFI_OPEN_SUCCES");
						//继续下一步
						QTimeEvt_disarm(&me->timeEvt); QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*5,0);
						status_ = Q_HANDLED();	
				break;
				}
				case TIMEOUT_SIG:{
					status_ =Q_TRAN(&TCP_WIFI_CONNECT);
				break;
				}
				case WIFI_OPEN_FAILED_SIG:
						Log("TCP_WIFI_OPEN WIFI_OPEN_FAILED");
						//给上层返回TCP连接失败
						QF_PUBLISH(&Q_NEW(QpEvt, TCP_CONNECT_FAILED_SIG)->super, me);
						status_ = Q_TRAN(&TCP_CLOSE);
				break;
        default: {
            status_ = Q_SUPER(&TCP_ON);
            break;
        }
    }
    return status_;
}

static QState TCP_WIFI_CONNECT(TcpWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        case Q_ENTRY_SIG: {
						Log("+++TCP_WIFI_CONNECT");
            if (QActive_recall(&me->super, &me->requestQueue)){
                Log("Request recalled ok");
            }
            else {
                Log("No deferred requests");
            }
						TcpLinkStaChangeCall(NET_STA_CONNECTEDING);
						status_ = Q_HANDLED();	
            break;
        }
				case TCP_CONNECT_SIG:{
					Log("TCP_WIFI_CONNECT TCP_CONNECT_SIG");
					 char *ip = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(ip)
						{
							TcpLog("TCP_WIFI_CONNECT ip:%s,port:%d",ip,Q_EVT_CAST(QpEvt)->v); 
							OpenWifiTcp(ip,(u16)(Q_EVT_CAST(QpEvt)->v));
							WifiFree(ip);
						}
						status_ = Q_HANDLED();	
						break;
				}
				case WIFI_CONNECT_SUCCES_SIG:
						Log("TCP_WIFI WIFI_CONNECT_SUCCES_SIG");
						//通知上层,转换状态为已连接
						QF_PUBLISH(&Q_NEW(QpEvt, TCP_CONNECT_SUCCES_SIG)->super, me);
						status_ = Q_TRAN(&TCP_WIFI_CONNECTED);
				break;
				case WIFI_CONNECT_FAILED_SIG:
						Log("TCP_WIFI WIFI_CONNECT_FAILED_SIG");
							//通知上层
						QF_PUBLISH(&Q_NEW(QpEvt, TCP_CONNECT_FAILED_SIG)->super, me);
						status_ = Q_HANDLED();
				break;
        default: {
            status_ = Q_SUPER(&TCP_ON);
            break;
        }
    }
    return status_;
}

//负责接收上层数据传输请求
//转发底层收到的数据到上层
static QState TCP_WIFI_CONNECTED(TcpWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
            case Q_ENTRY_SIG: {
						Log("+++TCP_WIFI_CONNECTED");
						//通知上层,转换状态为已连接
						TcpLinkStaChangeCall(NET_STA_CONNECTED);
						QF_PUBLISH(&Q_NEW(QpEvt, TCP_CONNECT_SUCCES_SIG)->super, me);
            if (QActive_recall(&me->super, &me->requestQueue)) {
                Log("tcp connect republish save event");
            }
            else {
                Log("tcp connect no event");
            }
						status_ = Q_HANDLED();	
            break;
        }
				//通过发送消息到TCP才能调用WIFI发送，是为了保证只有TCP为连接状态才能给底层发送数据
				case TCP_SEND_SIG:{
					status_ = Q_HANDLED();
					 Log("TCP_WIFI_CONNECTED TCP_SEND_SIG");
						NET_SENG_MSG *msg = (NET_SENG_MSG  *)(Q_EVT_CAST(QpEvt)->p);
						if(msg)
						{
							u8 *data = msg->data;
						
							//如果当前连接的ip port和消息ip port不等，需要断开连接重新连接
							Log("msg ip:%s,port:%d",msg->ip,msg->port);
							if(strcmp(msg->ip,me->ip) == 0 && msg->port == me->port )
							{
								if(data && msg->size)
								{
									WifiSendData(msg->data,msg->size);
								}
									if(msg->ip)
									{
										NetFree(msg->ip);
									}
									
									if(data)
									{							
										NetFree(data);
									}
									NetFree(msg);
									//发送完成后，重新发布保存的事件
									if (QActive_recall(&me->super, &me->requestQueue)){
											Log("tcp connect republish save event");
									}
									else {
											Log("tcp connect no event");
										//发送完了，调到等待延时或者关闭
										//如果que2为空就关闭
										if(me->requestQueue2.nFree < TCP_QUE_SIZE+1)
										{
											QTimeEvt_disarm(&me->timeEvt); 
											QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*2,0);
										}
										else
										{
											//延时2S后关闭
											QTimeEvt_disarm(&me->timeEvt2); 
											QTimeEvt_armX(&me->timeEvt2,BSP_TICKS_PER_SEC*(2+msg->closeDelay),0);
										}
										
									}
							}
							else
							{
								//再次缓存到另一个存储器，继续处理下一个事件
								Log("changed ip free:%d",me->requestQueue2.nFree);
								if(me->requestQueue2.nFree >=2)//保存延迟事件
								{
									if (QActive_defer(&me->super, &me->requestQueue2, e))
									{
											//打开网络
											Log("Save event to que2 ok");
									}
									else 
									{
										NetFree(msg->data);
										NetFree(msg->ip);
										NetFree(msg);
										NetLog("save faild");
									}
								}
								else
								{
									NetFree(msg->data);
									NetFree(msg->ip);
									NetFree(msg);
									NetLog("no free");
								}	
								if (QActive_recall(&me->super, &me->requestQueue)){
										Log("tcp connect republish save event");
									}
									else {
											Log("tcp connect no event");
										//IP相同的已经发送完了，重新开启后发送下一个IP
											QTimeEvt_disarm(&me->timeEvt); 
											QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*2,0);
									}
							}

						}
						
						break;
				}
				case TIMEOUT_SIG:
					//重新打开
						status_ =Q_TRAN(&TCP_QUE2_REOPEN);
				break;
				case TIMEOUT2_SIG:
					//转到关闭状态
						status_ =Q_TRAN(&TCP_CLOSE);
				break;
				case WIFI_RECV_SIG:{
						status_ = Q_HANDLED();
            char *rdata = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(rdata)
						{
							u16 size = Q_EVT_CAST(QpEvt)->v;
							QpEvt *te;
							te = Q_NEW(QpEvt, TCP_RECV_SIG);
							te->p = (uint32_t)rdata;
							te->v = size;
							//上层要保证该消息一定能被处理，防止内存泄漏
							QF_PUBLISH(&te->super, me);
						}
						
          break;
        }
        default: {
            status_ = Q_SUPER(&TCP_ON);
            break;
        }
    }
    return status_;
}
//没有打开超时检测
static QState TCP_GPRS_OPEN(TcpWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        case Q_ENTRY_SIG: {
						status_ = Q_HANDLED();	
						;static u8 i = 0;
						GprsInit();
						TcpLinkStaChangeCall(NET_STA_OPENING);
						QTimeEvt_disarm(&me->timeEvt2); 
						QTimeEvt_armX(&me->timeEvt2,BSP_TICKS_PER_SEC*60,0);
            break;
        }
				case GPRS_OPEN_SUCCES_SIG:
						Log("TCP_GPRS_OPEN GPRS_OPEN_SUCCES");
						//继续下一步
						QTimeEvt_disarm(&me->timeEvt); 
						QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*2,0);
						status_ = Q_HANDLED();	
				break;
				case TIMEOUT_SIG:
					status_ =Q_TRAN(&TCP_GPRS_CONNECT);
				break;
				case GPRS_OPEN_FAILED_SIG:
					status_ = Q_HANDLED();	
					Log("TCP_GPRS_OPEN GPRS_OPEN_FAILED");
					status_ =Q_TRAN(&TCP_REOPEN);
				break;
				case TIMEOUT2_SIG:
					status_ =Q_TRAN(&TCP_REOPEN);
				break;
				case Q_EXIT_SIG:{
					status_ = Q_HANDLED();
					Log("~~~~TCP_GPRS_OPEN");
					QTimeEvt_disarm(&me->timeEvt);
					QTimeEvt_disarm(&me->timeEvt2);
					break;
				}
        default: {
            status_ = Q_SUPER(&TCP_ON);
            break;
        }
    }
    return status_;
}


//GPRS模块连接一定要返回，返回失败或者成功
static QState TCP_GPRS_CONNECT(TcpWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        case Q_ENTRY_SIG: {
						status_ = Q_HANDLED();	
						Log("+++TCP_GPRS_CONNECT");
						OpenGprsTcp(me->ip,me->port);
						TcpLinkStaChangeCall(NET_STA_CONNECTEDING);
						QTimeEvt_disarm(&me->timeEvt); 
						QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*5*60,0);//5分钟还连不上
            break;
        }
				case GPRS_CONNECT_SUCCES_SIG:
						Log("TCP_GPRS GPRS_CONNECT_SUCCES_SIG");
						QTimeEvt_disarm(&me->timeEvt); 
						status_ = Q_TRAN(&TCP_GPRS_SEND);
				break;
				case GPRS_CONNECT_FAILED_SIG:
						status_ = Q_HANDLED();
						Log("TCP_GPRS GPRS_CONNECT_FAILED_SIG");
						//清除对应的IP和端口，重新连接
					status_ =Q_TRAN(&TCP_DELETE_QUE_RECONNECT);
				break;
				case TIMEOUT2_SIG:{
					status_ =Q_TRAN(&TCP_GPRS_CONNECT);
				break;
				}
				//5分钟还连不上，重新打开
				case TIMEOUT_SIG:{
					status_ =Q_TRAN(&TCP_REOPEN);
				break;
				}
				case Q_EXIT_SIG:{
					Log("~~~~TCP_GPRS_CONNECT");
					QTimeEvt_disarm(&me->timeEvt);
					QTimeEvt_disarm(&me->timeEvt2);
					status_ = Q_HANDLED();	
					break;
				}
        default: {
            status_ = Q_SUPER(&TCP_ON);
            break;
        }
    }
    return status_;
}

//先关闭，删除当前ip和端口的数据，更新新ip，再连接
static QState TCP_DELETE_QUE_RECONNECT(TcpWork * const me, QEvt const * const e) {
    QState status_; 
		static char clear_ip[41]={0};
		static u16 clear_port=0;
    switch (e->sig) {
        /*${Tcp::TcpWork::SM::NET_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
						status_ = Q_HANDLED();	
						Log("+++TCP_DELETE_QUE_RECONNECT");
						TcpLinkStaChangeCall(NET_STA_CLOSE);
						me->firstSaveIp = 0;
						while(1)
						{
							if (QActive_recall(&me->super, &me->requestQueue)) {
									Log("recall que ok");
							}
							else {
									Log("recall que failed");
								break;
							}						
						}			
						strncpy(clear_ip,me->ip,40);
						clear_port = me->port;			
					break;
        }
				case TCP_SEND_SIG:{
					status_ = Q_HANDLED();	
					Log("TCP_DELETE_QUE_RECONNECT TCP_SEND_SIG");
					NET_SENG_MSG *msg = (NET_SENG_MSG  *)(Q_EVT_CAST(QpEvt)->p);
					if(msg)
					{	
						NetLog("msg->ip:%s,msg->port:%d",msg->ip,msg->port);
						//当前ip和端口，删除
						if(strcmp(msg->ip,clear_ip) == 0 && msg->port == clear_port )
						{
							
							NetLog("delete msg");
							NetFree(msg->ip);
							NetFree(msg->data);
							NetFree(msg);	
						}
						else
						{
							
							Log("free:%d",me->requestQueue.nFree);
							if(me->requestQueue.nFree >=2)//保存延迟事件
							{
								if(QActive_defer(&me->super, &me->requestQueue, e))
								{
										//打开网络
										strncpy(me->ip,msg->ip,40);
										me->port = msg->port;
								}
								else 
								{
									NetFree(msg->data);
									NetFree(msg->ip);
									NetFree(msg);
									NetLog("save faild");
								}
							}
							else
							{
								NetFree(msg->data);
								NetFree(msg->ip);
								NetFree(msg);
								NetLog("no free");
							}
							
							if(me->firstSaveIp++ ==0)
							{
								strncpy(me->ip,msg->ip,40);
								me->port = msg->port;							
							}						
						}
					}
					QTimeEvt_disarm(&me->timeEvt); 
					QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*2,0);
				break;	
				}
				case TIMEOUT_SIG:{
					status_ = Q_HANDLED();
					//删除之后就没有待发送的了
					if(me->firstSaveIp>0)
					{
						status_ =Q_TRAN(&TCP_GPRS_CONNECT);
					}
					else
					{
						status_ =Q_TRAN(&TCP_CLOSE);
					}
				break;
				}
				case Q_EXIT_SIG:{
					QTimeEvt_disarm(&me->timeEvt);
					status_ = Q_HANDLED();	
					break;
				}
        default: {
            status_ = Q_SUPER(&TCP_ON);
            break;
        }
    }
    return status_;
}

//负责接收上层数据传输请求
//转发底层收到的数据到上层
static QState TCP_GPRS_SEND(TcpWork * const me, QEvt const * const e) {
    QState status_;
	
    switch (e->sig) {
        case Q_ENTRY_SIG: {
						status_ = Q_HANDLED();	
						Log("+++TCP_GPRS_SEND");
						//通知上层,转换状态为已连接
						TcpLinkStaChangeCall(NET_STA_CONNECTED);
						QF_PUBLISH(&Q_NEW(QpEvt, TCP_CONNECT_SUCCES_SIG)->super, me);
            if (QActive_recall(&me->super, &me->requestQueue)) {
                Log("tcp connect republish save event");
            }
            else {
                Log("tcp connect no event");
            }
						
            break;
        }
				//通过发送消息到TCP才能调用WIFI发送，是为了保证只有TCP为连接状态才能给底层发送数据
//				case TCP_SEND_SIG:{
//					status_ = Q_HANDLED();	
//					NET_SENG_MSG *msg = (NET_SENG_MSG  *)(Q_EVT_CAST(QpEvt)->p);
//					Log("TCP_SEND_SIG msg:%d",msg);
//					if(msg)
//					{
//						//如果当前连接的ip port和消息ip port不等，需要断开连接重新连接
//						if(strcmp(msg->ip,me->ip) == 0 && msg->port == me->port )
//						{
//							Log("msg send ip:%s,port:%d data:%d",msg->ip,msg->port,msg->data);
//							GprsSendData(msg->data,msg->size);
//							//如果是阻塞消息，需要超时或者接收到数据才能发送下一条
//							me->recvFun = msg->block;
//							me->keepConnect = msg->keepConnect;
//							me->sendDelay = msg->closeDelay;
//							if(msg->block)
//							{
//									QTimeEvt_disarm(&me->timeEvt3); 
//									QTimeEvt_armX(&me->timeEvt3,BSP_TICKS_PER_SEC*(5+msg->closeDelay),0);
//							}
//							//非阻塞消息，直接发布下一跳数据即可
//							else
//							{
////								QTimeEvt_disarm(&me->timeEvt4); 
////								QTimeEvt_armX(&me->timeEvt4,BSP_TICKS_PER_SEC*1,0);							
//									if(QActive_recall(&me->super, &me->requestQueue))
//									{
//											Log("tcp connect republish save event");
//									}
//									else 
//									{
//										Log("tcp connect no event");
//										//发送完了，调到等待延时或者关闭
//										//如果que2为空就关闭
//										//当前的IP发送完了，判断缓存2里面有没有数据
//										Log("que2 nfree:%d",me->requestQueue2.nFree);
//										if(me->requestQueue2.nFree < TCP_QUE_SIZE+1)
//										{
//											QTimeEvt_disarm(&me->timeEvt); 
//											QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*5,0);
//										}
//										else
//										{
//											//延时2S后关闭
//											QTimeEvt_disarm(&me->timeEvt2); 
//											QTimeEvt_armX(&me->timeEvt2,BSP_TICKS_PER_SEC*(5+me->sendDelay),0);
//										} 
//										
//									}
//							}							
//							NetFree(msg->ip);
//							NetFree(msg->data);
//							NetFree(msg);	
//					}
//					else
//					{
//						//再次缓存到另一个存储器，继续处理下一个事件
//						Log("changed me->ip:%s,me->port:%d,msg->ip:%s,msg->port:%d",me->ip,me->port,msg->ip,msg->port);
//						Log("que2 nfree:%d",me->requestQueue2.nFree);
//						if(me->requestQueue2.nFree >=2)//保存延迟事件
//						{
//							if (QActive_defer(&me->super, &me->requestQueue2, e))
//							{
//									//打开网络
//									Log("Save event to que2 ok");
//							}
//							else 
//							{
//								NetFree(msg->data);
//								NetFree(msg->ip);
//								NetFree(msg);
//								NetLog("save faild");
//							}
//						}
//						else
//						{
//							NetFree(msg->data);
//							NetFree(msg->ip);
//							NetFree(msg);
//							NetLog("no free");
//						}	
//						if(QActive_recall(&me->super, &me->requestQueue))
//						{
//								Log("tcp connect republish save event");
//						}
//						else
//						{
//							Log("tcp connect no event");
//							//IP相同的已经发送完了，重新开启后发送下一个IP
//							QTimeEvt_disarm(&me->timeEvt); 
//							QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*5,0);
//						}
//					}
//				}	
//				break;
//				}
				case TIMEOUT_SIG:
					//重新打开
						status_ =Q_TRAN(&TCP_QUE2_REOPEN);
				break;
				case TIMEOUT2_SIG:
					//转到关闭状态
					status_ =Q_TRAN(&TCP_CLOSE);
				break;
				//阻塞超时，直接发送下一条
				case TIMEOUT3_SIG:{
					//转到关闭状态
					status_ = Q_HANDLED();
					Log("block timeOut");
					//发送完成后，重新发布保存的事件
					//上层要保证该消息一定能被处理，防止内存泄漏
					QF_PUBLISH(&Q_NEW(QpEvt, TCP_RECV_FAILED_SIG)->super, me);
					if (QActive_recall(&me->super, &me->requestQueue))
					{
							Log("tcp connect republish save event");
					}
					else 
					{
						Log("tcp connect no event");
						//发送完了，调到等待延时或者关闭
						//如果que2为空就关闭
						//当前的IP发送完了，判断缓存2里面有没有数据
						if(me->requestQueue2.nFree < TCP_QUE_SIZE+1)
						{
							//重新打开
							status_ =Q_TRAN(&TCP_QUE2_REOPEN);
						}
						else
						{
							//延时2S后关闭
							status_ =Q_TRAN(&TCP_CLOSE);
						} 
					}
				break;
				}
				case GPRS_RECV_SIG:{
						status_ = Q_HANDLED();
					//如果需要接收，收到数据后清零定时器,发送吓一跳
					//收到之后是否要继续下一条，例如HTTP短连接方式可以下一条，长连接方式就只有延时后才继续下一条
						
						if(me->recvFun)
						{
						
							if(me->keepConnect==0)		
							{	
								QTimeEvt_disarm(&me->timeEvt3);	
								if (QActive_recall(&me->super, &me->requestQueue))
								{
										Log("tcp connect republish save event");
								}
								else 
								{
									Log("tcp connect no event");
									//发送完了，调到等待延时或者关闭
									//如果que2为空就关闭
									//当前的IP发送完了，判断缓存2里面有没有数据
									if(me->requestQueue2.nFree < TCP_QUE_SIZE+1)
									{
										//重新打开
										status_ =Q_TRAN(&TCP_QUE2_REOPEN);
									}
									else
									{
										//关闭
										status_ =Q_TRAN(&TCP_CLOSE);
									} 
								}							
							}

						}
            char *rdata = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(rdata)
						{
							u16 size = Q_EVT_CAST(QpEvt)->v;
						//	TcpLog("tcp recv size:%d",size);
							QpEvt *te;
							te = Q_NEW(QpEvt, TCP_RECV_SIG);
							te->p = (uint32_t)rdata;
							te->v = size;
							//上层要保证该消息一定能被处理，防止内存泄漏
							QF_PUBLISH(&te->super, me);
						}
						
          break;
        }
			 case Q_EXIT_SIG:{
					QTimeEvt_disarm(&me->timeEvt);
					QTimeEvt_disarm(&me->timeEvt2);
					QTimeEvt_disarm(&me->timeEvt3);	
					//QTimeEvt_disarm(&me->timeEvt4);
					status_ = Q_HANDLED();	
					break;
				}
        default: {
            status_ = Q_SUPER(&TCP_ON);
            break;
        }
    }
    return status_;
}


//关闭之后2S再打开
static QState TCP_REOPEN(TcpWork * const me, QEvt const * const e) {
    QState status_; 
    switch (e->sig) {
        /*${Tcp::TcpWork::SM::NET_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
						Log("+++TCP_REOPEN");
						TcpLinkStaChangeCall(NET_STA_CLOSE);
						me->firstSaveIp = 0;
						if(me->netType == NET_TYPE_WIFI)
						{
							WifiDeInit();
						}
						else if(me->netType == NET_TYPE_GPRS)
						{
							GprsDeInit();
						}
						//5S之后重新打开连接
						QTimeEvt_disarm(&me->timeEvt); 
						QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*10,0);
						status_ = Q_HANDLED();	
            break;
        }
				case TIMEOUT_SIG:
					me->netType = GetNetType();
					Log("me->netType:%d",me->netType);
					if(me->netType == NET_TYPE_WIFI)
					{
						status_ =Q_TRAN(&TCP_WIFI_OPEN);
					}
					else if(me->netType == NET_TYPE_GPRS)
					{
						status_ =Q_TRAN(&TCP_GPRS_OPEN);
					}
				break;
				case Q_EXIT_SIG:{
					QTimeEvt_disarm(&me->timeEvt);
					status_ = Q_HANDLED();	
					break;
				}
        default: {
            status_ = Q_SUPER(&TCP_ON);
            break;
        }
    }
    return status_;
}



//关闭之后2S再打开
static QState TCP_QUE2_REOPEN(TcpWork * const me, QEvt const * const e) {
    QState status_; 
    switch (e->sig) {
        /*${Tcp::TcpWork::SM::NET_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
						Log("+++TCP_QUE2_REOPEN");
						TcpLinkStaChangeCall(NET_STA_CLOSE);
						me->firstSaveIp = 0;
						if(me->netType == NET_TYPE_WIFI)
						{
							WifiDeInit();
						}
						else if(me->netType == NET_TYPE_GPRS)
						{
							GprsDeInit();
						}
						//把第二个缓存的事件全部推到第一个事件
						while(1)
						{
							if (QActive_recall(&me->super, &me->requestQueue2)) {
									Log("recall que2 ok");
							}
							else {
									Log("recall que2 failed");
								break;
							}						
						}
						//2S之后重新打开连接
						QTimeEvt_disarm(&me->timeEvt); 
						QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*10,0);
						status_ = Q_HANDLED();	
            break;
        }
				case TCP_SEND_SIG:
					status_ = Q_UNHANDLED();	
					Log("TCP REOPEN TCP_SEND_SIG");
					NET_SENG_MSG *msg = (NET_SENG_MSG  *)(Q_EVT_CAST(QpEvt)->p);
					if(msg)
					{	
						if(me->firstSaveIp++ ==0)
						{
							strncpy(me->ip,msg->ip,40);
							me->port = msg->port;							
						}
					}
				break;	
				case TIMEOUT_SIG:
					//转到关闭状态
					me->netType = GetNetType();
					Log("me->netType:%d",me->netType);
					if(me->netType == NET_TYPE_WIFI)
					{
						status_ =Q_TRAN(&TCP_WIFI_OPEN);
					}
					else if(me->netType == NET_TYPE_GPRS)
					{
						status_ =Q_TRAN(&TCP_GPRS_OPEN);
					}
				break;
				case Q_EXIT_SIG:{
					QTimeEvt_disarm(&me->timeEvt);
					status_ = Q_HANDLED();	
					break;
				}
        default: {
            status_ = Q_SUPER(&TCP_ON);
            break;
        }
    }
    return status_;
}

static QState TCP_PAUSE(TcpWork * const me, QEvt const * const e)
{
    QState status_;
    switch (e->sig) {
        case Q_ENTRY_SIG: {
						MqttLog("+++TCP_PAUSE");
						QF_PUBLISH(&Q_NEW(QpEvt, TCP_PAUSE_SIG)->super, me);
						status_ = Q_HANDLED();	
            break;
        }
				//如果配置成功了，进入连接状态，如果配置失败，也进入连接状态，但是下次重连老WIFI
				case WIFI_CONFIG_SUCESS:
				case WIFI_CONFIG_FAILED:
				{
					status_ = Q_HANDLED();	
					MqttLog("WIFI_CONFIG END");
					QF_PUBLISH(&Q_NEW(QpEvt, TCP_RESUME_SIG)->super, me);
					status_ =Q_TRAN(&TCP_WIFI_CONNECT);
				break;
				}
        default: {
            status_ = Q_SUPER(&TCP_ON);
            break;
        }
    }
    return status_;
}

