#include "GprsUserSendData.h"
#include "GprsCmdPack.h"

void UsartAddCrcSend(u8 *data,u16 size)
{
	u8 * crcData = GprsMalloc(size+2);
	if(crcData)
	{
		u16 crc = usMBCRC16(data,size);
		memcpy(crcData,data,size);
		crcData[size]=crc>>8;
		crcData[size+1]=crc&0xff;
		
		LogHex("usart send",crcData,size+2);
		UsartDataSendBuf(crcData,size+2);
		GprsFree(crcData);	
	}

}


// Header:开机测试指令
// File Name: 
// Author:
// Date:

 void SendTestCmd(void)
{
	u8 *func = 0;
	u16 size = CreateFuncCmd(&func,AIR_CMD_TEST,0,0);
	if(size > 0)
	{
		UsartAddCrcSend(func,size);
		GprsFree(func);
	}
}

// Header:查询所有连接指令
// File Name: 
// Author:
// Date:
void SendConnectSelectCmd(void)
{
	u8 *func = 0;
	u16 size = CreateFuncCmd(&func,AIR_CMD_CONNECT_SELECT,0,0);
	if(size > 0)
	{
		UsartAddCrcSend(func,size);
		GprsFree(func);
	}
}

void SendGetInfoCmd(void)
{
	u8 *func = 0;
	u16 size = CreateFuncCmd(&func,AIR_CMD_INFO,0,0);
	if(size > 0)
	{
		UsartAddCrcSend(func,size);
		GprsFree(func);
	}
}

// Header:关闭连接指令
// File Name: 
// Author:
// Date:
 void SendConnectCloseCmd(u8 id,u8 sta)
{
	u8 *connect = 0;
	GprsLog("发送关闭命令 id:%d,sta:%d",id,sta);
	u16 connectSize = CreateConnectChangeCmd(&connect,id,sta);
	if(!connectSize)
	{
		Log("err");
		return ;
	}
	u8 *func = 0;
	u16 size = CreateFuncCmd(&func,AIR_CMD_CONNECT_CLOSE,connect,connectSize);
	GprsFree(connect);
	if(size > 0)
	{
		UsartAddCrcSend(func,size);
		GprsFree(func);
	}
	
}


u8 SendConnectCmd(ConnectObj *connectObj)
{
	u8 res = 0;
	if(!connectObj)
	{
		return 0;
	}
	u8 *connect = 0;
	u16 connectSize = 0;
	Log("SendConnectCmd Connect->obj:%d",connectObj->obj);
	switch(connectObj->type)
	{
		case ConnectType_CONNECT_TYPE_TCP:
			connectSize = CreateTcpConnectCmd(&connect,connectObj->id,connectObj->obj);
			res =1;
		break;
		case ConnectType_CONNECT_TYPE_MQTT:
			res =1;
			connectSize = CreateMqttConnectCmd(&connect,connectObj->id,connectObj->obj);								
		break;
		case ConnectType_CONNECT_TYPE_HTTP:
			res =1;
			connectSize = CreateHttpConnectCmd(&connect,connectObj->id,connectObj->obj);			
		break;							
	}
	if(res)
	{
		u8 *func = 0;
		u16 size = CreateFuncCmd(&func,AIR_CMD_CONNECT,connect,connectSize);
		GprsFree(connect);
		if(size > 0)
		{
			UsartAddCrcSend(func,size);
			GprsFree(func);
		}		
	}
	return res;
}

void SendMqttSubCmd(u8 id,GprsMqttSubObj *obj)
{
	u8 *connect = 0;
	u16 connectSize = CreateMqttSendCmd(&connect,id,MqttSendType_MQTT_SUB,obj->topic,0,0,0);
	if(!connectSize)
	{
		Log("err");
		return ;
	}
	u8 *func = 0;
	u16 funSize = CreateFuncCmd(&func,AIR_CMD_MQTT_SEND,connect,connectSize);
	GprsFree(connect);
	if(funSize > 0)
	{
		UsartAddCrcSend(func,funSize);
		GprsFree(func);
	}
}

void SendMqttPubCmd(u8 id,GprsMqttPubObj *obj)
{
	u8 *connect = 0;
	Log("SendMqttPubCmd:%d,topic:%s,data:%s",obj,obj->topic,obj->data);
	u16 connectSize = CreateMqttSendCmd(&connect,id,MqttSendType_MQTT_PUB,obj->topic,obj->data,obj->size,obj->qos);
	if(!connectSize)
	{
		Log("err");
		return ;
	}
	u8 *func = 0;
	u16 funSize = CreateFuncCmd(&func,AIR_CMD_MQTT_SEND,connect,connectSize);
	GprsFree(connect);
	if(funSize > 0)
	{
		UsartAddCrcSend(func,funSize);
		GprsFree(func);
	}
}


void SendMqttObj(GprsMqttSendObj * mqttObj)
{
//	Log("GprsMqttSendObj:%d,id:%d,obj:%d",mqttObj,mqttObj->id,mqttObj->obj);
	switch(mqttObj->type)
	{
		case MqttSendType_MQTT_PUB:
			;SendMqttPubCmd(mqttObj->id,mqttObj->obj);
		break;		
		case MqttSendType_MQTT_SUB:
		;SendMqttSubCmd(mqttObj->id,mqttObj->obj);
		break;		
		case MqttSendType_MQTT_UNSUB:
			
		break;
	}

}

//发送TCP数据
void SendTcpObj(GprsTcpSendObj * tcpObj)
{
	u8 *resBuf = 0;
	u16 resSize = CreateTcpSendCmd(&resBuf,tcpObj->id,tcpObj->data,tcpObj->dataSize);
	if(!resSize)
	{
		Log("err");
		return ;
	}
	u8 *func = 0;
	u16 size = CreateFuncCmd(&func,AIR_CMD_TCP_SEND,resBuf,resSize);
	GprsFree(resBuf);
	if(size > 0)
	{
		UsartAddCrcSend(func,size);
		GprsFree(func);
	}
}



