#include "GprsInclude.h"

void DeleteConnectNodeCall(void * pNode);
void DeleteSendNodeCall(void * pNode);
u8 RePostSendDataNodeByIndex(T_OMCI_LIST *pList,u8 index);
//重新post指定的数据点
u8 RePostAllSendDataNodeById(T_OMCI_LIST *pList,u8 id);


//移出链表，释放内存
T_OMCI_LIST_NODE *DeleAllNodeFromListById(T_OMCI_LIST *pList,u8 id);