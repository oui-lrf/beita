#include "GprsRecvHandler.h"
#include "GprsCmdPack.h"
#include "GprsListDrive.h"
#include "GprsConnect.h"
#include "GprsSendCmd.h"
#include "GprsUserSendData.h"
#include "GprsCmdUnPack.h"

u8 CheckCrc16(u8 *data,u16 size)
{
	u16 recvCrc = (data[size-2]<<8) | data[size-1];
	u16 caculCrc = usMBCRC16(data,size-2);
	if(recvCrc == caculCrc)
	{
		return 1;
	}
	else
	{
		Log("recvCrc:%d,caculCrc:%d",recvCrc,caculCrc);
		return 0;
	}
}
//解析接受到的测试指令
u32 CheckErrCmd(u8 *data,u16 size)
{
	u32 err = 0;
	if(size >2 )
	{
		if(!CheckCrc16(data,size))
		{
			return 0;
		}
		FuncObj *typeObj = DecodeFuncObjFromBuff(data,size-2);
		if(typeObj)
		{
			if(typeObj->func == AIR_CMD_ERROR)
			{
				PbDataObj  *dataObj = typeObj->data.arg;
				//返回错误编码
				ErrorObj *errorObj = DecodeErrorObjFromBuff(dataObj->data,dataObj->size);
				if(errorObj)
				{
					err = errorObj->error;
					PbFree(errorObj);
				}
			}
			FuncObjDelete(typeObj);
		}	
	}
	return err;
}

//解析接受到的测试指令
u8 CheckTestCmd(u8 *data,u16 size)
{
	if(size >2 )
	{
		if(!CheckCrc16(data,size))
		{
			return 0;
		}
		FuncObj *typeObj = DecodeFuncObjFromBuff(data,size-2);
		if(typeObj)
		{
			if(typeObj->func == AIR_CMD_TEST)
			{
				return 1;
			}
			FuncObjDelete(typeObj);
		}	
	}
	return 0;
}

// Header:从接收的连接列表里面根据id获取一条连接
// File Name: 
// Author:
// Date:
static ConnectChangeObj * GetRecvConnectFromRecvListById(T_OMCI_LIST *pList,u8 id)
{
	if(!pList)return 0;
	T_OMCI_LIST_NODE *pListNode = pList->pHead->pNext;
	while(pListNode != pList->pHead)
	{
			T_OMCI_LIST_NODE *pTmpNode = pListNode->pNext; //fpTravNode内可能会销毁结点pListNode
			ConnectChangeObj * connectObj= *(ConnectChangeObj **)OmciGetNodeData(pListNode);
			return connectObj;
			pListNode = pTmpNode;
	}
	return 0;
}

// Header:遍历应用连接，如果应用程序有链接，模块没有--删除应用层连接
// File Name: 
// Author:
// Date:
 u8 HandlerRecvConnectListForSvList(T_OMCI_LIST *recvConnectList,T_OMCI_LIST * svList)
{
	CHECK_SINGLE_POINTER(svList->pHead, OMCI_LIST_ERROR);
	CHECK_SINGLE_POINTER(svList->pHead->pNext, OMCI_LIST_ERROR);
	T_OMCI_LIST_NODE *pListNode = svList->pHead->pNext;
	while(pListNode != svList->pHead)
	{
			ConnectObj *connect = *(ConnectObj **)OmciGetNodeData(pListNode);
			if(connect)
			{
				
				Log("HandlerRecvConnectListForSvList id:%d,type:%d,sta:%d,timeOut:%d",connect->id,connect->type,connect->sta,connect->timeOut);
				//如果应用程序有链接（状态不是init），模块没有--删除应用层连接
				//if(connect->sta != CONNECT_INIT)
				{
					ConnectChangeObj * connectObj = GetRecvConnectFromRecvListById(recvConnectList,connect->id);
					if(connectObj)
					{
						Log("recv connectObj:%d,id:%d,sta:%d,type:%d",connectObj,connectObj->id,connectObj->sta,connectObj->type);
					}
					else
					{					
						if(connect->timeOut == 0xffff)
						{
							Log("应用程序有链接,模块没有,但是是长连接-->重新创建连接");		
							//还要告知应用层，重新订阅，或者把订阅消息存入连接链表（暂不确定，模块可能会保持订阅）
							SendConnectCmd(connect);						
							connect->sta = CONNECT_INIT;
						}
						else
						{
							Log("应用程序有链接,模块没有-->删除应用层连接");
							OmciRemoveListNode(svList, pListNode);
						}							
					}					
				}				
			}
			pListNode = pListNode->pNext;	
	}
}

//遍历接收连接，如果应用没有对应连接，发送关闭指令
 LIST_STATUS HandlerRecvConnectListForRecvList(void *pvNode, void *pvTravInfo, uint32_t dwNodeDataSize)
{
	T_OMCI_LIST * svList = (T_OMCI_LIST *)pvTravInfo;
	CHECK_SINGLE_POINTER(pvNode, OMCI_LIST_ERROR);
	T_OMCI_LIST_NODE *pNode = (T_OMCI_LIST_NODE *)pvNode;
	ConnectChangeObj *connectObj = *((ConnectChangeObj **)GET_NODE_DATA(pNode));
	Log("HandlerRecvConnectListForRecvList node:%d,connectObj:%d,id:%d,sta:%d,type:%d",pNode,connectObj,connectObj->id,connectObj->sta,connectObj->type);
  //如果模块有链接，应用程序没有--调用关闭指令
	ConnectObj *connect = GetConnectById(svList,connectObj->id);	
	if(connect)
	{
		//如果模块有链接，应用也有，把应用层状态同步
		if(connect->sta != connectObj->sta)
		{
			if(connect->type == connectObj->type)
			{
				Log("更新应用层状态");
				connect->sta = connectObj->sta;
			}
			else
			{
				Log("链接类型，错误");
			}		
		}
	}
	else
	{
		Log("如果模块有链接，应用程序没有--调用关闭指令");
		SendConnectCloseCmd(connectObj->id,CONNECT_CLOSE);
	}
	return OMCI_LIST_OK;
}


