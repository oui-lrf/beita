#include "GprsDownload.h"
#include "GprsUserSendData.h"
#include "HttpPack.h"
#include "cJson.h"




static QState DownWork_initial(DownWork * const me, QEvt const * const e);

static QState DOWN_TOP(DownWork * const me, QEvt const * const e);
static QState DOWN_ON(DownWork * const me, QEvt const * const e);
static QState DOWN_START(DownWork * const me, QEvt const * const e);
static QState DOWN_CLOSE(DownWork * const me, QEvt const * const e);


typedef struct{
	u8 connectId;
	u32 startPos;
	u32 stopPos;
	u32 allBytes;
	u16 crc16;
	u8 *data;
	u32 dataSize;
}DownRecvObj;


void DeleteDownRecvObj(DownRecvObj *obj)
{
	if(obj)
	{
		GprsFree(obj->data);
		GprsFree(obj);
	}
}
extern QActive * const Gp_GprsWork;

void DownWork_ctor(DownWork * const me) {
    QHsm_ctor(&me->super, Q_STATE_CAST(&DownWork_initial));
}

QState DownWork_initial(DownWork * const me, QEvt const * const e) {
		OmciInitList(&me->downList, sizeof(DownFile *),10,DeleteGprsDownLoadNodeCall);
    return Q_TRAN(&DOWN_CLOSE);
}


static QState DOWN_TOP(DownWork * const me, QEvt const * const e){
    QState status_;
    switch (e->sig) {
        case Q_ENTRY_SIG: {
						Log("+++DOWN_OPEN");
						status_ = Q_HANDLED();	
            break;
        }
				
				case TICK_SIG:{
						 status_ = Q_HANDLED();
							
         break;
        }
				case GPRS_DOWN_SIG:{
					status_ = Q_HANDLED();	
					Log("GPRS_DOWN_SIG");
					//收到的是一个已经申请了内存，没有入库的结点
					T_OMCI_LIST_NODE  * node = (T_OMCI_LIST_NODE *)Q_EVT_CAST(QpEvt)->p;
					DownFile * downObj = *((DownFile **)OmciGetNodeData(node));	
					Log("下载信息存入链表");
					OmciAppendListNode(&me->downList,&downObj);
					break;
				}
				case GPRS_DOWN_RECV_SIG:{
					status_ = Q_HANDLED();	
					Log("GPRS_DOWN_SIG");
					DownRecvObj *downRecvObj = (DownRecvObj *)Q_EVT_CAST(QpEvt)->p;
					DeleteDownRecvObj(downRecvObj);
					break;
				}
        default: {
            status_ = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status_;
}

static QState DOWN_ON(DownWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        case Q_ENTRY_SIG: {
						Log("+++DOWN_ON");
						status_ = Q_HANDLED();	
            break;
        }
        default: {
            status_ = Q_SUPER(&DOWN_TOP);
            break;
        }
    }
    return status_;
}

void HttpDownFileRecvCall(HttpRecvObj *httpRecvObj)
{
	Log("HttpRecvObj id:%d",httpRecvObj->id);
	
	u32 startPos = 0;
	u32 stopPos  = 0;
	u32 allBytes = 0;
	u16 crc16=0;
	u32 bodyLen = 0;
	T_OMCI_LIST *headList = httpRecvObj->head.arg;
	T_OMCI_LIST_NODE *pListNode = headList->pHead->pNext;
	while(pListNode != headList->pHead)
	{
			HttpHead *httpHead = *((HttpHead **)GET_NODE_DATA(pListNode));
			char * headKey = (char *)((PbDataObj  *)(httpHead->key.arg))->data;
			char * headValue = (char *)((PbDataObj  *)(httpHead->value.arg))->data;
			//Log("HttpDownFileRecvCall head node:%d,%s:%s",httpHead,headKey,headValue);
			if(strcmp(headKey,"Content-Range")==0)
			{
				char res[40]={0};
				char *startPosStr = strfind(headValue,"bytes ","-",res,40);
				if(startPosStr)
				{
					startPos = atoi(res);
				}
				char *stopPosStr = strfind(startPosStr,"-","/",res,40);
				if(stopPosStr)
				{
					stopPos = atoi(res);
				}
				char *allByteStr = strfind(stopPosStr,"/"," ",res,40);
				if(allByteStr)
				{
					allBytes = atoi(res);
				}
			}
			else if(strcmp(headKey,"Crc16")==0)
			{
				crc16 = atoi(headValue);
			}
			else if(strcmp(headKey,"Content-Length")==0)
			{
				bodyLen = atoi(headValue);
			}
			pListNode = pListNode->pNext;	
	}
	
	DownRecvObj *downRecvObj = GprsMalloc(sizeof(DownRecvObj));
	if(!downRecvObj)
	{
		Log("Malloc err");
		return ;
	}
	
	
	PbDataObj *bodyDataObj = httpRecvObj->body.arg;
	u8 *data = GprsMalloc(bodyDataObj->size);
	if(!data)
	{
		Log("Malloc err");
		GprsFree(downRecvObj);
		return ;
	}
	memcpy(data,bodyDataObj->data,bodyDataObj->size);
	downRecvObj->data = data;
	downRecvObj->dataSize = bodyDataObj->size;
	downRecvObj->startPos = startPos;
	downRecvObj->stopPos = stopPos;
	downRecvObj->allBytes = allBytes;
	downRecvObj->crc16 = crc16;
	downRecvObj->connectId = httpRecvObj->id;
	
	QpEvt *te;
	te = Q_NEW(QpEvt, GPRS_DOWN_RECV_SIG);
	te->p = (u32)downRecvObj;
	te->v = sizeof(DownRecvObj);
	GprsWork *me = (GprsWork *)Gp_GprsWork;
	QACTIVE_POST(Gp_GprsWork,&te->super,0);	

}
#define DOWNLOAD_HTTP_WAITE 8
//下载完一个文件，从下载链表里面下载下一个文件
//下载完回调,然后从列表删除
static QState DOWN_START(DownWork * const me, QEvt const * const e) {
    QState status_;
		
		static u32 waitTime=0;
    switch (e->sig) {
        case Q_ENTRY_SIG: {
						Log("+++DOWN_START");
						status_ = Q_HANDLED();	
						me->downningNode = OmciGetListNodeByIndex(&me->downList,1);
						if(me->downningNode)
						{
							DownFile * downObj = *((DownFile **)OmciGetNodeData(me->downningNode));	
							Log("开始下载 File:%d,name:%s,url:%s",downObj,downObj->fileName,downObj->url);
							//发送HTTP请求，长连接
							//先检查文件是否存在，如果文件存在，从文件结尾开始下载
							mf_scan_files((u8 *)downObj->filePath);	
							FIL file; 
							char *wholePath = GprsMalloc(strlen(downObj->filePath)+strlen(downObj->fileName)+10);
							sprintf(wholePath,"%s/%s",downObj->filePath,downObj->fileName);	
							Log("wholePath:%s",wholePath);
							u8 res=f_open(&file,(const TCHAR*)wholePath,FA_READ);//模式0,或者尝试打开失败,则创建新文件	 
							if(res==0)
							{
								u32 bwr=0;
								u32 svSize = f_size(&file);
								Log("save fileSize:%d askSize:%d",svSize,downObj->fileSize);
								f_close(&file);
								//刚好等于，下载完成
								if(svSize == downObj->fileSize)
								{
									downObj->packSize =0;
									//回调应用层下载状态
									//下载结束
									Log("DownLoad End");
									if(downObj->userData)
									{
										void (*callBack)(u8,DownFile *) = downObj->userData;
										callBack(1,downObj);
									}
									//下载完成要把下载对象移出链表,开始下一个文件下载
									OmciRemoveListNode(&me->downList, me->downningNode);			
									//检测下载链表是否为空，如果非空开始下一个文件下载
									if(OmciIsListEmpty(&me->downList) != OMCI_LIST_TRUE)
									{
										status_ = Q_TRAN(&DOWN_START);
									}
									else
									{
										status_ = Q_TRAN(&DOWN_CLOSE);
									}								
								}
								//小于，下载未完成，继续下载
								else if(svSize < downObj->fileSize)
								{
									u32 rm = downObj->fileSize - svSize;
									if(rm < downObj->packSize)
									{
										downObj->packSize = rm;
									}
									downObj->downPos = svSize;
								}
								//大于，超了，删除重下
								else 
								{
									if( mf_unlink((u8 *)wholePath) == FR_OK)
									{
										downObj->downPos = 0;
									}
									else
									{
										Log("mf_unlink Err");
										status_ = Q_TRAN(&DOWN_CLOSE);
									}
									
								}
								
							}
							else
							{
								downObj->downPos = 0;
							}
							GprsFree(wholePath);
							GprsHttpConnectObj *http = CreateDownFileHttpConnect(downObj->url,downObj->downPos,downObj->downPos+downObj->packSize,DOWNLOAD_HTTP_WAITE);
							SendGprsHttpConnect(http,HttpDownFileRecvCall,DOWNLOAD_HTTP_WAITE);
							waitTime=DOWNLOAD_HTTP_WAITE*2;
						}		
            break;
        }
			
				case GPRS_DOWN_RECV_SIG:{
					status_ = Q_HANDLED();	
					Log("GPRS_DOWN_RECV_SIG");
					DownRecvObj *downRecvObj = (DownRecvObj *)Q_EVT_CAST(QpEvt)->p;
					Log("downRecvObj startPos:%d,stopPos:%d,allBytes:%d,crc16:%d,dataSize:%d",downRecvObj->startPos,downRecvObj->stopPos,downRecvObj->allBytes,downRecvObj->crc16,downRecvObj->dataSize);
					DownFile * downObj = *((DownFile **)OmciGetNodeData(me->downningNode));	
					
					//校验CRC16通过后写入文件,开始下一包下载
					u16 calCrc = usMBCRC16(downRecvObj->data,downRecvObj->dataSize);
					Log("recvCrc:%d,calculCrc:%d",downRecvObj->crc16,calCrc);
					if(downRecvObj->crc16 == calCrc)//
					{
						//第一包
						//如果文件夹不存在，创建文件夹
						//如果文件已存在，删除文件
						if(downRecvObj->startPos == 0)
						{
							if(mf_opendir((u8 *)downObj->filePath))
							{
								//打开失败
								mf_mkdir((u8 *)downObj->filePath);
							}
							else
							{
								mf_scan_delete_files((u8 *)downObj->filePath);
								mf_scan_files((u8 *)downObj->filePath);							
							}
							mf_closedir();	
						}
						FIL file; 
						char *wholePath = GprsMalloc(strlen(downObj->filePath)+strlen(downObj->fileName)+10);
						sprintf(wholePath,"%s/%s",downObj->filePath,downObj->fileName);	
						Log("wholePath:%s",wholePath);
						u8 res=f_open(&file,(const TCHAR*)wholePath,FA_WRITE|FA_OPEN_ALWAYS);//模式0,或者尝试打开失败,则创建新文件	 
						if(res==0)
						{
							u32 bwr=0;
							u32 size = f_size(&file);
							Log("fileSize:%d",size);
							if(downRecvObj->startPos >size)downRecvObj->startPos = size;
							f_lseek(&file,downRecvObj->startPos);
							res=f_write(&file,downRecvObj->data,downRecvObj->dataSize,&bwr);
							f_close(&file);
							if(res == FR_OK)
							{
								Log("f_write file ok");
								//Log("write:%s",downRecvObj->data);
								//下载未结束
								if(downRecvObj->stopPos < downRecvObj->allBytes)
								{
									u32 rm = downRecvObj->allBytes - downRecvObj->stopPos;
									if(rm < downObj->packSize)
									{
										downObj->packSize = rm;
									}
									downObj->downPos = downRecvObj->startPos+downRecvObj->dataSize;
									
								}
								//下载结束
								else
								{
									downObj->packSize =0;
									//回调应用层下载状态
									//下载结束
									Log("DownLoad End");
									if(downObj->userData)
									{
										void (*callBack)(u8,DownFile *) = downObj->userData;
										callBack(1,downObj);
									}
									//下载完成要把下载对象移出链表,开始下一个文件下载
									OmciRemoveListNode(&me->downList, me->downningNode);			
									//检测下载链表是否为空，如果非空开始下一个文件下载
									if(OmciIsListEmpty(&me->downList) != OMCI_LIST_TRUE)
									{
										status_ = Q_TRAN(&DOWN_START);
									}
									else
									{
										status_ = Q_TRAN(&DOWN_CLOSE);
									}
								}
							}

							
						}
						GprsFree(wholePath);
						
					}
					else//否则重新下载当前包
					{
						Log("crc err recvCrc:%d,calculCrc:%d",downRecvObj->crc16,calCrc);
					}
					DeleteDownRecvObj(downRecvObj);
					//下载没有完成,继续下载
					if(downObj->packSize>0)
					{
						GprsHttpConnectObj *http = CreateDownFileHttpConnect(downObj->url,downObj->downPos,downObj->downPos+downObj->packSize,DOWNLOAD_HTTP_WAITE);
						SendGprsHttpConnect(http,HttpDownFileRecvCall,DOWNLOAD_HTTP_WAITE);	
						waitTime=DOWNLOAD_HTTP_WAITE*2;		
					}
					break;
				}
				case LOOP_TIMEOUT_SIG:{
					status_ = Q_HANDLED();	
					if(waitTime>0)waitTime--;
					Log("LOOP_TIMEOUT_SIG waitTime:%d",waitTime);
						//下载当前包超时也是重新下载当前包
					if(waitTime ==1)
					{
						DownFile * downObj = *((DownFile **)OmciGetNodeData(me->downningNode));	
						GprsHttpConnectObj *http = CreateDownFileHttpConnect(downObj->url,downObj->downPos,downObj->downPos+downObj->packSize,DOWNLOAD_HTTP_WAITE);
						SendGprsHttpConnect(http,HttpDownFileRecvCall,DOWNLOAD_HTTP_WAITE);	
						waitTime=DOWNLOAD_HTTP_WAITE*2;					
					}
				break;
				}
        default: {
            status_ = Q_SUPER(&DOWN_TOP);
            break;
        }
    }
    return status_;
}


static QState DOWN_CLOSE(DownWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        case Q_ENTRY_SIG: {
						Log("+++DOWN_CLOSE");
						status_ = Q_HANDLED();	
            break;
        }
				case GPRS_DOWN_SIG:{
					status_ = Q_HANDLED();	
					Log("GPRS_DOWN_SIG");
					//收到的是一个已经申请了内存，没有入库的结点
					T_OMCI_LIST_NODE  * node = (T_OMCI_LIST_NODE *)Q_EVT_CAST(QpEvt)->p;
					DownFile * downObj = *((DownFile **)OmciGetNodeData(node));	
					Log("下载信息存入链表");
					OmciAppendListNode(&me->downList,&downObj);
					//跳转到开始下载
					status_ = Q_TRAN(&DOWN_START);
					break;
				}							
        default: {
            status_ = Q_SUPER(&DOWN_TOP);
            break;
        }
    }
    return status_;
}


