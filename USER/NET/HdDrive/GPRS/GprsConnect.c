#include "GprsConnect.h"
#include "GprsPort.h"


//连接管理
/////////////////////////////////////////////////////////////
osMutexId  connectMutexId;

void CreateConnectMutex(void)
{
	static 	osMutexDef_t  connectMutex;
	connectMutexId = osMutexCreate(&connectMutex);
}

void DeleteConnectMutex(void)
{
	osMutexDelete(connectMutexId);
}

void WaitConnectMutex(void)
{
	if(connectMutexId && xTaskGetSchedulerState() !=taskSCHEDULER_NOT_STARTED)osMutexWait(connectMutexId, portMAX_DELAY);//
}

void ReleaseConnectMutex(void)
{
	if(connectMutexId && xTaskGetSchedulerState() !=taskSCHEDULER_NOT_STARTED)osMutexRelease(connectMutexId);//
}

u8 staArray[MAX_CONNECT_COUNT];

void FreeConnect(u8 id)
{
	if(id <=0 || id > MAX_CONNECT_COUNT)
	{
		Log("id err");
		return ;
	}
	WaitConnectMutex();
	staArray[id-1]=0;
	ReleaseConnectMutex();
}

u8 MallocConnect(void)
{
	u8 res = 0;
	WaitConnectMutex();
	for(u8 i=0;i<MAX_CONNECT_COUNT;i++)
	{
		if(staArray[i] == 0)
		{
			staArray[i] = 1;
			res = i+1;
			break;
		}
	
	}
	ReleaseConnectMutex();
	return res;
}


void ClearAllConnectSta()
{
	WaitConnectMutex();
	memset(staArray,0,MAX_CONNECT_COUNT);
	ReleaseConnectMutex();
}


void ConnectStaInit(void)
{
	CreateConnectMutex();
	ClearAllConnectSta();
}
//////////////////////////////////////////////////////////////////

// Header:根据id获取一条连接
// File Name: 
// Author:
// Date:
ConnectObj * GetConnectById(T_OMCI_LIST *list,u8 id)
{
	T_OMCI_LIST_NODE *pListNode = NULL;
	
	LIST_ITER_LOOP(list, pListNode)
	{
		ConnectObj *connect = *(ConnectObj **)OmciGetNodeData(pListNode);
		if(connect->id == id )
		{
			return connect;
		}
	}
	return 0;
}

// Header: 根据id从链表删除一条连接
// File Name: 
// Author:
// Date:
u8 DeleteConnectById(T_OMCI_LIST * pList,u8 id)
{
	u8 res = 0;
	CHECK_SINGLE_POINTER(pList->pHead, OMCI_LIST_ERROR);
	CHECK_SINGLE_POINTER(pList->pHead->pNext, OMCI_LIST_ERROR);
	T_OMCI_LIST_NODE *pListNode = pList->pHead->pNext;
	while(pListNode != pList->pHead)
	{
			ConnectObj *connect = *(ConnectObj **)OmciGetNodeData(pListNode);
			if(connect->id == id)
			{
				Log("DeleteConnectById connect:%d id:%d,type:%d",connect,connect->id,connect->type);
				OmciRemoveListNode(pList, pListNode);	
				res = 1;	
			}
			pListNode = pListNode->pNext;	
	}
	return res;
}



// Header:打印一条连接的内容
// File Name: 
// Author:
// Date:
void PrintConnectList(void *pvNodeData, uint32_t dwNodeNum)
{
	CHECK_SINGLE_POINTER(pvNodeData, RETURN_void);
	ConnectObj *connect = *(ConnectObj **)pvNodeData;
	Log("PrintConnectList connect:%d id:%d,type:%d,sta:%d",connect,connect->id,connect->type,connect->sta);
}

