#pragma once
#include "GprsInclude.h"
#include "GprsConnect.h"
#include "qep.h"
#include "Qpc.h"
#include "GprsDownload.h"
void GprsWork_ctor(void);
typedef struct {
/* protected: */
    QActive super;

/* private: */
    QTimeEvt timeEvt;
		QTimeEvt timeEvt2;
		QTimeEvt timeEvtLoop;
	
		QEQueue connectQueue;
		QEvt const *connectQSto[10];
		DownWork downWork;
		char ccid[40+1];
		double lat;
		double lng;
		T_OMCI_LIST connectList;
		T_OMCI_LIST sendList;
} GprsWork;
