#include "GprsCmdPack.h"
#include "GprsPort.h"
#include "pb_common.h"


bool encode_unionmessage(pb_ostream_t *stream, const pb_field_t messagetype[], const void *message)
{
    const pb_field_t *field;
    for (field = ConnectChangeObj_fields; field->tag != 0; field++)
    {
        if (field->ptr == messagetype)
        {
            /* This is our field, encode the message using it. */
            if (!pb_encode_tag_for_field(stream, field))
                return false;
            
            return pb_encode_submessage(stream, messagetype, message);
        }
    }
    
    /* Didn't find the field for messagetype */
    return false;
}
//通信命令打包

u16 CreateConnectChangeCmd(u8 **resBuf,u32 id,u32 sta)
{
	u16 bufSize =  100;
	uint8_t *buffer = 0;
	u16 message_length;
  bool status;
	ConnectChangeObj pbObj={0};
	pbObj.id = id;
	pbObj.sta=sta;
	pb_ostream_t stream ;
	
	buffer = GprsMalloc(bufSize);
	stream = pb_ostream_from_buffer(buffer, bufSize);
	status = pb_encode(&stream, ConnectChangeObj_fields, &pbObj);
	message_length = stream.bytes_written;	
	if(message_length > 0 )
	{
		*resBuf = buffer;
	}
	else
	{
		GprsFree(buffer);
	}
	return message_length;
}


/**
 数组编码的回调函数
 **/
bool EncodeHeadCallBack(pb_ostream_t *stream, const pb_field_t *field, void * const *arg)
{	
		T_OMCI_LIST *headList = (T_OMCI_LIST *)*arg;
		
		Log(" Encode httpHeadList:%d",headList);
		T_OMCI_LIST_NODE *pListNode = headList->pHead->pNext;
		//遍历链表
		while(pListNode != headList->pHead)
		{
				HttpHead *head = *(HttpHead **)OmciGetNodeData(pListNode);
				if(head)
				{
						Log(" Encode key:%s value:%s",((PbDataObj *)(head->key.arg))->data,((PbDataObj *)(head->value.arg))->data);
						head->key.funcs.encode = EncodeBufferCallBack;
						head->value.funcs.encode = EncodeBufferCallBack;
						
						if (!pb_encode_tag_for_field(stream, field))
						return false; 
						 /* This encodes the data for the field, based on our FileInfo structure. */
            if (!pb_encode_submessage(stream, HttpHead_fields, head))
                return false;
						
				}
				pListNode = pListNode->pNext;	
		}
		return true;
}



u16 CreateHttpConnectCmd(u8 **resBuf,u8 id,GprsHttpConnectObj *httpConnectObj)
{
	Log("CreateHttpConnectCmd id:%d",id);
	u16 bufSize =  200;
	uint8_t *buffer = 0;
	u16 message_length;
  bool status;
	CreateHttpObj pbObj={0};
	pbObj.port = 0;
	pbObj.id = id;
	pbObj.waitTime=httpConnectObj->waitTime*1000;
	pbObj.method = httpConnectObj->method;
	pb_ostream_t stream ;
	
	u16 dataSize = strlen(httpConnectObj->url);
	u8 *src_data = (u8 *)httpConnectObj->url;
	if(dataSize >0 && src_data)
	{
		bufSize+=dataSize;
		PbDataObj *obj = GprsMalloc(sizeof(PbDataObj));
		if(!obj)
		{
			return 0;
		}
		u8 *data = GprsMalloc(dataSize);
		if(!data)
		{
			GprsFree(obj);
			return 0;
		}
		memcpy(data,src_data,dataSize);
		obj->data = data;
		obj->size = dataSize;
		pbObj.url.funcs.encode = EncodeBufferCallBack;
		pbObj.url.arg =obj;
	}
	////////////////////////////////
	dataSize = strlen(httpConnectObj->body);
	src_data = (u8 *)httpConnectObj->body;
	if(dataSize >0 && src_data)
	{
		bufSize+=dataSize;
		PbDataObj *obj = GprsMalloc(sizeof(PbDataObj));
		if(!obj)
		{
			DeletePbDataObj(pbObj.url.arg);
			return 0;
		}
		u8 *data = GprsMalloc(dataSize);
		if(!data)
		{
			DeletePbDataObj(pbObj.url.arg);			
			GprsFree(obj);
			return 0;
		}
		memcpy(data,src_data,dataSize);
		obj->data = data;
		obj->size = dataSize;
		pbObj.body.funcs.encode = EncodeBufferCallBack;
		pbObj.body.arg =obj;
	}
	
////////////////////////////////////////////////	
	//还差一个headList
	Log("add encode httpHeadList:%d",httpConnectObj->httpHeadList);
	if(httpConnectObj->httpHeadList)
	{
		pbObj.head.funcs.encode = EncodeHeadCallBack;
		pbObj.head.arg = httpConnectObj->httpHeadList;	
	}

	
	///////////////
	buffer = GprsMalloc(bufSize);
	if(!buffer)
	{
		Log("malloc err");
		DeletePbDataObj(pbObj.body.arg);	
		DeletePbDataObj(pbObj.url.arg);			
		return 0;
	}
	Log("url:%s",((PbDataObj *)pbObj.url.arg)->data);
	stream = pb_ostream_from_buffer(buffer, bufSize);
	//status = encode_unionmessage(&stream, CreateHttpObj_fields, &pbObj);
	CreateConnectObj connectObj={0};
	memcpy(&connectObj.httpCreate,&pbObj,sizeof(CreateHttpObj));
	connectObj.type = ConnectType_CONNECT_TYPE_HTTP;
	status = pb_encode(&stream, CreateConnectObj_fields, &connectObj);
	
	DeletePbDataObj(pbObj.url.arg);
	DeletePbDataObj(pbObj.body.arg);	
	message_length = stream.bytes_written;	
	if(message_length > 0 )
	{
		*resBuf = buffer;
	}
	else
	{
		GprsFree(buffer);
	}
	return message_length;
}



u16 CreateMqttConnectCmd(u8 **resBuf,u8 id,GprsMqttConnectObj *mqttConnectObj)
{
	u16 bufSize =  100;
	uint8_t *buffer = 0;
	u16 message_length;
  bool status;
	CreateMqttObj pbObj={0};
	pbObj.port = mqttConnectObj->port;
	pbObj.id = id;
	pbObj.heartTime=mqttConnectObj->heartTime;
	pb_ostream_t stream ;
	u16 dataSize = strlen(mqttConnectObj->ip);
	u8 * src_data = (u8 *)mqttConnectObj->ip;
	////////////////////////////////////////////////
	if(dataSize >0 && src_data)
	{
		bufSize+=dataSize;
		PbDataObj *obj = GprsMalloc(sizeof(PbDataObj));
		if(!obj)
		{
			return 0;
		}
		u8 *data = GprsMalloc(dataSize);
		if(!data)
		{
			GprsFree(obj);
			return 0;
		}
		memcpy(data,src_data,dataSize);
		obj->data = data;
		obj->size = dataSize;
		pbObj.ip.funcs.encode = EncodeBufferCallBack;
		pbObj.ip.arg =obj;
	}
	////////////////////////////////
	dataSize = strlen(mqttConnectObj->clientId);
	src_data = (u8 *)mqttConnectObj->clientId;
	if(dataSize >0 && src_data)
	{
		bufSize+=dataSize;
		PbDataObj *obj = GprsMalloc(sizeof(PbDataObj));
		if(!obj)
		{
			DeletePbDataObj(pbObj.ip.arg);
			return 0;
		}
		u8 *data = GprsMalloc(dataSize);
		if(!data)
		{
			DeletePbDataObj(pbObj.ip.arg);			
			GprsFree(obj);
			return 0;
		}
		memcpy(data,src_data,dataSize);
		obj->data = data;
		obj->size = dataSize;
		pbObj.clientId.funcs.encode = EncodeBufferCallBack;
		pbObj.clientId.arg =obj;
	}
	///////////////////////////////////////
	dataSize = strlen(mqttConnectObj->userName);
	src_data = (u8 *)mqttConnectObj->userName;
	if(dataSize >0 && src_data)
	{
		bufSize+=dataSize;
		PbDataObj *obj = GprsMalloc(sizeof(PbDataObj));
		if(!obj)
		{
			DeletePbDataObj(pbObj.ip.arg);
			DeletePbDataObj(pbObj.clientId.arg);			
			return 0;
		}
		u8 *data = GprsMalloc(dataSize);
		if(!data)
		{
			DeletePbDataObj(pbObj.ip.arg);
			DeletePbDataObj(pbObj.clientId.arg);
			GprsFree(obj);
			return 0;
		}
		memcpy(data,src_data,dataSize);
		obj->data = data;
		obj->size = dataSize;
		pbObj.userName.funcs.encode = EncodeBufferCallBack;
		pbObj.userName.arg =obj;
	}
	///////////////////////////////////////	
	dataSize = strlen(mqttConnectObj->userPass);
	src_data = (u8 *)mqttConnectObj->userPass;
	if(dataSize >0 && src_data)
	{
		bufSize+=dataSize;
		PbDataObj *obj = GprsMalloc(sizeof(PbDataObj));
		if(!obj)
		{
			DeletePbDataObj(pbObj.ip.arg);
			DeletePbDataObj(pbObj.clientId.arg);
			DeletePbDataObj(pbObj.userName.arg);			
			return 0;
		}
		u8 *data = GprsMalloc(dataSize);
		if(!data)
		{
			DeletePbDataObj(pbObj.ip.arg);
			DeletePbDataObj(pbObj.clientId.arg);
			DeletePbDataObj(pbObj.userName.arg);
			GprsFree(obj);
			return 0;
		}
		memcpy(data,src_data,dataSize);
		obj->data = data;
		obj->size = dataSize;
		pbObj.userPass.funcs.encode = EncodeBufferCallBack;
		pbObj.userPass.arg =obj;
	}
	///////////////////////////////////////	
		dataSize = strlen(mqttConnectObj->will);
		src_data = (u8 *)mqttConnectObj->will;
		if(dataSize >0 && src_data)
		{
			bufSize+=dataSize;
			PbDataObj *obj = GprsMalloc(sizeof(PbDataObj));
			if(!obj)
			{
				DeletePbDataObj(pbObj.ip.arg);
				DeletePbDataObj(pbObj.clientId.arg);
				DeletePbDataObj(pbObj.userName.arg);
				DeletePbDataObj(pbObj.userPass.arg);				
				return 0;
			}
			u8 *data = GprsMalloc(dataSize);
			if(!data)
			{
				DeletePbDataObj(pbObj.ip.arg);
				DeletePbDataObj(pbObj.clientId.arg);
				DeletePbDataObj(pbObj.userName.arg);
				DeletePbDataObj(pbObj.userPass.arg);
				GprsFree(obj);
				return 0;
			}
			memcpy(data,src_data,dataSize);
			obj->data = data;
			obj->size = dataSize;
			pbObj.will.funcs.encode = EncodeBufferCallBack;
			pbObj.will.arg =obj;
		}
	///////////////////////////////////////	
	buffer = GprsMalloc(bufSize);
	if(!buffer)
	{
		Log("malloc err");
		DeletePbDataObj(pbObj.ip.arg);
		DeletePbDataObj(pbObj.clientId.arg);
		DeletePbDataObj(pbObj.userName.arg);
		DeletePbDataObj(pbObj.userPass.arg);
		DeletePbDataObj(pbObj.will.arg);
		return 0;
	}
	stream = pb_ostream_from_buffer(buffer, bufSize);
	//status = encode_unionmessage(&stream, CreateMqttObj_fields, &pbObj);
	
	CreateConnectObj connectObj={0};
	memcpy(&connectObj.mqttCreate,&pbObj,sizeof(CreateMqttObj));
	connectObj.type = ConnectType_CONNECT_TYPE_MQTT;
	status = pb_encode(&stream, CreateConnectObj_fields, &connectObj);
	
	DeletePbDataObj(pbObj.ip.arg);
	DeletePbDataObj(pbObj.clientId.arg);
	DeletePbDataObj(pbObj.userName.arg);
	DeletePbDataObj(pbObj.userPass.arg);
	DeletePbDataObj(pbObj.will.arg);
	message_length = stream.bytes_written;	
	if(message_length > 0 )
	{
		*resBuf = buffer;
	}
	else
	{
		DeletePbDataObj(pbObj.ip.arg);
		DeletePbDataObj(pbObj.clientId.arg);
		DeletePbDataObj(pbObj.userName.arg);
		DeletePbDataObj(pbObj.userPass.arg);
		DeletePbDataObj(pbObj.will.arg);
		GprsFree(buffer);
	}
	return message_length;
}


u16 CreateMqttSendCmd(u8 **resBuf,u8 id,MqttSendType type,char *topic,u8 *dat,u16 datSize,u8 qos)
{
	u16 bufSize =  100;
	uint8_t *buffer = 0;
	u16 message_length;
  bool status;
	MqttSendObj pbObj={0};
	pbObj.id = id;
	pbObj.type = type;
	pbObj.qos = qos;
	pb_ostream_t stream ;
	u8 *src_data = (u8 *)topic;
	u16 dataSize = strlen(topic);
	if(dataSize >0 && src_data)
	{
		bufSize+=dataSize;
		PbDataObj *obj = GprsMalloc(sizeof(PbDataObj));
		if(!obj)
		{
			return 0;
		}
		u8 *data = GprsMalloc(dataSize);
		if(!data)
		{
			GprsFree(obj);
			return 0;
		}
		memcpy(data,src_data,dataSize);
		obj->data = data;
		obj->size = dataSize;
		pbObj.topic.funcs.encode = EncodeBufferCallBack;
		pbObj.topic.arg =obj;
	}
		///////////////////////////////////////	
	dataSize = datSize;
	src_data = (u8 *)dat;
	if(dataSize >0 && src_data)
	{
		bufSize+=dataSize;
		PbDataObj *obj = GprsMalloc(sizeof(PbDataObj));
		if(!obj)
		{
			DeletePbDataObj(pbObj.topic.arg);	
			return 0;
		}
		u8 *data = GprsMalloc(dataSize);
		if(!data)
		{
			DeletePbDataObj(pbObj.topic.arg);
			GprsFree(obj);
			return 0;
		}
		memcpy(data,src_data,dataSize);
		obj->data = data;
		obj->size = dataSize;
		pbObj.data.funcs.encode = EncodeBufferCallBack;
		pbObj.data.arg =obj;
	}
	///////////////////////////////////////	
	
	buffer = GprsMalloc(bufSize);
	if(!buffer)
	{
		Log("malloc err");
		DeletePbDataObj(pbObj.topic.arg);
		DeletePbDataObj(pbObj.data.arg);
		return 0;
	}
	stream = pb_ostream_from_buffer(buffer, bufSize);
	status = pb_encode(&stream, MqttSendObj_fields, &pbObj);
	DeletePbDataObj(pbObj.topic.arg);
	DeletePbDataObj(pbObj.data.arg);
	message_length = stream.bytes_written;	
	if(message_length > 0 )
	{
		*resBuf = buffer;
	}
	else
	{
		DeletePbDataObj(pbObj.topic.arg);
		DeletePbDataObj(pbObj.data.arg);
		GprsFree(buffer);
	}
	return message_length;
}




u16 CreateTcpSendCmd(u8 **resBuf,u8 id,u8 *src_data,u16 dataSize)
{
	u16 bufSize =  100;
	uint8_t *buffer = 0;
	u16 message_length;
  bool status;
	TcpSendObj pbObj={0};
	pbObj.id = id;
	pb_ostream_t stream ;

	if(dataSize >0 && src_data)
	{
		bufSize+=dataSize;
		PbDataObj *obj = GprsMalloc(sizeof(PbDataObj));
		if(!obj)
		{
			return 0;
		}
		u8 *data = GprsMalloc(dataSize);
		if(!data)
		{
			GprsFree(obj);
			return 0;
		}
		memcpy(data,src_data,dataSize);
		obj->data = data;
		obj->size = dataSize;
		pbObj.data.funcs.encode = EncodeBufferCallBack;
		pbObj.data.arg =obj;
	}
	buffer = GprsMalloc(bufSize);
	if(!buffer)
	{
		DeletePbDataObj(pbObj.data.arg);
		return 0;
	}
	stream = pb_ostream_from_buffer(buffer, bufSize);
	status = pb_encode(&stream, TcpSendObj_fields, &pbObj);
	DeletePbDataObj(pbObj.data.arg);
	message_length = stream.bytes_written;	
	if(message_length > 0 )
	{
		*resBuf = buffer;
	}
	else
	{
		GprsFree(buffer);
	}
	return message_length;
}

u16 CreateFuncCmd(u8 **resBuf,AIR_CMD cmd,u8 *src_data,u16 dataSize)
{
	u16 bufSize =  100;
	uint8_t *buffer = 0;
	u16 message_length;
  bool status;
	FuncObj pbObj={0};
	pbObj.func = cmd;
	pb_ostream_t stream ;
	if(dataSize >0 && src_data)
	{
		bufSize+=dataSize;
		PbDataObj *obj = PbMalloc(sizeof(PbDataObj));
		if(!obj)
		{
			return 0;
		}
		u8 *data = PbMalloc(dataSize);
		if(!data)
		{
			GprsFree(obj);
			return 0;
		}
		memcpy(data,src_data,dataSize);
		obj->data = data;
		obj->size = dataSize;
		pbObj.data.funcs.encode = EncodeBufferCallBack;
		pbObj.data.arg =obj;
	}
	buffer = PbMalloc(bufSize);
	if(!buffer)
	{
		Log("malloc err");
		if(pbObj.data.arg)
		{
			PbDataObj *obj = pbObj.data.arg;
			PbFree(obj->data);
			PbFree(obj);
		}
		return 0;
	}
	memset(buffer, 0, bufSize);
	stream = pb_ostream_from_buffer(buffer, bufSize);
	status = pb_encode(&stream, FuncObj_fields, &pbObj);
	DeletePbDataObj(pbObj.data.arg);
	message_length = stream.bytes_written;	
	Log("status:%d,message_length:%d",status,message_length);
	if(status && message_length > 0 )
	{
		*resBuf = buffer;
	}
	else
	{
		PbFree(buffer);
	}
	return message_length;
}



u16 CreateTcpConnectCmd(u8 **resBuf,u8 id,GprsTcpConnectObj *tcpCreateObj)
{
	Log("CreateTcpConnectCmd");
	u16 bufSize =  1024;
	uint8_t *buffer = 0;
	u16 message_length;
  bool status;
	CreateTcpObj pbObj={0};
	pbObj.port = tcpCreateObj->port;
	pbObj.id = id;
	pb_ostream_t stream ;
	u16 dataSize = strlen(tcpCreateObj->ip);
	u8 * src_data = (u8 *)tcpCreateObj->ip;
	
	if(dataSize >0 && src_data)
	{
		bufSize+=dataSize;
		PbDataObj *obj = GprsMalloc(sizeof(PbDataObj));
		if(!obj)
		{
			return 0;
		}
		u8 *data = GprsMalloc(dataSize);
		if(!data)
		{
			GprsFree(obj);
			return 0;
		}
		memcpy(data,src_data,dataSize);
		obj->data = data;
		obj->size = dataSize;
		pbObj.ip.funcs.encode = EncodeBufferCallBack;
		pbObj.ip.arg =obj;
	}
	buffer = GprsMalloc(bufSize);
	if(!buffer)
	{
		Log("malloc err");
		if(pbObj.ip.arg)
		{
			PbDataObj *obj = pbObj.ip.arg;
			PbFree(obj->data);
			PbFree(obj);
		}
		return 0;
	}
	stream = pb_ostream_from_buffer(buffer, bufSize);
	
	CreateConnectObj connectObj={0};
	memcpy(&connectObj.tcpCreate,&pbObj,sizeof(CreateTcpObj));
	connectObj.type = ConnectType_CONNECT_TYPE_TCP;
	status = pb_encode(&stream, CreateConnectObj_fields, &connectObj);
	DeletePbDataObj(pbObj.ip.arg);
	message_length = stream.bytes_written;	
	if(message_length > 0 )
	{
		*resBuf = buffer;
	}
	else
	{
		GprsFree(buffer);
	}
	return message_length;
}
