#ifndef __GPRSPORT_H__
#define __GPRSPORT_H__

#include "GprsInclude.h"
#include "GprsConnect.h"
#include "air.pb.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum{
	NET_STA_CLOSE,//关闭	
	NET_STA_OPENING,//打开中
	NET_STA_CONNECTEDING,//连接中
	NET_STA_CONNECTED,//已连接
}NET_STA;	
	
#define GprsMalloc(n) mymalloc(SRAMIN,n)
#define GprsFree(n)  myfree(SRAMIN,n)
	
#define GPRS_PIN_RST PAout(7)
#define GPRS_PW PGout(10)// 

void GprsSwInit(void);
void OpenGprsTcp(char *ip,u16 port);
void GprsPortSwOpen(void);

void GprsPortSwClose(void);

//受上层调用，释放所需资源
void GprsInit(void);
void GprsDeInit(void);
void GprsWorkLoop(void);
u8 GetGprsLive(void);

#define ICCID_LEN 40

typedef struct {
	char *filePath;
	char *fileName;
	char *url;
	u16 fileCrc16;
	void *userData;
	u32 fileSize;
	u32 packSize;
	u32 downPos;
}DownFile;
	
typedef struct{
	u8 id;
	u8 *data;
	u32 dataSize;
	u32 recvTimeOut;
	void *info;
}GprsTcpSendObj;

typedef struct{
	char *ip;
	u16 port;
}GprsTcpConnectObj;

typedef struct{
	char *ip;
	u16 port;
	char *clientId;
	char *userName;
	char *userPass;
	char *will;
	u32 heartTime;
}GprsMqttConnectObj;

typedef struct{
	T_OMCI_LIST *httpHeadList;
	HttpMethod method;
	char *url;
	u32 waitTime;
	char *body;
}GprsHttpConnectObj;

typedef struct{
	char *topic;
}GprsMqttSubObj;

typedef struct{
	char *topic;
	u8 *data;
	u16 size;
	u8 qos;
}GprsMqttPubObj;

typedef struct{
	u8 id;
	MqttSendType type;
	void *obj; 
}GprsMqttSendObj;

u8 GprsTcpSend(	u8 id,u8 *data,u32 dataSize,u16 recvTimeOut,void *info);
u8 GprsCreateMqtt(char *ip,u16 port,char *clientId,char *userName,char *userPass,char *will,u16 heartTime,void (*RecvCall)(MqttRecvObj *));
u8 CreateTcpConnect(char *ip,u16 port,u16 timeOut,void (*RecvCall)(TcpRecvObj *));
void DeleteConnectObj(ConnectObj *obj);
void GprsCreateTcpObjDelete(GprsTcpConnectObj *dataObj);
u8 PostGprsSendObj(T_OMCI_LIST_NODE *pInsertNode);
u8 GprsMqttPub(u8 id,char *topic,u8 *data,u16 size);
u8 GprsMqttSub(u8 id,char *topic);
u8 SendGprsHttpConnect(GprsHttpConnectObj *http,void (*RecvCall)(HttpRecvObj *),u32 waitTime);

//packSize 每包下载的字节数
u8 GprsDownLoad(char *path,char *name,char *url,u16 crc16,u32 fileSize,u32 packSize,void (*callBack)(u8,DownFile *));

#if 1
	#define GprsLog Log
#else
     #define GprsLog(...)
#endif
#ifdef __cplusplus
}
#endif
#endif
