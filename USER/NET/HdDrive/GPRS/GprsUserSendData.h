#pragma once
#include "GprsInclude.h"
#include "GprsConnect.h"

typedef struct{
	CONNECT_TYPE type;
	void *obj;
}GprsSendData;

	
//��������������
LIST_STATUS TraverseSendListNode(T_OMCI_LIST *pList);

void GprsSendDataObjDelete(GprsSendData * obj);
u8 PackGprsObj(void *obj,CONNECT_TYPE type);
void DeleteTcpSendData(GprsTcpSendObj * tcpObj);
GprsTcpSendObj * CreateTcpSendData(	u8 id,u8 *data,u32 dataSize,u32 recvTimeOut,void *info);
GprsSendData *CreateGprsSendObj(void *obj,CONNECT_TYPE type);
void DeleteGprsSendObj(GprsSendData * sendObj);
void DeleteGprsMqttPubObj(GprsMqttPubObj *dataObj);
GprsMqttPubObj * CreateGprsMqttPubObj(char *topic,u8 *data,u16 size);
void DeleteGprsMqttSendObj(GprsMqttSendObj *obj);
GprsMqttSendObj *CreateGprsMqttSendObj(u8 id,MqttSendType type,void *obj);
void DeleteGprsSendObj(GprsSendData * sendObj);	

void DeleteGprsMqttSubObj(GprsMqttSubObj *dataObj);

GprsMqttSubObj * CreateGprsMqttSubObj(u8 id,char *topic);

void DeleteGprsTcpConnectObj(GprsTcpConnectObj *dataObj);

GprsTcpConnectObj *CreateGprsTcpConnectObj(char *ip,u16 port);

void DeleteGprsMqttConnectObj(GprsMqttConnectObj *dataObj);

GprsMqttConnectObj * CreateGprsMqttConnectObj(char *ip,u16 port,char *clientId,char *userName,char *userPass,char *will,u32 heartTime);

void DeleteConnectObj(ConnectObj *obj);

ConnectObj * CreateGprsConnectObj(u8 type,void *obj,u32 timeOut,void *info);

////////////////////////////////////////////////////////////////////

void DeleteHeadObj(HttpHead *head);

HttpHead *CreateHead(char *key,char *value);
void DeleteGprsHttpConnectObj(GprsHttpConnectObj *http);
GprsHttpConnectObj * CreateGprsHttpConnectObj(HttpMethod method,char *url,u32 waitTime);
u8 AddHttpHead(GprsHttpConnectObj *http,char *headKey,char *headValue);

u8 AddHttpBody(GprsHttpConnectObj *http,char *body);
//////////////////////////////////////////////////////////////////////
void DeleteDownLoadFile(DownFile *file);

DownFile * CreateDownLoad(char *path,char *name,char *url,u16 crc16,u32 fileSize,u32 packSize,void (*callBack)(u8,DownFile *));
void DeleteGprsDownLoadNodeCall(void * pNode);
