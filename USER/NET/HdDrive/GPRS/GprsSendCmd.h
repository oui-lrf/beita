#ifndef __GPRSDRIVE_H
#define __GPRSDRIVE_H

#include "GprsInclude.h"

 void UsartAddCrcSend(u8 *data,u16 size);
 void SendTestCmd(void);
 void SendConnectCloseCmd(u8 id,u8 sta);
void SendTcpObj(GprsTcpSendObj * tcpObj);
void SendMqttObj(GprsMqttSendObj * mqttObj);
void SendConnectSelectCmd(void);
 void SendGetInfoCmd(void);
void SendMqttSubCmd(u8 id,GprsMqttSubObj *obj);
void SendMqttPubCmd(u8 id,GprsMqttPubObj *obj); 
u8 SendConnectCmd(ConnectObj *connectObj);
#endif
