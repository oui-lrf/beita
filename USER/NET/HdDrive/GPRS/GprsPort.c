#include "GprsPort.h"
#include "GuiPort.h"
#include "GprsUserSendData.h"
#include "GprsWork.h"



Q_DEFINE_THIS_MODULE("GprsPort")
extern QActive * const Gp_GprsWork;

void GprsSignalChangeCall(u16 value)
{
	SendUiMsg(SCREEN_MAIN,HEAD_NET_SIGNAL,value);
}

void GprsStaChangeCall(NET_STA sta)
{
	SendUiMsg(SCREEN_MAIN,HEAD_NET_STA,sta);
}

u8 PostGprsDownLoad(T_OMCI_LIST_NODE *pInsertNode)
{
	QpEvt *te;
	
	te = Q_NEW(QpEvt, GPRS_DOWN_SIG);
	te->p = (u32)pInsertNode;
	te->v = sizeof(T_OMCI_LIST_NODE);
	GprsWork *me = (GprsWork *)Gp_GprsWork;
	QACTIVE_POST(Gp_GprsWork,&te->super,0);
	return 1;
}


u8 GprsDownLoad(char *path,char *name,char *url,u16 crc16,u32 fileSize,u32 packSize,void (*callBack)(u8,DownFile *))
{
	DownFile * downObj = CreateDownLoad(path,name,url,crc16,fileSize,packSize,callBack);
	if(!downObj)
	{
		Log("downObj err");
		return 0;
	}

	//通过指定节点后跟的字节数量创建节点
	T_OMCI_LIST_NODE *pInsertNode = CreateListNodeDataSize(&downObj, sizeof(DownFile *));
	if(NULL == pInsertNode)
	{
			Log("CreateListNodeDataSize err");
			DeleteDownLoadFile(downObj);
			return 0;
	}
	PostGprsDownLoad(pInsertNode);
	return 1;
}

u8 PostGprsSendObj(T_OMCI_LIST_NODE *pInsertNode)
{
	QpEvt *te;
	te = Q_NEW(QpEvt, GPRS_SEND_SIG);
	te->p = (u32)pInsertNode;
	te->v = sizeof(T_OMCI_LIST_NODE);
	QACTIVE_POST(Gp_GprsWork,&te->super,0);
	return 1;
}


u8 GprsTcpSend(	u8 id,u8 *data,u32 dataSize,u16 recvTimeOut,void *info)
{
	return 0;
	GprsTcpSendObj * tcpObj = CreateTcpSendData(id,data,dataSize,recvTimeOut,info);
	if(!tcpObj)
	{
		Log("CreateTcpSendData err");
		return 0;
	}
	GprsSendData *gprsSendData= CreateGprsSendObj(tcpObj,ConnectType_CONNECT_TYPE_TCP);
	if(!gprsSendData)
	{
		DeleteTcpSendData(tcpObj);
		return 0;
	}
	//通过指定节点后跟的字节数量创建节点
	T_OMCI_LIST_NODE *pInsertNode = CreateListNodeDataSize(&gprsSendData, sizeof(GprsSendData *));
	if(NULL == pInsertNode)
	{
			Log("CreateListNodeDataSize err");
			DeleteGprsSendObj(gprsSendData);
			return 0;
	}
	PostGprsSendObj(pInsertNode);
	return 1;
}


u8 GprsMqttPub(u8 id,char *topic,u8 *data,u16 size)
{
	GprsMqttPubObj * pubObj = CreateGprsMqttPubObj(topic,data,size);
	if(!pubObj)
	{
		Log("CreateTcpSendData err");
		return 0;
	}
	GprsMqttSendObj *gprsMqttSendObj= CreateGprsMqttSendObj(id,MqttSendType_MQTT_PUB,pubObj);
	if(!gprsMqttSendObj)
	{
		DeleteGprsMqttPubObj(pubObj);
		return 0;
	}
	Log("GprsMqttPubObj:%d,GprsMqttSendObj:%d",pubObj,gprsMqttSendObj);
	GprsSendData *gprsSendData= CreateGprsSendObj(gprsMqttSendObj,ConnectType_CONNECT_TYPE_MQTT);
	if(!gprsSendData)
	{
		DeleteGprsMqttSendObj(gprsMqttSendObj);
		return 0;
	}
	//通过指定节点后跟的字节数量创建节点
	T_OMCI_LIST_NODE *pInsertNode = CreateListNodeDataSize(&gprsSendData, sizeof(GprsMqttSendObj *));
	if(NULL == pInsertNode)
	{
			Log("CreateListNodeDataSize err");
			DeleteGprsSendObj(gprsSendData);
			return 0;
	}
	PostGprsSendObj(pInsertNode);
	return 1;
}

u8 GprsMqttSub(u8 id,char *topic)
{
	GprsMqttSubObj * subObj = CreateGprsMqttSubObj(id,topic);
	if(!subObj)
	{
		Log("CreateTcpSendData err");
		return 0;
	}
	GprsMqttSendObj *gprsMqttSendObj= CreateGprsMqttSendObj(id,MqttSendType_MQTT_SUB,subObj);
	if(!gprsMqttSendObj)
	{
		DeleteGprsMqttSubObj(subObj);
		return 0;
	}
	
	GprsSendData *gprsSendData= CreateGprsSendObj(gprsMqttSendObj,ConnectType_CONNECT_TYPE_MQTT);
	if(!gprsSendData)
	{
		DeleteGprsMqttSendObj(gprsMqttSendObj);
		return 0;
	}
	//通过指定节点后跟的字节数量创建节点
	T_OMCI_LIST_NODE *pInsertNode = CreateListNodeDataSize(&gprsSendData, sizeof(GprsMqttSendObj *));
	if(NULL == pInsertNode)
	{
			Log("CreateListNodeDataSize err");
			DeleteGprsSendObj(gprsSendData);
			return 0;
	}
	PostGprsSendObj(pInsertNode);
	return 1;
}


u8 GprsCreateMqtt(char *ip,u16 port,char *clientId,char *userName,char *userPass,char *will,u16 heartTime,void (*RecvCall)(MqttRecvObj *))
{	
	GprsMqttConnectObj *mqttConnectObj = CreateGprsMqttConnectObj(ip,port,clientId,userName,userPass,will,heartTime);
	if(!mqttConnectObj)
	{
		GprsLog("Malloc err");
		return 0;
	}
	
	
	ConnectObj *connect = CreateGprsConnectObj(ConnectType_CONNECT_TYPE_MQTT,mqttConnectObj,0xffff,RecvCall);
	if(!connect)
	{
		GprsLog("Malloc err");
		DeleteGprsMqttConnectObj(mqttConnectObj);		
		return 0;
	}
	QpEvt *te;
	te = Q_NEW(QpEvt, GPRS_CONNECT_SIG);
	te->p = (uint32_t)connect;
	te->v = sizeof(ConnectObj);
	QACTIVE_POST(Gp_GprsWork, &te->super, me);	
	return connect->id;
}

u8 SendGprsHttpConnect(GprsHttpConnectObj *http,void (*RecvCall)(HttpRecvObj *),u32 waitTime)
{
	ConnectObj *connect = CreateGprsConnectObj(ConnectType_CONNECT_TYPE_HTTP,http,waitTime,RecvCall);
	if(!connect)
	{
		GprsLog("Malloc err");
		DeleteGprsHttpConnectObj(http);		
		return 0;
	}
	QpEvt *te;
	te = Q_NEW(QpEvt, GPRS_CONNECT_SIG);
	te->p = (uint32_t)connect;
	te->v = sizeof(ConnectObj);
	QACTIVE_POST(Gp_GprsWork, &te->super, me);	
	return connect->id;
}


//创建连接
u8 CreateTcpConnect(char *ip,u16 port,u16 timeOut,void (*RecvCall)(TcpRecvObj *))
{
		GprsTcpConnectObj *tcpCreateObj = CreateGprsTcpConnectObj(ip,port);
		if(!tcpCreateObj)
		{
			Log("malloc err");
			return 0;
		}
		ConnectObj *connect = CreateGprsConnectObj(ConnectType_CONNECT_TYPE_TCP,tcpCreateObj,timeOut,RecvCall);
		if(!connect)
		{
			DeleteGprsTcpConnectObj(tcpCreateObj);
			return 0;
		}
		QpEvt *te;
		te = Q_NEW(QpEvt, GPRS_CONNECT_SIG);
		te->p = (uint32_t)connect;
		te->v = sizeof(ConnectObj);
		QACTIVE_POST(Gp_GprsWork, &te->super, me);	
		return connect->id;
}

//改函数只能又一个管理线程调用，可不多线程调用
//受上层调用，初始化所需资源，如定时器，状态机等
void GprsInit(void)
{
//	printf("GprsInit sta:%d\r\n",Gp_GprsWork->super.state.fun);
	if(Gp_GprsWork->super.state.fun == 0)
	{
		static StackType_t GprsWorkStack[1024];
		static const QEvt *GprsWork_queueSto[20];
		GprsWork_ctor();
		QActive_setAttr(Gp_GprsWork, TASK_NAME_ATTR, "GprsWork");
		
		QACTIVE_START(Gp_GprsWork,          /* AO to start */
		(uint_fast8_t)(2U), /* QP priority of the AO */
		GprsWork_queueSto,               /* event queue storage */
		Q_DIM(GprsWork_queueSto),        /* queue length [events] */
		GprsWorkStack,                  /* stack storage */
		sizeof(GprsWorkStack),          /* stack size [bytes] */
		(QEvt *)0);                  /* initialization event (not used) */
	}
}


//受上层调用，释放所需资源
void GprsDeInit(void)
{
	GprsLog("GprsDeInit");
	if(Gp_GprsWork->super.state.fun != 0)
	{
		QACTIVE_POST(Gp_GprsWork, &Q_NEW(QpEvt, GPRS_CLOSE_SIG)->super, me);
	}
}



