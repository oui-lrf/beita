#include "GprsSendCmd.h"    /* Board Support Package interface */
#include "GprsWork.h"
#include "GprsCmdPack.h"
#include "mbcrc.h"
#include "GprsRecvHandler.h"
#include "GprsPort.h"
#include "GprsUserSendData.h"
#include "GprsListDrive.h"
#include "GprsCmdUnPack.h"


extern void GprsStaChangeCall(NET_STA sta);

/* protected: */

extern void GprsSignalChangeCall(u16 value);

static QState GprsWork_initial(GprsWork * const me, QEvt const * const e);

static QState GPRS_TOP(GprsWork * const me, QEvt const * const e);
static QState GPRS_OPEN(GprsWork * const me, QEvt const * const e);
static QState GPRS_OPEN_RST(GprsWork * const me, QEvt const * const e);
static QState GPRS_OPEN_RST_AT(GprsWork * const me, QEvt const * const e);
static QState GPRS_RST(GprsWork * const me, QEvt const * const e);
static QState GPRS_CONNECT(GprsWork * const me, QEvt const * const e);


static GprsWork gprsWork;
QActive * const Gp_GprsWork = &gprsWork.super;
extern QActive * const US_UsartWork;

Q_DEFINE_THIS_MODULE("GprsWork")

extern QActive * const TP_TcpWork;



void GprsWork_ctor(void) {
		GprsLog("GprsWork_ctor");
    GprsWork *me = (GprsWork *)Gp_GprsWork;
	
    QActive_ctor(&me->super, Q_STATE_CAST(&GprsWork_initial));
    QTimeEvt_ctorX(&me->timeEvt, &me->super, TIMEOUT_SIG, 0U);
		QTimeEvt_ctorX(&me->timeEvt2, &me->super, TIMEOUT2_SIG, 0U);
		QTimeEvt_ctorX(&me->timeEvtLoop, &me->super, LOOP_TIMEOUT_SIG, 0U);
		DownWork_ctor(&me->downWork);
		QEQueue_init(&me->connectQueue,me->connectQSto, Q_DIM(me->connectQSto));
}


static QState GprsWork_initial(GprsWork * const me, QEvt const * const e) {
		GprsLog("GprsWork_initial");
		GPRS_PIN_RST = 1;	
    OmciInitList(&me->connectList, sizeof(ConnectObj *),10,DeleteConnectNodeCall);
		OmciInitList(&me->sendList, sizeof(GprsSendData *),10,DeleteSendNodeCall);
		QHSM_INIT((QHsm *)&me->downWork, (QEvt *)0);
		ConnectStaInit();
		return Q_TRAN(&GPRS_OPEN);
}

//在所有状态下都可以接收关闭命令
static QState GPRS_TOP(GprsWork * const me, QEvt const * const e) {
    QState status_;
	
    switch (e->sig) {
        /*${Gprs::GprsWork::SM::GPRS_OPEN} */
        case Q_ENTRY_SIG: {
						GprsLog("+++GPRS_TOP");
						QACTIVE_POST(US_UsartWork, &Q_NEW(QpEvt, USART_SWITCH_GPRS)->super, me);
						QTimeEvt_armX(&me->timeEvtLoop,BSP_TICKS_PER_SEC,BSP_TICKS_PER_SEC);
						status_ = Q_HANDLED();	
            break;
        }
				case TICK_SIG:{
						status_ = Q_HANDLED();
						QHSM_DISPATCH(&me->downWork.super,e);
            break;
        }
				case GPRS_DOWN_SIG:{
						status_ = Q_HANDLED();
            QHSM_DISPATCH((QHsm *)&me->downWork, e);
            break;
					break;
				}
				case GPRS_DOWN_RECV_SIG:{
						status_ = Q_HANDLED();
            QHSM_DISPATCH((QHsm *)&me->downWork, e);
            break;
					break;
				}
				//还没有连上就发送，数据，直接把数据释放，并上报数据发送失败
				case GPRS_SEND_SIG:{
					status_ = Q_HANDLED();	
					Log("TCP_Gprs TCP_SEND_SIG");
					T_OMCI_LIST_NODE *pInsertNode = (T_OMCI_LIST_NODE *)Q_EVT_CAST(QpEvt)->p;
					GprsLog("未打开,待发送数据存入链表");
					if(OmciAppendListPackNode(&me->sendList,pInsertNode) == OMCI_LIST_OK)
					{
						GprsLog("OmciAppendListNode ok");
					}
					else 
					{
						RemoveListNode(&me->sendList,pInsertNode);
						GprsLog("OmciAppendListNode failed");
					}
					
					break;
				}
				case GPRS_CONNECT_SIG:{
					status_ = Q_HANDLED();	
					Log("GPRS_CONNECT_SIG");				
					ConnectObj *connectObj = (ConnectObj *)(Q_EVT_CAST(QpEvt)->p);
					if(connectObj)
					{
						if(me->connectQueue.nFree >=2)
						{
							if(QActive_defer(&me->super, &me->connectQueue,e))
							{
								GprsLog("save connect create ok");
							}
							else 
							{
								DeleteConnectObj(connectObj);
							}
						}
						else
						{
							DeleteConnectObj(connectObj);
						}
					}
					break;
				}
				//在任何状态下一定要订阅消息接收，防止内存泄漏
				case USART_WORK_RECV_SIG:
					Log("TCP_Gprs USART_WORK_RECV_SIG");
					u8 *rdata = (u8 *)(Q_EVT_CAST(QpEvt)->p);
					if(rdata)
					{
						GprsFree(rdata);
					}
					status_ = Q_HANDLED();	
				break;
			 	case LOOP_TIMEOUT_SIG:{
					status_ = Q_HANDLED();	
					QHSM_DISPATCH((QHsm *)&me->downWork, e);
				break;
				}
				case Q_EXIT_SIG:{
					status_ = Q_HANDLED();	
					break;
				}		
        default: {
            status_ = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status_;
}


static QState GPRS_OPEN(GprsWork * const me, QEvt const * const e){
    QState status_;
		static u8 timer = 0;
    switch (e->sig) {
        /*${Gprs::GprsWork::SM::GPRS_OPEN} */
        case Q_ENTRY_SIG: {
						status_ = Q_HANDLED();	
						GprsLog("+++GPRS_OPEN");		
						GprsStaChangeCall(NET_STA_OPENING);
						SendTestCmd();
						GPRS_PW = 1;
						QTimeEvt_disarm(&me->timeEvt);
						QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*2,0);
						timer = 0;
            break;
        }
        /*${Gprs::GprsWork::SM::GPRS_OPEN::GPRS_TIMEOUT} */
        case TIMEOUT_SIG: {
						status_ = Q_HANDLED();
						GprsLog("GPRS_OPEN TIMEOUT_SIG:%d",timer);			
						if(timer++ >10)
						{
							timer = 0;
							GprsLog("START GPRS_OPEN_RST");			
							status_ = Q_TRAN(&GPRS_OPEN_RST);
						}
						else
						{
							GPRS_PW =!GPRS_PW;
							SendTestCmd();
							QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*3,0);
						}
        break;
        } 
				 case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            u8 *data = (u8 *)(Q_EVT_CAST(QpEvt)->p);
						u16 size = (u16)(Q_EVT_CAST(QpEvt)->v);
						if(data)
						{
							LogHex("recv:",data,size);
							if(CheckTestCmd(data,size))
							{
								GprsLog("GPRS_OPEN OK");
								QTimeEvt_disarm(&me->timeEvt);
								GPRS_PW =1;
								status_ = Q_TRAN(&GPRS_CONNECT);
							}
							else
							{
								GprsLog("GPRS_OPEN ERR,WAIT AGIN");
							}								
							GprsFree(data);
					}	
          break;
        }
				case Q_EXIT_SIG:{
					status_ = Q_HANDLED();	
					Log("~~~ GPRS_TYPE_TEST");
					QTimeEvt_disarm(&me->timeEvt);
					break;
				}
        default: {
            status_ = Q_SUPER(&GPRS_TOP);
            break;
        }
    }
    return status_;
}


static QState GPRS_OPEN_RST(GprsWork * const me, QEvt const * const e)
{
	  QState status_;
		static u8 count=0;
    switch (e->sig) {
        /*${Gprs::GprsWork::SM::GPRS_AT} */
        case Q_ENTRY_SIG: {
						GprsLog("+++GPRS_OPEN_RST");
						GprsLog("GPRS_OPEN_RST Pull Down");
						count = 0;
						GPRS_PIN_RST = 0;
						QTimeEvt_disarm(&me->timeEvt); 
						QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*1,0);
						status_ = Q_HANDLED();
            break;
        }
				case USART_WORK_RECV_SIG:{
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							GprsLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v); 
							if(strstr(data,"+CPIN: READY"))
							{
								GprsLog("GPRS_OPEN_RST OK");
								QTimeEvt_disarm(&me->timeEvt);
								status_ = Q_TRAN(&GPRS_CONNECT);
								QF_PUBLISH(&Q_NEW(QpEvt,GPRS_OPEN_SUCCES_SIG)->super, me);
							}
							else if(strstr(data,"+CPIN: NOT INSERTED"))
							{
								GprsLog("未插入SIM卡");
								QTimeEvt_disarm(&me->timeEvt);
								QF_PUBLISH(&Q_NEW(QpEvt,GPRS_OPEN_FAILED_SIG)->super, me);
							}else if(strstr(data,"+CPIN: SIM PIN"))
							{
								GprsLog("SIM卡开启了PIN码");
								QTimeEvt_disarm(&me->timeEvt);
								QF_PUBLISH(&Q_NEW(QpEvt,GPRS_OPEN_FAILED_SIG)->super, me);
							}
							else
							{
								GprsLog("GPRS_OPEN_RST ERR,WAIT AGIN");
							}
							GprsFree(data);
						}
						
          break;
        }
        /*${Gprs::GprsWork::SM::GPRS_AT::GPRS_TIMEOUT} */
        case TIMEOUT_SIG: {
					status_ = Q_HANDLED();
					GprsLog("GPRS_OPEN_RST TIMEOUT_SIG");
					switch(count)
					{
						case 0:
						GprsLog("GPRS_OPEN_RST Pull Up");
						GPRS_PIN_RST = 1;
						QTimeEvt_disarm(&me->timeEvt); 
						QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*1,0);							
						break;
						case 1:
							 status_ = Q_TRAN(&GPRS_OPEN);
						break;
						
					}
					count++;
         break;
        }
				case Q_EXIT_SIG:{
					GprsLog("~~~GPRS_OPEN_RST");
					QTimeEvt_disarm(&me->timeEvt);
					status_ = Q_HANDLED();	
					break;
				}
        default: {
            status_ = Q_SUPER(&GPRS_TOP);
            break;
        }
    }
    return status_;
}


// Header:链接处理定时器函数，每秒检查连接列表里面的超时时间，如果超时间到，关闭连接
// File Name: 
// Author:
// Date:
u8 ConnectTimerCall(T_OMCI_LIST * pList)
{
	CHECK_SINGLE_POINTER(pList->pHead, OMCI_LIST_ERROR);
	CHECK_SINGLE_POINTER(pList->pHead->pNext, OMCI_LIST_ERROR);
	T_OMCI_LIST_NODE *pListNode = pList->pHead->pNext;
	while(pListNode != pList->pHead)
	{
			ConnectObj *connect = *(ConnectObj **)OmciGetNodeData(pListNode);
			
			if(connect)
			{
				Log("ConnectTimer id:%d type:%d,sta:%d,timeOut:%d",connect->id,connect->type,connect->sta,connect->timeOut);
				if(connect->sta != CONNECT_INIT)
				{
					if(connect->timeOut>0 && connect->timeOut != 0xffff)
					{
						connect->timeOut--;
						//超时时间到达，给模块发送关闭指令
						if(connect->timeOut == 0)
						{
							connect->timeOut = 10;
							connect->sta = CONNECT_CLOSE;
							SendConnectCloseCmd(connect->id,connect->sta);
						}
					}
				}
			}
			pListNode = pListNode->pNext;	
	}
}

//遍历接收连接，如果应用没有对应连接，发送关闭指令
 LIST_STATUS TraveHttpRecvHeadList(void *pvNode, void *pvTravInfo, uint32_t dwNodeDataSize)
{
	
	CHECK_SINGLE_POINTER(pvNode, OMCI_LIST_ERROR);
	T_OMCI_LIST_NODE *pNode = (T_OMCI_LIST_NODE *)pvNode;
	HttpHead *httpHead = *((HttpHead **)GET_NODE_DATA(pNode));
	Log("HttpRecv head:%s:%s",((PbDataObj  *)(httpHead->key.arg))->data,((PbDataObj  *)(httpHead->value.arg))->data);
 
	return OMCI_LIST_OK;
}

//根据ip端口识别连接，每条连接都有超时时间
//发送数据的时候如果连接存在，直接发送，如果不存在，新建连接，连接成功后发送
//新建连接的时候如果已经达到最大连接，则返回创建失败
static QState GPRS_CONNECT(GprsWork * const me, QEvt const * const e)
{
	  QState status_; 
    switch (e->sig) {
        /*${Gprs::GprsWork::SM::GPRS_AT} */
        case Q_ENTRY_SIG: {
						status_ = Q_HANDLED();
						GprsLog("+++GPRS_CONNECT");
						while(1)
						{
							if(QActive_recall(&me->super, &me->connectQueue)){
									GprsLog("send connectQueue save");
							}
							else {
									GprsLog("connectQueue no save");
								break;
							}					
						}
            break;
        }
				
				//根据ip端口识别连接，每条连接都有超时时间
				//发送数据的时候如果连接存在，直接发送，如果不存在，新建连接，连接成功后发送
				//新建连接的时候如果已经达到最大连接，则返回创建失败
					case GPRS_SEND_SIG:{
					status_ = Q_HANDLED();	
					Log("GPRS_SEND_SIG");
					//收到的是一个已经申请了内存，没有入库的结点
					T_OMCI_LIST_NODE  * node = (T_OMCI_LIST_NODE *)Q_EVT_CAST(QpEvt)->p;
					GprsSendData * obj = *((GprsSendData **)OmciGetNodeData(node));	
					Log("SendNode:%d nodeData:%d type:%d",node,obj,obj->type);
					switch(obj->type)
					{
						case ConnectType_CONNECT_TYPE_TCP:{
							;u8 svSendData = 0;
							GprsTcpSendObj * tcpObj = obj->obj;
							ConnectObj *connect = GetConnectById(&me->connectList,tcpObj->id);	
							if(connect)
							{
								if(connect->sta == CONNECT_LINKED )
								{
									//发送成功后判断该条数据是否要接收关闭、接收超时、连接关闭超时
									SendTcpObj(tcpObj);
									//把对应的连接设置为发送占用
									//如果接受超时状态不为0且接收超时时间大于0，开启接收超时倒计时，倒计时到了回调失败，并删除数据
									RemoveListNode(&me->sendList,node);
								}
								else if(connect->sta == CONNECT_CLOSE)
								{
									GprsLog("连接存在,但已准备关闭,直接删除数据");
									RemoveListNode(&me->sendList,node);
								}
								else if(connect->sta == CONNECT_INIT)
								{
									svSendData = 1;
								}
							}
							//连接不存在，新建连接
							else
							{
								svSendData = 1;
							}
							///////////////////////
							if(svSendData == 1)
							{
								GprsLog("连接不存在,待发送数据存入链表");
								if(OmciAppendListPackNode(&me->sendList,node) == OMCI_LIST_OK )
								{
									GprsLog("OmciAppendListNode ok");
								}
								else 
								{
									GprsLog("OmciAppendListNode failed");
									RemoveListNode(&me->sendList,node);
								}
							}
							break;
						}
						case ConnectType_CONNECT_TYPE_MQTT:{
							;u8 svSendData = 0;
							GprsMqttSendObj * mqttObj = obj->obj;
							ConnectObj *connect = GetConnectById(&me->connectList,mqttObj->id);	
							if(connect)
							{
								if(connect->sta == CONNECT_LINKED )
								{
									//发送成功后判断该条数据是否要接收关闭、接收超时、连接关闭超时
									SendMqttObj(mqttObj);
									//把对应的连接设置为发送占用
									//如果接受超时状态不为0且接收超时时间大于0，开启接收超时倒计时，倒计时到了回调失败，并删除数据
									RemoveListNode(&me->sendList,node);
									printf("----------->SendMqtt end mem:%d\r\n",my_mem_perused(SRAMIN));		
								}
								else if(connect->sta == CONNECT_CLOSE)
								{
									GprsLog("连接存在,但已准备关闭,直接删除数据");
									RemoveListNode(&me->sendList,node);
								}
								else if(connect->sta == CONNECT_INIT)
								{
									svSendData = 1;
								}
							}
							//连接不存在，新建连接
							else
							{
								svSendData = 1;
							}
							///////////////////////
							if(svSendData == 1)
							{
								GprsLog("连接不存在,待发送数据存入链表");
								if(OmciAppendListPackNode(&me->sendList,node) == OMCI_LIST_OK )
								{
									GprsLog("OmciAppendListNode ok");
								}
								else 
								{
									GprsLog("OmciAppendListNode failed");
									RemoveListNode(&me->sendList,node);
								}
							}							
							break;
						}
						case ConnectType_CONNECT_TYPE_HTTP:
							
						break;
					}
				break;
				}
					
				case GPRS_CONNECT_SIG:{
					status_ = Q_HANDLED();	
					Log("GPRS_CONNECT_SIG");
					ConnectObj *connectObj = (ConnectObj *)(Q_EVT_CAST(QpEvt)->p);
					GprsStaChangeCall(NET_STA_CONNECTEDING);
					if(SendConnectCmd(connectObj))
					{
						Log("创建连接成功,存入链表");
						OmciAppendListNode(&me->connectList,&connectObj);
					}
					else
					{
						Log("创建连接失败,删除");
						DeleteConnectObj(connectObj);
					}
					break;
				}				
				case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            u8 *data = (u8 *)(Q_EVT_CAST(QpEvt)->p);
						u16 size = (u16)(Q_EVT_CAST(QpEvt)->v);
						if(data)
						{
//							LogHex("recv:",data,size);
							if(size >2 )
							{
								if(CheckCrc16(data,size))
								{
									FuncObj *typeObj = DecodeFuncObjFromBuff(data,size-2);
									
									if(typeObj)
									{
										Log("cmd func:%d",typeObj->func);
										switch(typeObj->func)
										{
											case AIR_CMD_INFO:{
												Log("AIR_CMD_INFO");
												PbDataObj  *dataObj = typeObj->data.arg;
//												LogHex("info",dataObj->data,dataObj->size);			
												GprsInfoObj *info = DecodeGprsInfoObjFromBuff(dataObj->data,dataObj->size);
												if(info)
												{											
													Log("csq:%d,lng:%f,lat:%f",info->csq,info->lng,info->lat);
													PbDataObj * iccidData = (PbDataObj *)(info->iccid.arg);
													PbDataObj * imeidData = (PbDataObj *)(info->imei.arg);
													GprsSignalChangeCall(info->csq*100/33);
													Log("iccid:%d-%s,imei:%d-%s",iccidData->size,iccidData->data,imeidData->size,imeidData->data);
													DeleteGprsInfoRecvObj(info);
												}
												
											break;
											}
											case AIR_CMD_CONNECT:{
												Log("AIR_CMD_CONNECT");
												PbDataObj  *dataObj = typeObj->data.arg;
												LogHex("connect",dataObj->data,dataObj->size);
												ConnectChangeObj *pbConnect = DecodeConnectObjFromBuff(dataObj->data,dataObj->size);

												if(pbConnect)
												{												
													if(pbConnect->sta == CONNECT_LINKED)
													{
														GprsStaChangeCall(NET_STA_CONNECTED);
													}
													PbDataObj  *ipData = pbConnect->ip.arg;																			
													if(pbConnect->sta == CONNECT_LINKED)
													{
														Log("connect linked id：%d,type:%d,sta:%d",pbConnect->id,pbConnect->type,pbConnect->sta);
														
														ConnectObj *connect = GetConnectById(&me->connectList,pbConnect->id);
														connect->sta = pbConnect->sta;
														//把该ip端口的数据发送出去
														if(RePostAllSendDataNodeById(&me->sendList,pbConnect->id))
														{
															GprsLog("RePostSendDataNodeByIndex ok");
														}
													}
													else if(pbConnect->sta == CONNECT_CLOSE)
													{
														Log("connect close id：%d,type:%d,sta:%d",pbConnect->id,pbConnect->type,pbConnect->sta);
														//连接失败则删除链接
														DeleAllNodeFromListById(&me->connectList,pbConnect->id);
														DeleteConnectById(&me->connectList,pbConnect->id);
													}														
													//删除接收到的pb体
													ConnectChangeObjDelete(pbConnect);
												}
												break;
											}
											
											case AIR_CMD_TCP_RECV:{
												PbDataObj  *dataObj = typeObj->data.arg;
												TcpRecvObj * recvObj = DecodeTcpRecvObjFromBuff(dataObj->data,dataObj->size);
												if(recvObj)
												{
													PbDataObj  *ipData = recvObj->data.arg;					
													if(ipData)
													{
														Log("tcp recv id:%d size:%d,data:%s",recvObj->id,ipData->size,ipData->data);
														
													}
													 DeleteTcpRecvObj(recvObj);
												}
												break;
											}
											case AIR_CMD_MQTT_RECV:{
												PbDataObj  *dataObj = typeObj->data.arg;
												MqttRecvObj * recvObj = DecodeMqttRecvObjFromBuff(dataObj->data,dataObj->size);
												if(recvObj)
												{
													PbDataObj  *dataObj = recvObj->data.arg;		
													PbDataObj  *topicObj = recvObj->topic.arg;		
													Log("mqtt recv id:%d topic:%s,data:%s",recvObj->id,topicObj->data,dataObj->data);			
													ConnectObj *connect = GetConnectById(&me->connectList,recvObj->id);
													if(connect)
													{
														if(connect->info)((void (*)(MqttRecvObj *))connect->info)(recvObj);
													}
													DeleteMqttRecvObj(recvObj);
												}
												break;
											}
											case AIR_CMD_HTTP_RECV:{
												Log("AIR_CMD_HTTP_RECV");
												PbDataObj  *dataObj = typeObj->data.arg;
												printf("SelectConnectObj------------------>1:%d\r\n",my_mem_perused(SRAMIN));		
												HttpRecvObj *httpRecvObj = DecodeHttpRecvObjFromBuff(dataObj->data,dataObj->size);
												if(httpRecvObj)
												{
													//Log("HttpRecvObj id:%d,body:%s",httpRecvObj->id,((PbDataObj  *)(httpRecvObj->body.arg))->data);
													T_OMCI_LIST *headList = httpRecvObj->head.arg;
													OmciTraverseListNode(headList, 0, TraveHttpRecvHeadList);
													ConnectObj *connect = GetConnectById(&me->connectList,httpRecvObj->id);
													if(connect)
													{
														if(connect->info)((void (*)(HttpRecvObj *))connect->info)(httpRecvObj);
													}
													DeleteHttpRecvObj(httpRecvObj);
													printf("SelectConnectObj------------------>2:%d\r\n",my_mem_perused(SRAMIN));	

													//不等于65535的接收到就关闭连接
													if(connect->timeOut != 0xffff)
													{
														connect->sta = CONNECT_CLOSE;
														SendConnectCloseCmd(connect->id,connect->sta);
													}
												}
											break;
											}											
											case AIR_CMD_CONNECT_SELECT:{
												
												PbDataObj  *dataObj = typeObj->data.arg;
												printf("SelectConnectObj------------------>1:%d\r\n",my_mem_perused(SRAMIN));		
												SelectConnectObj * selectConnectObj = DecodeSelectConnectObjFromBuff(dataObj->data,dataObj->size);
												if(selectConnectObj)
												{
													//先获取连接队列
													//再变量队列里面的连接体
													T_OMCI_LIST *selectConnectList = selectConnectObj->connectArray.arg;
													
													//如果模块有链接，应用程序没有--调用关闭指令
													//如果应用程序有链接（状态不是init），模块没有--删除应用层连接
													//如果返回为空--删除所有应用层连接
													OmciTraverseListNode(selectConnectList, &me->connectList, HandlerRecvConnectListForRecvList);
													//如果应用程序有链接（状态不是init），模块没有--删除应用层连接
													HandlerRecvConnectListForSvList(selectConnectList,&me->connectList);
													
													SelectDeleteConnectObj(selectConnectObj);
													printf("SelectConnectObj------------------>2:%d\r\n",my_mem_perused(SRAMIN));		
												}
												//如果返回为空--删除所有应用层连接
												else
												{
													//OmciClearList(&me->connectList);
													HandlerRecvConnectListForSvList(0,&me->connectList);
												}
												break;
											}
										}
										FuncObjDelete(typeObj);
									}											
								}
							}						
							GprsFree(data);
						}	
						break;
					}
				case LOOP_TIMEOUT_SIG:{
					status_ = Q_UNHANDLED();	
					ConnectTimerCall(&me->connectList);
					
					static u8 select =0;
					if(select++ >=5)
					{
						u32 num = OmciGetListNodeNum(&me->connectList);
						Log("SendConnectSelectCmd num:%d",num);
						if(num >0)
						{
							SendConnectSelectCmd();
						}
						
						//发送查询设备信息
						SendGetInfoCmd();
						
						select =0;
					}
				break;
				}
				case Q_EXIT_SIG:{
					status_ = Q_HANDLED();	
					break;
				}				
        default: {
            status_ = Q_SUPER(&GPRS_TOP);
            break;
        }
    }
    return status_;
}
