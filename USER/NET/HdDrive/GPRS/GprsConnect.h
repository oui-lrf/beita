#pragma once
#include "sys.h"
#include "DoublyList.h"
typedef enum{
	CONNECT_INIT,
	CONNECT_LINKED,
	CONNECT_CLOSE,
}CONNECT_STA;

//最大连接数
#define MAX_CONNECT_COUNT 6
//定义最大IP长度
#define GPRS_MAX_IP_LEN 40
#define GPRS_HTTP_HEAD_MAX_STR_LEN 40


#include "air.pb.h"
#define CONNECT_TYPE ConnectType

typedef struct {
	CONNECT_TYPE  type; //TCP,MQTT,HTTP
	CONNECT_STA sta;
	u8 id;
	u16 timeOut;
	void *obj;
	void *info;
}ConnectObj;


// Header:根据ip端口获取一条连接
// File Name: 
// Author:
// Date:
ConnectObj * GetConnectByIpPortType(T_OMCI_LIST *list,char *ip,u16 port,CONNECT_TYPE type);

// Header:根据id获取一条连接
// File Name: 
// Author:
// Date:
ConnectObj * GetConnectById(T_OMCI_LIST *list,u8 id);

// Header: 根据id从链表删除一条连接
// File Name: 
// Author:
// Date:
u8 DeleteConnectById(T_OMCI_LIST * pList,u8 id);

// Header:从列表移除节点的时候回调,释放节点后跟随的指针对应的内存
// File Name: 
// Author:
// Date:
void DeleteConnectNodeCall(void * pNode);

// Header:打印一条连接的内容
// File Name: 
// Author:
// Date:
void PrintConnectList(void *pvNodeData, uint32_t dwNodeNum);

void ConnectStaInit(void);
u8 MallocConnect(void);

void DeleteConnectObj(ConnectObj *obj);
void FreeConnect(u8 id);
