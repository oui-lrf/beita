//Gprs应用双链表的一些接口函数
#include "GprsListDrive.h"
#include "GprsConnect.h"
#include "GprsUserSendData.h"

// Header:从列表移除节点的时候回调,释放节点后跟随的指针对应的内存
// File Name: 
// Author:
// Date:
void DeleteConnectNodeCall(void * pNode)
{
	ConnectObj *connect = *((ConnectObj **)(pNode));
	DeleteConnectObj(connect);
}

void DeleteSendNodeCall(void * pNode)
{
	GprsSendData  * obj=*((GprsSendData  **)pNode);
	DeleteGprsSendObj(obj);
}

						

//移出链表，释放内存
T_OMCI_LIST_NODE *DeleAllNodeFromListById(T_OMCI_LIST *pList,u8 id)
{
	T_OMCI_LIST_NODE *pListNode = pList->pHead->pNext;
	while(pListNode != pList->pHead)
	{
			GprsSendData  * obj=*((GprsSendData  **)OmciGetNodeData(pListNode));
		
			switch(obj->type)
			{
				case ConnectType_CONNECT_TYPE_TCP:{
					;GprsTcpSendObj * tcpObj = obj->obj;
					if(tcpObj->id == id)
					{
							OmciRemoveListNode(pList,pListNode);	
					}
				break;
				}
				case ConnectType_CONNECT_TYPE_MQTT:{
					;GprsMqttSendObj * mqttObj = obj->obj;
					if(mqttObj->id == id)
					{
							OmciRemoveListNode(pList,pListNode);	
					}
				break;
				}
				case ConnectType_CONNECT_TYPE_HTTP:
					
				break;
			
			}
			pListNode = pListNode->pNext;
	}
	return 0;
}

//移出链表但是不释放内存
T_OMCI_LIST_NODE *GetNodeFromListById(T_OMCI_LIST *pList,u8 id)
{
	T_OMCI_LIST_NODE *pListNode = pList->pHead->pNext;
	while(pListNode != pList->pHead)
	{
			GprsSendData  * obj=*((GprsSendData  **)OmciGetNodeData(pListNode));
		
			switch(obj->type)
			{
				case ConnectType_CONNECT_TYPE_TCP:
					;GprsTcpSendObj * tcpObj = obj->obj;
					if(tcpObj->id == id)
					{
						return pListNode;
					}
				break;
				case ConnectType_CONNECT_TYPE_MQTT:
					;GprsMqttSendObj * mqttObj = obj->obj;
					if(mqttObj->id == id)
					{
						return pListNode;
					}					
				break;
				case ConnectType_CONNECT_TYPE_HTTP:
					
				break;
			
			}
			pListNode = pListNode->pNext;
	}
	return 0;
}

//重新post指定的数据点
u8 RePostSendDataNodeByIndex(T_OMCI_LIST *pList,u8 index)
{
	T_OMCI_LIST_NODE  *outNode = OmciGetListNodeByIndex(pList,index);
	if(outNode)
	{
		RemoveListNodeNoFree(pList,outNode);		
		PostGprsSendObj(outNode);			
		return 1;
	}
	return 0;
}

//重新post指定的数据点
u8 RePostAllSendDataNodeById(T_OMCI_LIST *pList,u8 id)
{
	u8 res = 0;
	T_OMCI_LIST_NODE *pListNode = pList->pHead->pNext;
	while(pListNode != pList->pHead)
	{
			GprsSendData  * obj=*((GprsSendData  **)OmciGetNodeData(pListNode));
		
			switch(obj->type)
			{
				case ConnectType_CONNECT_TYPE_TCP:
					;GprsTcpSendObj * tcpObj = obj->obj;
					if(tcpObj->id == id)
					{
						RemoveListNodeNoFree(pList,pListNode);		
						PostGprsSendObj(pListNode);		
						res = 1;					
					}
				break;
				case ConnectType_CONNECT_TYPE_MQTT:
					;GprsMqttSendObj * mqttObj = obj->obj;
					if(mqttObj->id == id)
					{
						RemoveListNodeNoFree(pList,pListNode);		
						PostGprsSendObj(pListNode);		
						res = 1;					
					}					
				break;
				case ConnectType_CONNECT_TYPE_HTTP:
					
				break;
			
			}
			pListNode = pListNode->pNext;
	}
	return res;
}

