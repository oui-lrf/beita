#pragma once
#include "GprsInclude.h"
ConnectChangeObj *DecodeConnectObjFromBuff(u8 *data,u16 size);
TcpRecvObj *DecodeTcpRecvObjFromBuff(u8 *data,u16 size);
SelectConnectObj *DecodeSelectConnectObjFromBuff(u8 *data,u16 size);
void SelectDeleteConnectObj(SelectConnectObj *selectConnectObj);

void FuncObjDelete(FuncObj *typeObj);
FuncObj *DecodeFuncObjFromBuff(u8 *data,u16 size);
void ConnectChangeObjDelete(ConnectChangeObj *pbObj);
LIST_STATUS PrintConnectChangeObj(void *pvNode, void *pvTravInfo, uint32_t dwNodeDataSize);
void ConnectChangeObjNodeDelete(void *pData);
ErrorObj *DecodeErrorObjFromBuff(u8 *data,u16 size);
void DeleteTcpRecvObj(TcpRecvObj *obj);
void DeleteMqttRecvObj(MqttRecvObj *obj);
//从数组解码为MQTT接收结构体
MqttRecvObj *DecodeMqttRecvObjFromBuff(u8 *data,u16 size);

HttpRecvObj *DecodeHttpRecvObjFromBuff(u8 *data,u16 size);
void DeleteHttpRecvObj(HttpRecvObj *httpRecvObj);
HttpHead * GetHeadByName(T_OMCI_LIST *headList,char *keyName);

void DeleteGprsInfoRecvObj(GprsInfoObj *obj);

//从数组解码为信息结构体
GprsInfoObj *DecodeGprsInfoObjFromBuff(u8 *data,u16 size);
