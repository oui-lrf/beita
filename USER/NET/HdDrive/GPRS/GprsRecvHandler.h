#pragma once
#include "GprsInclude.h"

u8 CheckTestCmd(u8 *data,u16 size);
u8 CheckCrc16(u8 *data,u16 size);
//遍历接收连接，如果应用没有对应连接，发送关闭指令
 LIST_STATUS HandlerRecvConnectListForRecvList(void *pvNode, void *pvTravInfo, uint32_t dwNodeDataSize);
 
 
 // Header:遍历应用连接，如果应用程序有链接，模块没有--删除应用层连接
// File Name: 
// Author:
// Date:
 u8 HandlerRecvConnectListForSvList(T_OMCI_LIST *recvConnectList,T_OMCI_LIST * svList);
 
 // Header:从接收的连接列表里面根据id获取一条连接
// File Name: 
// Author:
// Date:
static ConnectChangeObj * GetRecvConnectFromRecvListById(T_OMCI_LIST *pList,u8 id);

/**********************************************************************
* 功能描述：连接建立成功后，把该ip端口下的所有待发送数据包发送出去 
* 输入参数： 
*           
* 输出参数： NA
* 返 回 值： LIST_STATUS
***********************************************************************/
//LIST_STATUS TcpSendAllConnectNode(T_OMCI_LIST *dataList,u8 id,char *ip,u16 port);

u32 CheckErrCmd(u8 *data,u16 size);