#pragma once

#include "qep.h"
#include "Qpc.h"
#include "DoublyList.h"

typedef struct {
/* protected: */
  QHsm super;
	T_OMCI_LIST downList;
	T_OMCI_LIST_NODE  *downningNode;
/* private: */
}DownWork;
void DownWork_ctor(DownWork * const me);
