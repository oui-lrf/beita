#pragma once
#include "sys.h"
#include "GprsPort.h"


typedef enum{

	AIR_CMD_TEST=1,
	AIR_CMD_INFO,
	AIR_CMD_CONNECT,
	AIR_CMD_TCP_SEND,
	AIR_CMD_MQTT_SEND,	
	AIR_CMD_HTTP_SEND,
	AIR_CMD_TCP_RECV,
	AIR_CMD_MQTT_RECV,//11
	AIR_CMD_HTTP_RECV,
	AIR_CMD_CONNECT_SELECT,
	AIR_CMD_CONNECT_CLOSE,
	AIR_CMD_ERROR,
}AIR_CMD;




typedef enum{
	GPRS_ERR_CRC16 = 1,
}GPRS_ERR_NUM;

u16 CreateFuncCmd(u8 **resBuf,AIR_CMD cmd,u8 *src_data,u16 dataSize);
u16 CreateConnectChangeCmd(u8 **resBuf,u32 id,u32 sta);
u16 CreateTcpSendCmd(u8 **resBuf,u8 id,u8 *src_data,u16 dataSize);

u16 CreateMqttConnectCmd(u8 **resBuf,u8 id,GprsMqttConnectObj *mqttConnectObj);
u16 CreateMqttSendCmd(u8 **resBuf,u8 id,MqttSendType type,char *topic,u8 *dat,u16 datSize,u8 qos);
u16 CreateHttpConnectCmd(u8 **resBuf,u8 id,GprsHttpConnectObj *httpConnectObj);
u16 CreateTcpConnectCmd(u8 **resBuf,u8 id,GprsTcpConnectObj *tcpCreateObj);
