//用户发送的数据被封装成的数据发送体，存入链表
#include "GprsSendCmd.h"
#include "GprsUserSendData.h"
#include "GprsPort.h"


extern QActive * const Gp_GprsWork;


void DeleteTcpSendData(GprsTcpSendObj * tcpObj)
{
	if(tcpObj)
	{
		GprsFree(tcpObj->data);
		GprsFree(tcpObj);
	}
}

GprsTcpSendObj * CreateTcpSendData(	u8 id,u8 *data,u32 dataSize,u32 recvTimeOut,void *info)
{
	u8 *sdata = GprsMalloc(dataSize);
	if(!sdata)
	{
		GprsLog("Malloc err");
		return 0;
	}
	GprsTcpSendObj * tcpObj = GprsMalloc(sizeof(GprsTcpSendObj));
	if(!tcpObj)
	{
		GprsFree(sdata);
		GprsLog("Malloc err");
		return 0;
	}
	
	memcpy(sdata,data,dataSize);
	tcpObj->data = sdata;
	tcpObj->dataSize = dataSize;
	tcpObj->id = id;
	tcpObj->recvTimeOut = recvTimeOut;
	tcpObj->info = info;
	return tcpObj;
}
////////////////////////////////////////////////////////////////
void DeleteGprsMqttSubObj(GprsMqttSubObj *dataObj)
{
	if(dataObj)
	{
		GprsFree(dataObj->topic);
		GprsFree(dataObj);
	}
}

GprsMqttSubObj * CreateGprsMqttSubObj(u8 id,char *topic)
{
	
	u8 topicLen = strlen(topic);
	if(topicLen >40)
	{
		GprsLog("topicLen err");
		return 0;
	}
	GprsMqttSubObj *dataObj = GprsMalloc(sizeof(GprsMqttSubObj));
	if(!dataObj)
	{
		GprsLog("Malloc err");
		return 0;
	}
	char *sTopic = GprsMalloc(topicLen+1);
	if(!sTopic)
	{
		GprsLog("Malloc err");
		GprsFree(dataObj);
		return 0;	
	}
	strncpy(sTopic,topic,topicLen);
	dataObj->topic = sTopic;
	return dataObj;
}

void DeleteGprsMqttPubObj(GprsMqttPubObj *dataObj)
{
	if(dataObj)
	{
		GprsFree(dataObj->topic);
		GprsFree(dataObj->data);	
		GprsFree(dataObj);
	}
}

GprsMqttPubObj * CreateGprsMqttPubObj(char *topic,u8 *data,u16 size)
{
	
	u8 topicLen = strlen(topic);
	if(topicLen >40)
	{
		GprsLog("topicLen err");
		return 0;
	}
	GprsMqttPubObj *dataObj = GprsMalloc(sizeof(GprsMqttPubObj));
	if(!dataObj)
	{
		GprsLog("Malloc err");
		return 0;
	}
	char *sTopic = GprsMalloc(topicLen+1);
	if(!sTopic)
	{
		GprsLog("Malloc err");
		GprsFree(dataObj);
		return 0;	
	}
	
	u16 dataSize = size; 
	u8 *sData = GprsMalloc(dataSize);
	if(!sData)
	{
		GprsFree(dataObj);
		GprsFree(sTopic);
		return 0;
	}

	strncpy(sTopic,topic,topicLen);
	memcpy(sData,data,dataSize);
	
	dataObj->topic = sTopic;
	dataObj->data = sData;
	dataObj->size = dataSize;
	return dataObj;
}

void DeleteGprsMqttSendObj(GprsMqttSendObj *obj)
{
	if(obj)
	{
		switch(obj->type)
		{
			case MqttSendType_MQTT_UNSUB:
				
			break;
			case MqttSendType_MQTT_SUB:
				DeleteGprsMqttSubObj(obj->obj);
			break;
		  case MqttSendType_MQTT_PUB:
				DeleteGprsMqttPubObj(obj->obj);
			break;
		}
	
		GprsFree(obj);
	}

}

GprsMqttSendObj *CreateGprsMqttSendObj(u8 id,MqttSendType type,void *obj)
{
	GprsMqttSendObj *gprsMqttSendObj = GprsMalloc(sizeof(GprsMqttSendObj));
	if(!gprsMqttSendObj)
	{
		return 0;
	}
	gprsMqttSendObj->id = id;
	gprsMqttSendObj->obj = obj;
	gprsMqttSendObj->type = type;
	return gprsMqttSendObj;
}

void DeleteGprsSendObj(GprsSendData * sendObj)
{
	if(sendObj)
	{
		switch(sendObj->type)
		{
			case ConnectType_CONNECT_TYPE_TCP:
				DeleteTcpSendData(sendObj->obj);
			break;
			case ConnectType_CONNECT_TYPE_MQTT:
				DeleteGprsMqttSendObj(sendObj->obj);
			break;
			case ConnectType_CONNECT_TYPE_HTTP:
				
			break;
		
		}
		
		GprsFree(sendObj);
	}
	
}

GprsSendData *CreateGprsSendObj(void *obj,CONNECT_TYPE type)
{
	GprsSendData * sendObj = GprsMalloc(sizeof(GprsSendData));
	if(!sendObj)
	{
		return 0;
	}
	sendObj->obj = obj;
	sendObj->type = type;
	return sendObj;
}




//遍历待发送数据
LIST_STATUS TraverseSendListNode(T_OMCI_LIST *pList)
{
    CHECK_SINGLE_POINTER(pList->pHead, OMCI_LIST_ERROR);
    CHECK_SINGLE_POINTER(pList->pHead->pNext, OMCI_LIST_ERROR);

    T_OMCI_LIST_NODE *pListNode = pList->pHead->pNext;
    while(pListNode != pList->pHead)
    {				
				GprsSendData  * obj=*((GprsSendData  **)OmciGetNodeData(pListNode));
				Log("TraverseSendListNode sendListNode:%d pListNode->pNext:%d",pListNode,pListNode->pNext);
        T_OMCI_LIST_NODE *pTmpNode = pListNode->pNext; //fpTravNode内可能会销毁结点pListNode

        pListNode = pTmpNode;
    }

    return OMCI_LIST_OK;
}


void DeleteGprsTcpConnectObj(GprsTcpConnectObj *dataObj)
{
	if(dataObj)
	{
		GprsFree(dataObj->ip);
		GprsFree(dataObj);		
	}
}

GprsTcpConnectObj *CreateGprsTcpConnectObj(char *ip,u16 port)
{
		char *sIp = GprsMalloc(GPRS_MAX_IP_LEN+1);
		if(!sIp)
		{
			Log("malloc err");
			return 0;
		}
		GprsTcpConnectObj *tcpCreateObj = GprsMalloc(sizeof(GprsTcpConnectObj));
		if(!tcpCreateObj)
		{
			Log("malloc err");
			GprsFree(sIp);
			return 0;
		}
		strncpy(sIp,ip,GPRS_MAX_IP_LEN);	
		tcpCreateObj->ip = sIp;
		tcpCreateObj->port = port;	
		return tcpCreateObj;
}

void DeleteGprsMqttConnectObj(GprsMqttConnectObj *dataObj)
{
	if(dataObj)
	{
		GprsFree(dataObj->ip);
		GprsFree(dataObj->clientId);
		GprsFree(dataObj->userName);
		GprsFree(dataObj->userPass);
		GprsFree(dataObj->will);		
		GprsFree(dataObj);
	}
}

GprsMqttConnectObj * CreateGprsMqttConnectObj(char *ip,u16 port,char *clientId,char *userName,char *userPass,char *will,u32 heartTime)
{
		u8 ipLen = strlen(ip);
	if( ipLen >GPRS_MAX_IP_LEN)
	{
		GprsLog("ipLen err:%d",ipLen);
		return 0;
	}
	
	u8 clientIdLen = strlen(clientId);
	if(clientIdLen >40)
	{
		GprsLog("cilentIdLen err");
		return 0;
	}
	
	u8 userNameLen = strlen(userName);
	if(userNameLen >40)
	{
		GprsLog("userName err");
		return 0;
	}	
	
	u8 userPassLen = strlen(userPass);
	if(userPassLen >40)
	{
		GprsLog("userPass err");
		return 0;
	}		
	u8 willLen = strlen(will);
	if(willLen >40)
	{
		GprsLog("userPass err");
		return 0;
	}			
	
	GprsMqttConnectObj *mqttConnectObj = GprsMalloc(sizeof(GprsMqttConnectObj));
	if(!mqttConnectObj)
	{
		GprsLog("Malloc err");
		return 0;
	}
	char *sip = GprsMalloc(ipLen +1);
	if(!sip)
	{
		GprsLog("Malloc err");
		GprsFree(mqttConnectObj);
		return 0;
	}
	char *sClientId = GprsMalloc(clientIdLen+1);
	if(!sClientId)
	{
		GprsLog("Malloc err");
		GprsFree(mqttConnectObj);
		GprsFree(sip);
		return 0;	
	}
	
	char *sUserName = GprsMalloc(userNameLen+1);
	if(!sUserName)
	{
		GprsLog("Malloc err");
		GprsFree(mqttConnectObj);
		GprsFree(sip);
		GprsFree(sClientId);
		return 0;	
	}	

	char *sUserPass = GprsMalloc(userPassLen+1);
	if(!sUserPass)
	{
		GprsLog("Malloc err");
		GprsFree(mqttConnectObj);
		GprsFree(sip);
		GprsFree(sClientId);
		GprsFree(sUserName);
		return 0;	
	}
	char *sWill = GprsMalloc(willLen+1);
	if(!sWill)
	{
		GprsLog("Malloc err");
		GprsFree(mqttConnectObj);
		GprsFree(sip);
		GprsFree(sClientId);
		GprsFree(sUserName);
		GprsFree(sUserPass);
		return 0;	
	}
	strncpy(sip,ip,ipLen);
	strncpy(sClientId,clientId,clientIdLen);
	strncpy(sUserName,userName,userNameLen);
	strncpy(sUserPass,userPass,userPassLen);
	
	mqttConnectObj->ip = sip;
	mqttConnectObj->port = port;
	mqttConnectObj->clientId = sClientId;
	mqttConnectObj->userName = sUserName;
	mqttConnectObj->userPass = sUserPass;
	mqttConnectObj->will = sWill;
	mqttConnectObj->heartTime = heartTime;
	return mqttConnectObj;
}

void DeleteConnectObj(ConnectObj *obj)
{
	Log("DeleteConnectObj:%d",obj);
	if(obj)
	{
		switch(obj->type)
		{
			case ConnectType_CONNECT_TYPE_TCP:
				;
				DeleteGprsTcpConnectObj(obj->obj);
			break;
			case ConnectType_CONNECT_TYPE_MQTT:
				;DeleteGprsMqttConnectObj(obj->obj);
			break;
			case ConnectType_CONNECT_TYPE_HTTP:
				;DeleteGprsHttpConnectObj(obj->obj);		
			break;		
		}
		FreeConnect(obj->id);
		GprsFree(obj);
	}
}

ConnectObj * CreateGprsConnectObj(u8 type,void *obj,u32 timeOut,void *info)
{
		u8 id = 0;
		id =  MallocConnect();
		if(!id)
		{
			Log("MallocConnect err");
			return 0;
		}
		if(!obj)
		{
			return 0;
		}
		ConnectObj * newCon = GprsMalloc(sizeof(ConnectObj));
		if(!newCon)
		{
			Log("malloc err");
			return 0;
		}
		newCon->obj = obj;
		newCon->sta = CONNECT_INIT;
		newCon->type = type;
		newCon->timeOut = timeOut;
		newCon->id = id;
		newCon->info = info;
		return newCon;
}

////////////////////////////////////////////////////////////////

void DeleteHeadObj(HttpHead *head)
{
	Log("Delete head:%d",head);
	if(head)
	{
		Log("Delete head key:%d",head->key.arg);
		Log("Delete head value:%d",head->value.arg);
		DeletePbDataObj(head->key.arg);
		DeletePbDataObj(head->value.arg);
		GprsFree(head);
	}
}

void DeleteHeadObjNodeCall(void * pNode)
{
	HttpHead  * obj=*((HttpHead  **)pNode);
	DeleteHeadObj(obj);
}

HttpHead *CreateHead(char *key,char *value)
{
	const u8 STR_MAX_LEN = 40;
	u16 keySize = strlen(key)+1;
	u16 valueSize = strlen(value)+1;
	
	char *keyNode = MallocCpy(key,keySize,STR_MAX_LEN);
	if(!keyNode)
	{
		return 0;
	}
	char *valueNode = MallocCpy(value,valueSize,STR_MAX_LEN);
	if(!valueNode)
	{
		GprsFree(keyNode);
		return 0;
	}

	HttpHead *head = GprsMalloc(sizeof(HttpHead));
	Log("CreateHead:%d",head);
	if(!head)
	{
		GprsFree(keyNode);
		GprsFree(valueNode);
		return 0;
	}
	
	PbDataObj *keyDataObj = GprsMalloc(sizeof(PbDataObj));
	Log("create keyDataObj:%d",keyDataObj);
	if(!keyDataObj)
	{
		GprsFree(keyNode);
		GprsFree(valueNode);
		GprsFree(head);
		return 0;
	}

	PbDataObj *valueDataObj = GprsMalloc(sizeof(PbDataObj));
	Log("create valueDataObj:%d",valueDataObj);
	if(!valueDataObj)
	{
		GprsFree(keyNode);
		GprsFree(valueNode);
		GprsFree(head);
		GprsFree(keyDataObj);
		return 0;
	}		
	keyDataObj->data = (u8 *)keyNode;
	keyDataObj->size = keySize;
	valueDataObj->data = (u8 *)valueNode;
	valueDataObj->size = valueSize;
	
	head->key.arg = keyDataObj;
	head->value.arg = valueDataObj;
	return head;
}

void DeleteGprsHttpConnectObj(GprsHttpConnectObj *http)
{
	Log("Delete HttpConnect:%d",http);
	if(http)
	{
		T_OMCI_LIST *httpHeadListt = http->httpHeadList;
		OmciDestroyList(httpHeadListt);
		PbFree(httpHeadListt);
		PbFree(http->body);	
		PbFree(http->url);	
		PbFree(http);	
	}
}



//创建一个HTTP，其中包括一个head链表指针和一个body指针

GprsHttpConnectObj * CreateGprsHttpConnectObj(HttpMethod method,char *url,u32 waitTime)
{
	const u8 STR_MAX_LEN = 200;
	GprsHttpConnectObj *http = GprsMalloc(sizeof(GprsHttpConnectObj));
	Log("Create HttpConnect:%d",http);
	if(!http)
	{
		Log("malloc err");
		return 0;
	}
	Log("url:%s len:%d",url,strlen(url));
	char *sUrl = MallocCpy(url,strlen(url)+1,STR_MAX_LEN);
	if(!sUrl)
	{
		Log("malloc err");
		GprsFree(http);
		return 0;
	}
	
	T_OMCI_LIST *httpHeadList = PbMalloc(sizeof(T_OMCI_LIST));
	Log("Create httpHeadList:%d",httpHeadList);
	if(!httpHeadList)
	{
		Log("malloc err");
		GprsFree(http);
		GprsFree(sUrl);
		return 0;
	}
	OmciInitList(httpHeadList, sizeof(HttpHead *),20,(void (*)(void *))DeleteHeadObjNodeCall);
	http->httpHeadList = httpHeadList;
	http->method = method;
	http->waitTime = waitTime;
	http->url = sUrl;
	return http;
}

u8 AddHttpHead(GprsHttpConnectObj *http,char *headKey,char *headValue)
{
	HttpHead *head = CreateHead(headKey,headValue);
	if(head)
	{
		if(OmciAppendListNode(http->httpHeadList,&head) == OMCI_LIST_OK)
		{
			return 1;
		}
		else
		{
			DeleteHeadObj(head);
		}
	}
	return 0;
}


u8 AddHttpBody(GprsHttpConnectObj *http,char *body)
{
	const u8 STR_MAX_LEN = 200;
	char *addBody = MallocCpy(body,strlen(body)+1,STR_MAX_LEN);
	if(!addBody)
	{
		return 0;
	}
	http->body = addBody;
	return 1;
}


void DeleteDownLoadFile(DownFile *file)
{
	if(file)
	{
		GprsFree(file->filePath);
		GprsFree(file->fileName);
		GprsFree(file->url);
		GprsFree(file);
	}
}

void DeleteGprsDownLoadNodeCall(void * pNode)
{
	DownFile  * obj=*((DownFile  **)pNode);
	DeleteDownLoadFile(obj);
}

//packSize 每包下载的字节数
DownFile * CreateDownLoad(char *path,char *name,char *url,u16 crc16,u32 fileSize,u32 packSize,void (*callBack)(u8,DownFile *))
{
	const u8 STR_MAX_LEN = 200;
	char *filePath = MallocCpy(path,strlen(path)+1,STR_MAX_LEN);
	if(!filePath)
	{
		return 0;
	}
	
	char *fileName = MallocCpy(name,strlen(name)+1,STR_MAX_LEN);
	if(!fileName)
	{
		GprsFree(filePath);
		return 0;
	}
	char *fileUrl = MallocCpy(url,strlen(url)+1,STR_MAX_LEN);
	if(!fileUrl)
	{
		GprsFree(filePath);
		GprsFree(fileName);
		return 0;
	}	
	DownFile *file = GprsMalloc(sizeof(DownFile));
	if(!file)
	{
		GprsFree(filePath);
		GprsFree(fileName);	
		GprsFree(fileUrl);	
		return 0;
	}
	file->filePath = filePath;
	file->fileName = fileName;
	file->url = fileUrl;
	file->fileCrc16 = crc16;
	file->userData = callBack;
	file->packSize = packSize;
	file->downPos = 0;
	file->fileSize = fileSize;
	return file;

}



