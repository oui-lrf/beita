#include "GprsCmdUnPack.h"
#include "GprsConnect.h"


//从数组解码为功能结构体
FuncObj *DecodeFuncObjFromBuff(u8 *data,u16 size)
{
		pb_istream_t stream ;
		FuncObj *typeObj = PbMalloc(sizeof(FuncObj));		
		if(!typeObj)
		{
			Log("Malloc err");
			return 0;
		}
		
		PbDataObj  *dataObj  = PbMalloc(sizeof(PbDataObj));
		if(!dataObj)
		{
			PbFree(typeObj);
			Log("Malloc err");
			return 0;
		}
		
		dataObj->size = size;
		typeObj->data.funcs.decode = DecodeBufferCallBack;
		typeObj->data.arg = dataObj;
		
		stream = pb_istream_from_buffer(data, size);
		
		u8 status = pb_decode(&stream, FuncObj_fields,typeObj);
		if(status)
		{
			return typeObj;
		}
		return 0;
}

ErrorObj *DecodeErrorObjFromBuff(u8 *data,u16 size)
{
		pb_istream_t stream ;
		ErrorObj *typeObj = PbMalloc(sizeof(ErrorObj));		
		if(!typeObj)
		{
			Log("Malloc err");
			return 0;
		}
		stream = pb_istream_from_buffer(data, size);
		u8 status = pb_decode(&stream, ErrorObj_fields,typeObj);
		if(status)
		{
			return typeObj;
		}
		return 0;
}

void DeleteGprsInfoRecvObj(GprsInfoObj *obj)
{
	if(obj)
	{
		DeletePbDataObj(obj->iccid.arg);
		DeletePbDataObj(obj->imei.arg);
		PbFree(obj);
	}
}

//从数组解码为信息结构体
GprsInfoObj *DecodeGprsInfoObjFromBuff(u8 *data,u16 size)
{
		pb_istream_t stream ;
		GprsInfoObj *infoObj = PbMalloc(sizeof(GprsInfoObj));		
		if(!infoObj)
		{
			Log("Malloc err");
			return 0;
		}
		
		PbDataObj  *iccidDataObj  = PbMalloc(sizeof(PbDataObj));
		if(!iccidDataObj)
		{
			PbFree(infoObj);
			Log("Malloc err");
			return 0;
		}

		PbDataObj  *imeiDataObj  = PbMalloc(sizeof(PbDataObj));
		if(!imeiDataObj)
		{
			PbFree(infoObj);
			PbFree(iccidDataObj);
			Log("Malloc err");
			return 0;
		}

		
		iccidDataObj->size = size;
		infoObj->iccid.funcs.decode = DecodeBufferCallBack;
		infoObj->iccid.arg = iccidDataObj;
		
		imeiDataObj->size = size;
		infoObj->imei.funcs.decode = DecodeBufferCallBack;
		infoObj->imei.arg = imeiDataObj;
		
		stream = pb_istream_from_buffer(data, size);
		u8 status = pb_decode(&stream, GprsInfoObj_fields,infoObj);
		if(status)
		{
			return infoObj;
		}
		return 0;
}
//从数组解码为连接结构体
ConnectChangeObj *DecodeConnectObjFromBuff(u8 *data,u16 size)
{
		pb_istream_t stream ;
		ConnectChangeObj *pbObj = PbMalloc(sizeof(ConnectChangeObj));		
		if(!pbObj)
		{
			Log("Malloc err");
			return 0;
		}
		
		PbDataObj  *dataObj  = PbMalloc(sizeof(PbDataObj));
		if(!dataObj)
		{
			PbFree(pbObj);
			Log("Malloc err");
			return 0;
		}
		dataObj->size = size;
		pbObj->ip.funcs.decode = DecodeBufferCallBack;
		pbObj->ip.arg = dataObj;
		
		stream = pb_istream_from_buffer(data, size);
		u8 status = pb_decode(&stream, ConnectChangeObj_fields,pbObj);
		Log("DecodeConnectObjFromBuff status:%d",status);
		if(status)
		{
			return pbObj;
		}
		return 0;
}

void DeleteTcpRecvObj(TcpRecvObj *obj)
{
	if(obj)
	{
		DeletePbDataObj(obj->data.arg);
		PbFree(obj);
	}
}

//从数组解码为TCP接收结构体
TcpRecvObj *DecodeTcpRecvObjFromBuff(u8 *data,u16 size)
{
		pb_istream_t stream ;
		TcpRecvObj *pbObj = PbMalloc(sizeof(TcpRecvObj));		
		if(!pbObj)
		{
			Log("Malloc err");
			return 0;
		}
		
		PbDataObj  *dataObj  = PbMalloc(sizeof(PbDataObj));
		if(!dataObj)
		{
			PbFree(pbObj);
			Log("Malloc err");
			return 0;
		}
		dataObj->size = size;
		pbObj->data.funcs.decode = DecodeBufferCallBack;
		pbObj->data.arg = dataObj;
		
		stream = pb_istream_from_buffer(data, size);
		u8 status = pb_decode(&stream, TcpRecvObj_fields,pbObj);
		//Log("DecodeConnectObjFromBuff status:%d",status);
		if(status)
		{
			return pbObj;
		}
		return 0;
}

void DeleteMqttRecvObj(MqttRecvObj *obj)
{
	if(obj)
	{
		DeletePbDataObj(obj->topic.arg);
		DeletePbDataObj(obj->data.arg);
		PbFree(obj);
	}
}

//从数组解码为MQTT接收结构体
MqttRecvObj *DecodeMqttRecvObjFromBuff(u8 *data,u16 size)
{
		pb_istream_t stream ;
		MqttRecvObj *pbObj = PbMalloc(sizeof(MqttRecvObj));		
		if(!pbObj)
		{
			Log("Malloc err");
			return 0;
		}
		
		PbDataObj  *dataObj  = PbMalloc(sizeof(PbDataObj));
		if(!dataObj)
		{
			PbFree(pbObj);
			Log("Malloc err");
			return 0;
		}
		dataObj->size = size;
		pbObj->data.funcs.decode = DecodeBufferCallBack;
		pbObj->data.arg = dataObj;

		PbDataObj  *topicObj  = PbMalloc(sizeof(PbDataObj));
		if(!topicObj)
		{
			PbFree(pbObj);
			PbFree(dataObj);
			Log("Malloc err");
			return 0;
		}		
		topicObj->size = size;
		pbObj->topic.funcs.decode = DecodeBufferCallBack;
		pbObj->topic.arg = topicObj;
		
		stream = pb_istream_from_buffer(data, size);
		u8 status = pb_decode(&stream, MqttRecvObj_fields,pbObj);
		//Log("DecodeConnectObjFromBuff status:%d",status);
		if(status)
		{
			return pbObj;
		}
		return 0;
}


/**
 解码连接回调函数
 **/
bool DecodeConnectCallBack(pb_istream_t *stream, const pb_field_t *field, void **arg)
{
		T_OMCI_LIST *selectConnectList =  *arg;		
		ConnectChangeObj *connectObj = PbMalloc(sizeof(ConnectChangeObj));		
		if(!connectObj)
		{
			Log("Malloc err");
			return 0;
		}
		
		PbDataObj  *dataObj  = PbMalloc(sizeof(PbDataObj));
		if(!dataObj)
		{
			PbFree(connectObj);
			Log("Malloc err");
			return 0;
		}
		Log("stream->bytes_left:%d",stream->bytes_left);
		dataObj->size = GPRS_MAX_IP_LEN;
		connectObj->ip.funcs.decode = DecodeBufferCallBack;
		connectObj->ip.arg = dataObj;
		
		u8 status = pb_decode(stream, ConnectChangeObj_fields,connectObj);
		if(status)
		{
			//把connect存入列表
			//传入的参数是一个指针，会把指针里面的数据拷贝到结点后面
			//如果传入的是一个指针的地址，会把该指针拷贝到结点后面
			OmciAppendListNode(selectConnectList,&connectObj);
		}
		else
		{
			ConnectChangeObjDelete(connectObj);
		}
		
	return true;
}

void DeleteHttpHeadObj(HttpHead *headObj)
{
	if(headObj)
	{
		DeletePbDataObj(headObj->key.arg);
		DeletePbDataObj(headObj->value.arg);
		PbFree(headObj);
	}

}

//参数是一个结点数据
void DeleteHttpHeadObjNode(void *pData)
{
	HttpHead *headObj = *((HttpHead **)(pData));
	//Log("DeleteHttpHeadObjNode headObj:%d %s:%s",headObj,((PbDataObj  *)(headObj->key.arg))->data,((PbDataObj  *)(headObj->value.arg))->data);
	DeleteHttpHeadObj(headObj);
}

//先释放连接查询列表
//再释放连接查询体
void DeleteHttpRecvObj(HttpRecvObj *httpRecvObj)
{
	if(httpRecvObj)
	{
		T_OMCI_LIST *headList = httpRecvObj->head.arg;
		//遍历查询列表里面的每一个连接体进行释放
		//删除了连接列表里面的每一个连接对象
		OmciDestroyList(headList);
		Log("Delete HeadList:%d",headList);
		PbFree(headList);
		DeletePbDataObj(httpRecvObj->body.arg);
		PbFree(httpRecvObj);	
	}
}
/**
 解码连接回调函数
 **/
bool DecodeHttpRecvHeadCallBack(pb_istream_t *stream, const pb_field_t *field, void **arg)
{
		T_OMCI_LIST *headList =  *arg;		
		HttpHead *httpHead = PbMalloc(sizeof(HttpHead));		
		//Log("CreateHttpHead:%d",httpHead);
		if(!httpHead)
		{
			Log("Malloc err");
			return 0;
		}
		
		PbDataObj  *keyDataObj  = PbMalloc(sizeof(PbDataObj));
		if(!keyDataObj)
		{
			PbFree(httpHead);
			Log("Malloc err");
			return 0;
		}
		keyDataObj->size = GPRS_HTTP_HEAD_MAX_STR_LEN;
		httpHead->key.funcs.decode = DecodeBufferCallBack;
		httpHead->key.arg = keyDataObj;
		
		PbDataObj  *valueDataObj  = PbMalloc(sizeof(PbDataObj));
		if(!valueDataObj)
		{
			PbFree(httpHead);
			PbFree(keyDataObj);
			Log("Malloc err");
			return 0;
		}
		valueDataObj->size = GPRS_HTTP_HEAD_MAX_STR_LEN;
		httpHead->value.funcs.decode = DecodeBufferCallBack;
		httpHead->value.arg = valueDataObj;
		
		u8 status = pb_decode(stream, HttpHead_fields,httpHead);
		if(status)
		{
			//Log("Create httpHead:%d %s:%s",httpHead,((PbDataObj  *)(httpHead->key.arg))->data,((PbDataObj  *)(httpHead->value.arg))->data);
			//把connect存入列表
			//传入的参数是一个指针，会把指针里面的数据拷贝到结点后面
			//如果传入的是一个指针的地址，会把该指针拷贝到结点后面
			OmciAppendListNode(headList,&httpHead);
		}
		else
		{
			DeleteHttpHeadObj(httpHead);
		}
		
	return true;
}



//从数组解码为HTTP接收结构体
HttpRecvObj *DecodeHttpRecvObjFromBuff(u8 *data,u16 size)
{
		if(!data || size == 0)
		{
			Log("null");
			return 0;
		}
		pb_istream_t stream ;
		HttpRecvObj *httpRecvObj = PbMalloc(sizeof(HttpRecvObj));		
		if(!httpRecvObj)
		{
			Log("Malloc err");
			return 0;
		}
		
		 PbDataObj  *dataObj  = PbMalloc(sizeof(PbDataObj));
		if(!dataObj)
		{
			Log("Malloc err");
			PbFree(httpRecvObj);
			return 0;
		}
		dataObj->size = size;
		httpRecvObj->body.funcs.decode = DecodeBufferCallBack;
		httpRecvObj->body.arg = dataObj;
		
		T_OMCI_LIST *headList = PbMalloc(sizeof(T_OMCI_LIST));
		Log("Create HeadList:%d",headList);
		if(!headList)
		{
			PbFree(dataObj);
			PbFree(httpRecvObj);
		}
		OmciInitList(headList, sizeof(HttpHead *),20,(void (*)(void *))DeleteHttpHeadObjNode);
		httpRecvObj->head.funcs.decode = DecodeHttpRecvHeadCallBack;
		httpRecvObj->head.arg = headList;
		stream = pb_istream_from_buffer(data, size);
		u8 status = pb_decode(&stream,HttpRecvObj_fields,httpRecvObj);
		if(status)
		{
			return httpRecvObj;
		}
		else
		{
			DeleteHttpRecvObj(httpRecvObj);
			return 0;
		}
		
}
//从接收headList查找一个head
HttpHead * GetHeadByName(T_OMCI_LIST *headList,char *keyName)
{
	HttpHead *resHead=0;
	T_OMCI_LIST_NODE *pListNode = headList->pHead->pNext;
	while(pListNode != headList->pHead)
	{
			HttpHead *httpHead = *((HttpHead **)GET_NODE_DATA(pListNode));
			char * headKey = (char *)((PbDataObj  *)(httpHead->key.arg))->data;
			char * headValue = (char *)((PbDataObj  *)(httpHead->value.arg))->data;
			//Log("HttpDownFileRecvCall head node:%d,%s:%s",httpHead,headKey,headValue);
			if(strcmp(headKey,keyName)==0)
			{
				resHead = httpHead;
				break;
			}
			pListNode = pListNode->pNext;	
	}
	return resHead;
}


//先释放连接查询列表
//再释放连接查询体
void SelectDeleteConnectObj(SelectConnectObj *selectConnectObj)
{
	T_OMCI_LIST *selectConnectList = selectConnectObj->connectArray.arg;
	//遍历查询列表里面的每一个连接体进行释放
	//删除了连接列表里面的每一个连接对象
	OmciDestroyList(selectConnectList);
	Log("SelectDeleteConnectObj:%d",selectConnectList);
	PbFree(selectConnectList);
	PbFree(selectConnectObj);
}
//从数组解码为查询连接结构体
SelectConnectObj *DecodeSelectConnectObjFromBuff(u8 *data,u16 size)
{
	
		if(!data || size == 0)
		{
			Log("null");
			return 0;
		}
		pb_istream_t stream ;
		
		
		T_OMCI_LIST *selectConnectList = PbMalloc(sizeof(T_OMCI_LIST));
		OmciInitList(selectConnectList, sizeof(ConnectChangeObj *),10,(void (*)(void *))ConnectChangeObjNodeDelete);
		
		SelectConnectObj *selectConnectObj = PbMalloc(sizeof(SelectConnectObj));		
		if(!selectConnectObj)
		{
			Log("Malloc err");
			return 0;
		}
		selectConnectObj->connectArray.funcs.decode = DecodeConnectCallBack;
		selectConnectObj->connectArray.arg = selectConnectList;
		
		stream = pb_istream_from_buffer(data, size);
		u8 status = pb_decode(&stream, SelectConnectObj_fields,selectConnectObj);
		Log("DecodeConnectObjFromBuff status:%d",status);
		if(status)
		{
			return selectConnectObj;
		}
		else
		{
			SelectDeleteConnectObj(selectConnectObj);
			return 0;
		}
		
}


//连接里面的ip是一个数组
//先释放数组，再释放连接体
void ConnectChangeObjDelete(ConnectChangeObj *pbObj)
{
	Log("ConnectChangeObjDelete:%d",pbObj);
	PbDataObj  *dataObj = pbObj->ip.arg;
	if(dataObj->data)
	{
		printf("delete ip:%s",dataObj->data);
		PbFree(dataObj->data);
	}
	PbFree(dataObj);
	PbFree(pbObj);
}

//功能结构体删除
void FuncObjDelete(FuncObj *typeObj)
{
	PbDataObj  *dataObj = typeObj->data.arg;
	if(dataObj->data)
	{
		PbFree(dataObj->data);
	}
	PbFree(dataObj);
	PbFree(typeObj);
}

//参数是一个结点数据
void ConnectChangeObjNodeDelete(void *pData)
{
	ConnectChangeObj *connectObj = *((ConnectChangeObj **)(pData));
	//Log("ConnectChangeObjNodeDelete pData:%d,connectObj:%d,id:%d,sta:%d,type:",pData,connectObj,connectObj->id,connectObj->sta);
	ConnectChangeObjDelete(connectObj);
}



//参数是一个结点地址
 LIST_STATUS PrintConnectChangeObj(void *pvNode, void *pvTravInfo, uint32_t dwNodeDataSize)
{
	CHECK_SINGLE_POINTER(pvNode, OMCI_LIST_ERROR);
	T_OMCI_LIST_NODE *pNode = (T_OMCI_LIST_NODE *)pvNode;
	ConnectChangeObj *connectObj = *((ConnectChangeObj **)GET_NODE_DATA(pNode));
	Log("PrintConnectChangeObj node:%d,connectObj:%d,id:%d,sta:%d,type:",pNode,connectObj,connectObj->id,connectObj->sta);
  return OMCI_LIST_OK;
}

