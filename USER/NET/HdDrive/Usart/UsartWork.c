#include "UsartWork.h"
#include "GprsRecvHandler.h"

#define  USART_SEND_WAIT_TIME  		800 //200ms
#define  USART_MAX_RESEND_COUNT   3
#include "PbPort.h"
#include "GprsCmdUnPack.h"
#include "GprsCmdPack.h"

typedef struct {
	u8 *data;
	u16 size;
}UsartDataObj;

typedef struct {
/* protected: */
    QActive super;
		UsartDataObj chacheObj;
		QTimeEvt timeEvt;
		QEQueue requestQueue;
		QEvt const *requestQSto[10];
} UsartWork;

/* protected: */
static QState UsartWork_initial(UsartWork * const me, QEvt const * const e);
static QState USART_ON(UsartWork * const me, QEvt const * const e);
static QState USART_GPRS_RECV(UsartWork * const me, QEvt const * const e);
static QState USART_GPRS_SEND_WAITE(UsartWork * const me, QEvt const * const e);
static QState USART_CLOSE(UsartWork * const me, QEvt const * const e);
/*$enddecl${Usart::UsartWork} ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

/* instantiate the Blinky active object ------------------------------------*/
static UsartWork usartWork;
QActive * const US_UsartWork = &usartWork.super;

extern QActive * const WF_WifiWork;
extern QActive * const Gp_GprsWork;

/*$skip${QP_VERSION} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv*/
/* Check for the minimum required QP version */
#if (QP_VERSION < 650U) || (QP_VERSION != ((QP_RELEASE^4294967295U) % 0x3E8U))
#error qpc version 6.5.0 or higher required
#endif

u16 UsartDataSendDerive(u8 *buf,u16 len)
{
	UsartSendBuf(6,buf,len);
	return len;
}

static cycleQueue usartQue;

static void UsartRecvDataCallBack(u8 *dat,u16 size)
{
	u16 rcvSize;
	if(size <= MAX_USART6_SIZE)
	{
	 rcvSize= size;
	}
	else
	{
		rcvSize= MAX_USART6_SIZE;
	}
	if(US_UsartWork->super.state.fun)
	{
		BaseType_t xHigherPriorityTaskWoken = pdFALSE;
		InsertQueBuf(&usartQue,dat,rcvSize);
		UsartEvt *te;
		te = Q_NEW_FROM_ISR(UsartEvt, USART_RECV_SIG);
		te->v = rcvSize;
		QACTIVE_POST_FROM_ISR(US_UsartWork,
																	&te->super,
																	&xHigherPriorityTaskWoken,
																	&l_TickHook);
		portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
	}
}

void UsartWork_ctor(void) {
	UsartWork *me = (UsartWork *)US_UsartWork;
  QActive_ctor(&me->super, Q_STATE_CAST(&UsartWork_initial));
	QTimeEvt_ctorX(&me->timeEvt, &me->super, TIMEOUT_SIG, 0U);
//	QTimeEvt_ctorX(&me->timeEvt2, &me->super, TIMEOUT2_SIG, 0U);
	QEQueue_init(&me->requestQueue,me->requestQSto, Q_DIM(me->requestQSto));
}

static QState UsartWork_initial(UsartWork * const me, QEvt const * const e) {
    /*${Usart::UsartWork::SM::initial} */
		
		QActive_subscribe(&me->super, USART_SWITCH_FREE);
	
		int res = CreateQueue(&usartQue,MAX_USART6_SIZE*2);
		UsartLog("createQueue res:%d",res);
		UsartLog("UsartWork_initial");
    return Q_TRAN(&USART_CLOSE);
}

static QState USART_ON(UsartWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
					 case Q_ENTRY_SIG: {
						UsartLog("+++USART_ON");
						status_ = Q_HANDLED();
						SetUsartRecvBufCallBack(6,UsartRecvDataCallBack);
						UsartStart(6);
            break;
					}
					 case USART_RECV_SIG:{
						status_ = Q_HANDLED();
						u16 size = (Q_EVT_CAST(UsartEvt)->v);
						if(size)
						{
							u8 *data = NetMalloc(size);
							if(data)
							{
								ReadQueBuf(&usartQue,data,size);
								UsartLog("recv:%s,size:%d",(char *)data,size);
								NetFree(data);							
							}						
						}
						break;
					}
					 case USART_SEND_SIG: {
						status_ = Q_HANDLED();
						Log("USART_SEND_SIG");
            u8 *data = (u8 *)(Q_EVT_CAST(UsartEvt)->p);
						u16 size = Q_EVT_CAST(UsartEvt)->v;
						if(data)
						{	
							if(me->requestQueue.nFree >=2)
							{
								if(QActive_defer(&me->super, &me->requestQueue,e))
								{
									UsartLog("save send");
								}
								else 
								{
									/* notify the request sender that his request was denied... */
									UsartLog("ignored send");
									UsartFree(data);
								}
							}
							else
							{
								UsartFree(data);
							}
							
						}
          break;
					}
					 case USART_CLOSE_SIG:
						 status_ = Q_TRAN(&USART_CLOSE);
					 break;
				
        default: {
            status_ = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status_;
}


static QState USART_GPRS_RECV(UsartWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
					 case Q_ENTRY_SIG: {
						UsartLog("+++USART_GPRS_RECV");
						status_ = Q_HANDLED();	
						if(QActive_recall(&me->super, &me->requestQueue)){
									GprsLog("usart send save");
							}
							else {
									//ModLog("mod no save");
							}
            break;
					}
				 case USART_RECV_SIG: {
						status_ = Q_HANDLED();
						if(Gp_GprsWork->super.state.fun)
						{					
							u16 size = (Q_EVT_CAST(UsartEvt)->v);
							if(size)
							{
								u8 *data = NetMalloc(size+1);
								if(data)
								{
									u16 readSize = ReadQueBuf(&usartQue,data,size);
//									UsartLog("recv:%s,size:%d,readSize:%d",(char *)data,size,readSize);
									if(readSize)
									{
										data[readSize]=0;
										QpEvt *te;
										te = Q_NEW(QpEvt, USART_WORK_RECV_SIG);
										te->p = (uint32_t)data;
										te->v = readSize;
										QACTIVE_POST(Gp_GprsWork, &te->super, me);								
									}
									else
									{
										NetFree(data);		
									}			
								}						
							}	
						}						
          break;
        }
				
				case USART_SEND_SIG: {
						status_ = Q_HANDLED();
						Log("USART_SEND_SIG");
            u8 *data = (u8 *)(Q_EVT_CAST(UsartEvt)->p);
						u16 size = Q_EVT_CAST(UsartEvt)->v;
						if(data)
						{	
							me->chacheObj.data = data;
							me->chacheObj.size = size;
							status_ = Q_TRAN(&USART_GPRS_SEND_WAITE);
							//NetFree(data);
						}
          break;
        }
			default: {
					status_ = Q_SUPER(&USART_ON);
					break;
			}
    }
    return status_;
}




static QState USART_GPRS_SEND_WAITE(UsartWork * const me, QEvt const * const e) {
    QState status_;
		static u8 sendCount = 0;
    switch (e->sig) {
					 case Q_ENTRY_SIG: {
						UsartLog("+++USART_GPRS_SEND_WAITE");
						status_ = Q_HANDLED();	
						sendCount = 1;
						UsartDataSendDerive(me->chacheObj.data,me->chacheObj.size);
						QTimeEvt_armX(&me->timeEvt,USART_SEND_WAIT_TIME,0);
            break;
					}
	
					//检测是否为CRC16错误
					//如果CRC错误，重发
				 case USART_RECV_SIG: {
						status_ = Q_HANDLED();
						if(Gp_GprsWork->super.state.fun)
						{					
							u16 size = (Q_EVT_CAST(UsartEvt)->v);
							if(size)
							{
								u8 *data = NetMalloc(size+1);
								if(data)
								{
									u16 readSize = ReadQueBuf(&usartQue,data,size);
									//LogHex("usart recv:",(u8 *)data,readSize);
									if(readSize)
									{
										//判断是否为crc16错误包
										u32 err = CheckErrCmd(data,size);
										if(err == GPRS_ERR_CRC16 )
										{	
											Log("cmd err:%d,sendCount:%d",err,sendCount);
											if(sendCount <= USART_MAX_RESEND_COUNT)
											{
												UsartDataSendDerive(me->chacheObj.data,me->chacheObj.size);
												QTimeEvt_disarm(&me->timeEvt);
												QTimeEvt_armX(&me->timeEvt,USART_SEND_WAIT_TIME,0);
												sendCount++;
											}
											//交给上层处理
											else
											{
												data[readSize]=0;
												QpEvt *te;
												te = Q_NEW(QpEvt, USART_WORK_RECV_SIG);
												te->p = (uint32_t)data;
												te->v = readSize;
												QACTIVE_POST(Gp_GprsWork, &te->super, me);		
											}											
										}
										//交给上层处理
										else
										{
											data[readSize]=0;
											QpEvt *te;
											te = Q_NEW(QpEvt, USART_WORK_RECV_SIG);
											te->p = (uint32_t)data;
											te->v = readSize;
											QACTIVE_POST(Gp_GprsWork, &te->super, me);		
										}

									}
									else
									{
										NetFree(data);		
									}			
								}						
							}	
						}						
          break;
        }
				case TIMEOUT_SIG:{
						status_ = Q_HANDLED();			
						status_ =Q_TRAN(&USART_GPRS_RECV);
						break;
					}
				case Q_EXIT_SIG:{
					status_ = Q_HANDLED();	
					UsartLog("~~~USART_GPRS_SEND_WAITE");
					QTimeEvt_disarm(&me->timeEvt);
					UsartFree(me->chacheObj.data);
					me->chacheObj.data = 0;
					me->chacheObj.size = 0;
				break;
				}
					
					//////////////////////////////////	
			default: {
					status_ = Q_SUPER(&USART_ON);
					break;
			}
    }
    return status_;
}


/*${Usart::UsartWork::SM::USART_CLOSE} ......................................*/
static QState USART_CLOSE(UsartWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
				 case Q_ENTRY_SIG: {
						UsartLog("+++USART_CLOSE");
					 SetUsartRecvBufCallBack(6,0);
						UsartStop(6);
						status_ = Q_HANDLED();	
            break;
        }
				case USART_SEND_SIG: {
						status_ = Q_HANDLED();
						Log("USART_SEND_SIG");
            u8 *data = (u8 *)(Q_EVT_CAST(UsartEvt)->p);
						u16 size = Q_EVT_CAST(UsartEvt)->v;
						if(data)
						{	
							if(me->requestQueue.nFree >=2)
							{
								if(QActive_defer(&me->super, &me->requestQueue,e))
								{
									UsartLog("save send");
								}
								else 
								{
									/* notify the request sender that his request was denied... */
									UsartLog("ignored send");
									UsartFree(data);
								}
							}
							else
							{
								UsartFree(data);
							}
							
						}
          break;
					}
				case USART_RECV_SIG:{
						status_ = Q_HANDLED();
						u16 size = (Q_EVT_CAST(UsartEvt)->v);
						if(size)
						{
							u8 *data = NetMalloc(size);
							if(data)
							{
								ReadQueBuf(&usartQue,data,size);
								UsartLog("recv:%s,size:%d",(char *)data,size);
								NetFree(data);							
							}						
						}
						break;
					}			 
				case USART_SWITCH_GPRS:{
						UsartLog("USART_CLOSE USART_SWITCH_GPRS");
						status_ = Q_TRAN(&USART_GPRS_RECV);
					break;
				}
        default: {
            status_ = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status_;
}
/*$enddef${Usart::UsartWork} ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

