#pragma once
#include "NetInclude.h"
#include "qpc.h"
#include "malloc.h"
#include "bsp.h"
#include "sys.h"
#include "usart.h"
#include "GprsWork.h"

#if 1
	#define UsartLog Log
#else
	#define UsartLog 
#endif


#define UsartMalloc(n) mymalloc(SRAMIN,n)
#define UsartFree(n) myfree(SRAMIN,n)
