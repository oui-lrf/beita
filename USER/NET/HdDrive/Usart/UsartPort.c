#include "UsartInclude.h"
#include "UsartWork.h" 

extern QActive * const US_UsartWork;

void UsartDataSendBuf(u8 *data,u16 size)
{
	//printf("UsartDataSendBuf malloc:%d\r\n",my_mem_perused(SRAMIN));	
	u8 *dat = NetMalloc(size);
	if(!dat)
	{
		Log("Usart Malloc err");
		return;
	}
	memcpy(dat,data,size);
	UsartEvt *te;
	te = Q_NEW(UsartEvt, USART_SEND_SIG);
	te->p = (uint32_t)dat;
	te->v = size;
	QACTIVE_POST(US_UsartWork,&te->super,0);
}


