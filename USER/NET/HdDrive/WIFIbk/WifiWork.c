
#include "WifiDrive.h"    /* Board Support Package interface */
#include "WifiPort.h"
#include "WifiWork.h"

/* ask QM to declare the Blinky class --------------------------------------*/
/*$declare${Wifi::WifiWork} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv*/
/*${Wifi::WifiWork} ........................................................*/
typedef struct {
/* protected: */
    QActive super;

/* private: */
    QTimeEvt timeEvt;
		QTimeEvt timeEvt2;
		QEQueue requestQueue;
		QEvt const *requestQSto[4];
		char wifiName[40+1];
		char wifiPass[40+1];
		char saveIp[40+1];
		u16 savePort;
} WifiWork;


extern void WifiSignalChangeCall(u16 value);

/* protected: */
static QState WifiWork_initial(WifiWork * const me, QEvt const * const e);
static QState WIFI_OPEN(WifiWork * const me, QEvt const * const e);
static QState WIFI_OPEN_AT(WifiWork * const me, QEvt const * const e);
static QState WIFI_OPEN_RST(WifiWork * const me, QEvt const * const e);
static QState WIFI_OPEN_RST_AT(WifiWork * const me, QEvt const * const e);
static QState WIFI_ON(WifiWork * const me, QEvt const * const e);

static QState WIFI_START_AT(WifiWork * const me, QEvt const * const e);
static QState WIFI_ATE0(WifiWork * const me, QEvt const * const e);
static QState WIFI_START_CMD(WifiWork * const me, QEvt const * const e);
static QState WIFI_CWMODE_CK(WifiWork * const me, QEvt const * const e);
static QState WIFI_CWMODE(WifiWork * const me, QEvt const * const e);
static QState WIFI_CWJAP_CK(WifiWork * const me, QEvt const * const e);
static QState WIFI_CWJAP(WifiWork * const me, QEvt const * const e);

static QState WIFI_CWLAP(WifiWork * const me, QEvt const * const e);
static QState WIFI_CIPMUX(WifiWork * const me, QEvt const * const e);

static QState WIFI_CIPMODE(WifiWork * const me, QEvt const * const e);
static QState WIFI_CIPSTART(WifiWork * const me, QEvt const * const e);
static QState WIFI_TRANS_IN(WifiWork * const me, QEvt const * const e);
static QState WIFI_TRANS(WifiWork * const me, QEvt const * const e);
static QState WIFI_CIPSTATUS(WifiWork * const me, QEvt const * const e);
static QState WIFI_CIPCLOSE(WifiWork * const me, QEvt const * const e);

static QState WIFI_CWSMARTSTART_AT(WifiWork * const me, QEvt const * const e);
static QState WIFI_CWSMARTSTART_WAIT(WifiWork * const me, QEvt const * const e);
static QState WIFI_CWSMARTSTART_EXIT_TRANS(WifiWork * const me, QEvt const * const e);
static QState WIFI_CWSMART(WifiWork * const me, QEvt const * const e);
static QState WIFI_CWSMART_EXIT(WifiWork * const me, QEvt const * const e);

static QState WIFI_CWSMART_JAP_CK(WifiWork * const me, QEvt const * const e);
static QState WIFI_CWSMART_JAP(WifiWork * const me, QEvt const * const e);
static QState WIFI_CONNECT(WifiWork * const me, QEvt const * const e);
static QState WIFI_RST(WifiWork * const me, QEvt const * const e);

/*$enddecl${Wifi::WifiWork} ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

/* instantiate the Blinky active object ------------------------------------*/
static WifiWork wifiWork;
QActive * const WF_WifiWork = &wifiWork.super;
extern QActive * const US_UsartWork;
Q_DEFINE_THIS_MODULE("WifiWork")
/**
* @brief
* @param  cmd:命令字符串
timeOut:超时未收到命令触发失败时间,单位：秒
call:超时回调函数
* @retval
* @author oui
* @date   2018-06-11
*/
//发送命令后开启一个定时器，定时时间到没有收到回复就回调失败函数，并通知接收线程即使收到回复也不处理
//如果在规定时间内得到了返回，就关闭定时器不在等待计数,失败函数就不会被触发
//发送命令前检测如果有指令待返回就回复上层失败
static void WifiWorkSendCmd(WifiWork * const me,char *cmd,u16 timeOut)
{
    if(cmd)
    {
        //WifiLog("WifiDriveSend cmd:%s wait:%d",cmd,timeOut);
        WifiDriveSend((u8 *)cmd,strlen(cmd));
    }
    else
    {
        WifiLog("Wifi No Send wait:%d",timeOut);
    }
    if(timeOut >0)
    {
				//if(QTimeEvt_wasDisarmed(&me->timeEvt) != true)
				{
					QTimeEvt_disarm(&me->timeEvt);
				}
      QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*timeOut,0);
    }

}

extern QActive * const TP_TcpWork;
/* ask QM to define the Blinky class ---------------------------------------*/
/*$skip${QP_VERSION} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv*/
/* Check for the minimum required QP version */
#if (QP_VERSION < 650U) || (QP_VERSION != ((QP_RELEASE^4294967295U) % 0x3E8U))
#error qpc version 6.5.0 or higher required
#endif
/*$endskip${QP_VERSION} ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
/*$define${Wifi::WifiWork_ctor} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv*/
/*${Wifi::WifiWork_ctor} ...................................................*/
void WifiWork_ctor(void) {
		WifiLog("WifiWork_ctor");
    WifiWork *me = (WifiWork *)WF_WifiWork;
	
    QActive_ctor(&me->super, Q_STATE_CAST(&WifiWork_initial));
    QTimeEvt_ctorX(&me->timeEvt, &me->super, TIMEOUT_SIG, 0U);
		QTimeEvt_ctorX(&me->timeEvt2, &me->super, TIMEOUT2_SIG, 0U);
		QEQueue_init(&me->requestQueue,
                 me->requestQSto, Q_DIM(me->requestQSto));
}

/*$enddef${Wifi::WifiWork_ctor} ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
/*$define${Wifi::WifiWork} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv*/
/*${Wifi::WifiWork} ........................................................*/
/*${Wifi::WifiWork::SM} ....................................................*/
static QState WifiWork_initial(WifiWork * const me, QEvt const * const e) {
    /*${Wifi::WifiWork::SM::initial} */
   // QS_SIG_DICTIONARY(USART_WORK_RECV_SIG,(void *)0);
		WifiLog("WifiWork_initial");
	
		GetWifiName(me->wifiName,40);
		GetWifiPass(me->wifiPass,40);
    return Q_TRAN(&WIFI_OPEN);
}


//在所有状态下都可以接收关闭命令
static QState WIFI_ON(WifiWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_OPEN} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_ON");
						QACTIVE_POST(US_UsartWork, &Q_NEW(QpEvt, USART_SWITCH_WIFI)->super, me);
						status_ = Q_HANDLED();	
            break;
        }
				//对连接请求进行储存，实现打开成功后自动进入连接状态
				case WIFI_CONNECT_SIG:{
					Log("WIFI_ON TCP_CONNECT_SIG");
					char *ip = (char *)(Q_EVT_CAST(QpEvt)->p);
					if(ip)
					{
						TcpLog("WIFI_ON ip:%s,port:%d nFree:%d",ip,Q_EVT_CAST(QpEvt)->v,me->requestQueue.nFree); 
						if(me->requestQueue.nFree >=5)//只延续保存一个延迟事件
						{
							if (QActive_defer(&me->super, &me->requestQueue, e))
							{
								TcpLog("Request port:%d",(int)Q_EVT_CAST(QpEvt)->v);
							}
							else 
							{
								/* notify the request sender that his request was denied... */
								TcpLog("Request IGNORED:%d",(int)Q_EVT_CAST(QpEvt)->v);
							}
						}
						else
						{
							WifiFree(ip);
						}
            
					}
					status_ = Q_HANDLED();	
					break;
				}
				//还没有连上就发送，数据，直接把数据释放，并上报数据发送失败
				case WIFI_TCP_SEND_SIG:{
					Log("TCP_WIFI TCP_SEND_SIG");
					u8 *data = (u8 *)(Q_EVT_CAST(QpEvt)->p);
					if(data)
					{
						QF_PUBLISH(&Q_NEW(QpEvt, WIFI_SEND_FAILED_SIG)->super, me);
						WifiFree(data);
					}
					status_ = Q_HANDLED();	
					break;
				}
				//在任何状态下一定要订阅消息接收，防止内存泄漏
				case USART_WORK_RECV_SIG:{
					Log("TCP_WIFI USART_WORK_RECV_SIG");
					u8 *rdata = (u8 *)(Q_EVT_CAST(QpEvt)->p);
					if(rdata)
					{
						WifiFree(rdata);
					}
					status_ = Q_HANDLED();	
					break;
				}
				case WIFI_CONFIG_SIG:{
					status_ = Q_TRAN(&WIFI_CWSMARTSTART_AT);
					break;
				}
			 case WIFI_CLOSE_SIG:{
						status_ = Q_HANDLED();	
						WifiLog("WIFI_ON WIFI_CLOSE_SIG");
						QACTIVE_POST(US_UsartWork, &Q_NEW(QpEvt, USART_CLOSE_SIG)->super, me);
						WifiSwClose(); 
						QActive_unsubscribeAll(&me->super);
						QF_gc(e);
						QActive_stop(&me->super);
						
					break;
				 }
        default: {
            status_ = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status_;
}

/*${Wifi::WifiWork::SM::WIFI_OPEN} .........................................*/
static QState WIFI_OPEN(WifiWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_OPEN} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_OPEN");
						WifiSwOpen(); 
						QTimeEvt_disarm(&me->timeEvt);
						QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*5,0);
						status_ = Q_HANDLED();	
            break;
        }
        /*${Wifi::WifiWork::SM::WIFI_OPEN::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
						WifiLog("WIFI_OPEN TIMEOUT_SIG");				
//					//如果打开失败，要执行OPEN_AT
//				  //如果OPEN_TA失败，要执行OPEN_RST
//				  //如果OPEN_RST失败，要执行OPEN_RST_AT
//				  //如果OPEN_RST_AT失败，则返回上层OPEN_WIFI_FAILED
						status_ = Q_TRAN(&WIFI_OPEN_AT);
            break;
        } 
				 case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
//							WifiLog("wifi recv size:%d",Q_EVT_CAST(QpEvt)->v); 
							if(strstr(data,"ready") || strstr(data,"WIFI GOT IP"))
							{
								QTimeEvt_disarm(&me->timeEvt);
								WifiLog("WIFI_OPEN OK");
								status_ = Q_TRAN(&WIFI_CONNECT);
								QF_PUBLISH(&Q_NEW(QpEvt, WIFI_OPEN_SUCCES_SIG)->super, me);
							}
							WifiFree(data);
						}
						
          break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}

/*${Wifi::WifiWork::SM::WIFI_OPEN} .........................................*/
static QState WIFI_OPEN_AT(WifiWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_OPEN} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_OPEN_AT");
            WifiWorkSendCmd(me,"AT\r\n",2);
						status_ = Q_HANDLED();	
            break;
        }
        /*${Wifi::WifiWork::SM::WIFI_OPEN::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
						WifiLog("WIFI_OPEN_AT Timeout");
						status_ = Q_TRAN(&WIFI_OPEN_RST);
            break;
        } 
				 case USART_WORK_RECV_SIG:{
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v); 
							if(strstr(data,"OK"))
							{
								WifiLog("WIFI_OPEN AT OK");
								//广播消息
								//返回ON状态
								QTimeEvt_disarm(&me->timeEvt);
								QF_PUBLISH(&Q_NEW(QpEvt, WIFI_OPEN_SUCCES_SIG)->super, me);
								status_ = Q_TRAN(&WIFI_CONNECT);
							}
							WifiFree(data);
						}
						
          break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}

static QState WIFI_OPEN_RST(WifiWork * const me, QEvt const * const e) {
	QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_OPEN} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_OPEN_RST");
            WifiRst();
						QTimeEvt_disarm(&me->timeEvt);
						QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*5,0);
						status_ = Q_HANDLED();	
            break;
        }
        /*${Wifi::WifiWork::SM::WIFI_OPEN::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
						WifiLog("WIFI_OPEN_RST Timeout");
						status_ = Q_TRAN(&WIFI_OPEN_RST_AT);
            break;
        } 
				 case USART_WORK_RECV_SIG: {
					  status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v); 
							if(strstr(data,"ready") || strstr(data,"WIFI GOT IP"))
							{
								WifiLog("WIFI_OPEN RST OK");
								QTimeEvt_disarm(&me->timeEvt);
								//广播消息
								//返回ON状态
								QF_PUBLISH(&Q_NEW(QpEvt, WIFI_OPEN_SUCCES_SIG)->super, me);
								status_ = Q_TRAN(&WIFI_CONNECT);
							}
							WifiFree(data);
						}
						break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;

}


static QState WIFI_OPEN_RST_AT(WifiWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_OPEN} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_OPEN_RST_AT");
            WifiWorkSendCmd(me,"AT\r\n",2);
						status_ = Q_HANDLED();	
            break;
        }
        /*${Wifi::WifiWork::SM::WIFI_OPEN::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
						WifiLog("WIFI_OPEN_RST_AT Timeout");
					//向上层发送打开失败
						QF_PUBLISH(&Q_NEW(QpEvt, WIFI_OPEN_FAILED_SIG)->super, me);
						status_ = Q_HANDLED();	
            break;
        } 
				 case USART_WORK_RECV_SIG:{
					  status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v); 
							if(strstr(data,"OK"))
							{
								WifiLog("WIFI_OPEN_RST_AT OK");
								//广播消息
								QTimeEvt_disarm(&me->timeEvt);
								QF_PUBLISH(&Q_NEW(QpEvt, WIFI_OPEN_SUCCES_SIG)->super, me);
								status_ = Q_TRAN(&WIFI_CONNECT);
							}
							WifiFree(data);
						}
						
          break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}

static QState WIFI_CONNECT(WifiWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_CONNECT");
						if (QActive_recall(&me->super, &me->requestQueue)) {
                Log("Request recalled ok");
            }
            else {
                Log("No deferred requests");
            }
						status_ = Q_HANDLED();
            break;
        }
				 /*${AOs::Philo::SM::hungry::EAT} */
        case WIFI_CONNECT_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v);
							strncpy(me->saveIp,data,40);
							me->savePort = Q_EVT_CAST(QpEvt)->v;
							status_ = Q_TRAN(&WIFI_START_AT);
							//status_ = Q_TRAN(&WIFI_RST);
							WifiFree(data);
						}
						
          break;
        }

        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}


static QState WIFI_RST(WifiWork * const me, QEvt const * const e) {
	QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_OPEN} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_RST");
            WifiRst();
						QTimeEvt_disarm(&me->timeEvt);
						QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*5,0);
						status_ = Q_HANDLED();	
            break;
        }
        /*${Wifi::WifiWork::SM::WIFI_OPEN::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
						WifiLog("WIFI_RST Timeout");
						status_ = Q_TRAN(&WIFI_START_AT);
            break;
        } 
				 case USART_WORK_RECV_SIG: {
					  status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v); 
							if(strstr(data,"ready") || strstr(data,"WIFI GOT IP"))
							{
								WifiLog("WIFI_RST OK");
								QTimeEvt_disarm(&me->timeEvt);
								status_ = Q_TRAN(&WIFI_START_AT);
							}
							WifiFree(data);
						}
						break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;

}

static QState WIFI_START_AT(WifiWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_AT");
            WifiWorkSendCmd(me,"AT\r\n",2);
						status_ = Q_HANDLED();
            break;
        }
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v);
							QTimeEvt_disarm(&me->timeEvt);
							WifiLog("AT OK\r\n");
							status_ = Q_TRAN(&WIFI_ATE0);
							WifiFree(data);
						}
						
          break;
        }
        /*${Wifi::WifiWork::SM::WIFI_AT::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
           status_ = Q_TRAN(&WIFI_RST);
            break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}

static QState WIFI_ATE0(WifiWork * const me, QEvt const * const e)
{
	QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_ATE0");
            WifiWorkSendCmd(me,"ATE0\r\n",2);
						status_ = Q_HANDLED();
            break;
        }
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
						
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v);
							if(strstr(data,"OK"))
							{	
								QTimeEvt_disarm(&me->timeEvt);
								status_ = Q_TRAN(&WIFI_CWMODE_CK);
							}
							WifiFree(data);
						}
						
          break;
        }
        /*${Wifi::WifiWork::SM::WIFI_AT::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
           status_ = Q_TRAN(&WIFI_START_AT);
            break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}

static QState WIFI_START_CMD(WifiWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_START_CMD");
            WifiWorkSendCmd(me,"AT+CWMODE=1\r\n",2);
						status_ = Q_HANDLED();
            break;
        }
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v);
							QTimeEvt_disarm(&me->timeEvt);
							status_ = Q_TRAN(&WIFI_START_AT); 
							WifiFree(data);
						}
          break;
        }
        /*${Wifi::WifiWork::SM::WIFI_AT::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
           status_ = Q_TRAN(&WIFI_START_AT);
            break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}
static QState WIFI_CWMODE_CK(WifiWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_CWMODE_CK");
            WifiWorkSendCmd(me,"AT+CWMODE?\r\n",4);
						status_ = Q_HANDLED();
            break;
        }
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v);
							if(strstr(data,"CWMODE:1") ||strstr(data,"CWMODE:3"))
							{
								QTimeEvt_disarm(&me->timeEvt);
								status_ = Q_TRAN(&WIFI_CWJAP_CK);
							}
							else if(strstr(data,"CWMODE:2"))
							{
								QTimeEvt_disarm(&me->timeEvt);
								status_ = Q_TRAN(&WIFI_CWMODE);
							}
							WifiFree(data);
						}
          break;
        }
        /*${Wifi::WifiWork::SM::WIFI_AT::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
						WifiLog("WIFI_CWMODE_CK TIMEOUT_SIG");
           status_ = Q_TRAN(&WIFI_CWMODE_CK);
            break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}
static QState WIFI_CWMODE(WifiWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_CWMODE");
            WifiWorkSendCmd(me,"AT+CWMODE=1\r\n",2);
						status_ = Q_HANDLED();
            break;
        }
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							QTimeEvt_disarm(&me->timeEvt);
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v);
							if(strstr(data,"OK"))
							{
								status_ = Q_TRAN(&WIFI_RST);
							}
							else
							{
								status_ = Q_TRAN(&WIFI_START_AT);
							}
							WifiFree(data);
						}
						
          break;
        }
        /*${Wifi::WifiWork::SM::WIFI_AT::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
           status_ = Q_TRAN(&WIFI_RST);
            break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}
static QState WIFI_CWJAP_CK(WifiWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_CWJAP_CK");
            WifiWorkSendCmd(me,"AT+CWJAP?\r\n",2);
						status_ = Q_HANDLED();
            break;
        }
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							QTimeEvt_disarm(&me->timeEvt);
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v);
							//如果已经连接网络,进入下一步
							//检测已经连上的是否为指定名称，如果不是，断线重连
							if(strstr(data,"+CWJAP:"))
							{
								WifiLog("CWJAP CK connect to wifi ok");
								if(strstr(data,me->wifiName))
								{
									status_ = Q_TRAN(&WIFI_CWLAP);
								}
								else
								{
									status_ = Q_TRAN(&WIFI_CWJAP);
								}
							}
							else if(strstr(data,"No AP"))
							{
								status_ = Q_TRAN(&WIFI_CWJAP);
							}
							else if(strstr(data,"CONNECTED"))
							{
								WifiLog("connect to wifi ok");
								status_ = Q_TRAN(&WIFI_CWLAP);
							}
							else //如果没有连上wifi
							{
								status_ = Q_TRAN(&WIFI_CWJAP); 
							}
							WifiFree(data);
						}
						
          break;
        }
        /*${Wifi::WifiWork::SM::WIFI_AT::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
           status_ = Q_TRAN(&WIFI_CWJAP);
            break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}




static QState WIFI_CWJAP(WifiWork * const me, QEvt const * const e)
{
	  QState status_ ;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_CWJAP");
					
						char *rebuf;
						rebuf = NetMalloc(100);//申请1K字节c
						if(rebuf)
						{
								snprintf((char *)rebuf,100,"AT+CWJAP=\"%s\",\"%s\"\r\n",me->wifiName,me->wifiPass);
								WifiWorkSendCmd(me,rebuf,30);
								myfree(SRAMIN,rebuf);
						}
						status_ = Q_HANDLED();
            break;
        }
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v);
							if(strstr(data,"CONNECTED"))
							{
								QTimeEvt_disarm(&me->timeEvt);
								WifiLog("connect to wifi ok");
								status_ = Q_TRAN(&WIFI_CWLAP);
							}
							else if(strstr(data,"DISCONNECT"))
							{
								QTimeEvt_disarm(&me->timeEvt);
								status_ = Q_TRAN(&WIFI_CWJAP); 
							}
							else if(strstr(data,"No AP"))
							{
								QTimeEvt_disarm(&me->timeEvt);
								status_ = Q_TRAN(&WIFI_CWJAP); 
							}
							else if(strstr(data,"OK"))
							{
								QTimeEvt_disarm(&me->timeEvt);
								status_ = Q_TRAN(&WIFI_CWJAP_CK); 
							}
							else if(strstr(data,"FAIL") || strstr(data,"connect station fail")) //提交给上层 显示密码错误
							{
								QTimeEvt_disarm(&me->timeEvt);
								QF_PUBLISH(&Q_NEW(QpEvt, WIFI_CONNECT_FAILED_SIG)->super, me);
							}
							else
							{
								WifiLog("connect to wifi ,wait");
							}
							WifiFree(data);
						}
          break;
        }
        /*${Wifi::WifiWork::SM::WIFI_AT::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
           status_ = Q_TRAN(&WIFI_RST);
            break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}


static QState WIFI_CWLAP(WifiWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_CWLAP");
            WifiWorkSendCmd(me,"AT+CWLAP\r\n",4);
						status_ = Q_HANDLED();
            break;
        }
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							//printf("recv:%s\r\n",data);
							u16 len = strlen(me->wifiName);
							char *selectStr = NetMalloc(len+4);
							if(selectStr)
							{
								snprintf(selectStr,len+4,"\"%s\"",me->wifiName);			
								char *namePos = strstr(data,selectStr);
								//Log("selectStr:%s",selectStr);
								if(namePos) 
								{
									char *sigPos = strstr(namePos,"\",");
									if(sigPos)
									{
										int dbSig = atoi(sigPos+2);
										if(dbSig<0)
										{
											int sig = 2*(dbSig+100);
											Log("sig:%d",sig);
											WifiSignalChangeCall(sig);
											QTimeEvt_disarm(&me->timeEvt);
											status_ = Q_TRAN(&WIFI_CIPMUX);
										}
									}
								}								
								NetFree(selectStr);
							}
							

							WifiFree(data);
						}
          break;
        }
        /*${Wifi::WifiWork::SM::WIFI_AT::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
           status_ = Q_TRAN(&WIFI_CWLAP);
            break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}
static QState WIFI_CIPMUX(WifiWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_CIPMUX");
            WifiWorkSendCmd(me,"AT+CIPMUX=0\r\n",5);
						status_ = Q_HANDLED();
            break;
        }
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
							status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							WifiLog("recv:%s",data);
						if(strstr(data,"OK")||strstr(data,"link is builded")) 
							{
								QTimeEvt_disarm(&me->timeEvt);
								WifiLog("WF_CIPMUX ok");
								status_ = Q_TRAN(&WIFI_CIPMODE);
							}
							else if(strstr(data,"busy p"))
							{
								QTimeEvt_disarm(&me->timeEvt);
								status_ = Q_TRAN(&WIFI_CIPMUX);
							}
							WifiFree(data);
						}
          break;
        }
        /*${Wifi::WifiWork::SM::WIFI_AT::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
           status_ = Q_TRAN(&WIFI_RST);
            break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}


static QState WIFI_CIPMODE(WifiWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_CIPMODE");
            WifiWorkSendCmd(me,"AT+CIPMODE=1\r\n",2);
						status_ = Q_HANDLED();
            break;
        }
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v);
							QTimeEvt_disarm(&me->timeEvt);
							if(strstr(data,"OK"))
							{
								WifiLog("WF_CIPMODE ok");
								status_ = Q_TRAN(&WIFI_CIPSTART);
							}
							else
							{
								WifiLog("WF_CIPMODE err");
								status_ = Q_TRAN(&WIFI_CIPMUX);
							}
							
							WifiFree(data);
						}
          break;
        }
        /*${Wifi::WifiWork::SM::WIFI_AT::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
           status_ = Q_TRAN(&WIFI_RST);
            break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}
static QState WIFI_CIPSTART(WifiWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_CIPSTART");
            u8 *conbuf;
						conbuf = NetMalloc(100);//申请1K字节c
						if(conbuf)
						{
								memset(conbuf,0,100);
								WifiLog("ip:%s",me->saveIp);
								WifiLog("port:%d",me->savePort);
								snprintf((char *)conbuf,100,(char *)"AT+CIPSTART=\"TCP\",\"%s\",%d\r\n",me->saveIp,me->savePort);
								WifiLog("conbuf:%s",conbuf);
								WifiWorkSendCmd(me,(char *)conbuf,60);
								NetFree(conbuf);
						}
						else
						{
							WifiLog("WF_CIPSTART malloc err");
						}
						status_ = Q_HANDLED();
            break;
        }
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v);
							QTimeEvt_disarm(&me->timeEvt);
							if(strstr(data,"CONNECT"))
							{
								WifiLog("WF_CIPSTART ok");
								status_ = Q_TRAN(&WIFI_TRANS_IN);
							}
							else if(strstr(data,"busy p"))
							{
								QTimeEvt_disarm(&me->timeEvt);
								QTimeEvt_armX(&me->timeEvt2,BSP_TICKS_PER_SEC*2,0);
							}
							else if(strstr(data,"no ip") || strstr(data,"ERROR") || strstr(data,"CLOSED"))
							{
								WifiLog("WF_CIPSTART failed");
								QF_PUBLISH(&Q_NEW(QpEvt, WIFI_CONNECT_FAILED_SIG)->super, me);
							}
							WifiFree(data);
						}
          break;
        }
				case TIMEOUT2_SIG: {
						WifiLog("TIMEOUT2_SIG");
						status_ = Q_TRAN(&WIFI_CIPSTATUS);
            break;
        } 
        /*${Wifi::WifiWork::SM::WIFI_AT::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
           QF_PUBLISH(&Q_NEW(QpEvt, WIFI_CONNECT_FAILED_SIG)->super, me);
            break;
        }
				case Q_EXIT_SIG:{
						QTimeEvt_disarm(&me->timeEvt2);
						status_ = Q_HANDLED();	
				break;
				}
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}

static QState WIFI_TRANS_IN(WifiWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_TRANS_IN");
            WifiWorkSendCmd(me,"AT+CIPSEND\r\n",2);
						status_ = Q_HANDLED();
            break;
        }
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v);
							QTimeEvt_disarm(&me->timeEvt);
							if(strstr(data,"OK")) //进入传输模式后挂起接收任务
							{
								WifiLog("WF_TRANS_IN ok");
								QF_PUBLISH(&Q_NEW(QpEvt, WIFI_CONNECT_SUCCES_SIG)->super, me);
								status_ = Q_TRAN(&WIFI_TRANS);
							}
							else
							{
								WifiLog("WF_TRANS_IN err");
								QF_PUBLISH(&Q_NEW(QpEvt, WIFI_CONNECT_FAILED_SIG)->super, me);
							}
							WifiFree(data);
						}
						
          break;
        }
        /*${Wifi::WifiWork::SM::WIFI_AT::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
           status_ = Q_TRAN(&WIFI_CIPMUX);
            break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}


static QState WIFI_TRANS(WifiWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_TRANS");
						status_ = Q_HANDLED();
            break;
        }
				////通过发送消息到TCP才能调用WIFI发送，是为了保证只有TCP为连接状态才能给底层发送数据
				case WIFI_TCP_SEND_SIG:{
//					 Log("WIFI_TRANS WIFI_SEND_SIG");
					 u8 *data = (u8 *)(Q_EVT_CAST(QpEvt)->p);
					 u16 size = (u16)(Q_EVT_CAST(QpEvt)->v);
						if(data && size >0)
						{
							WifiDriveSend(data,size);
							WifiFree(data);
						}
						status_ = Q_HANDLED();	
						break;
				}
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							u16 size = Q_EVT_CAST(QpEvt)->v;
//							WifiLog("wifi recv size:%d",size);
							
							QpEvt *te;
							te = Q_NEW(QpEvt, WIFI_RECV_SIG);
							te->p = (uint32_t)data;
							te->v = size;
							//上层要保证该消息一定能被处理，防止内存泄漏
							QF_PUBLISH(&te->super, me);
						}
          break;
        }
				case WIFI_CONFIG_SIG:{
					status_ = Q_TRAN(&WIFI_CWSMARTSTART_EXIT_TRANS); 
					break;
				}
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}



static QState WIFI_CIPSTATUS(WifiWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_AT");
            WifiWorkSendCmd(me,"ATO\r\n",2);
						status_ = Q_HANDLED();
            break;
        }
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v);
							if(strstr(data,"+CIPSTATUS:"))
							{
								QTimeEvt_disarm(&me->timeEvt);
								status_ = Q_TRAN(&WIFI_TRANS_IN);
							}
							else if(strstr(data,"busy p"))
							{
								//什么也不做，就延时等待到超时处理
							}
							else
							{
								QTimeEvt_disarm(&me->timeEvt);
								status_ = Q_TRAN(&WIFI_CIPSTART);
							}
							WifiFree(data);
						}
						
          break;
        }
        /*${Wifi::WifiWork::SM::WIFI_AT::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
           status_ = Q_TRAN(&WIFI_CIPSTATUS);
            break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}
static QState WIFI_CIPCLOSE(WifiWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_AT");
            WifiWorkSendCmd(me,"ATO\r\n",2);
						status_ = Q_HANDLED();
            break;
        }
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							QTimeEvt_disarm(&me->timeEvt);
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v);
							status_ = Q_TRAN(&WIFI_CIPMUX); 
							WifiFree(data);
						}
						
          break;
        }
        /*${Wifi::WifiWork::SM::WIFI_AT::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
           status_ = Q_TRAN(&WIFI_RST);
            break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}

static QState WIFI_CWSMARTSTART_AT(WifiWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_CWSMARTSTART_AT");
            WifiWorkSendCmd(me,"AT\r\n",2);
						status_ = Q_HANDLED();
            break;
        }
				case WIFI_CONFIG_SIG:{
					status_ = Q_HANDLED();
					break;
				}				
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v);
							QTimeEvt_disarm(&me->timeEvt);
							if(strstr(data,"OK"))
							{
								WifiLog("WF_CWSMARTSTART_EXIT_TRANS OK");
								status_ = Q_TRAN(&WIFI_CWSMART); 
							}
							else
							{
								WifiLog("WF_CWSMARTSTART_AT ERR");
								status_ = Q_TRAN(&WIFI_CWSMARTSTART_EXIT_TRANS); 
							}		
							
							WifiFree(data);
						}
						
          break;
        }
        /*${Wifi::WifiWork::SM::WIFI_AT::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
						status_ = Q_TRAN(&WIFI_CWSMARTSTART_EXIT_TRANS);
            break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}
static QState WIFI_CWSMARTSTART_EXIT_TRANS(WifiWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_CWSMARTSTART_EXIT_TRANS");
            WifiWorkSendCmd(me,"+++",2);
						status_ = Q_HANDLED();
            break;
        }
				case WIFI_CONFIG_SIG:{
					status_ = Q_HANDLED();
					break;
				}				
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v);
							QTimeEvt_disarm(&me->timeEvt);
							if(strstr(data,"OK"))
							{
								WifiLog("WF_CWSMARTSTART_EXIT_TRANS OK");
								status_ = Q_TRAN(&WIFI_CWSMART); 
							}else
							{
								WifiLog("WF_CWSMARTSTART_EXIT_TRANS ERR");
								status_ = Q_TRAN(&WIFI_CWSMARTSTART_AT); 
							}
							WifiFree(data);
						}
          break;
        }
        /*${Wifi::WifiWork::SM::WIFI_AT::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
           status_ = Q_TRAN(&WIFI_CWSMARTSTART_AT);
        break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}
static QState WIFI_CWSMART(WifiWork * const me, QEvt const * const e)
{
	  QState status_;
		static u8 errCount;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_CWSMART");
						//AT+CWSTARTSMART
            WifiWorkSendCmd(me,"AT+CWSTARTSMART\r\n",2);//AT+CWSMARTSTART
						errCount =0;
						status_ = Q_HANDLED();
            break;
        }
				case WIFI_CONFIG_SIG:{
					status_ = Q_HANDLED();
					break;
				}				
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v);
							
							if(strstr(data,"OK") || strstr(data,"CLOSED"))
							{
								QTimeEvt_disarm(&me->timeEvt);
								WifiLog("WF_CWSMART OK");
								QF_PUBLISH(&Q_NEW(QpEvt, WIFI_CONFIG_START)->super, me);
								status_ = Q_TRAN(&WIFI_CWSMARTSTART_WAIT); 
							}
							else
							{
								if(errCount ++ >=3)
								{
									QTimeEvt_disarm(&me->timeEvt);
									status_ = Q_TRAN(&WIFI_CWSMART_EXIT);
								}
								
							}			
							WifiFree(data);
						}
          break;
        }
        /*${Wifi::WifiWork::SM::WIFI_AT::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
           status_ = Q_TRAN(&WIFI_CWSMART);
        break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}

static QState WIFI_CWSMARTSTART_WAIT(WifiWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_CWSMARTSTART_WAIT");
						QTimeEvt_disarm(&me->timeEvt); QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*60,0);
						status_ = Q_HANDLED();
            break;
        }
				case WIFI_CONFIG_SIG:{
					status_ = Q_HANDLED();
					break;
				}				
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							u16 size = Q_EVT_CAST(QpEvt)->v;
							WifiLog("recv:%s,size:%d",data,size);
							for(u16 i=0;i<size;i++)
							{
								printf("%02X ",data[i]);
							}
							printf("\r\n");
							if(strstr(data,"Smart get wifi info") && size >10)
							{
								char *name;
								char *pass;
								name =NetMalloc(100);
								if(name)
								{
									pass =NetMalloc(100);
									if(pass)
									{
										
										char *nameStart = strstr(data,"ssid:")+5;
										char *nameEnd = strstr(nameStart,"\r\n");
										if(nameStart && nameStart)
										{
											memcpy(name,nameStart,nameEnd-nameStart);
											char *passStart =  strstr(nameEnd,"password:")+9;
											char *passEnd = strstr(passStart,"\r\n");
											if(passStart && passEnd)
											{
												memcpy(pass,passStart,passEnd-passStart);
												
		//										picked((char *)data,"ssid:","\r\npassword",name);
		//										picked((char *)data,"password:","\r\n",pass);
												WifiLog("WifiName:%s,Pass:%s",name,pass);
												//配置完后检测连接状态
												//配置成功后才保存名称和密码
												//配置失败退出
												//添加一个Wifi状态显示在界面上
												//连接Wifi的时候如果密码错误则需要提示在Wifi状态处
												u8 nameLen=0;
												u8 passLen=0;
												nameLen = strlen(name);
												passLen = strlen(pass);
												if(nameLen >=4 && nameLen<40 && passLen >=8 && passLen<40)
												{
													strcpy(me->wifiName,name);
													strcpy(me->wifiPass,pass);
													WifiLog("WF_CWSMARTSTART_WAIT OK");
													QTimeEvt_disarm(&me->timeEvt);
													status_ = Q_TRAN(&WIFI_CWSMART_JAP_CK); 
												}else
												{
													status_ = Q_TRAN(&WIFI_CWSMART_EXIT); 
												}			
											}
										}
										WifiFree(pass);
									}
									else
									{
										WifiLog("WF_CWSMARTSTART_WAIT malloc name err");
										status_ = Q_TRAN(&WIFI_CWSMART_EXIT); 
									}
									WifiFree(name);
								}
								else
								{
									WifiLog("WF_CWSMARTSTART_WAIT malloc name err");
									status_ = Q_TRAN(&WIFI_CWSMART_EXIT); 
								}
							}
							WifiFree(data);
						}
						
          break;
        }
        /*${Wifi::WifiWork::SM::WIFI_AT::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
          status_ = Q_TRAN(&WIFI_CWSMART_EXIT);
        break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}

static QState WIFI_CWSMART_EXIT(WifiWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_CWSMART_EXIT");
            WifiWorkSendCmd(me,"AT+CWSTOPSMART\r\n",2);
						status_ = Q_HANDLED();
            break;
        }
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							if(strstr(data,"OK"))
							{
								QTimeEvt_disarm(&me->timeEvt);
								QF_PUBLISH(&Q_NEW(QpEvt, WIFI_CONFIG_FAILED)->super, me);
								status_ = Q_TRAN(&WIFI_CONNECT);
							}
							else
							{
								WifiLog("WF_CWSMART_EXIT ERR,WAIT");
							}
							WifiFree(data);
						}
          break;
        }
				case WIFI_CONFIG_SIG:{
					status_ = Q_HANDLED();
					break;
				}				
        /*${Wifi::WifiWork::SM::WIFI_AT::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
					 status_ = Q_TRAN(&WIFI_CONNECT);
           QF_PUBLISH(&Q_NEW(QpEvt, WIFI_CONFIG_FAILED)->super, me);
					break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}
static QState WIFI_CWSMART_JAP_CK(WifiWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_CWSMART_JAP_CK");
            WifiWorkSendCmd(me,"AT+CWJAP?\r\n",4);
						status_ = Q_HANDLED();
            break;
        }
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v);
							
							//如果已经连接网络,进入下一步
							QTimeEvt_disarm(&me->timeEvt);
							if(strstr(data,"+CWJAP:"))
							{
								UpdataApToFlash(me->wifiName,me->wifiPass);
								WifiLog("CWSMARTJAP CK ok");
								status_ = Q_TRAN(&WIFI_CONNECT);
								QF_PUBLISH(&Q_NEW(QpEvt, WIFI_CONFIG_SUCESS)->super, me);
							}
							else if(strstr(data,"No AP"))
							{
								status_ = Q_TRAN(&WIFI_CWSMART_JAP);
							}
							else if(strstr(data,"CONNECTED"))
							{
								UpdataApToFlash(me->wifiName,me->wifiPass);
								WifiLog("WF_CWSMART_JAP_CK ok");
								QF_PUBLISH(&Q_NEW(QpEvt, WIFI_CONFIG_SUCESS)->super, me);
								status_ = Q_TRAN(&WIFI_CONNECT);
							}
							else //如果没有连上wifi
							{
								status_ = Q_TRAN(&WIFI_CWSMART_JAP);
							}
							WifiFree(data);
						}
          break;
        }
				case WIFI_CONFIG_SIG:{
					status_ = Q_HANDLED();
					break;
				}				
        /*${Wifi::WifiWork::SM::WIFI_AT::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
            status_ = Q_TRAN(&WIFI_CWSMART_JAP);
            break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}
static QState WIFI_CWSMART_JAP(WifiWork * const me, QEvt const * const e)
{
	  QState status_;
    switch (e->sig) {
        /*${Wifi::WifiWork::SM::WIFI_AT} */
        case Q_ENTRY_SIG: {
						WifiLog("+++WIFI_CWSMART_JAP");
						char *smRebuf;
						smRebuf = NetMalloc(100);//申请1K字节c
						if(smRebuf)
						{
							memset(smRebuf,0,100);
							snprintf((char *)smRebuf,100,"AT+CWJAP=\"%s\",\"%s\"\r\n",me->wifiName,me->wifiPass);
							WifiWorkSendCmd(me,(char *)smRebuf,20);
							NetFree(smRebuf);
						}
						else
						{
							WifiLog("WIFI_CWSMART_JAP malloc err");
						}
						status_ = Q_HANDLED();
            break;
        }
				case WIFI_CONFIG_SIG:{
					status_ = Q_HANDLED();
					break;
				}
				 /*${AOs::Philo::SM::hungry::EAT} */
        case USART_WORK_RECV_SIG: {
						status_ = Q_HANDLED();
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							WifiLog("recv:%s,size:%d",data,Q_EVT_CAST(QpEvt)->v);
							
							if(strstr(data,"CONNECTED"))
							{
								QTimeEvt_disarm(&me->timeEvt);
								WifiLog("WF_CWSMART_JAP connect to wifi ok");
								UpdataApToFlash(me->wifiName,me->wifiPass);
								status_ = Q_TRAN(&WIFI_CONNECT);
								QF_PUBLISH(&Q_NEW(QpEvt, WIFI_CONFIG_SUCESS)->super, me);
							}
							else if(strstr(data,"DISCONNECT"))
							{
								QTimeEvt_disarm(&me->timeEvt);
								status_ = Q_TRAN(&WIFI_CWSMART_JAP); 
							}
							
							else if(strstr(data,"No AP"))
							{
								QTimeEvt_disarm(&me->timeEvt);
								status_ = Q_TRAN(&WIFI_CWSMART_JAP); 
							}
							else if(strstr(data,"FAIL")) //提交给上层 显示密码错误
							{
								QTimeEvt_disarm(&me->timeEvt);
								status_ = Q_TRAN(&WIFI_CWSMART_EXIT);
							}
							else if(strstr(data,"connect station fail"))
							{
								QTimeEvt_disarm(&me->timeEvt);
								status_ = Q_TRAN(&WIFI_CWSMART_EXIT);
							}
							else if(strstr(data,"OK"))
							{
								QTimeEvt_disarm(&me->timeEvt);
								status_ = Q_TRAN(&WIFI_CWSMART_JAP_CK);
							}
							else
							{
								WifiLog("connect to wifi waitting");
								status_ = Q_HANDLED();
							}
							WifiFree(data);
						}
						
          break;
        }
        /*${Wifi::WifiWork::SM::WIFI_AT::WIFI_TIMEOUT} */
        case TIMEOUT_SIG: {
							WifiLog("TIMEOUT_SIG");
							status_ = Q_TRAN(&WIFI_CWSMART_EXIT);
            break;
        }
        default: {
            status_ = Q_SUPER(&WIFI_ON);
            break;
        }
    }
    return status_;
}

