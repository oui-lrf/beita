#ifndef __WIFIDRIVE_H
#define __WIFIDRIVE_H

#include "WifiInclude.h"

#define WIFI_RST_PIN PGout(12)   
#define WIFI_CHP PGout(13)

void WifiSwOpen(void);
void WifiSwClose(void);
int WifiSendCmd(char *sendStr);
void WifiDriveSend(u8 *send_buf,u16 len);
void WifiRst(void);
#endif
