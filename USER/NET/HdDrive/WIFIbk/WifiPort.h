#ifndef __WIFIPORT_H__
#define __WIFIPORT_H__

#include "WifiInclude.h"
#ifdef __cplusplus
 extern "C" {
	#endif
u16 WifiSendData(u8 *buf,u16 len);
//受上层调用，释放所需资源
void WifiInit(void);
void WifiDeInit(void);
void UpdataApToFlash(char *name,char *pass);
void OpenWifiTcp(char *ip,u16 port);
void WifiStartConfig(void);
#define WifiDataObj UsartDataObj

#if 0
	#define WifiLog Log
#else
     #define WifiLog(...)
#endif
#ifdef __cplusplus
 }
 #endif
#endif

