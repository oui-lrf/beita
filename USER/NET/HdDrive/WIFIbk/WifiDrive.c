#include "WifiDrive.h"


void WifiRst(void)
{
	WifiLog("WifiRst");
	WIFI_RST_PIN=0;
	//WIFI_CHP = 0;
	WifiDelayMs(1000);
	WIFI_RST_PIN=1;
	//WIFI_CHP = 1;
}

void WifiSwClose(void)
{
	WifiLog("WifiClose");
	WIFI_RST_PIN=0;
	WIFI_CHP = 0;
}

void WifiSwOpen(void)
{
	WifiLog("WifiSwOpen");
	WIFI_CHP = 1;
	WIFI_RST_PIN=1;
}


void WifiDriveSend(u8 *send_buf,u16 len)
{
	 UsartDataSendBuf(send_buf,len);
}
