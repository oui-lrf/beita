#include "WifiPort.h"
#include "WifiWork.h"

extern QActive * const WF_WifiWork;
Q_DEFINE_THIS_MODULE("WifiPort")

//应该改为消息发送的方式，保证只有WIFI处于传输状态，才能接受数据发送
u16 WifiSendData(u8 *dat,u16 size)
{
	u8 *data = WifiMalloc(size);
	if(!data)
	{
		WifiLog("Usart Malloc err");
		return 0;
	}
	memcpy(data,dat,size);
	QpEvt *te;
	te = Q_NEW(QpEvt, WIFI_TCP_SEND_SIG);
	te->p = (u32)data;
	te->v = size;
	QACTIVE_POST(WF_WifiWork,&te->super,0);
	return size;
}

void UpdataApToFlash(char *name,char *pass)
{
	SetWifiPass(pass);
	SetWifiName(name);
	PostFreeEvent(EVENT_SAVE_SYS_FLASH);
}


void OpenWifiTcp(char *ip,u16 port)
{
	u16 ipLen = strlen(ip);
	if(ipLen>10 && ipLen<=40 && port>0)
	{
		char *pip = WifiMalloc(ipLen+1);
		if(pip)
		{
			strncpy(pip,ip,ipLen+1);
			QpEvt *te;
			te = Q_NEW(QpEvt, WIFI_CONNECT_SIG);
			te->p = (uint32_t)pip;
			te->v = port;
			QACTIVE_POST(WF_WifiWork, &te->super, me);		
		}

	}
}

//改函数只能又一个管理线程调用，可不多线程调用
//受上层调用，初始化所需资源，如定时器，状态机等
void WifiInit(void)
{
	if(WF_WifiWork->super.state.fun == 0)
	{
		static StackType_t wifiWorkStack[512];
		static const QEvt *wifiWork_queueSto[20];
		WifiWork_ctor();
		QActive_setAttr(WF_WifiWork, TASK_NAME_ATTR, "WifiWork");
		
		QACTIVE_START(WF_WifiWork,          /* AO to start */
		(uint_fast8_t)(2U), /* QP priority of the AO */
		wifiWork_queueSto,               /* event queue storage */
		Q_DIM(wifiWork_queueSto),        /* queue length [events] */
		wifiWorkStack,                  /* stack storage */
		sizeof(wifiWorkStack),          /* stack size [bytes] */
		(QEvt *)0);                  /* initialization event (not used) */
	}
}


//受上层调用，释放所需资源
void WifiDeInit(void)
{
	if(WF_WifiWork->super.state.fun != 0)
	{
		QACTIVE_POST(WF_WifiWork, &Q_NEW(QpEvt, WIFI_CLOSE_SIG)->super, me);
	}
}

void WifiStartConfig(void)
{
	if(WF_WifiWork->super.state.fun != 0)
	{
		QACTIVE_POST(WF_WifiWork, &Q_NEW(QpEvt, WIFI_CONFIG_SIG)->super, me);
	}
}

void WifiSignalChangeCall(u16 value)
{
	SendUiMsg(SCREEN_MAIN,HEAD_NET_SIGNAL,value);
}

