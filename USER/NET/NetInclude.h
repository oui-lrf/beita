#pragma once

//对外部文件的引用
#include "sys.h"
#include "string.h"
#include "malloc.h"
#include "Debug.h"
#include "stdio.h"
#include "NetConfig.h"
#include "FreeEvent.h"
#include "GuiPort.h"
#include "qpc.h"
#include "StringUtils.h"
#include "qf_port.h"
#include "UsartPort.h"
#include "GprsPort.h"
#include "ChipUtils.h"
#include "bsp.h"
#include "SysConfig.h"
#include "DeviceRunPort.h"

#define NetMalloc(n) mymalloc(SRAMIN,n)
#define NetFree(n)  myfree(SRAMIN,n)

#if 1
	#define NetLog Log
#else
	#define NetLog(...)
#endif


