#pragma once

#ifdef __cplusplus
 extern "C" {
#endif
#include "NetInclude.h"

typedef enum{
	NET_STA_CLOSE,//关闭	
	NET_STA_START,//开始
	NET_STA_OPENING,//打开中
	NET_STA_CONNECTEDING,//连接中
	NET_STA_CONNECTED,//已连接
	NET_STA_PAUSE,
}NET_STA;

typedef struct{
u8 *data;
u16 size;
char *ip;
u16 port;
u16 closeDelay;
u8 block; //0为非阻塞，1为阻塞
u8 keepConnect;//收到之后是否要关闭连接
}NET_SENG_MSG;

void ChangeNetType(u8 type);
void NetSendData(u8 *dat,u16 size,char *sip,u16 port,u16 closeDelay,u8 block,u8 keepConnect);
void SetConnectCloseCall(void (*call)(void));
#ifdef __cplusplus
 }
#endif
