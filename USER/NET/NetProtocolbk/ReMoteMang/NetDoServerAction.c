#include "NetDoServerAction.h"
#include "string.h"
#include "DeviceConfig.h"
#include "TimePort.h"

int ReRegistDevice(cJSON *json)
{
	int deviceId = 0;
//	char *Sta = cJSON_GetObjectItem(json, "Sta")->valuestring;
//	
//	if(strcmp(Sta,"OK")==0) //已经完成生产的设备，重新烧程序会出现这种情况
//	{
//		cJSON *data =  cJSON_GetObjectItem(json,"Data");
//		if(!data)
//		{
//			printf("data is null\r\n");
//			return 0;
//		}
//		deviceId = cJSON_GetObjectItem(data, "DeviceId")->valueint;
//		int Time = cJSON_GetObjectItem(data, "Time")->valueint;
//		
//		printf("deviceId:%d\r\n",deviceId);
//		printf("Time:%d\r\n",Time);
//		set_int_time(Time);
//		sysParam.deviceId = deviceId;
//		FREE_EVENT_TYPE FlashEvent = EVENT_SAVE_SYS_PARAM;
//		PostFreeEvent(FlashEvent);	
//	}
	return deviceId;
}

int ReAskDeviceData(cJSON *json)
{
	int permiss =0;
//	char *Sta = cJSON_GetObjectItem(json, "Sta")->valuestring;
//	
//	if(strcmp(Sta,"OK")==0) //已经完成生产的设备，重新烧程序会出现这种情况
//	{
//		cJSON *data =  cJSON_GetObjectItem(json,"Data");
//		if(!data)
//		{
//			printf("data is null\r\n");
//			return 0;
//		}
//		int Time = cJSON_GetObjectItem(data, "Time")->valueint;
//		permiss = cJSON_GetObjectItem(data, "ProdcPermiss")->valueint;
//		printf("Time:%d\r\n",Time);
//		set_int_time(Time);
//	}
	return permiss;
}

void ReAskTime(cJSON *json)
{
	char *Sta = cJSON_GetObjectItem(json, "Sta")->valuestring;
	
	if(strcmp(Sta,"OK")==0) //已经完成生产的设备，重新烧程序会出现这种情况
	{
		cJSON *data =  cJSON_GetObjectItem(json,"Data");
		if(!data)
		{
			printf("data is null\r\n");
			return;
		}
		u32 Time = cJSON_GetObjectItem(data, "Time")->valueint;
		printf("Time:%d\r\n",Time);
		sysParam.updataTime =0;
		SaveDeviceParam();
		SetIntTime(Time);
	}
}

void AnalyzeServerAction(cJSON *json)
{
	char *Action = cJSON_GetObjectItem(json, "Action")->valuestring;
	printf("--->Action:%s\r\n",Action);
	if(!strcmp(Action,"ReAskTime"))
	{
		ReAskTime(json);
	}
}

void AnalyzeHttpReturn(char *jsonStr)
{
	cJSON *cmdJson ={0};
	cmdJson= cJSON_Parse(jsonStr);
	if(cmdJson)
	{
		AnalyzeServerAction(cmdJson);
	}else
	{
		printf("--->cmdJson is null\r\n");
	}
	cJSON_Delete(cmdJson);
}

