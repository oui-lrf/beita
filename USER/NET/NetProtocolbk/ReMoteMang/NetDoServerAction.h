#ifndef __NETDOSERVERACTION_H__
#define __NETDOSERVERACTION_H__
#include "cJson.h"

int ReRegistDevice(cJSON *json);
int ReAskDeviceData(cJSON *json);
void AddMang(cJSON *json);
void AddStaff(cJSON *json);
void DeleteStaff(void);
void DeleteMang(void);
void SetStaffPermiss(cJSON *json);
void AnalyzeHttpReturn(char *jsonStr);

#endif
