#ifndef __NETSENDCMD_H
#define __NETSENDCMD_H
#include "sys.h"
#include "DeviceConfig.h"

u8 RegistDevice(void);
u8 AskDeviceData(void);
#endif
