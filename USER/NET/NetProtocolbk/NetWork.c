#include "NetPort.h"
#include "NetWork.h"


typedef struct {
/* protected: */
    QActive super;
		QTimeEvt timeEvt;
		QTimeEvt timeEvt2;
		QTimeEvt timeEvt3;
		QEQueue requestQueue;
		QEvt const *requestQSto[10];
		char ip[40];
		u16 port;
}NetWork;


extern QActive * const TP_TcpWork;
extern void LinkStaChangeCall(NET_STA sta);
extern void LinkTypeChangeCall(NET_TYPE type);
extern void LinkSatrtCall(void);
extern void LinkStopCall(void);
extern void NetTaskInitCall(void);
//extern void  ConnectCloseCall(void);
extern void RecvCmdCall(u8 *data,u16 dataSize);


static QState NetWork_initial(NetWork * const me, QEvt const * const e);

static QState NET_TOP(NetWork * const me, QEvt const * const e);
static QState NET_ON(NetWork * const me, QEvt const * const e);
static QState NET_CLOSE(NetWork * const me, QEvt const * const e);
static QState NET_SWITCH_NET_TYPE_WIFI(NetWork * const me, QEvt const * const e);
static QState NET_SWITCH_NET_TYPE_GPRS(NetWork * const me, QEvt const * const e);
static QState NET_PAUSE(NetWork * const me, QEvt const * const e); 
static NetWork netWork;
QActive * const WF_NetWork = &netWork.super;
Q_DEFINE_THIS_MODULE("NetWork")

void NetWork_ctor(void) {
    NetWork *me = (NetWork *)WF_NetWork;
    QActive_ctor(&me->super, Q_STATE_CAST(&NetWork_initial));
		QTimeEvt_ctorX(&me->timeEvt, &me->super, TIMEOUT_SIG, 0U);
		QTimeEvt_ctorX(&me->timeEvt2, &me->super, TIMEOUT2_SIG, 0U);
		QTimeEvt_ctorX(&me->timeEvt3, &me->super, TIMEOUT3_SIG, 0U);
		QEQueue_init(&me->requestQueue,me->requestQSto, Q_DIM(me->requestQSto));
}

static QState NetWork_initial(NetWork * const me, QEvt const * const e) {
    /*${Net::NetWork::SM::initial} */
	
		QActive_subscribe(&me->super, TCP_CONNECT_SUCCES_SIG);
		QActive_subscribe(&me->super, TCP_CONNECT_FAILED_SIG);
		QActive_subscribe(&me->super, TCP_RECV_SIG);
		QActive_subscribe(&me->super, TCP_PAUSE_SIG);
		QActive_subscribe(&me->super, TCP_CLOSE_CALL_SIG);
		QActive_subscribe(&me->super, TCP_RESUME_SIG);
		QActive_subscribe(&me->super, TCP_SWITCH_CLOSE_SIG);
		QActive_subscribe(&me->super, SWITCH_NET_TYPE_WIFI_SIG);
		QActive_subscribe(&me->super, SWITCH_NET_TYPE_GPRS_SIG);
		QActive_subscribe(&me->super, TCP_RECV_FAILED_SIG);
		
	
		NetTaskInitCall();	
		u8 type =GetNetType();
		LinkTypeChangeCall(type);
//	Log("net type:%d",type);
//		if(type != NET_TYPE_CLOSE)
//		{
//			char ip[40];
//			if(GetIp(ip,40))
//			{
//				strncpy(me->ip,ip,40);
//				me->port = GetPort();
//				return Q_TRAN(&NET_ON);
//			}
//			else
//			{
//				return Q_TRAN(&NET_CLOSE);
//			}
//		}
//		else
		{
			return Q_TRAN(&NET_CLOSE);
		}
}


static QState NET_TOP(NetWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        /*${Net::NetWork::SM::NET_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
						Log("+++NET_TOP");
						status_ = Q_HANDLED();	
            break;
					}
					case TCP_RECV_SIG:{
						NetLog("NET_TOP TCP_RECV_SIG");
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							NetFree(data);
						}
						status_ = Q_HANDLED();	
          break;
        }
				//如果在非ON状态收到数据发送请求先缓存数据，待连接成功后发送
				case NET_SEND_SIG:{
							status_ = Q_HANDLED();	
							NET_SENG_MSG *msg = (NET_SENG_MSG  *)(Q_EVT_CAST(QpEvt)->p);
							if(msg)
							{	
								char *ip = msg->ip;
								u16 port = msg->port;
								NetLog("NET_TOP NET_SEND_SIG ip:%s,port:%d",ip,port);
								Log("free:%d",me->requestQueue.nFree);
								if(me->requestQueue.nFree >=2)//只延续保存一个延迟事件
								{
									if (QActive_defer(&me->super, &me->requestQueue, e))
									{
										TcpLog("Request ip:%s,port:%d",ip,port);
										if(ip && port)
										{
											//打开网络
											TcpLog("Request on net");
											me->port= port;
											strncpy(me->ip,ip,40);
											status_ =Q_TRAN(&NET_ON);
										}
										else
										{
											NetFree(msg->data);
											NetFree(msg->ip);
											NetFree(msg);
											NetLog("Net ip or port err");
										}
										
									}
									else 
									{
										NetFree(msg->data);
										NetFree(msg->ip);
										NetFree(msg);
										NetLog("save faild");
									}
								}
								else
								{
									NetFree(msg->data);
									NetFree(msg->ip);
									NetFree(msg);
									NetLog("no free");
								}
							}
						
						break;
				}
        default: {
            status_ = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status_;
}

static QState NET_ON(NetWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        /*${Net::NetWork::SM::NET_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
						Log("+++NET_ON");
						//网络状态设置为连接中
						LinkStaChangeCall(NET_STA_CONNECTEDING);
						status_ = Q_HANDLED();	
						if(QActive_recall(&me->super, &me->requestQueue)){
                Log("Net send save ok");
            }
            else {
                Log("net no save");
            }
            break;
					}
				case TCP_RECV_FAILED_SIG:{
					status_ = Q_HANDLED();	
					Log("TCP_RECV_FAILED_SIG");
				}
				break;
					case TCP_RECV_SIG:{
						NetLog("NET_ON TCP_RECV_SIG");
            char *data = (char *)(Q_EVT_CAST(QpEvt)->p);
						if(data)
						{
							u16 size = Q_EVT_CAST(QpEvt)->v;
							RecvCmdCall((u8 *)data,size);
							NetFree(data);
						}
						status_ = Q_HANDLED();	
          break;
        }
				case NET_SEND_SIG:{
						Log("NET_SEND_SIG");
						status_ = Q_HANDLED();	
						NET_SENG_MSG *msg = (NET_SENG_MSG  *)(Q_EVT_CAST(QpEvt)->p);
						if(msg)
						{
							char *ip = msg->ip;
							u8 *data = msg->data;
							u16 size = msg->size;
							if(ip && data && size)
							{
								TcpAppSendData(msg->ip,msg->port,msg->data,msg->size,msg->closeDelay,msg->block,msg->keepConnect);
							}
							if(ip)
							{
								NetFree(ip);
							}
							
							if(data)
							{							
								NetFree(data);
							}
							NetFree(msg);
						}
						//////////////////////
						//发送完了这次，检测是否有保存，如果有重新发布
						if (QActive_recall(&me->super, &me->requestQueue)){
                Log("Net send save ok");
            }
            else {
                Log("net save recall finish");
            }
						break;
				}
				case TCP_CLOSE_CALL_SIG:{
						LinkStaChangeCall(NET_STA_CLOSE);
						status_ = Q_TRAN(&NET_CLOSE);
				break;
				}
//				case TIMEOUT2_SIG:{
//						status_ = Q_HANDLED();	
//						Log("NET_ON TIMEOUT2_SIG");
//						//重新发布
//						if (QActive_recall(&me->super, &me->requestQueue)) {
//                Log("Republish save envent ok");
//            }
//            else {
//                Log("No save envent");
//							LinkStaChangeCall(NET_STA_CLOSE);
//							TcpClose();
//							status_ = Q_TRAN(&NET_CLOSE);
//            }
//				break;
//				}
				case TCP_CONNECT_SUCCES_SIG:
					NetLog("NET_ON TCP_CONNECT_SUCCES_SIG");
						//网络状态设置为已连接
						LinkStaChangeCall(NET_STA_CONNECTED);
						status_ = Q_HANDLED();	
				break;
				case TCP_CONNECT_FAILED_SIG:{
						Log("NET_LINK_FAILED_SIG");
						//网络状态设置为连接失败
//						LinkStaChangeCall(NET_STA_CLOSE);
//						QTimeEvt_disarm(&me->timeEvt);
//						QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*2,0);
						status_ = Q_HANDLED();	
				break;
				}
				case SWITCH_NET_TYPE_WIFI_SIG:{
						status_ =Q_TRAN(&NET_SWITCH_NET_TYPE_WIFI);
				break;
				}
				case SWITCH_NET_TYPE_GPRS_SIG:{
						status_ =Q_TRAN(&NET_SWITCH_NET_TYPE_GPRS);
				break;
				}
				case TCP_SWITCH_CLOSE_SIG:{
						Log("TCP_SWITCH_CLOSE_SIG");
						LinkStaChangeCall(NET_STA_CLOSE);
						TcpClose();
						status_ = Q_TRAN(&NET_CLOSE);
				break;
				}
				case TCP_PAUSE_SIG:{
						status_ = Q_TRAN(&NET_PAUSE);
					break;
				}
				case TIMEOUT_SIG:{
						status_ = Q_TRAN(&NET_ON);
				break;
				}
				case Q_EXIT_SIG:{
						Log("~~~NET_ON");
//						ConnectCloseCall();
						QTimeEvt_disarm(&me->timeEvt);
//						QTimeEvt_disarm(&me->timeEvt2);
						status_ = Q_HANDLED();	
					break;
				}
        default: {
            status_ = Q_SUPER(&NET_TOP);
            break;
        }
    }
    return status_;
}

//切换过程中，就不需要再接收切换命令，如果发出切换命令，则延时后直接执行新切换的
static QState NET_SWITCH_NET_TYPE_WIFI(NetWork * const me, QEvt const * const e){
    QState status_;
    switch (e->sig) {
        /*${Net::NetWork::SM::NET_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
						Log("+++NET_SWITCH_NET_TYPE_WIFI");
						//关闭tcp
						LinkStaChangeCall(NET_STA_CLOSE);
						TcpClose();
						QTimeEvt_disarm(&me->timeEvt); QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*1,0);
						status_ = Q_HANDLED();	
            break;
        }
				case TIMEOUT_SIG:{
  					SetNetType(NET_TYPE_WIFI);
						//PostFreeEvent(EVENT_SAVE_NET_FLASH);
						LinkTypeChangeCall(NET_TYPE_WIFI);
						status_ = Q_TRAN(&NET_ON);
				break;
				}
				case SWITCH_NET_TYPE_GPRS_SIG:{
						status_ =Q_TRAN(&NET_SWITCH_NET_TYPE_GPRS);
				break;
				}
				case TCP_SWITCH_CLOSE_SIG:{
					status_ = Q_TRAN(&NET_CLOSE);
				break;
				}
				case Q_EXIT_SIG:{
					QTimeEvt_disarm(&me->timeEvt);
					status_ = Q_HANDLED();	
					break;
				}
        default: {
            status_ = Q_SUPER(&NET_TOP);
            break;
        }
    }
    return status_;
}

//切换过程中，就不需要再接收切换命令，如果发出切换命令，则延时后直接执行新切换的
static QState NET_SWITCH_NET_TYPE_GPRS(NetWork * const me, QEvt const * const e){
    QState status_;
    switch (e->sig) {
        /*${Net::NetWork::SM::NET_MODEL_OPEN} */
        case Q_ENTRY_SIG: {
						Log("+++NET_SWITCH_NET_TYPE_GPRS");
						//关闭Tcp
						LinkStaChangeCall(NET_STA_CLOSE);
						TcpClose();
						QTimeEvt_disarm(&me->timeEvt); QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*1,0);
						status_ = Q_HANDLED();	
            break;
        }
				case TIMEOUT_SIG:{
						SetNetType(NET_TYPE_GPRS);
						PostFreeEvent(EVENT_SAVE_NET_FLASH);	
						LinkTypeChangeCall(NET_TYPE_GPRS);
						status_ = Q_TRAN(&NET_ON);
				break;
				}
			 case SWITCH_NET_TYPE_WIFI_SIG:{
						status_ =Q_TRAN(&NET_SWITCH_NET_TYPE_WIFI);
				break;
				}
				case TCP_SWITCH_CLOSE_SIG:{
					status_ = Q_TRAN(&NET_CLOSE);
				break;
				}
				case Q_EXIT_SIG:{
					QTimeEvt_disarm(&me->timeEvt);
					status_ = Q_HANDLED();	
					break;
				}
        default: {
            status_ = Q_SUPER(&NET_TOP);
            break;
        }
    }
    return status_;
}

//应该再有一个TOP状态

static QState NET_CLOSE(NetWork * const me, QEvt const * const e) {
    QState status_;
    switch (e->sig) {
        case Q_ENTRY_SIG: {
						Log("+++NET_CLOSE");
						status_ = Q_HANDLED();	
            break;
        }
				case TIMEOUT_SIG:{
						SetNetType(NET_TYPE_GPRS);
						LinkTypeChangeCall(NET_TYPE_GPRS);
						status_ = Q_TRAN(&NET_ON);
				break;
				}
				case SWITCH_NET_TYPE_WIFI_SIG:{
						status_ = Q_HANDLED();	
						SetNetType(NET_TYPE_WIFI);
						PostFreeEvent(EVENT_SAVE_NET_FLASH);	
						LinkTypeChangeCall(NET_TYPE_WIFI);
				break;
				}
				case SWITCH_NET_TYPE_GPRS_SIG:{
						status_ = Q_HANDLED();	
						SetNetType(NET_TYPE_GPRS);
						PostFreeEvent(EVENT_SAVE_NET_FLASH);	
						LinkTypeChangeCall(NET_TYPE_GPRS);
				break;
				}
				case Q_EXIT_SIG:{
					QTimeEvt_disarm(&me->timeEvt);
					status_ = Q_HANDLED();	
					break;
				}
        default: {
            status_ = Q_SUPER(&NET_TOP);
            break;
        }
    }
    return status_;
}

static QState NET_PAUSE(NetWork * const me, QEvt const * const e) 
{
    QState status_;
    switch (e->sig) {
        case Q_ENTRY_SIG: {
						NetLog("+++NET_PAUSE");
						LinkStaChangeCall(NET_STA_PAUSE);
						status_ = Q_HANDLED();	
            break;
        }
				case TCP_RESUME_SIG:{
					NetLog("TCP_RESUME_SIG");
					QTimeEvt_disarm(&me->timeEvt);
					QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*1,0);
					status_ = Q_HANDLED();	
				break;
				}
        default: {
            status_ = Q_SUPER(&NET_ON);
            break;
        }
    }
    return status_;
}

