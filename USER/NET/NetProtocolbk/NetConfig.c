#include "NetConfig.h"
#include "string.h"
#include "stdio.h"
#include "FlashPort.h"
#include "Debug.h"
#include "cmsis_os.h"




typedef struct{
	u8 netType;
	u16 EmpPermiss;
	char ip[MAX_IP_LEN];
	u16 port;
	char MangTopic[MAX_TOPIC_LEN];
	char EmpTopic[MAX_TOPIC_LEN];
	char wifiName[MAX_WIFI_NAME_LEN];
	char wifiPass[MAX_WIFI_PASS_LEN];
}NetParam;

NetParam netParam;
osMutexId  netConfigMutexId=0;
static void CreateMutx(void)
{
	static osMutexDef_t  mutex;
	netConfigMutexId = osMutexCreate(&mutex);
}
static void GetMutx(void)
{
	if(netConfigMutexId)osMutexWait(netConfigMutexId, portMAX_DELAY);
}

static void ReleaseMutx(void)
{
	if(netConfigMutexId)osMutexRelease(netConfigMutexId);
}

//从flash中读出数据
void NetConfigInit(void)
{
	CreateMutx();
	read_para_flash(NET_FLASH,sizeof(netParam),&netParam);
	PrintfNetParam();
}	

void SaveNetParam(void)
{
	save_para_flash(NET_FLASH,sizeof(NetParam),&netParam);
}

void ResetNetParam(void)
{
	netParam.netType=NET_TYPE_GPRS;
	netParam.EmpPermiss=0;
	netParam.port = DEFAULT_SERVER_PORT;
	strcpy(netParam.ip,DEFAULT_SERVER_IP);
	strcpy(netParam.wifiName,"PHICOMM");
	strcpy(netParam.wifiPass,"oui12345");
	strcpy(netParam.MangTopic,"");
	strcpy(netParam.EmpTopic,"");
	SaveNetParam();
}

void PrintfNetParam(void)
{
	printf("MangTopic:%s\r\n",netParam.MangTopic);
	printf("EmpTopic:%s\r\n",netParam.EmpTopic);
	printf("EmpPermiss:%d\r\n",netParam.EmpPermiss);
	printf("netType:%d\r\n",netParam.netType);
	printf("WifiName:%s\r\n",netParam.wifiName);
	printf("WifiPass:%s\r\n",netParam.wifiPass);
}

u8 GetNetType(void)
{
	u8 type;
	GetMutx();
	type = netParam.netType;
	ReleaseMutx();
	return type;
}

u16 GetPort(void)
{
	u16 port;
	GetMutx();
	port = netParam.port;
	ReleaseMutx();
	return port;
}

u8 GetEmpPermiss(void)
{
	u8 per;
	GetMutx();
	per =netParam.EmpPermiss;
	ReleaseMutx();
	return per;
}

void SetEmpPermiss(u8 per)
{
	GetMutx();
	netParam.EmpPermiss = per;
	ReleaseMutx();
}
//成功返回1
u8 GetMangTopic(char * topic, u16 size)
{
	if(size<MAX_TOPIC_LEN+1)
	{
		return 0;
	}
	if(!topic)
	{
		return 0;
	}
	GetMutx();
	char *top = strncpy(topic,netParam.EmpTopic,size);
	ReleaseMutx();
	u16 len =strlen(top);
	if(len <= MAX_TOPIC_LEN)
	{
		topic[len]=0;	
		return 1;
	}
	else
	{
		return 0;
	}

}
//成功返回1
u8 GetEmpTopic(char * topic, u16 size)
{
	if(size<MAX_TOPIC_LEN)
	{
		return 0;
	}
	if(!topic)
	{
		return 0;
	}
	GetMutx();
	Log("topLen:%d",strlen(netParam.EmpTopic));
	
	char *top = strncpy(topic,netParam.EmpTopic,size);
	ReleaseMutx();
	u16 len =strlen(top);
	if(len <= MAX_TOPIC_LEN)
	{
		topic[len]=0;
		return 1;
	}
	else
	{
		return 0;
	}
}

u8 GetWifiName(char * name, u16 size)
{
	if(size<=MAX_WIFI_NAME_LEN)
	{
		return 0;
	}
	if(!name)
	{
		return 0;
	}
	GetMutx();
	char *top = strncpy(name,netParam.wifiName,size);
	ReleaseMutx();
	u16 len =strlen(top);
	if(len <= MAX_TOPIC_LEN)
	{
		name[len]=0;
		return 1;
	}
	else
	{
		return 0;
	}
}



u8 GetWifiPass(char * pass, u16 size)
{
	if(size<=strlen(netParam.wifiPass))
	{
		return 0;
	}
	if(!pass)
	{
		return 0;
	}
	GetMutx();
	char *top = strncpy(pass,netParam.wifiPass,size);
	ReleaseMutx();
	u16 len =strlen(top);
	if(len <= MAX_TOPIC_LEN)
	{
		pass[len]=0;
		return 1;
	}
	else
	{
		return 0;
	}
}

u8 GetIp(char * res, u16 size)
{
	if(size<=strlen(netParam.ip))
	{
		return 0;
	}
	if(!res)
	{
		return 0;
	}
	GetMutx();
	char *top = strncpy(res,netParam.ip,size);
	ReleaseMutx();
	u16 len =strlen(top);
	if(len <= MAX_IP_LEN)
	{
		res[len]=0;
		return 1;
	}
	else
	{
		return 0;
	}
}

u8 SetNetType(u8 type)
{
	if(type == NET_TYPE_WIFI || type == NET_TYPE_GPRS || type == NET_TYPE_CLOSE)
	{
		GetMutx();
		netParam.netType = type;
		ReleaseMutx();	
		return 1;
	}
	else
	{
		return 0;
	}
}

u8 SetEmpTopic(char *topic)
{
	u8 len = strlen(topic);
	if(len >= MAX_TOPIC_LEN)
	{
		return 0;
	}
	GetMutx();
	char *top  =strncpy(netParam.EmpTopic,topic,MAX_TOPIC_LEN);
	ReleaseMutx();
	u16 ln =strlen(top);
	netParam.EmpTopic[ln]=0;
	return 1;
}


u8 SetMangTopic(char *topic)
{
	u8 len = strlen(topic);
	if(len >= MAX_TOPIC_LEN)
	{
		return 0;
	}
	GetMutx();
	char *top  =strncpy(netParam.MangTopic,topic,MAX_TOPIC_LEN);
	ReleaseMutx();
	u16 ln =strlen(top);
	netParam.MangTopic[ln]=0;
	return 1;
}

u8 SetWifiName(char *name)
{
	u8 len = strlen(name);
	if(len >= MAX_WIFI_NAME_LEN)
	{
		return 0;
	}
	GetMutx();
	char *top  =strncpy(netParam.wifiName,name,MAX_WIFI_NAME_LEN);
	ReleaseMutx();
	u16 ln =strlen(top);
	netParam.wifiName[ln]=0;
	return 1;
}

u8 SetWifiPass(char *pass)
{
	u8 len = strlen(pass);
	if(len >= MAX_WIFI_PASS_LEN)
	{
		return 0;
	}
	GetMutx();
	char *top  =strncpy(netParam.wifiName,pass,MAX_WIFI_PASS_LEN);
	ReleaseMutx();
	u16 ln =strlen(top);
	netParam.wifiPass[ln]=0;
	return 1;
}

u8 SetIp(char* data)
{
	u8 len = strlen(data);
	if(len >= MAX_IP_LEN)
	{
		return 0;
	}
	GetMutx();
	char *top  =strncpy(netParam.ip,data,MAX_IP_LEN);
	ReleaseMutx();
	u16 ln =strlen(top);
	netParam.ip[ln]=0;
	return 1;
}

u8 SetPort(u16 data)
{
	GetMutx();
	netParam.port =data; 
	ReleaseMutx();	
	return 1;
}
