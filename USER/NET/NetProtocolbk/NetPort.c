#include "NetPort.h"
#include "NetWork.h"
#include "NethandleNode.h"
#include "HttpPort.h"
#include "iap.h"
extern QActive * const WF_NetWork;
//由外部调用
//要即使切换当前状态
//同时把类型保存进网络参数
void ChangeNetType(u8 type)
{
	Log("ChangeNetType type:%d",type);
	//关闭网络-->再打开
	switch(type)
	{
		case NET_TYPE_WIFI:
			QF_PUBLISH(&Q_NEW(QpEvt, SWITCH_NET_TYPE_WIFI_SIG)->super, me);
		break;
		case NET_TYPE_GPRS:
			QF_PUBLISH(&Q_NEW(QpEvt, SWITCH_NET_TYPE_GPRS_SIG)->super, me);
		break;
		case NET_TYPE_CLOSE:
			QF_PUBLISH(&Q_NEW(QpEvt, TCP_SWITCH_CLOSE_SIG)->super, me);
		break;
	}
}
//内部调用
//连接改变的时候由网络模块内部调用
void LinkStaChangeCall(NET_STA sta)
{
	SendUiMsg(SCREEN_MAIN,HEAD_NET_STA,sta);
}

//内部调用
void LinkTypeChangeCall(NET_TYPE type)
{
	SendUiMsg(SCREEN_SET,HEAD_NET_TYPE,type);
}


//内部调用
//网络线程启动时调用
extern void NetHandleNetWorkInitCall(void);

static void HttpEndCall(void)
{
	Log("HttpEndCall");	
	
}


void NetTaskInitCall(void)
{
	UpdataFlashToParam();
	PrintUpgrateParam();
	//升级成功
	if(GetUpdating() ==0)
	{
		HttpUploadUpdated();
	}
	else
	{
//		HttpAskUpdata();
	}
}

extern void TcpRecvCallBack(u8 *data,u16 size);

//网络收到数据的时候调用
void RecvCmdCall(u8 *data,u16 dataSize)
{
	HttpRecvCall(data,dataSize);
}


	
void LinkSatrtCall(void)
{
	NetHandleLinkSatrtCall();
}

//内部调用
void LinkStopCall(void)
{
	NetHandleLinkStopCall();
}

//给TCP发送发送消息
//数据包类型有阻塞和非阻塞
//阻塞消息需要等待超时或者返回之后才能发送下一包，阻塞消息的执行情况需要通知返回上一层
//u8 block; //0为非阻塞，1为阻塞
void NetSendData(u8 *dat,u16 size,char *sip,u16 port,u16 closeDelay,u8 block,u8 keepConnect)
{	
	Log("NetSendData ip:%s,port:%d,size:%d",sip,port,size);
	GprsTcpSend(sip,port,dat,size,0,10,10);

}
