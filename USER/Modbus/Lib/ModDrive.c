#include "ModDrive.h"
#include "qpc.h"
#include "ModInclude.h"

extern QActive * const US_ModWork;

extern cycleQueue modQue;

static void ModRecvDataCallBack(u8 *dat,u16 size)
{
	u16 rcvSize;
	if(size <= MAX_MOD_SIZE)
	{
	 rcvSize= size;
	}
	else
	{
		rcvSize= MAX_MOD_SIZE;
	}
	if(US_ModWork->super.state.fun)
	{
		BaseType_t xHigherPriorityTaskWoken = pdFALSE;
		InsertQueBuf(&modQue,dat,rcvSize);
		QpEvt *te;
		te = Q_NEW_FROM_ISR(QpEvt, USART_RECV_SIG);
		te->v = rcvSize;
		QACTIVE_POST_FROM_ISR(US_ModWork,
																	&te->super,
																	&xHigherPriorityTaskWoken,
																	&l_TickHook);
		portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
	}
}

void ModUsartWorkInit(void)
{
	SetUsartRecvBufCallBack(4,ModRecvDataCallBack);
	UsartStart(4);
}

void ModUsartWorkDeInit(void)
{
	SetUsartRecvBufCallBack(4,0);
	UsartStop(4);
}

void ModUsartSendBuf(u8 *data,u16 size)
{
	HAL_GPIO_WritePin(SW485_Port, SW485_Pin, GPIO_PIN_SET);
	UsartSendBuf(4,data,size);
	HAL_GPIO_WritePin(SW485_Port, SW485_Pin, GPIO_PIN_RESET);
}
