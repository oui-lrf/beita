#pragma once
#include "sys.h"
#include "usart.h"
#include "mbcrc.h"
#include "malloc.h"
#include "GuiPort.h"
#include "circular_queue.h"

#define ModMalloc(n) mymalloc(SRAMIN,n)
#define ModFree(n) myfree(SRAMIN,n)
#define CRC16_Modbus usMBCRC16

#if 0
	#define ModLog Log
#else
	#define ModLog(...) 
#endif


#define OPEN_SEND_PIN()  HAL_GPIO_WritePin(GPIOC, MOD485_SW_Pin, GPIO_PIN_SET)
#define CLOSE_SEND_PIN() HAL_GPIO_WritePin(GPIOC, MOD485_SW_Pin, GPIO_PIN_RESET)	

