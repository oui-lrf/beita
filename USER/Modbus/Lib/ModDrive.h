#pragma once

#include "usart.h"

 #define MAX_MOD_SIZE MAX_USART4_SIZE

void ModUsartWorkInit(void);
void ModUsartWorkDeInit(void);
void ModUsartSendBuf(u8 *data,u16 size);
