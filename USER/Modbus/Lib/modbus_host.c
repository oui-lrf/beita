#include "modbus_host.h"


extern void ModRecvReadCall(u8 type,u16 addr,u16 reg,u16 value);

void ModRecvReadHold(u8 fun,u16 reg,u8 *data,u16 size)
{
	uint8_t bytes;
	u16 value; 
	bytes = data[2];
	ModLog("bytes:%d",bytes);
	for(int i=0;i<(bytes/2);i++)
	{
		u8 *p = &data[3]+2*i;
		value = ((*p)<<8) | *(p+1);		
		ModRecvReadCall(fun,data[0],reg+i,value);
	}
}

void ModRecvWriteHold(u8 fun,u16 reg,u8 *data,u16 size)
{
	u16 value = (data[4]<<8)|data[5]; 
	ModRecvReadCall(fun,data[0],reg,value);
}

void ModRecvReadInput(u8 fun,u16 reg,u8 *data,u16 size)
{
	uint8_t bytes;
	u16 value; 
	bytes = data[2];
	//ModLog("bytes:%d",bytes);
	for(int i=0;i<(bytes/2);i++)
	{
		u8 *p = &data[3]+2*i;
		value = ((*p)<<8) | *(p+1);		
		ModRecvReadCall(fun,data[0],reg+i,value);
	}	
}

void ModRecvReadCoils(u8 fun,u16 reg,u16 count,u8 *data,u16 size)
{
	uint8_t bytes;
	u8 value=0; 
	bytes = data[2];
	for(u16 i=0;i<bytes;i++)
	{
		u8 *p = &data[3]+i;
		value = *p;		
		//8个字节，取出每一位,再加上起始位
		//ModLog("value:%02x",value);
		for(u8 n=0;n<8 && (i*8+n)<count;n++)
		{
			ModRecvReadCall(fun,data[0],reg+i*8+n,!!(value&(0x01<<n)));
		}
	}	
}

void ModRecvWriteCoil(u8 fun,u16 reg,u8 *data,u16 size)
{
		u16 value = (data[4]<<8)|data[5];		
		ModLog("value:%04x",value);
		ModRecvReadCall(fun,data[0],reg,value);
}


/*
*********************************************************************************************************
*	函 数 名: MODH_AnalyzeApp
*	功能说明: 分析应用层协议。处理应答。
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
u8 MODH_AnalyzeApp(MOD_CMD *sendCmd,u8 *data,u16 size)
{	
	u8 res =0;
	if(size >4 && data)
	{
		u16 cacu1Crc = CRC16_Modbus(data,size-2);
		u16 recvCrc = (data[size-1]<<8)+data[size-2];
		if(cacu1Crc == recvCrc)
		{
			res =1;
			u8 fun = data[1];
			switch (fun)	/* 第2个字节 功能码 */
			{
				case MOD_CMD_READ_COILS:	/* 读取线圈状态 */
						;MOD_READ_COILS_OBJ *coilsObj = sendCmd->obj;
						ModRecvReadCoils(fun,(coilsObj->startRegH<<8)|coilsObj->startRegL,(coilsObj->regCountH>>8)|(coilsObj->regCountL),data,size);
					break;
				case MOD_CMD_READ_HOLDREGIST:	/* 读取保持寄存器 在一个或多个保持寄存器中取得当前的二进制值 */
					;
						MOD_READ_HOLD_OBJ *obj = sendCmd->obj;
						ModRecvReadHold(fun,(obj->startRegH<<8)|obj->startRegL,data,size);
					break;
				
				case MOD_CMD_READ_INPUTREGIST:	/* 读取输入寄存器 */
						;
						MOD_READ_INPUT_OBJ *inputobj = sendCmd->obj;
						ModRecvReadInput(fun,(inputobj->startRegH<<8)|inputobj->startRegL,data,size);
					break;

				case MOD_CMD_WRITE_COIL:	/* 强制单线圈 */
						;MOD_WRITE_COIL_OBJ *wcoilObj = sendCmd->obj;
						ModRecvWriteCoil(fun,(wcoilObj->startRegH<<8)|wcoilObj->startRegL,data,size);
						break;
				case MOD_CMD_WRITE_HOLDREGIST:	/* 写单个寄存器 */
						ModRecvWriteHold(fun,(data[2]<<8)|data[3],data,size);
					break;		
				
				default:
					break;
			}
		}
	}
	return res;
}
