#include "ModInclude.h"
#include "ModWork.h" 
#include "ModPort.h"
#include "DeviceRunPort.h"

extern QActive * const US_ModWork;


void ModInitCall(void)
{
	//有泵
}

static void ModSendCmd(MOD_CMD *cmd)
{
	QpEvt *te;
	te = Q_NEW(QpEvt, MOD_SEND_SIG);
	te->p = (uint32_t)cmd;
	te->v = sizeof(MOD_CMD);
	QACTIVE_POST(US_ModWork,&te->super,0);
}

void DeleteModCmd(MOD_CMD *cmd)
{
	ModFree(cmd->obj);
	ModFree(cmd);
}

MOD_CMD * CreateModCmd(MOD_CMD_TYPE type,void *obj)
{
	MOD_CMD *cmd = ModMalloc(sizeof(MOD_CMD));
	if(!cmd)
	{
		Log("malloc err");
		ModFree(obj);
		return 0;
	}
	cmd->type = type;
	cmd->obj =obj;
	return cmd;
}


void ModRecvReadCall(u8 type,u16 addr,u16 reg,u16 value)
{
	DeviceRecvModCallEx(type,addr,reg,value);
}


void ModSendReadCoils(uint8_t _addr, uint16_t _reg, uint16_t _num)
{
	ModLog("ModSendReadCoils addr:%x rex:%x num:%x",_addr,_reg,_num);
	MOD_READ_COILS_OBJ *cmd01H = ModMalloc(sizeof(MOD_READ_COILS_OBJ));
	if(!cmd01H)
	{
		Log("malloc err");
		return ;
	}
	cmd01H->addr =_addr;
	cmd01H->fun =MOD_CMD_READ_COILS;
	cmd01H->startRegH = _reg>>8;
	cmd01H->startRegL = _reg&0xff;
	cmd01H->regCountH = _num>>8;
	cmd01H->regCountL = _num&0xff;
	uint16_t crc = CRC16_Modbus((u8 *)cmd01H, sizeof(MOD_READ_COILS_OBJ)-2);
	cmd01H->crc16L = crc>>8;
	cmd01H->crc16H = crc&0xff;
	MOD_CMD *cmd = CreateModCmd(MOD_CMD_READ_COILS,cmd01H);
	cmd->objSize = sizeof(MOD_READ_COILS_OBJ);
	if(!cmd)
	{
		Log("malloc err");
		return ;
	}
	ModSendCmd(cmd);
}
void ModSendWriteCoil(uint8_t _addr, uint16_t _reg, uint16_t value)
{
	ModLog("addr:%x rex:%x num:%x",_addr,_reg,value);
	MOD_WRITE_COIL_OBJ *modCmd = ModMalloc(sizeof(MOD_WRITE_COIL_OBJ));
	if(!modCmd)
	{
		Log("malloc err");
		return ;
	}
	modCmd->addr =_addr;
	modCmd->fun =MOD_CMD_WRITE_COIL;
	modCmd->startRegH = _reg>>8;
	modCmd->startRegL = _reg&0xff;
	modCmd->valueH = value>>8;
	modCmd->valueL = value&0xff;
	uint16_t crc = CRC16_Modbus((u8 *)modCmd, sizeof(MOD_WRITE_COIL_OBJ)-2);
	modCmd->crc16L = crc>>8;
	modCmd->crc16H = crc&0xff;
	MOD_CMD *cmd = CreateModCmd(MOD_CMD_WRITE_COIL,modCmd);
	cmd->objSize = sizeof(MOD_WRITE_COIL_OBJ);
	if(!cmd)
	{
		Log("malloc err");
		return ;
	}
	ModSendCmd(cmd);
}


/*
*********************************************************************************************************
*	函 数 名: ModSendReadHoldRegist
*	功能说明: 发送03H指令，查询1个或多个保持寄存器
*	形    参: _addr : 从站地址
*			  _reg : 寄存器编号
*			  _num : 寄存器个数
*	返 回 值: 无
*********************************************************************************************************
*/
void ModSendReadHoldRegist(uint8_t _addr, uint16_t _reg, uint16_t _num)
{
	//ModLog("addr:%x rex:%x num:%x",_addr,_reg,_num);
	MOD_READ_HOLD_OBJ *cmd03H = ModMalloc(sizeof(MOD_READ_HOLD_OBJ));
	if(!cmd03H)
	{
		Log("malloc err");
		return ;
	}
	cmd03H->addr =_addr;
	cmd03H->fun =MOD_CMD_READ_HOLDREGIST;
	cmd03H->startRegH = _reg>>8;
	cmd03H->startRegL = _reg&0xff;
	cmd03H->regCountH = _num>>8;
	cmd03H->regCountL = _num&0xff;
	uint16_t crc = CRC16_Modbus((u8 *)cmd03H, sizeof(MOD_READ_HOLD_OBJ)-2);
	cmd03H->crc16L = crc>>8;
	cmd03H->crc16H = crc&0xff;
	
	//Log("cmd03H addr:%x rex:%x num:%x",_addr,_reg,_num);
	MOD_CMD *cmd = CreateModCmd(MOD_CMD_READ_HOLDREGIST,cmd03H);
	cmd->objSize = sizeof(MOD_READ_HOLD_OBJ);
	if(!cmd)
	{
		Log("malloc err");
		return ;
	}
	ModSendCmd(cmd);
}

/*
*********************************************************************************************************
*	函 数 名: ModSendReadHoldRegist
*	功能说明: 发送03H指令，查询1个或多个保持寄存器
*	形    参: _addr : 从站地址
*			  _reg : 寄存器编号
*			  _num : 寄存器个数
*	返 回 值: 无
*********************************************************************************************************
*/
void ModSendReadInputRegist(uint8_t _addr, uint16_t _reg, uint16_t _num)
{
	ModLog("addr:%x rex:%x num:%x",_addr,_reg,_num);
	MOD_READ_INPUT_OBJ *cmd04H = ModMalloc(sizeof(MOD_READ_INPUT_OBJ));
	if(!cmd04H)
	{
		Log("malloc err");
		return ;
	}
	cmd04H->addr =_addr;
	cmd04H->fun =MOD_CMD_READ_INPUTREGIST;
	cmd04H->startRegH = _reg>>8;
	cmd04H->startRegL = _reg&0xff;
	cmd04H->regCountH = _num>>8;
	cmd04H->regCountL = _num&0xff;
	uint16_t crc = CRC16_Modbus((u8 *)cmd04H, sizeof(MOD_READ_INPUT_OBJ)-2);
	cmd04H->crc16L = crc>>8;
	cmd04H->crc16H = crc&0xff;
	
	//Log("cmd04H addr:%x rex:%x num:%x",_addr,_reg,_num);
	MOD_CMD *cmd = CreateModCmd(MOD_CMD_READ_INPUTREGIST,cmd04H);
	cmd->objSize = sizeof(MOD_READ_INPUT_OBJ);
	if(!cmd)
	{
		Log("malloc err");
		return ;
	}
	ModSendCmd(cmd);
}

/*
*********************************************************************************************************
*	函 数 名: ModSendReadHoldRegist
*	功能说明: 发送03H指令，查询1个或多个保持寄存器
*	形    参: _addr : 从站地址
*			  _reg : 寄存器编号
*			  _num : 寄存器个数
*	返 回 值: 无
*********************************************************************************************************
*/
void ModSendWrightHoldRegist(uint8_t _addr, uint16_t _reg, uint16_t value)
{
	ModLog("addr:%x rex:%x num:%x",_addr,_reg,value);
	MOD_WRITE_HOLD_OBJ *cmd = ModMalloc(sizeof(MOD_WRITE_HOLD_OBJ));
	if(!cmd)
	{
		Log("malloc err");
		return ;
	}
	cmd->addr =_addr;
	cmd->fun =MOD_CMD_WRITE_HOLDREGIST;
	cmd->startRegH = _reg>>8;
	cmd->startRegL = _reg&0xff;
	cmd->valueH = value>>8;
	cmd->valueL = value&0xff;
	uint16_t crc = CRC16_Modbus((u8 *)cmd, sizeof(MOD_READ_HOLD_OBJ)-2);
	cmd->crc16L = crc>>8;
	cmd->crc16H = crc&0xff;
	MOD_CMD *sendCmd = CreateModCmd(MOD_CMD_WRITE_HOLDREGIST,cmd);
	sendCmd->objSize = sizeof(MOD_READ_HOLD_OBJ);
	if(!sendCmd)
	{
		Log("malloc err");
		return ;
	}
//	printf("ModSendWrightHoldRegist send:\r\n");
//	for(u8 i=0;i<sizeof(MOD_READ_HOLD_OBJ);i++)
//	{
//		printf("%02x ",((u8 *)cmd)[i]);
//	}
//	printf("\r\n");
	ModSendCmd(sendCmd);
}
