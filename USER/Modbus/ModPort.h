#pragma once
#include "ModWork.h"


#define MOD_RESEND_COUNT 2
#define MIN_SEND_SPACE_TIME 50
#define WAIT_TIME 600

typedef enum{
	MOD_CMD_READ_COILS=0x01,
	MOD_CMD_READ_HOLDREGIST=0x03,
	MOD_CMD_READ_INPUTREGIST=0x04,
	MOD_CMD_WRITE_COIL=0x05,
	MOD_CMD_WRITE_HOLDREGIST=0x06,
	MOD_CMD_WRITE_MULHOLDREGIST=0x10,
}MOD_CMD_TYPE;


typedef struct{
	u8 addr;
	u8 fun;
	u8 startRegH;
	u8 startRegL;
	u8 regCountH;
	u8 regCountL;
	u8 crc16H;
	u8 crc16L;
}MOD_READ_COILS_OBJ;

typedef struct{
	u8 addr;
	u8 fun;
	u8 startRegH;
	u8 startRegL;
	u8 valueH;
	u8 valueL;
	u8 crc16H;
	u8 crc16L;
}MOD_WRITE_COIL_OBJ;

typedef struct{
	u8 addr;
	u8 fun;
	u8 startRegH;
	u8 startRegL;
	u8 regCountH;
	u8 regCountL;
	u8 crc16H;
	u8 crc16L;
}MOD_READ_HOLD_OBJ;


typedef struct{
	u8 addr;
	u8 fun;
	u8 startRegH;
	u8 startRegL;
	u8 regCountH;
	u8 regCountL;
	u8 crc16H;
	u8 crc16L;
}MOD_READ_INPUT_OBJ;


typedef struct{
	u8 addr;
	u8 fun;
	u8 startRegH;
	u8 startRegL;
	u8 valueH;
	u8 valueL;
	u8 crc16H;
	u8 crc16L;
}MOD_WRITE_HOLD_OBJ;

typedef struct{
	MOD_CMD_TYPE type;
	void *obj;
	u16 objSize;
}MOD_CMD;

typedef struct{
	MOD_CMD_TYPE type;
	u8 addr;
	u16 reg;
	u16 value;
}ModRecvObj;

void ModSendReadCoils(uint8_t _addr, uint16_t _reg, uint16_t _num);
void ModSendWriteCoil(uint8_t _addr, uint16_t _reg, uint16_t value);
void ModSendReadInputRegist(uint8_t _addr, uint16_t _reg, uint16_t _num);
void ModSendReadHoldRegist(uint8_t _addr, uint16_t _reg, uint16_t _num);
void ModSendWrightHoldRegist(uint8_t _addr, uint16_t _reg, uint16_t value);
//设备地址
#define SENDOR1_ADDR 0x01

//电压
#define VOL1_H_REG 0x2007
#define VOL1_L_REG 0x2008

#define VOL2_H_REG 0x2009
#define VOL2_L_REG 0x200A

#define VOL3_H_REG 0x200B
#define VOL3_L_REG 0x200C

//电流
#define CUR1_H_REG 0x200D
#define CUR1_L_REG 0x200E

#define CUR2_H_REG 0x200F
#define CUR2_L_REG 0x2010

#define CUR3_H_REG 0x2011
#define CUR3_L_REG 0x2012

//功率

#define POW_H_REG 0x2013
#define POW_L_REG 0x2014

//电能

#define CONSUME_H_REG 0x401F
#define CONSUME_L_REG 0x4020



