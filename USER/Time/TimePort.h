#ifndef __TIME_PORT_H
#define __TIME_PORT_H
#include "sys.h"

void UpdataTimeForomRct(void);
u16  IntToTimeStr(char *timeBuf,u16 bufSize,u32 time);
u32 GetIntTime(void);
void SetRtcDate(u16 year,u8 month,u8 day);
void SetRtcTime(u8 timeAPM,u8 hour,u8 min,u8 sec);
void SetIntTime(u32 time);
#endif

