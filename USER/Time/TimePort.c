#include "TimePort.h"
#include "malloc.h"
#include "time.h"
#include "stdio.h"
#include "SysConfig.h"
#include "rtc.h"
#include "cmsis_os.h"


u32 GetIntTime(void)
{
	return GetRtcIntTime();
}


u16  IntToTimeStr(char *timeBuf,u16 bufSize,u32 time)
{
	u16 rt = 0;
	time_t t;  
	struct tm *lt; 
	t = time;  
	lt = localtime(&t);  
	lt->tm_hour = (lt->tm_hour+8)%24;
	rt = strftime(timeBuf, bufSize, "%Y-%m-%d %H:%M:%S", lt);  
	return rt;
}

void SetIntTime(u32 time)
{
		time_t t;  
		char nowtime[24];   
		struct tm *lt; 
		t = time;  
		lt = localtime(&t);  
//		printf("local time:%d-%d-%d %d:%d:%d",lt->tm_year+1900,lt->tm_mon+1,lt->tm_mday,(lt->tm_hour+8)%24,lt->tm_min,lt->tm_sec);
    strftime(nowtime, 24, "%Y-%m-%d %H:%M:%S", lt);  
//		printf("Unix time:%s\r\n",nowtime);
		RTC_Set_Date(lt->tm_year+1852,lt->tm_mon+1,lt->tm_mday,0);
		RTC_Set_Time((lt->tm_hour+8)%24,lt->tm_min,lt->tm_sec,RTC_HOURFORMAT12_AM);
}
