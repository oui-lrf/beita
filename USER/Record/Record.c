#include "Record.h"
#include "malloc.h"
#include "string.h"
#include "ecc.h"
#include "TimePort.h"
#include "SysConfig.h"
#include "SysConfig.h"
#define   HEAD_ADDR flash_mallco_dev.flashbase[SPI_FLASH] 
Record *headRecord = NULL;

u8 pvd=0;
Record *ReadHeadRecord(void);
u8 DeleteEndRecord(Record *rcd);


void RecordInit(void)
{
	//PrintFlash(SPI_FLASH);
	printf("-->RecordInit\r\n");
	my_flash_init(SPI_FLASH);
	headRecord = ReadHeadRecord();
	printf("RecordInit headRecord:%d,thisAddr:%d\r\n",(u32)headRecord,(u32)headRecord->thisAddr);
	u16 number = PrintAllRecord(headRecord);
	u16 use = flash_mallco_dev.perused(SPI_FLASH);
	printf("flash perused:%d number:%d\r\n",use,number);
	if((use == 1000 && number <50) ||number >1000)//保存异常检测
	{
		DeleteAllRecord();
		u32 time = GetIntTime();
		u32 addRcdRes = AddRecord(time,FLASH_SAVE_ERR,0,use);
	}
}

u8 SaveHeadAddr(u32 addr)
{
//	u32	addraddr =0;
//	addraddr= (u32)flashmallocaddr(SPI_FLASH,HEAD_ADDR,sizeof(addr));
//	if(!addraddr) 
//	{
//		printf("SaveHeadAddr flashmallocaddr err,re malloc\r\n");
//		myflashfree(SPI_FLASH,HEAD_ADDR);
//		addraddr= (u32)flashmallocaddr(SPI_FLASH,HEAD_ADDR,sizeof(addr));
//	}
//	if(!addraddr)
//	{
//		printf("SaveHeadAddr flashmallocaddr err\r\n");
//		return 0;
//	}
//	printf("SaveHeadAddr addraddr:%d,addr:%d\r\n",addraddr,addr);
	u8 rt = FlashWrite(SPI_FLASH,HEAD_ADDR,(u8 *)&addr,sizeof(addr));
	if(rt == 0)
	{
		return HEAD_ADDR;
	}
	else
	{
		myflashfree(SPI_FLASH,HEAD_ADDR);
		printf("SaveHeadAddr FlashWrite err\r\n");
		return 0;
	}
}

Record *ReadHeadRecord(void)
{
	u8 res=0;
	u32 addraddr =0;
	u32 ck = check_flash_malloc(SPI_FLASH,HEAD_ADDR,sizeof(addraddr));
	if(ck == 0xffffffff)
	{
		printf("not malloc flash head\r\n");
		FactoryRecordFlash();
		return 0;
	}
//	else
//	{
//		printf("head size:%d\r\n",ck);
//	}
	res = FlashRead(SPI_FLASH,HEAD_ADDR,(u8 *)&addraddr,sizeof(addraddr));	
	printf("ReadHeadRecord addraddr:%d\r\n",addraddr);
	if(addraddr == 0xffffffff)
	{
		//
		return 0;
	}
	
	if(res == 0)
	{
		return  ReadRecord(addraddr);
		
	}else if(res == 1)
	{
		printf("ReadHeadRecord FlashRead addr err\r\n");
		return 0;
	}
	else
	{
		printf("ReadHeadRecord FlashRead other err\r\n");
		return 0;
	}
}


u8 DeleteEndRecord(Record *rcd)
{
	u8 rt = 0;
	Record* thisRcd = rcd;
	Record* nextRcd = 0;
//	u32 naddr=0;
	printf("DeleteEndRecord:%d\r\n",(u32)rcd);
	if(thisRcd == 0)
	{
		printf("DeleteEndRecord Rcd is null\r\n");
		return 0;
	}
	
	if(thisRcd->nextAddr == 0)//头结点就是要删除的
	{
		printf("DeleteEndRecord delete headRecord\r\n");
		myflashfree(SPI_FLASH,thisRcd->thisAddr);
		myfree(SRAMIN,thisRcd);		
		headRecord = 0;
		SaveHeadAddr(0xffffffff);
		return 1;
	}
	while(1)
	{
		nextRcd  = ReadRecord(thisRcd->nextAddr);
		
		if(nextRcd !=0 )
		{
			if(nextRcd->nextAddr == 0)//下一条就是最后一条
			{
				myflashfree(SPI_FLASH,nextRcd->thisAddr);
				thisRcd->nextAddr=nextRcd->nextAddr;
				memset(thisRcd->ecc,0,3);
				calculate_ecc((u_char *)thisRcd,sizeof(Record),thisRcd->ecc);
				u8 res =FlashWrite(SPI_FLASH,thisRcd->thisAddr,(u8 *)thisRcd,sizeof(Record));
				if( res !=0)
				{
					printf("DeleteEndRecord FlashWrite err\r\n");
				}else
				{
					rt =1;
				}
				if(thisRcd->thisAddr != headRecord->thisAddr)myfree(SRAMIN,thisRcd);
				myfree(SRAMIN,nextRcd);
				
				break;
			}
		}
		else
		{
			printf("DeleteEndRecord err nextRcd:%d,nextAddr:%d\r\n",(int)nextRcd,nextRcd->nextAddr);
			if(thisRcd->thisAddr != headRecord->thisAddr)myfree(SRAMIN,thisRcd);
			break;
		}

		if(thisRcd->thisAddr != headRecord->thisAddr)
		{
			myfree(SRAMIN,thisRcd);
		}
		thisRcd = nextRcd;

	}
	return rt;

}
/** 
* @brief  将新的数据节点插入队列头，删除非头部节点
* @param	
* @retval 成功返回flash地址，失败返回0
* @author oui
* @date   2018-06-11
*/
u32 InsertRecord(Record *newRecord)
{
	u32 flashAddr =0;
	
	flashAddr = (u32)myflashmalloc(SPI_FLASH,sizeof(Record));
	//如果内存分配失败了,删除最后一个节点，再次申请
	if(!flashAddr)
	{
		u8 delRes = DeleteEndRecord(headRecord);
		printf("delRes:%d\r\n",delRes);
		if(delRes)
		{
			flashAddr = (u32)myflashmalloc(SPI_FLASH,sizeof(Record));
		}
	}
	if(!flashAddr)
	{
		return 0;
	}
	if(headRecord == 0)//如果没有头结点，新插入的做为头结点
	{
		newRecord->thisAddr=flashAddr;
		newRecord->nextAddr = 0;
	}else
	{
		newRecord->thisAddr=flashAddr;
		newRecord->nextAddr = headRecord->thisAddr;
		myfree(SRAMIN,headRecord);
	}
	memset(newRecord->ecc,0,3);
	calculate_ecc((u_char *)newRecord,sizeof(Record),newRecord->ecc);
//	u8 *rcd = (u8 *)newRecord;
//	for(int i=0;i<sizeof(Record);i++)
//	{
//		printf("-->rea:%x\r\n",rcd[i]);
//	}
//	printf("InsertRecord newRecord:time:%d,type:%d,value:%d,thisAddr:%d,nextAddr:%d ecc0:%x,ecc1:%x,ecc2:%x\r\n",newRecord->time,newRecord->type,newRecord->value,
//	newRecord->thisAddr,newRecord->nextAddr,newRecord->ecc[0],newRecord->ecc[1],newRecord->ecc[2]);	
	u8 rt = FlashWrite(SPI_FLASH,newRecord->thisAddr,(u8 *)newRecord,sizeof(Record));
	if(rt != 0)
	{
		printf("-->InsertRecord FlashWrite err\r\n");
		myflashfree(SPI_FLASH,flashAddr);
		return 0;
	}else
	{
		headRecord = newRecord; 
		SaveHeadAddr(headRecord->thisAddr);
		return flashAddr;
	}
	
}
/** 
* @brief  创建一个数据节点
* @param
* @retval 
* @author oui
* @date   2018-06-11
*/
Record* CreateRecord(u32 time,RECORD_TYPE type,u8 sta,int value)
{
	Record * newRecord =0;
	newRecord = mymalloc(SRAMIN,sizeof(Record));
	if(!newRecord)
	{
		printf("CreateRecord malloc newRecord err\r\n");
		return 0;
	}
	newRecord->time=time;
	newRecord->type=type;
	newRecord->sta =sta;
	newRecord->value=value;
	return newRecord;
}

u32 AddRecord(u32 time,RECORD_TYPE type,u8 sta,int value)
{
	u32 resAddr=0;
	Record* newRecord = CreateRecord(time,type,sta,value);
	//printf("-->AddRecord InsertRecord\r\n");
	if(pvd !=1)
	{
		resAddr = InsertRecord(newRecord);
	}
	if(!resAddr)//失败后删除新节点
	{
		myfree(SRAMIN,newRecord);
	}
	return resAddr;
}


Record* ReadRecord(u32 addr)
{
	u8 res=0;
	Record* record = 0;
	record = mymalloc(SRAMIN,sizeof(Record));
	if(!record) 
	{
		printf("ReadRecord malloc revord err\r\n");
		return 0;
	}
	res = FlashRead(SPI_FLASH,addr,(u8 *)record,sizeof(Record));	
	if(res !=0)
	{
		printf("-->ReadRecord FlashRead err\r\n");
		myfree(SRAMIN,record);
		return 0;
	}
//	u8 *rcd = (u8 *)record;
//	for(int i=0;i<sizeof(Record);i++)
//	{
//		printf("-->read:%x\r\n",rcd[i]);
//	}
//	printf("ReadRecord newRecord:time:%d,type:%d,value:%d,thisAddr:%d,nextAddr:%d\r\n",record->time,record->type,record->value,record->thisAddr,record->nextAddr);
	u8 readEcc[3]={0};
	memcpy(readEcc,record->ecc,3);
	memset(record->ecc,0,3);
	calculate_ecc((u_char *)record,sizeof(Record),record->ecc);
	if(readEcc[0] != record->ecc[0] ||readEcc[1] != record->ecc[1] || readEcc[2] != record->ecc[2])
	{
		printf("ReadRecord newRecord:time:%d,type:%d,value:%d,thisAddr:%d,nextAddr:%d\r\n",record->time,record->type,record->value,record->thisAddr,record->nextAddr);
		printf("-->ReadRecord FlashRead err svecc0:%x,svecc1:%x,svecc2:%x,necc0:%x,necc1:%x,necc2:%x\r\n",readEcc[0],readEcc[1],readEcc[2],
		record->ecc[0],record->ecc[1],record->ecc[2]);
		int res = correct_data((u_char *)record,53,readEcc,record->ecc);
		if(res ==1)
		{
			return record;
		}
		else
		{
			printf("correct_data err res:%d\r\n",res);
			myfree(SRAMIN,record);
		}
		return 0;
	}
	return record;
}
void FactoryRecordFlash(void)
{
	flash_reset(SPI_FLASH);
	u32	addraddr =0;
	addraddr= (u32)flashmallocaddr(SPI_FLASH,HEAD_ADDR,sizeof(u32));//
	if(!addraddr)
	{
		printf("SaveHeadAddr flashmallocaddr err\r\n");
	}
	
}

void DeleteAllRecord(void)
{
	printf("DeleteAllRecord\r\n");
	FactoryRecordFlash();
	myfree(SRAMIN,headRecord);
	headRecord =0;
}

u8 DleteRecord(u32 addr)
{
//	u16 i=0;
	u8 rt = 0;
	Record* thisRcd = headRecord;
	Record* nextRcd = 0;
//	u32 naddr=0;
	//printf("DleteRecord addr:%d\r\n",addr);
	if(!thisRcd)
	{
		printf("DleteRecord Rcd is null\r\n");
		return rt;
	}
	
	if(thisRcd->thisAddr == addr)//头结点就是要删除的
	{
		//printf("delete headRecord\r\n");
		if(pvd)
		{
			return 0;
		}
		myflashfree(SPI_FLASH,thisRcd->thisAddr);
		myfree(SRAMIN,thisRcd);
		headRecord  = ReadRecord(thisRcd->nextAddr);
		SaveHeadAddr(headRecord->thisAddr);
		return 1;
	}
	while(1)
	{
		nextRcd  = ReadRecord(thisRcd->nextAddr);
		if(nextRcd !=0)
		{
			if(nextRcd->thisAddr == addr)//删除下一条
			{
				if(pvd)
				{
					rt=0;
					break;
				}
				myflashfree(SPI_FLASH,nextRcd->thisAddr);
				thisRcd->nextAddr=nextRcd->nextAddr;
				memset(thisRcd->ecc,0,3);
				calculate_ecc((u_char *)thisRcd,sizeof(Record),thisRcd->ecc);
				rt =FlashWrite(SPI_FLASH,thisRcd->thisAddr,(u8 *)thisRcd,sizeof(Record));
				if( rt !=0)
				{
					printf("DleteRecord FlashWrite err\r\n");
					rt =0;
				}else
				{
					rt =1;
				}
				if(thisRcd->thisAddr != headRecord->thisAddr)myfree(SRAMIN,thisRcd);
				myfree(SRAMIN,nextRcd);
				break;
			}
			else if(nextRcd->nextAddr == 0)
			{
				printf("DleteRecord nextRcd->nextAddr == 0,end\r\n");
				if(thisRcd->thisAddr != headRecord->thisAddr)myfree(SRAMIN,thisRcd);
				myfree(SRAMIN,nextRcd);
				break;
			}
		}
		else
		{
			printf("DleteRecord nextRcd == 0,end\r\n");
			if(thisRcd->thisAddr != headRecord->thisAddr)myfree(SRAMIN,thisRcd);
			break;
		}
		if(thisRcd->thisAddr != headRecord->thisAddr)
		{
			myfree(SRAMIN,thisRcd);
		}
		thisRcd = nextRcd;

	}
	return rt;
}



u16 PrintAllRecord(Record* rcd)
{
	u16 i = 0;
	u32 addr=0;
	if( !rcd ) return 0;

	Record* nRcd = rcd;
	if(!rcd) return 0;
	printf("head addr:%d\r\n",(u32)headRecord);
	printf("i:%d,time:%d,type:%d,value:%d,thisAddr:%d,nextAddr:%d\r\n",i,nRcd->time,nRcd->type,nRcd->value,nRcd->thisAddr,nRcd->nextAddr);
	addr = nRcd->nextAddr;
	while(1)
	{
		i++;
		if(addr !=0)
		{
			nRcd = ReadRecord(addr);
			if(nRcd != 0)
			{
				printf("i:%d,time:%d,type:%d,value:%d,thisAddr:%d,nextAddr:%d\r\n",i,nRcd->time,nRcd->type,nRcd->value,nRcd->thisAddr,nRcd->nextAddr);
				addr = nRcd->nextAddr;
				myfree(SRAMIN,nRcd);
			}else
			{
				break;
			}
			
		}
		else
		{
			break;
		}
		if(i > 1000)
		{
			break;
		}
	}
	return i;
}

