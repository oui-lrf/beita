#ifndef __RECORD_H__
#define __RECORD_H__
#include "sys.h"
#include "RecordDrive.h"
#ifdef __cplusplus
 extern "C" {
#endif
typedef enum{
	OUTWATER_TEMP_WARING=1,
	INWATER_TEMP_WARING,
	MATER_TEMP_WARING,
	MATER2_TEMP_WARING,
	HOT_WARING,
	WATER_WARING,
	BURN_ERR_WARING,
	SMOKE_TEMP_WARING,
	RESS_WARING,
	WATER_PUMP_WARING,
	CIRCUL_PUMP_WARING,
	FLASH_SAVE_ERR,
}RECORD_TYPE;

typedef struct {
	u32 time; 
	RECORD_TYPE type;
	u8  sta;
	int value;
	u32 thisAddr;
	u32 nextAddr;
	u8 ecc[3];
}Record;
 

extern Record *headRecord;
void RecordInit(void);
u32 AddRecord(u32 time,RECORD_TYPE type,u8 sta,int value);
Record* ReadRecord(u32 addr);
u8 DleteRecord(u32 addr);
u16 PrintAllRecord(Record* rcd);
void FactoryRecordFlash(void);
void DeleteAllRecord(void);
#ifdef __cplusplus
}
#endif
#endif
