#pragma once
#include "sys.h"
#include "ff.h"
#include "exfuns.h"
#include "fattester.h"	
#include "diskio.h"	

typedef enum{
	VALUE_U32,
	VALUE_FLOAT,
}VALUE_TYPE;

typedef struct{
	u16 id;
	u32 time;
	u32 value;
	VALUE_TYPE  valueType;
	char unit[10];
	char name[20];
}FileRecord;

//单个文件允许的最大记录条数
#define MAX_ONE_FILE_RECORD 9999


