#include "RecordDrive.h"
#include "string.h"
#include "malloc.h"
#include "bsp_spi_flash.h"
//#define FLASH_MALLOC_ADDR (32*1024*1024-4096)
#define FLASH_MALLOC1_TB_ADDR (8-2)*1024*1024 //为4096的倍数
#define FLASH_MALLOC1_ADDR (8-1)*1024*1024 //为4096的倍数

//内存池(32字节对齐)
//__align(32) u8 flash1base[SPI_FLASH_MAX_SIZE];													//内部SRAM内存池


//内存管理表
u32 flash1mapbase[SPI_FLASH_ALLOC_TABLE_SIZE];													//内部SRAM内存池MAP	
//内存管理参数	   
const u32 flashtblsize[SPI_FLASHBANK]={SPI_FLASH_ALLOC_TABLE_SIZE};	//内存表大小
const u32 flashblksize[SPI_FLASHBANK]={SPI_FLASH_BLOCK_SIZE};					//内存分块大小

const u32 flashsize[SPI_FLASHBANK]={SPI_FLASH_MAX_SIZE};							//内存总大�

const u32 flashtbbase[SPI_FLASHBANK]={FLASH_MALLOC1_TB_ADDR};					//内存分块大小
 u32 flash1base[SPI_FLASHBANK]={FLASH_MALLOC1_ADDR};//FLASH内存基地址

//内存管理控制器
struct _spi_flash_mallco_dev flash_mallco_dev=
{
	my_flash_init,						//内存初始化
	my_flash_perused,						//内存使用率
	flash1base,			//内存池
	flash1mapbase,//内存管理状态表
	0,  		 					//内存管理未就绪
};

//复制内存
//*des:目的地址
//*src:源地址
//n:需要复制的内存长度(字节为单位)
//void myflashcpy(void *des,void *src,u32 n)  
//{  
//    u8 *xdes=des;
//	u8 *xsrc=src; 
//    while(n--)*xdes++=*xsrc++;  
//} 

//格式化所在扇区，状态表设置为0
void flash_reset(u8 flashx)
{
	memset(flash_mallco_dev.flashmap[flashx],0xff,flashtblsize[flashx]*4);
	//W25QXX_Erase_Sector(flashtbbase[flashx]/4096);
	sf_EraseSector(flashtbbase[flashx]/4096);
	//W25QXX_Erase_Sector(flash_mallco_dev.flashbase[flashx]/4096); 	
	sf_EraseSector(flash_mallco_dev.flashbase[flashx]/4096); 
}

//读出整个状态表
//设置内存
//*s:内存首地址
//c :要设置的值
//count:需要设置的内存大小(字节为单位)
void myflashset(void *s,u32 addr,u32 count)  
{  
    u8 *xs = s;  
    //while(count--)*xs++=0;  
		//W25QXX_Read(xs,addr,count);	
		sf_ReadBuffer(xs,addr,count);	
}	
//内存管理初始化  
//flashx:所属内存块
void my_flash_init(u8 flashx)  
{  
	printf("SPI_FLASH_ALLOC_TABLE_SIZE*4:%d\r\n",SPI_FLASH_ALLOC_TABLE_SIZE*4);
	printf("flashtblsize[flashx]*4:%d\r\n",flashtblsize[flashx]*4);
  myflashset(flash_mallco_dev.flashmap[flashx],flashtbbase[flashx],flashtblsize[flashx]*4);	//内存状态表数据清零 
 	flash_mallco_dev.flashrdy[flashx]=1;								//内存管理初始化OK  
}  
//获取内存使用率
//flashx:所属内存块
//返回值:使用率(扩大了10倍,0~1000,代表0.0%~100.0%)
u16 my_flash_perused(u8 flashx)  
{  
    u32 used=0;  
    u32 i;  
    for(i=0;i<flashtblsize[flashx];i++)  
    {  
        if(flash_mallco_dev.flashmap[flashx][i] != 0xffffffff)used++; 
    } 
    return (used*1000)/(flashtblsize[flashx]);  
}  
//内存分配(内部调用)
//flashx:所属内存块
//size:要分配的内存大小(字节)
//返回值:0XFFFFFFFF,代表错误;其他,内存偏移地址 
u32 my_flash_malloc(u8 flashx,u32 size)  
{  
    signed long offset=0;  
    u32 nflashb;	//需要的内存块数  
	  u32 cflashb=0;//连续空内存块数
    u32 i;  
    if(!flash_mallco_dev.flashrdy[flashx])flash_mallco_dev.init(flashx);//未初始化,先执行初始化 
    if(size==0)return 0XFFFFFFFF;//不需要分配
    nflashb=size/flashblksize[flashx];  	//获取需要分配的连续内存块数
    if(size%flashblksize[flashx])nflashb++;  
    for(offset=flashtblsize[flashx]-1;offset>=0;offset--)//搜索整个内存控制区  
    {     
			if(flash_mallco_dev.flashmap[flashx][offset] == 0xffffffff)cflashb++;//连续空内存块数增加
			else cflashb=0;								//连续内存块清零
			if(cflashb==nflashb)							//找到了连续nflashb个空内存块
			{
					for(i=0;i<nflashb;i++)  					//标注内存块非空 
					{  
							flash_mallco_dev.flashmap[flashx][offset+i]=nflashb; 
					}
					//W25QXX_Write((u8 *)flash_mallco_dev.flashmap[flashx],flashtbbase[flashx],flashtblsize[flashx]*4);
					sf_WriteBuffer((u8 *)flash_mallco_dev.flashmap[flashx],flashtbbase[flashx],flashtblsize[flashx]*4);
					return (offset*flashblksize[flashx]);//返回偏移地址  			
			}
    }  
    return 0XFFFFFFFF;//未找到符合分配条件的内存块  
}  

/** 
* @brief 检测指定的内存地址是否被分配
* @param 0xffffffff 未分配，其它表示已经分配字节大小
* @retval 
* @author oui
* @date   2018-06-11
*/

u32 check_flash_malloc(u8 flashx,u32 addr,u32 size)
{
    signed long offset=0;  
    u32 nflashb;	//需要的内存块数  
	//  u32 cflashb=0;//连续空内存块数
    if(!flash_mallco_dev.flashrdy[flashx])flash_mallco_dev.init(flashx);//未初始化,先执行初始化 
    if(size==0)return 0XFFFFFFFF;//不需要分配
    nflashb=size/flashblksize[flashx];  	//获取需要分配的连续内存块数
    if(size%flashblksize[flashx])nflashb++; 
		if(addr <flash_mallco_dev.flashbase[flashx] || (addr+size) >flash_mallco_dev.flashbase[flashx]+flashsize[flashx])
		{
			printf("my_flash_malloc_addr addr of size overrange\r\n");
			return 0XFFFFFFFF;//未找到符合分配条件的内存块  
		}
		offset = (addr-flash_mallco_dev.flashbase[flashx])/flashblksize[flashx] +nflashb-1;
		if((addr-flash_mallco_dev.flashbase[flashx])%flashblksize[flashx])offset++;
		//printf("-->check_flash_malloc offset:%d,map:%x\r\n",offset,flash_mallco_dev.flashmap[flashx][offset]);
		if(flash_mallco_dev.flashmap[flashx][offset] != 0xffffffff)
		{
			return flash_mallco_dev.flashmap[flashx][offset];
		}
		else
		{
			return 0XFFFFFFFF;
		}
}
//内存分配(内部调用)
//flashx:所属内存块
//size:要分配的内存大小(字节)
//返回值:0XFFFFFFFF,代表错误;其他,内存偏移地址 
u32 my_flash_malloc_addr(u8 flashx,u32 addr,u32 size)  
{  
		//printf("my_flash_malloc_addr flashx:%d,addr:%d,size:%d\r\n",flashx,addr,size);
    signed long offset=0;  
    u32 nflashb;	//需要的内存块数  
	  u32 cflashb=0;//连续空内存块数
    u32 i;  
    if(!flash_mallco_dev.flashrdy[flashx])flash_mallco_dev.init(flashx);//未初始化,先执行初始化 
    if(size==0)return 0XFFFFFFFF;//不需要分配
    nflashb=size/flashblksize[flashx];  	//获取需要分配的连续内存块数
    if(size%flashblksize[flashx])nflashb++;  
		if(addr <flash_mallco_dev.flashbase[flashx] || (addr+size) >flash_mallco_dev.flashbase[flashx]+flashsize[flashx])
		{
			printf("my_flash_malloc_addr addr of size overrange\r\n");
			return 0XFFFFFFFF;//未找到符合分配条件的内存块  
		}
		offset = (addr-flash_mallco_dev.flashbase[flashx])/flashblksize[flashx] +nflashb-1;
    for(;offset>=0;offset--)//搜索整个内存控制区  offset=flashtblsize[flashx]-1
    {     
			
			if(flash_mallco_dev.flashmap[flashx][offset] == 0xffffffff)cflashb++;//连续空内存块数增加
			else cflashb=0;								//连续内存块清零
			//printf("offset:%d,mapv:%x,cflashb:%d,nflashb:%d\r\n",offset,flash_mallco_dev.flashmap[flashx][offset],cflashb,nflashb);
			if(cflashb==nflashb)							//找到了连续nflashb个空内存块
			{
					for(i=0;i<nflashb;i++)  					//标注内存块非空 
					{  
							flash_mallco_dev.flashmap[flashx][offset+i]=nflashb; 
					}
					//W25QXX_Write((u8 *)flash_mallco_dev.flashmap[flashx],flashtbbase[flashx],flashtblsize[flashx]*4);
					sf_WriteBuffer((u8 *)flash_mallco_dev.flashmap[flashx],flashtbbase[flashx],flashtblsize[flashx]*4);
					return (offset*flashblksize[flashx]);//返回偏移地址  			
			}
    }  
    return 0XFFFFFFFF;//未找到符合分配条件的内存块  
} 
//释放内存(内部调用) 
//flashx:所属内存块
//offset:内存地址偏移
//返回值:0,释放成功;1,释放失败;  
u8 my_flash_free(u8 flashx,u32 offset)  
{  
    int i;  
    if(!flash_mallco_dev.flashrdy[flashx])//未初始化,先执行初始化
		{
		flash_mallco_dev.init(flashx);    
        return 1;//未初始化  
    }  
    if(offset<flashsize[flashx])//偏移在内存池内. 
    {  
        int index=offset/flashblksize[flashx];			//偏移所在内存块号码  
        int nflashb=flash_mallco_dev.flashmap[flashx][index];	//内存块数量
//				printf("free offset:%d--num:%d\r\n",offset,nflashb);
        for(i=0;i<nflashb;i++)  						//内存块清零
        {  
            flash_mallco_dev.flashmap[flashx][index+i]=0xffffffff;					
        }
				if(nflashb >0)
				{
					//W25QXX_Write((u8 *)flash_mallco_dev.flashmap[flashx],flashtbbase[flashx],flashtblsize[flashx]*4);
					sf_WriteBuffer((u8 *)flash_mallco_dev.flashmap[flashx],flashtbbase[flashx],flashtblsize[flashx]*4);
				}
        return 0;  
    }else return 2;//偏移超区了.  
}  
//释放内存(外部调用) 
//flashx:所属内存块
//ptr:内存首地址 
void myflashfree(u8 flashx,u32 ptr)  
{  
	u32 offset;  
	u8 res;
	if(ptr==NULL)return;//地址为0.  
 	offset=(u32)ptr-(u32)flash_mallco_dev.flashbase[flashx]; 
   res = my_flash_free(flashx,offset);	//释放内存  
	if(res)printf("free error res:%d\r\n",res);
//	printf("--------->myfree ex:%d\r\n",my_flash_perused(SRAMEX));	 	
}  
//分配内存(外部调用)
//flashx:所属内存块
//size:内存大小(字节)
//返回值:分配到的内存首地址.
void *myflashmalloc(u8 flashx,u32 size)  
{  
  u32 offset;   
	void *res=0;
	offset=my_flash_malloc(flashx,size);
    if(offset==0XFFFFFFFF)
		{
			printf("myflashmalloc err\r\n");
			return NULL;  
		}
    else 
		{
			res = (void*)((u32)flash_mallco_dev.flashbase[flashx]+offset);
			//flashset(res,0,size);
//			printf("flash malloc res:%d\r\n",res);
			return res;  
		}
}  

//指定内存地址分配内存(外部调用)
//flashx:所属内存块
//size:内存大小(字节)
//返回值:分配到的内存首地址.
void *flashmallocaddr(u8 flashx,u32 addr,u32 size)  
{  
  u32 offset;   
	void *res=0;
	offset=my_flash_malloc_addr(flashx,addr,size);
    if(offset==0XFFFFFFFF)
		{
			printf("flashmallocaddr\r\n");
			return NULL;  
		}
    else 
		{
			res = (void*)((u32)flash_mallco_dev.flashbase[flashx]+offset);
			//printf("flashmallocaddr res:%d\r\n",res);
			return res;  
		}
} 
//重新分配内存(外部调用)
//flashx:所属内存块
//*ptr:旧内存首地址
//size:要分配的内存大小(字节)
//返回值:新分配到的内存首地址.
//void *myflashrealloc(u8 flashx,void *ptr,u32 size)  
//{  
//    u32 offset;    
//    offset=my_flash_malloc(flashx,size);   	
//    if(offset==0XFFFFFFFF)return NULL;     
//    else  
//    {  									   
//	    myflashcpy((void*)((u32)flash_mallco_dev.flashbase[flashx]+offset),ptr,size);	//拷贝旧内存内容到新内存   
//        myflashfree(flashx,ptr);  											  		//释放旧内存
//        return (void*)((u32)flash_mallco_dev.flashbase[flashx]+offset);  				//返回新内存首地址
//    }  
//}
u8 FlashWrite(u8 flashx,u32 addr,u8 *data,u16 size)
{
	if(addr <flash_mallco_dev.flashbase[flashx] || (addr+size) >flash_mallco_dev.flashbase[flashx]+flashsize[flashx])
	{
		 printf("FlashWrite addr of size overrange\r\n"); 	
		 return 1;//未找到符合分配条件的内存块
	}
	u8 offset = (addr-flash_mallco_dev.flashbase[flashx])/flashblksize[flashx];
	if(flash_mallco_dev.flashmap[flashx][offset] == 0)
	{
		printf("FlashWrite flash not malloc\r\n");
		return 2;
	}
	//W25QXX_Write(data,addr,size);
	sf_WriteBuffer(data,addr,size);
	return 0;
}

u8 FlashRead(u8 flashx,u32 addr,u8 *data,u16 size)
{
	if(addr == 0xffffffff)
	{
		printf("addr err addr:%x\r\n",addr);	
		return 1;
	}
	if(addr <flash_mallco_dev.flashbase[flashx] || (addr+size) >flash_mallco_dev.flashbase[flashx]+flashsize[flashx])
	{
		printf("FlashRead addr or size overrange\r\n");
		return 2;//未找到符合分配条件的内存块  
	}
	u8 offset = (addr-flash_mallco_dev.flashbase[flashx])/flashblksize[flashx];
	if(flash_mallco_dev.flashmap[flashx][offset] == 0)
	{
		printf("FlashRead flash not malloc\r\n");
		return 3;
	}
	//W25QXX_Read(data,addr,size);	
	sf_ReadBuffer(data,addr,size);	
	return 0;
}


void PrintFlash(u8 flashx)
{
	u8 * flashtb;
	u8 * flash;
	u16 tbSize= flashtblsize[flashx]*4;
	u16 flashSize= flashsize[flashx];
  flashtb	= mymalloc(SRAMIN,tbSize);
	if(!flashtb)  
	{
		printf("-->PrintFlash malloc flash failed!\r\n");
		return;
	}
	memset(flashtb,0,tbSize);
	//W25QXX_Read(flashtb,flashtbbase[flashx],tbSize);	
	sf_ReadBuffer(flashtb,flashtbbase[flashx],tbSize);	
	printf("\r\ntable----------------------------------\r\n");
	for(int i=0;i<tbSize;i++)
	{
		printf("%x,",flashtb[i]);
	}
	printf("\r\n----------------------------------table\r\n");
	myfree(SRAMIN,flashtb);
	
	
	flash	= mymalloc(SRAMIN,flashSize);
	if(!flash)  
	{
		printf("-->PrintFlash malloc flash failed!\r\n");
		return;
	}
	memset(flash,0,flashSize);
	//W25QXX_Read(flash,flash1base[flashx],flashSize);	
	sf_ReadBuffer(flash,flash1base[flashx],flashSize);	
	printf("\r\nflash++++++++++++++++++++++++++++++++++\r\n");
	for(int i=0;i<flashSize;i++)
	{
		printf("%x,",flash[i]);
	}
	printf("\r\n+++++++++++++++++++++++++++++++++++flash\r\n");
	myfree(SRAMIN,flash);
}
